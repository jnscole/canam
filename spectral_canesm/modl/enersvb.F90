!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine enersvb (ener,ttrac,t,es,trac,phis,ps,press, &
                          tcol,escol,tener,tracol, &
                          qsrcratl,tficql,tficq, &
                          xsrcratl,tficxl,tficx,tficxm,tphsxl, &
                          xsrcrat,xsrcrm1,itradv,xref,xpp,xp0,xpm, &
                          qmom,scal,tmom,tscal,ttot,tscm, &
                          xsrc0,xsrcm,xscl0,xsclm, &
                          delt,moist,itrvar,la,ilev,levs, &
                          iepr,kount,fs,fx, &
                          ntrac,ntraca,dofiltr)
  !
  !     * apr 15/2009 - m.lazare.  new version for gcm15i:
  !     *                          - remove "DOPHYS" and obselete
  !     *                            s/l options.
  !     *                          - remove "ITRAC" and "NUPR".
  !     *                          - remove if block on save so that
  !     *                            conservation calculations are
  !     *                            always done each step (no iesav).
  !     *                          - general conservation done each step
  !     *                            regardless of choice of "ITRVAR", but
  !     *                            consistent with rest of changes in
  !     *                            the model, this involves "C" arrays
  !     *                            (ie tficxm1c instead of tficxm1).
  !     *                          - calls new rlncon5.
  !     * oct 02/2006 - f.majaess. previous version for gcm15f/gcm15g/gcm15h:
  !     *                          - replace direct binary "DATA" record
  !     *                            writes by "PUTFLD2" calls.
  !     * dec 04/2004 - m.lazare/  previous version for gcm15c/d/e:
  !     *               d.plummer. - revised hole-filling calculation,
  !     *                            level-by-level. this requires
  !     *                            passing in of extra level arrays
  !     *                            qsrcratl,xsrcratl,tficql,tficxl.
  !     *                          - do vertical sums of tficq and tficx
  !     *                            (arrays on each level come out of
  !     *                            new ener8).
  !     * apr 01/2004 - m.lazare.  previous version enersv8x for gcm15b.
  !     *
  !     * energy/mass conservation calculation routine.
  !
  implicit none
  real :: avlnps
  real :: avps
  real, intent(inout) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(inout) :: fs
  real, intent(inout) :: fx
  integer :: i
  integer :: ibuf
  integer :: ibuf2
  integer :: idat
  integer, intent(inout) :: iepr
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: imdh
  integer :: isavdts
  integer, intent(inout) :: itrvar
  integer :: iyear
  integer, intent(inout) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: la
  integer, intent(inout) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer :: m
  integer, intent(inout) :: moist
  integer :: myrssti
  integer :: n
  integer :: na
  integer :: nc4to8
  integer :: nrec
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(inout) :: ntraca !< Variable description\f$[units]\f$
  real :: qctoff
  real :: qscl0
  real :: qsclm
  real :: qsrc0
  real :: qsrcm
  real :: qsrcrat
  real :: qsrcrm1
  real :: qtot
  real :: scl
  real :: smin
  real :: temp
  real, intent(inout) :: tficq
  real :: tficqm
  real :: wp0
  real :: wpm
  real :: wpp
  real :: xtrdef
  !
  complex, intent(inout) :: t(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: es(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: trac(la,ilev,ntraca) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ps(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: phis(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: press(la) !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilev,ntrac) :: ttrac !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,7) :: ener !< Variable description\f$[units]\f$
  real, intent(inout) :: tener(7+ntrac) !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tcol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: escol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tracol !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ntrac) :: xref !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xpp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xp0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xpm !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilev) :: qmom !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev) :: scal !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev,ntrac) :: tmom !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev,ntrac) :: tscal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ntrac) :: ttot !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ntrac) :: tscm !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: qsrcratl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: xsrcratl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: tficql !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: tficxl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev,ntrac) :: tphsxl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ntrac) :: tficx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ntrac) :: tficxm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xsrcrat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xsrcrm1 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xsrc0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xsrcm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xscl0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ntrac) :: xsclm !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer, intent(inout), dimension(ntrac) :: itradv !<
  integer*4 :: mynode !<
  !
  logical :: pnt !<
  logical, intent(inout) :: dofiltr !<
  !
  common /consq/ qsrcrat,qsrcrm1,wpp,wp0,wpm,tficqm,qsrc0,qsrcm, &
                qscl0,qsclm,qctoff
  common /keeptim/ iyear,imdh,myrssti,isavdts
  common /mpinfo/ mynode

  !     * icom is a shared i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8),idat(1)
  !
  data smin / 1.e-09 /
  !----------------------------------------------------------------------
  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts/=0) then
    !        * in 32-bit, this only works until iyear=2147 !
    ibuf2=1000000*iyear + imdh
  else
    ibuf2=kount
  end if
  !
  if (iepr>0) then
    if (mod(kount,iepr)==0 .and. mynode==0) then
      pnt=.true.
    else
      pnt=.false.
    end if
  else
    pnt=.false.
  end if
  !
  !     * finish calculations on energies and then save/print.
  !
  avps=real(press(1))/sqrt(2.)
  !
  !     * add phis contribution to cp*t for p.e., and add to k.e.
  !
  do l=1,ilev
    ener(l,2)=ener(l,2) + real(phis(1))/sqrt(2.)
    ener(l,3)=ener(l,1) + ener(l,2)
  end do ! loop 200
  !
  !     * sum up level contributions to tficq.
  !
  do l=1,ilev
    tficq = tficq + tficql(l)
  end do ! loop 225
  !
  !     * perform vertical integral.
  !
  nrec=7+ntrac
  do i=1,nrec
    tener(i)=0.
  end do ! loop 250
  !
  do i=1,7
    do l=1,ilev
      tener(i)=tener(i) + ener(l,i)
    end do
  end do ! loop 300
  !
  qtot=0.
  scl=0.
  do l=1,ilev
    qtot=qtot + qmom(l)
    scl=max(scl,scal(l))
  end do ! loop 322
  !
  if (ntrac>0) then
    do i=8,nrec
      do l=1,ilev
        tener(i)=tener(i) + ttrac(l,i-7)
      end do
    end do ! loop 330
    !
    do n=1,ntrac
      ttot(n)=0.
      tscm(n)=0.
      do l=1,ilev
        ttot(n)=ttot(n) + tmom(l,n)
        tscm(n)=max(tscm(n),tscal(l,n))
      end do ! loop 333
      !
      !           * sum up level contributions to tficx.
      !
      do l=1,ilev
        tficx(n) = tficx(n) + tphsxl(l,n)
      end do
    end do ! loop 335
  end if
  !
  if (moist/=nc4to8("   Q")) then
    !
    !        * calculate "TIME-STEPPED" precipitable water.
    !        * this is later used at next step to conserve moisture,
    !        * by forming ratio of it to actual precipitable water
    !        * obtained by gaussian quadrature (in small latitude
    !        * loop prior to main one) and subsequently modifying
    !        * moisture field accordingly.
    !        * apply time filter on global precipitable water to be
    !        * consistent with moisture field.
    !
    if (kount==0) then
      wp0=tener(4)
      wpm=tener(4)
      tficqm=tficq
      qsrcrm1=qsrcrat
      qsrcm=qsrc0
      qsclm=qscl0
      wpp=wp0 + delt*tficq
    else
      temp=wpp
      wpm=wp0
      wp0=temp
      wpp=wpm + 2.*delt*tficq
      tficqm=tficq
      qsrcrm1=qsrcrat
      qsrcm=qsrc0
      qsclm=qscl0
      !
      qsrcrat=wpp/tener(4)
      qsrcrat=max(qsrcrat,0.01)
      qsrcrat=min(qsrcrat,100.)
      qsrc0=tener(4)/qtot
      !
      !           * following line is to be included if desire time
      !           * filter.
      !
      wp0=fs*wpp + (1.-2.*fs)*temp + fs*wpm
      !
    end if
    qscl0=0.
    if (qsrcrat<1.) then
      qscl0=((1.-smin)/(1.-qsrcrat)-qsrc0*scl)/(1.-qsrc0*scl)
      qscl0=max(min(qscl0,1.),0.)
    end if
    if (pnt) write(6,6060)qsrcrat,tficq
  else
    !
    !        * evaluate ratio of global artificial source of moisture
    !        * due to "HOLE-FILLING" and its ratio to global precipitable
    !        * water.
    !        * this ratio will be used to try and correct moisture field
    !        * if moist="   Q".
    !        * initialize wpp,wp0,wpm,tficqm in any event so read/write
    !        * from restart file will proceed ok.
    !
    xtrdef=0.0
    do l=1,ilev
      if (ener(l,4)>0.0) then
        qsrcratl(l) = tficql(l) / ener(l,4)
        if (qsrcratl(l)>0.2) then
          xtrdef = xtrdef + &
                           (qsrcratl(l)-0.2)*ener(l,4)
          qsrcratl(l) = 0.2
        end if
      else
        xtrdef= xtrdef + tficql(l)
        qsrcratl(l) = 0.0
      end if
    end do
    xtrdef = xtrdef / tener(4)
    do l=1,ilev
      qsrcratl(l) = min(qsrcratl(l) + xtrdef,1.0)
    end do
    wpp=0.
    wp0=0.
    wpm=0.
    tficqm=0.
    if (pnt) write(6,6050) qsrcrat,tficq
  end if
  !
  !     * now tracers.
  !
  do n=1,ntrac
    if (itrvar==nc4to8("   Q") .or. &
        itrvar==nc4to8("QHYB") .and. xref(n)==0.) then
      !
      !           * evaluate ratio of global artificial source of tracer due
      !           * to "HOLE-FILLING".
      !
      if (itradv(n)>0) then
        xtrdef=0.0
        do l=1,ilev
          if (ttrac(l,n)>0.0) then
            xsrcratl(l,n) = tficxl(l,n) / ttrac(l,n)
            if (xsrcratl(l,n)>0.2) then
              xtrdef = xtrdef + &
                                 (xsrcratl(l,n)-0.2)*ttrac(l,n)
              xsrcratl(l,n) = 0.2
            end if
          else
            xtrdef= xtrdef + tficxl(l,n)
            xsrcratl(l,n) = 0.0
          end if
        end do
        if (tener(n+7)>0.)  xtrdef = xtrdef / tener(n+7)
        do l=1,ilev
          xsrcratl(l,n) = min(xsrcratl(l,n) + xtrdef,1.0)
        end do
      else
        do l=1,ilev
          xsrcratl(l,n) = 1.
        end do
      end if
    end if
    !
    !        * general conservation for all tracers regardless of "ITRVAR".
    !        * calculate the "TIME-STEPPED" excess of mass of tracer
    !        * according to the utilisation of a function of x.
    !        * this is later used at the next step to conserve tracer.
    !
    call rlncon5(tener(7+n),tficx(n),xsrcrat(n), &
                      xsrcrm1(n),xpp(n),xp0(n),xpm(n), &
                      tficxm(n),itradv(n), &
                      itrvar,delt,fx,kount,dofiltr, &
                      xsrc0(n),xsrcm(n),xscl0(n),xsclm(n), &
                      ttot(n),tscm(n),smin)
    if (pnt) write(6,6054) n,xsrcrat(n),tficx(n)
    !
    if (pnt) then
      write(6,'(A15,I4,1PE14.6)')'TRACER ',n,tener(n+7)
      if (itradv(n)>0) then
        do l=1,ilev
          write(6,'(I4,1P3E14.6)')l,ttrac(l,n), &
                   tficxl(l,n),xsrcratl(l,n)
        end do
      end if
    end if
  end do ! loop 360
  !
  !     * print energies and vertical profiles of temp. and es.
  !
  if (pnt) then
    do l=1,ilev
      tcol(l)=real(t(1,l))/sqrt(2.)
      escol(l)=0.
    end do ! loop 400
    do m=1,levs
      l=m+ilev-levs
      escol(l)=real(es(1,m))/sqrt(2.)
    end do ! loop 450
    avlnps=real(ps(1))/sqrt(2.)
    !
    write(6,6080) kount,avlnps,avps,tener(4) &
                 ,tener(1),tener(2),tener(3),tener(5),tener(6)
    write(6,6082)(l, &
                  ener(l,1),ener(l,2),ener(l,3), &
                  ener(l,5),ener(l,6),tcol(l),escol(l), &
                  l=1,ilev)
    !
    na=0
    do n=1,ntrac
      write(6,6084) n,tener(7+n)
      if (itradv(n)>0) then
        na=na+1
        do l=1,ilev
          tracol(l)=real(trac(1,l,na))/sqrt(2.)
        end do ! loop 500
        write(6,6086)(l,tracol(l),l=1,ilev)
      end if
    end do ! loop 510
  end if
  !
  return
  !-----------------------------------------------------------------------
  6050 format('0QSRCRAT,TFICQ= ',2(e12.5,1x))
  6054 format(72x,'TRACEUR ',i3,' XSRCRAT,TFICX:',3x,2(e12.5,1x))
  6060 format('0QSRCRAT,GLOBAL-AVERAGE E-P= ',2(e12.5,1x))
  6080 format('0KOUNT=',i6,1p, &
        ', AVG LN SFC PRES=',e14.7, &
        ', AVG SFC PRES=',   e14.7, &
        ', PCPABLE WATER=',      e14.7,/, &
        10x,'K.E.',     11x,'P.E.',    11x,'KE+PE', &
        10x,'M.SQ.VORT', 6x,'M.SQ.DIV', 7x,'TEMP', 11x,'ES', &
        /,  '   TOTAL',5e15.7)
  6082 format(' ',i5,1p,2x,7e15.7)
  6084 format('0MASSE TOT. TRACEUR',i3,':',5x,e15.7)
  6086 format(i6,2x,e15.7)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
