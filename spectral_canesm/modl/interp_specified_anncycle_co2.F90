!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine interp_specified_anncycle_co2(result, & ! output
                                               delt,gmt,iday,mdayt)                       ! input
  !
  !     * aug 10/2013 - m.lazare.
  !
  !     * interpolates between twelve mid-month values representing
  !     * departure from annual mean of co2_ppm, to be added
  !     * to co2_ppm (conserving annual mean) to get specified
  !     * co2 for lowest level under that cpp option.
  !
  !     * result = interpolated value to add to annual mean.
  !
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * mdayt  = date of next mid-month.
  !
  !     * internal:
  !     * mdayt1 = date of previous mid-month.
  !     * mon    = integer :: number of next mid-month (ie may=5).
  !     * mon1   = integer :: number of previous mid-month.
  !
  implicit none

  real, intent(inout)    :: result !< Variable description\f$[units]\f$
  real, intent(in)    :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in)    :: gmt   !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: mdayt   !< Julian calendar day of next mid-month for forcing fields \f$[day]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real :: day !<
  real :: fmday !<
  real :: flday !<
  real :: daysm !<
  real :: daysl !<
  real :: stepsm !<
  real :: stepsl !<
  real :: co2m !<
  real :: co2p !<
  integer :: l !<
  integer :: m !<
  integer :: mm !<
  integer :: mon !<
  integer :: mon1 !<
  integer :: mdayt1 !<
  !
  !     * mid-month specified co2 departure from annual mean.
  !
  real, dimension(12) :: co2t !<
  data co2t /0.725193, 1.12429,        1.22137,   0.950729, &
            0.504833, 5.2302983e-04, -0.434364, -0.958304, &
           -1.55863, -1.24994,       -0.469586,  0.217938 /
  !
  !     * mid-month julian day values as per usual.
  !
  integer, dimension(12) :: mmd !<
  data  mmd/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mdayt and
  !     * from mdayt1 to here.
  !
  day=real(iday)+gmt/86400.
  !
  !     * get previous mid-month target for initialization at kount=0.
  !
  m=0
  do mm=1,12
    if (mmd(mm)==mdayt) m=mm
  end do
  mon=m
  l=m-1
  if (l==0) l=12
  mdayt1=mmd(l)
  mon1=l
  !
  fmday=real(mdayt)
  flday=real(mdayt1)
  if (fmday<day) fmday=fmday+365.
  if (flday>day) flday=flday-365.
  daysm=fmday-day
  daysl=day-flday
  stepsm=daysm*86400./delt
  stepsl=daysl*86400./delt
  co2p=co2t(mon)
  co2m=co2t(mon1)
  !
  !     * now interpolate.
  !
  result = (stepsm*co2m + stepsl*co2p) / (stepsl+stepsm)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
