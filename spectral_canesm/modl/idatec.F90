!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine idatec(month,mday,iday)
  !
  !     * oct 05/93 - m.lazare. new name for idate to avoid conflict
  !     *                       with system routine of same name.
  !     * feb 20/81 - j.d.henderson. previous name idate.
  !
  !     * determines the date from the day of the year.
  !     * month = month (jan,feb,...,etc.)
  !     *  mday = day of the month (1 to 31)
  !     *  iday = day of the year (1 to 365)
  !
  implicit none
  integer :: i
  integer, intent(inout) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(inout) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$

  integer, dimension(13) :: nfdm !<
  character*3 :: mon(12) !<
  character*3, intent(inout) :: month !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  data mon/'JAN','FEB','MAR','APR','MAY','JUN', &
          'JUL','AUG','SEP','OCT','NOV','DEC'/
  data nfdm/1,32,60,91,121,152,182,213,244,274,305,335,366/
  !--------------------------------------------------------------------
  !     * if iday is not between 1 and 365 (inclusive)
  !     * the month returns as xxx and mday is set to iday.
  !
  month='XXX'
  mday=iday
  if (iday<1) return
  !
  do i=1,12
    if (iday>=nfdm(i+1)) cycle
    mday=iday-nfdm(i)+1
    month=mon(i)
    return
  end do ! loop 210
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
