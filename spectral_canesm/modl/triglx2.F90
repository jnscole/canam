!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine triglx2(nlath,ilat,sr,wr,cr,radr,wosq, &
                         srx,wrx,crx,radrx,wosqx,pi)
  !
  !     * jun 12/06 - m. lazare. new version for gcm15f:
  !     *                        - passes in pi and removes
  !     *                          "PARAMS" common block.
  !     * jun 30/03 - m. lazare. previous version triglx:
  !     *                        like previous version trigl, except
  !     *                        only generates fields for a given
  !     *                        node, based on "MPINFO".
  !     * jul 14/92 - e. chan. previous version trigl.
  !
  !     * the argument list is the same as for gaussg.
  !     * gaussg fills only the n hem ordered n to s.
  !     * this routine makes the arrays global and ordered from s to n.
  !     *      sr=sin(lat),  cr=cos(lat),  radr=latitude in radians.
  !     *      wr = gaussian weights,  wosq = wr/(sr**2).
  !
  implicit none
  integer, intent(inout) :: ilat
  integer :: j
  integer :: jend
  integer :: jstart
  integer :: k
  integer :: nl
  integer :: nlat
  integer, intent(inout) :: nlath
  real, intent(inout) :: pi
  real :: pih
  !
  real*8, intent(inout) , dimension(1) :: sr !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: wr !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: cr !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: radr !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: wosq !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: srx !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: wrx !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: crx !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: radrx !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(1) :: wosqx !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer*4 :: mynode !<
  !
  common /mpinfo/ mynode
  !--------------------------------------------------------------------
  !     * first make s-n complete grid.
  !     * crx,wrx,wosqx are symmetric about the equator.
  !     * srx and radrx are antisymmetric.
  !
  pih=3.14159265/2.
  nlat=nlath*2
  !
  do j=1,nlath
    k=nlat+1-j
    crx(k)   = crx(j)
    wrx(k)   = wrx(j)
    wosqx(k) = wosqx(j)
    srx(k)   = srx(j)
    srx(j)   =-srx(j)
    radrx(k) = pih-radrx(j)
    radrx(j) =-radrx(k)
  end do ! loop 150
  !
  !     * note that only the values relevant to the node are obtained !
  !     * mpi hook.
  !
  jstart=mynode*ilat+1
  jend  =mynode*ilat+ilat
  nl=0
  !
  do j=1,nlat
    if (j>=jstart .and. j<=jend) then
      nl       = nl+1
      cr(nl)   = crx(j)
      wr(nl)   = wrx(j)
      wosq(nl) = wosqx(j)
      sr(nl)   = srx(j)
      radr(nl) = radrx(j)
    end if
  end do ! loop 200
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
