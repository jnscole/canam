!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine matcal3 ( &
                          osiila,del2la,osii,del2,am,bm,cm,nk,nmax,lm,la, &
                          ag,bg,ah,bh,lsr,mindex,ptoit, &
                          ci,trav, &
                          dsg,dsh,dlnsg,a1sg,a2sg, &
                          a,tref,psref,r,rscp,dt,alfmod)

  !     * nov 06/03 - m.lazare. pass in mindex and use to determine
  !     *                       correct n-value for local osiila and del2la.
  !     * feb 02/94 - m.lazare. previous version matcal3.
  !     * apr 10/89 - f.majaess. previous version matcalh.
  !
  !     * modele spectral,semi-implicite,elements finis constant
  !     * calcul des matrices am, bm, cm,
  !     * de la matrice  osii et de del2
  !     * utilisation faite par lincal et nouval

  implicit none
  real, intent(inout) :: a
  real, intent(inout) :: alfmod
  real :: am2
  real :: d1sg1
  real :: d1sgk
  real :: d2sg1
  real :: d2sgk
  real :: det
  real :: det1
  real :: det2
  real, intent(inout) :: dt
  integer :: ier
  integer :: k
  integer :: km
  integer :: kp
  integer :: l
  integer, intent(inout) :: la
  integer, intent(inout) :: lm
  integer :: m
  integer :: mn
  integer :: mnl
  integer :: mnr
  integer :: ms
  integer :: n
  integer, intent(inout) :: nk
  integer, intent(inout) :: nmax
  integer :: ns
  real, intent(inout) :: psref
  real, intent(inout) :: ptoit
  real, intent(inout) :: r
  real, intent(inout) :: rscp
  real :: sgb1
  real :: sgbk
  real :: sgbkm
  real :: shb1
  real :: shbk
  real :: shbkm
  real :: stoit
  real, intent(inout) :: tref

  real, intent(inout), dimension(la,nk,nk) :: osiila !< Variable description\f$[units]\f$
  real, intent(inout), dimension(la) :: del2la !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nmax,nk,nk) :: osii !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nmax) :: del2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk,nk) :: ci !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk,nk) :: trav !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk,nk) :: am !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk,nk) :: bm !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk,nk) :: cm !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: ag !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bg !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: ah !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: dsg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: dsh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: dlnsg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: a1sg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nk) :: a2sg !< Variable description\f$[units]\f$

  integer, intent(in) :: lsr(2,lm+1) !< Variable description\f$[units]\f$
  integer, intent(in), dimension(lm) :: mindex !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  stoit    =ptoit/psref
  sgb1     =ag(1)/psref+bg(1)
  shb1     =ah(1)/psref+bh(1)
  dsg(1)   =sgb1-stoit
  dsh(1)   =shb1-stoit
  dlnsg(1) =log(shb1/stoit)
  d1sg1    =sgb1-stoit
  d2sg1    =0.
  a1sg(1)  =alfmod*d1sg1+sgb1*log(shb1/sgb1)-stoit*dlnsg(1)
  a2sg(1)  =0.

  do k=2,nk
    sgbk    =ag(k)  /psref+bg(k)
    sgbkm   =ag(k-1)/psref+bg(k-1)
    shbk    =ah(k)  /psref+bh(k)
    shbkm   =ah(k-1)/psref+bh(k-1)
    dsg(k)  =sgbk -sgbkm
    dsh(k)  =shbk -shbkm
    d1sgk   =sgbk -shbkm
    d2sgk   =shbkm-sgbkm
    dlnsg(k)=log(shbk/shbkm)
    a1sg(k) =d1sgk +sgbk *log(shbk /sgbk) -sgbkm*dlnsg(k)
    a2sg(k) =d2sgk -sgbkm*log(shbkm/sgbkm)
  end do ! loop 10

  !     * partie triangulaire superieure de am, bm, cm

  do k=1,nk-1
    kp=k+1
    do l=kp,nk
      am(l,k) =r*dlnsg(l)
      bm(l,k) =0.
      bm(kp,k)=rscp*tref*a2sg(kp)/dsh(k)
      cm(l,k) =dsg(l)
    end do
  end do ! loop 11

  !     * partie triangulaire inferieure de am, bm, cm

  do k=2,nk
    km=k-1
    do l=1,km
      am(l,k) =0.
      am(km,k)=r*a2sg(k)/dsg(k)
      bm(l,k) =rscp*dlnsg(k)/dsh(k)*tref*dsg(l)
      cm(l,k) =dsg(l)
    end do
  end do ! loop 12

  !     * partie diagonale am, bm, cm

  do k=1,nk
    am(k,k)=r*a1sg(k)/dsg(k)
    bm(k,k)=rscp*tref*a1sg(k)/dsh(k)
    cm(k,k)=dsg(k)
  end do ! loop 13
  !
  !      do 101 k=1,nk
  !  101 print*," AM(L,",k,")= ",(am(l,k),l=1,nk)
  !      do 102 k=1,nk
  !  102 print*," BM(L,",k,")= ",(bm(l,k),l=1,nk)
  !      do 103 k=1,nk
  !  103 print*," CM(L,",k,")= ",(cm(l,k),l=1,nk)
  !
  am2=1./(a*a)
  do n=1,nmax
    !
    del2(n)=-float(n*(n-1))*am2

    !       la matrice unitaire

    do k=1,nk
      do l=1,nk
        ci(l,k)=0.
      end do
    end do ! loop 14
    do k=1,nk
      ci(k,k)=1.
    end do ! loop 15

    !     * a matrice du scheme semi-implicite

    do k=1,nk
      do l=1,nk
        ci(l,k)=ci(l,k)-dt*dt*del2(n)*r*tref*cm(l,k)
        do m=1,nk
          ci(l,k)=ci(l,k)-dt*dt*del2(n)*am(m,k)*bm(l,m)
        end do
      end do
    end do ! loop 16
    !
    !      PRINT*," NOMBRE D'ONDE= ",N-1
    !      do 104 k=1,nk
    !  104 print*," CI(L,",k,")= ",(ci(l,k),l=1,nk)

    !     * la matrice inverse du scheme semi-implicite

    !     eps = smach(2)
    !     call minv(ci,nk,nk,trav,det,eps,0,1)
    det1=0.0
    call linv3f(ci,trav,1,nk,nk,det1,det2,trav,ier)
    if (ier==130) then
      print *,' MATRIX CI IS ALGORITHMICALLY SINGULAR '
      call xit('MATCALH',-1)
    end if
    det=det1*(2**det2)
    if (det<1.e-15) print*,' MATRIX CI, DETERMINANT = ',det
    !
    do l=1,nk
      do k=1,nk
        osii(n,l,k)=ci(l,k)
      end do
    end do ! loop 17
    !
  end do ! loop 20
  !
  !     * fill in full-triangle array equivilent for future us in lincal3/
  !     * nouval3.
  !
  do m=1,lm
    ms=mindex(m)
    mnl=lsr(1,m)
    mnr=lsr(1,m+1)-1
    do mn=mnl,mnr
      ns = ms + (mn-mnl)
      del2la(mn) = del2(ns)
      do l=1,nk
        do k=1,nk
          osiila(mn,l,k) = osii(ns,l,k)
        end do
      end do ! loop 30
    end do ! loop 40
  end do ! loop 50
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
