!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rlncon5(tener,tficq,qsrcrat,qsrcrm1,wpp,wp0,wpm, &
                         tficqm,itradv,itrvar,delt,fs,kount,dofiltr, &
                         qsrc0,qsrcm,qscl0,qsclm, &
                         qtot,scal,smin)
  !
  !     * apr 25/10 - m.lazare.    new version for gcm15i:
  !     *                          - calculation of qsrcrat and qsrc0
  !     *                            only done if qtot>0.
  !     * may 05/03 - k.vonsalzen. previous version rlncon4 from gcm14
  !     *                          through gcm15h:
  !     *                          - correct problems with mass fixer.
  !     *                          - add calculation of qsrc0.
  !     *                          - "ITRADV" and "ITRVAR" passed in and
  !     *                            used to define proper default
  !     *                            (non-advecting) for qsrcrat, depending
  !     *                            on whether itrvar=sl3d or itrvar=slqb.
  !     * sep 30/99 - m.lazare. previous version rlncon3 for gcm13.
  !     * nov. 18/97  - r. harvey/ previous version rlncon2 for gcm12.
  !     *               m. holzer.
  !
  !     * SUBROUTINE QUI CALCULE L'EXCES OU LE DEFICIT DE MASSE DE CHACUN DES
  !     * traceurs a chaque pas de temps

  !     *   tener    masse globale de la quantite x au temps t avant correction
  !     *   TFICQ    MASSE Y AJOUTEE AU SYSTEME DANS L'INTERVALLE DE TEMPS DT
  !     *            precedent, due a la presence des termes de source.
  !     *     wp0    masse globale de la quantite x au temps t telle que predit
  !     *            au temps t-dt.
  !     *     wpp    estimation de la masse de x au temps t+dt
  !     * qsrcrat    correction apportee a la masse globale au temps t
  !     * qsrcrm1    correction apportee a la masse globale au temps t-1
  !
  implicit none
  real, intent(inout) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(inout) :: fs
  integer, intent(inout) :: itradv
  integer, intent(inout) :: itrvar
  integer, intent(inout) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: nc4to8
  real, intent(inout) :: qscl0
  real, intent(inout) :: qsclm
  real, intent(inout) :: qsrc0
  real, intent(inout) :: qsrcm
  real, intent(inout) :: qsrcrat
  real, intent(inout) :: qsrcrm1
  real, intent(inout) :: qtot
  real, intent(inout) :: scal
  real, intent(inout) :: smin
  real :: temp
  real, intent(inout) :: tener
  real, intent(inout) :: tficq
  real, intent(inout) :: tficqm
  real, intent(inout) :: wp0
  real, intent(inout) :: wpm
  real, intent(inout) :: wpp
  !
  logical, intent(inout) :: dofiltr !< Variable description\f$[units]\f$
  logical :: pnt !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  data pnt /.false./
  !----------------------------------------------------------------------
  !     * qsrcrat=0 from "INIT" common deck at kount=0,
  !     * for general conservation done on physics grid.
  !
  if (kount==0) then
    wp0=tener
    wpm=tener
    tficqm=tficq
    if (itrvar==nc4to8("SL3D") .or. itrvar==nc4to8("SLQB")) &
        qsrcrat=1.
    qsrcrm1=qsrcrat
    qsrcm=qsrc0
    qsclm=qscl0
    wpp=wp0 + delt*tficq
  else
    dofiltr=.true.
    temp=wpp
    wpm=wp0
    wp0=temp
    wpp=wpm + 2.*delt*tficq
    if (pnt) write(*,*) ' 2.*DELT*TFICQ',2.*delt*tficq
    !
    if (tficqm==0.0 .and. tficq/=0.0) then
      if (pnt) print *,' INJECTION OF TRACER AT NEXT TIME STEP.'
      if (pnt) print *,' TIME FILTER NOT DONE FOR XP0.'
      dofiltr=.false.
    end if
    tficqm=tficq
    qsrcrm1=qsrcrat
    qsrcm=qsrc0
    qsclm=qscl0
    !
    if (pnt) write(*,*) ' ****KOUNT=', kount
    if (pnt) write(*,*) ' TENER=',tener
    if (pnt) write(*,*) ' WPM,WP0,WPP = ', wpm,wp0,wpp

    !===     inflag = 0
    if ((wp0== 0.0).and.(tener>0.0)) then
      if (pnt) write(*,*) ' INJECTION AT KOUNT=', kount
      wp0=wpp
      if (pnt) write(*,*) ' TIME FILTER NOT DONE FOR XP0.'
      !===         inflag = 1
      dofiltr=.false.
    end if
    !
    if (tener > 0. .and. itradv/=0 .and. qtot > 0.) then
      qsrcrat=wpp/tener
      qsrcrat=max(qsrcrat,0.01)
      qsrcrat=min(qsrcrat,100.)
      qsrc0=tener/qtot
    else
      if (pnt) write(*,*) ' IN RLNCONS TENER=',tener
      if (pnt) write(*,*) ' QSRCRAT SET TO UNITY'
      qsrcrat=1.
      qsrc0=1.
    end if
    qscl0=0.
    if (qsrcrat<1.) then
      qscl0=((1.-smin)/(1.-qsrcrat)-qsrc0*scal)/(1.-qsrc0*scal)
      qscl0=max(min(qscl0,1.),0.)
    end if
    !
    if (pnt) write(*,*) ' QSRCRAT=', qsrcrat
    !
    !        * following line is to be included if desire time
    !        * filter.
    !
    if (pnt) then
      if (dofiltr) print *,' TIME FILTER DONE FOR XP0.'
      if (.not.dofiltr) print *,' TIME FILTER NOT DONE FOR XP0.'
    end if
    if (dofiltr) then
      wp0=fs*wpp + (1.-2.*fs)*wp0 + fs*wpm
    end if
    !
  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
