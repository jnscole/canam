!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine calcw (wj, omegj, &
                        dj, uj, vj, tj, pslmj, psthj, psj, &
                        sgj, dsgj, dshj, dlnsgj, &
                        a1sgj, b1sgj, a2sgj, b2sgj, db, &
                        ilev, ilg, il1, il2)
  !
  !     * sep 07/2009 - m.lazare. new routine for gcm15i.
  !
  !     * calculates vertical velocity on physics grid (m/sec).
  !     * code parallels calculation on dynamics grid in dyncal4d.
  !
  implicit none
  !
  !     * output fields:
  !
  real, intent(inout), dimension(ilg,ilev) :: wj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: omegj !< Variable description\f$[units]\f$
  !
  !     * primary input fields:
  !
  real, intent(in), dimension(ilg,ilev) :: dj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: uj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: vj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pslmj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: psthj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: psj !< Variable description\f$[units]\f$
  !
  !     * vertical discretization input fields:
  !
  real, intent(in), dimension(ilg,ilev) :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: dlnsgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: a1sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: b1sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: a2sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: b2sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev) :: db !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * work fields:
  !
  real, dimension(ilg,ilev) :: vgrpsj !<
  real, dimension(ilg) :: ssdrjk !<
  !
  !     * scalars:
  !
  real :: omspj !<
  real :: drjki !<
  real :: drjkpi !<
  real :: ww !<
  real :: tw !<
  real :: a !<
  real :: asq !<
  real :: grav !<
  real :: rgas !<
  real :: rgocp !<
  real :: rgoasq !<
  real :: cpres !<
  real :: rgasv !<
  real :: cpresv !<
  integer :: k !<
  integer :: i !<
  integer, intent(inout) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  !
  common /params/ ww,tw,a,asq,grav,rgas,rgocp,rgoasq,cpres, &
                 rgasv,cpresv
  !-----------------------------------------------------------------------
  ssdrjk=0.
  !
  do k = 1,ilev
    do i = il1,il2
      vgrpsj(i,k) =  uj(i,k)*pslmj(i) + vj(i,k)*psthj(i)
    end do
  end do ! loop 150

  !     -----------------------------------------------------------------
  !     calculate omspj (omega sur p) and diagnostic w
  !     -----------------------------------------------------------------

  do k=1,ilev-1
    do i = il1,il2
      drjki      =  dj(i,k) + vgrpsj(i,k)*db(k)/dsgj(i,k)
      drjkpi     =  dj(i,k+1) + vgrpsj(i,k+1)*db(k+1)/dsgj(i,k+1)

      omspj      = ( - ssdrjk(i)*dlnsgj(i,k) &
                      - drjki*a1sgj(i,k) - drjkpi*a2sgj(i,k+1) &
                      + vgrpsj(i,k)*b1sgj(i,k) &
                      + vgrpsj(i,k+1)*b2sgj(i,k+1) )/dshj(i,k)
      omegj(i,k) = omspj * psj(i) * sgj(i,k)
      wj(i,k)    = -1. * (rgas*tj(i,k)/grav) * omspj
      ssdrjk(i)  = ssdrjk(i) +drjki*dsgj(i,k)
    end do
  end do ! loop 200

  !     -----------------------------------------------------------------

  k=ilev
  do i = il1,il2
    drjki      =  dj(i,k) + vgrpsj(i,k)*db(k)/dsgj(i,k)
    omspj      = ( - ssdrjk(i)*dlnsgj(i,k) - drjki*a1sgj(i,k) &
                      + vgrpsj(i,k)*b1sgj(i,k) )/dshj(i,k)
    omegj(i,k) = omspj * psj(i) * sgj(i,k)
    wj   (i,k) = -1. * (rgas*tj(i,k)/grav) * omspj
  end do ! loop 250

  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
