!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine imvrai2 (ug,vg,psdlg,psdpg, cosj,ilg,lon,ilev,a)
  !
  !     * oct 15/92 - m.lazare. cosj now a passed array for multi-
  !     *                       latitude formulation.
  !     * jul 14/92 - previous version imavrai.
  !
  !     * convert wind images and d(ps)/dx and /dy to real :: values.
  !
  implicit none
  real, intent(inout) :: a
  integer :: i
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: lon

  real, intent(inout), dimension(ilg,ilev) :: ug !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: vg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: psdlg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: psdpg !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilg) :: cosj !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !----------------------------------------------------------------
  do l=1,ilev
    do i=1,lon
      ug(i,l)   = ug(i,l)     *(a/cosj(i))
      vg(i,l)   = vg(i,l)     *(a/cosj(i))
    end do ! loop 100
  end do ! loop 200
  !
  do i=1,lon
    psdlg(i)  = psdlg(i)*(1./(a*cosj(i)))
    psdpg(i)  = psdpg(i)*(1./(a*cosj(i)))
  end do ! loop 300
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
