!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hdifus6 (p,c,t,es,x,la,ilev,levs, &
                          lm,mindex,lmtotal,lsr, &
                          dt,ktr,s,sh,itrac,ntrac, &
                          disp,disc,dist,dises,disx,scrit, &
                          kfirst,klast,nfirst,lmam)

  !     * jan 12/07 - m.lazare/     new version for gcm15f:
  !     *             c.mclandress. correct laplacian diffusion
  !     *                           with additional option for
  !     *                           "DEL4" laplacian.
  !     * dec 15/03 - m.lazare. previous version hdifus5x for
  !     *                       gcm15c/d/e.
  !     *
  !     * performs dissipation on global spectral fields in the gcm.
  !     * on vorticity (p), divergence (c), temperature (t),
  !     * and moisture variable (es).
  !     * also tracer (x), when itrac>0.
  !
  implicit none
  real :: a
  real :: amp
  real :: des
  real, intent(inout) :: disc
  real, intent(inout) :: dises
  real, intent(inout) :: disp
  real, intent(inout) :: dist
  real, intent(inout) :: disx
  real :: dsc
  real :: dsp
  real :: dst
  real :: dsx
  real, intent(inout) :: dt
  real :: fact
  real :: fns
  real :: fnstar
  real :: fr
  integer :: icount
  integer :: idel4
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: k
  integer :: kl
  integer :: kr
  integer, intent(inout) :: ktr
  integer :: l
  integer, intent(inout) :: la
  integer, intent(inout) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(inout) :: lm
  integer, intent(inout) :: lmtotal
  integer :: m
  integer :: ms
  integer :: n
  integer :: nsmin
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real :: sc
  real, intent(inout) :: scrit
  real :: sss

  integer, intent(in) :: lsr(2,lm+1) !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lm) :: kfirst !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lm) :: klast !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lm) :: nfirst !< Variable description\f$[units]\f$
  integer, intent(in), dimension(lm) :: mindex !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilev) :: s !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilev) :: sh !< Variable description\f$[units]\f$
  !
  complex, intent(inout) :: p(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: c(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: t(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: es(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: x(la,ilev,ntrac) !< Variable description\f$[units]\f$
  logical, intent(inout) :: lmam  !< Switch to use MAM (.true. = use, .false. = don't use) \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real :: fs
  fs(sss,a,sc)=max(1., 1.+(a-1.)*(sc-sss)/sc) **2
  !
  !     * "IDEL4" controls whether laplacian (idel4=0) or "DEL4"
  !     * diffusion is applied.
  !
  data idel4 /0/
  !---------------------------------------------------------------------
  amp=sqrt(3.)
  fr=real(lmtotal-1)
  if (ktr==2) fr=fr/sqrt(2.)
  fnstar=sqrt(2.)*fr
  nsmin=int(.55*fnstar+.5)
  !
  !     * computation of the vector indices of the coef. to be dissipated.
  !     * kfirst(m) contains the indice of first (or  left) coef. for a m.
  !     * klast(m)  contains the indice of last  (or right) coef. for a m.
  !
  do m=1,lm
    kl=lsr(1,m)
    kr=lsr(1,m+1)-1
    ms=mindex(m)-1
    klast(m)=kr
    do k=kl,kr
      nfirst(m)=ms+(k-kl)
      kfirst(m)=k
      !
      !           * when .true. nfirst(m) will leave the loop with the
      !           * wavenumber n of the first coef. to be dissipated.
      !           * kfirst(m) is the vector index of that coef.
      !
      if (nfirst(m)>=nsmin) exit
    end do ! loop 50
  end do ! loop 100
  !
  !     ***  dissipation on p,c, and t.
  !
  do l=1,ilev

    if (lmam) then
      dsp=disp*1.0
      dsc=disc*1.0
      dst=dist*1.0
   else
      dsp=disp*fs(s(l),amp,scrit)
      dsc=disc*fs(s(l),amp,scrit)
      dst=dist*fs(sh(l),amp,scrit)
   end if

   do m=1,lm
      icount=0
      do k=kfirst(m),klast(m)
        fns=real(nfirst(m) + icount)
        fact=2.*dt*4*(fns/fnstar-.55)**2
        icount=icount+1
        p(k,l)=p(k,l)/(1.+dsp*fact)
        c(k,l)=c(k,l)/(1.+dsc*fact)
        t(k,l)=t(k,l)/(1.+dst*fact)
      end do ! loop 200
    end do ! loop 210
  end do ! loop 220
  !
  !     ***  dissipation on tracers
  !
  if (itrac>0) then
    do n=1,ntrac
      do l=1,ilev

        if (lmam) then
           dsx=disx*1.0
        else
           dsx=disx*fs(sh(l),amp,scrit)
        end if

        do m=1,lm
          icount=0
          do k=kfirst(m),klast(m)
            fns=real(nfirst(m) + icount)
            if (idel4==1) then
              fact=2.*dt*(fns*(fns+1.))**2
            else
              fact=2.*dt*fns*(fns+1.)
            end if
            icount=icount+1
            x(k,l,n)=x(k,l,n)/(1.+dsx*fact)
          end do ! loop 300
        end do ! loop 310
      end do ! loop 320
    end do ! loop 330
  end if
  !
  !     ***  dissipation on moisture.
  !
  if (levs>0) then
    do l=1,levs

      if (lmam) then
         des=dises*1.0
      else
         des=dises*fs(sh(l+ilev-levs),amp,scrit)
      end if

      do m=1,lm
        icount=0
        do k=kfirst(m),klast(m)
          fns=real(nfirst(m) + icount)
          if (idel4==1) then
            fact=2.*dt*(fns*(fns+1.))**2
          else
            fact=2.*dt*fns*(fns+1.)
          end if
          icount=icount+1
          es(k,l)=es(k,l)/(1.+des*fact)
        end do ! loop 400
      end do ! loop 410
    end do ! loop 420
  end if
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
