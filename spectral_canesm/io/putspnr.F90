!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putspnr(nf,g,la, &
                   lsrtotal,latotal,lmtotal,gll, &
                   jbuf,maxj,ipiosw,ok)

  !     * may 01/18 - s.kharin.  write re-ordered fields.
  !     * oct 11/06 - f.majaess (replace direct binary "DATA" record
  !     *                        writes by "PUTFLD2" calls).
  !     * nov 04/03 - m.lazare. new version to support mpi, which uses
  !     *                       new switch "IPIOSW" and work array "GLL".
  !
  !     * saves global spectral forecast on restart file.
  !     * all fields are written unpacked.
  !
  implicit none
  integer :: ibuf
  integer :: idat
  integer, intent(in) :: ipiosw
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: lmtotal
  integer, intent(in) :: maxj
  integer :: maxx
  integer, intent(in) :: nf

  complex, intent(inout) :: g(la)                     !< Variable description\f$[units]\f$
  complex, intent(inout) :: gll(latotal)              !< Variable description\f$[units]\f$
  complex :: fll(latotal)

  integer, intent(in), dimension(2,lmtotal + 1) :: lsrtotal !< Variable description\f$[units]\f$
  integer, intent(in), dimension(8) :: jbuf !< Variable description\f$[units]\f$

  integer*4 :: mynode
  common /mpinfo/ mynode

  integer*4 :: nnode
  integer*4 :: agcm_commwrld
  integer*4 :: my_cmplx_type
  integer*4 :: my_real_type
  integer*4 :: my_int_type
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  integer*4 :: sz
  integer*4 :: master
  integer*4 :: ierr

  logical, intent(in) :: ok !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !     * icom is a shared i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8),idat(1)
  !--------------------------------------------------------------------


  if (nnode > 1 .and. ipiosw == 0) then
    !
    !       * mpi call to gather data in load-balanced (non-triangular) order.
    !
    master =  0
    sz     =  la
    call wrap_gather(g,      sz,            my_cmplx_type, &
                     gll,    sz,            my_cmplx_type, &
                     master, agcm_commwrld, ierr)

    if (mynode == 0) then

      ibuf(1:8) = jbuf(1:8)
      ibuf(5)  = latotal
      maxx = 2 * latotal
      call rec2tri(fll,gll,latotal,lsrtotal,lmtotal)
      call putfld2(nf,fll,ibuf,maxx)

    end if

  else

    ibuf(1:8) = jbuf(1:8)
    ibuf(5) = la
    maxx = 2 * la
    call putfld2(nf,g,ibuf,maxx)

  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
