!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putnatr3(nf,tracnap,lon1,ilat,ijpak,ilev,lh,kount,lstr, &
                    khem,ntrac,itrnam,itrlvs,itrlvf, &
                    indxna,ntracn,gll,wrks)
  !
  !     * jan 05/04 - m.lazare.   modified for revised model structure
  !     *                         following ibm conversion.
  !     * oct 29/02 - j.scinocca. previous version putnatr.
  !
  !     * save tracer field onto model grid history file,
  !     * if ntracn/=0 (when spectral model option selected).
  !
  implicit none
  integer :: ibuf2
  integer :: ibuf3
  integer, intent(in) :: ijpak
  integer, intent(in) :: ilat
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer, intent(in) :: khem
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: lon1
  integer :: myrssti
  integer :: n
  integer, intent(in) :: nf
  integer :: nn
  integer :: npgg
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(in) :: ntracn !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ijpak,ilev,ntracn) :: tracnap !< Variable description\f$[units]\f$
  real, intent(in) :: gll( * ) !< Variable description\f$[units]\f$
  real, intent(in), dimension(1) :: wrks !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itrnam !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itrlvs !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itrlvf !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntracn) :: indxna !< Variable description\f$[units]\f$
  logical, intent(in) :: lstr !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  common /keeptim/ iyear,imdh,myrssti,isavdts
  !-------------------------------------------------------------------------
  if (ntracn == 0 .or. .not.lstr) return
  !
  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts /= 0) then
    !        * in 32-bit, this only works until iyear=2147 !
    ibuf2 = 1000000 * iyear + imdh
  else
    ibuf2 = kount
  end if
  npgg = 0
  !
  do nn = 1,ntracn
    n = indxna(nn)
    ibuf3 = itrnam(n)
    do l = itrlvs(n),itrlvf(n)
      call putgg(tracnap(1,l,nn),lon1,ilat,khem,npgg,ibuf2,nf, &
                 ibuf3,lh(l),gll,wrks)
    end do ! loop 200
  end do ! loop 300
  !-----------------------------------------------------------------------
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
