!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getdue(nf,spotpak,st01pak,st02pak, &
                  st03pak,st04pak,st06pak,st13pak, &
                  st14pak,st15pak,st16pak,st17pak, &
                  ijpak,nlon,nlat,gg)
  !
  !     * may/08 - y.peng. new routine for gcm16 to get
  !     *                  fields required for new dust
  !     *                  emission scheme.
  !
  implicit none
  integer, intent(in) :: ijpak
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer :: nc4to8
  real, intent(in) :: st01pak
  !
  !     * invariant fields:
  !
  real, intent(in), dimension(ijpak) :: spotpak !< Variable description\f$[units]\f$
  real, dimension(ijpak) :: st01pal !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st02pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st03pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st04pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st06pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st13pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st14pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st15pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st16pak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: st17pak !< Variable description\f$[units]\f$

  !
  !     * work field:
  !
  real, intent(in), dimension(1) :: gg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !-----------------------------------------------------------------------
  !     * grids read from file nf.

  rewind nf
  !
  call getggbx(spotpak,nc4to8("SPOT"),nf,nlon,nlat,0,1,gg)
  call getggbx(st01pak,nc4to8("ST01"),nf,nlon,nlat,0,1,gg)
  call getggbx(st02pak,nc4to8("ST02"),nf,nlon,nlat,0,1,gg)
  call getggbx(st03pak,nc4to8("ST03"),nf,nlon,nlat,0,1,gg)
  call getggbx(st04pak,nc4to8("ST04"),nf,nlon,nlat,0,1,gg)
  call getggbx(st06pak,nc4to8("ST06"),nf,nlon,nlat,0,1,gg)
  call getggbx(st13pak,nc4to8("ST13"),nf,nlon,nlat,0,1,gg)
  call getggbx(st14pak,nc4to8("ST14"),nf,nlon,nlat,0,1,gg)
  call getggbx(st15pak,nc4to8("ST15"),nf,nlon,nlat,0,1,gg)
  call getggbx(st16pak,nc4to8("ST16"),nf,nlon,nlat,0,1,gg)
  call getggbx(st17pak,nc4to8("ST17"),nf,nlon,nlat,0,1,gg)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

