!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine wpkphs4(nf,g,nlev,ibuf,lon,nlat,ilat,ilev,lev,lh)

  !     * may 01/18 - s.kharin.  write re-ordered fields.
  !     * jun 30/06 - l.solhiem. cosmetic change to ensure that pointer
  !     *                        "GBUF" does not reference out of bounds
  !     *                        in the non-mpi case. the actual run-
  !     *                        time code does not do this but it still
  !     *                        generates run-time errors under linux
  !     *                        compilers (and would under aix with
  !     *                        different compiler flags) because it
  !     *                        "THINKS" it is referencing out of bounds.
  !     * dec 19/03 - m. lazare. hard code "XBUF" local array dimension
  !     *                        due to encountered problem with "NANS"
  !     *                        initialization under aix.
  !     * oct 05/03 - r.mclay.   support for mpi i/o added.
  !     * may 31/03 - m. lazare. new routine, based on wpkphs3, but with
  !     *                        extra "WORD" added for each level, consistent
  !     *                        with the dimension of pak arrays being
  !     *                        lonsl*ilat+1, to avoid memory-bank conflicts
  !     *                        with new formulation.
  !     * nov 22/94 - m. lazare. previous version wpkphs3.

  !     * writes a packed array g, preceeded by its eight word label,
  !     * to the sequential file nf.

  !     * the repeating grenwich longitude is added before writing
  !     * array g to the sequential file.
  !     * note that this calculation, to avoid the overhead of going
  !     * through the packer, assumes all grid fields are unpacked
  !     * internally in the gcm (new standard).

  !     * g can be multidimentional, but otherwise nlev has to be set
  !     * to one.

  !     * the fields emd,emm are handled differently because they have
  !     * "LEV" levels and are ordered bottom up.
  !
  !     * input: nf    = sequential restart file.
  !     *        nlev  = number of levels. can be either 1,lev,ilev,levs.
  !     *        ibuf  = eight word label.
  !     *         lon  = number of distinct equally-spaced longitudes.
  !     *        ilat  = number of gaussian latitudes.
  !     *        ilev  = number of model sigma levels.
  !     *         lev  = ilev+1
  !     *          lh  = vector of model half-level sigma values.
  !     *
  !     * output: g    = packed array preceded by the eight word
  !     *                label ibuf.
  !
  implicit none
  integer :: i
  integer :: idat
  integer, intent(in) :: ilat
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: intsize
  integer :: j
  integer :: jbuf
  integer :: jx
  integer :: l
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lon
  integer :: lon1
  integer :: machine
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlev
  integer :: nc4to8
  !
  integer, intent(inout), dimension(8) :: ibuf !< Variable description\f$[units]\f$
  integer, dimension(8) :: kbuf
  integer, intent(in), dimension(nlev) :: lh !< Variable description\f$[units]\f$

  common /icom/ jbuf(8), idat(1)
  common /machtyp/ machine,intsize
  integer*4 :: mynode
  common /mpinfo/ mynode

  !     * common block to hold the number of nodes and communicator.

  integer*4 :: nnode
  integer*4 :: agcm_commwrld
  integer*4 :: my_cmplx_type
  integer*4 :: my_real_type
  integer*4 :: my_int_type
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  !
  !     * on the ibm, the mpi_integer :: is 32 bits.  mpi_int_sz is the number of
  !     * mpi_integers that comprise an integer :: so normally mpi_int_sz
  !     * is 2.  that is there are 232 bit integers in a 64-bit integer.
  !
  integer*4 :: mpi_int_sz
  common /mpisize/ mpi_int_sz

  real, intent(in) :: g( (lon * ilat + 1) * nlev) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !     * hard code "XBUF" dimension due to problem with "NANS" initialization
  !     * UNDER AIX. IT'S SET TO HANDLE UP TO 641X320 (T213 GRID).

  real, dimension(205120) :: xbuf
  real, dimension(205120) :: gbuf
  integer*4 :: sz
  integer*4 :: master
  integer*4 :: ierr
  !----------------------------------------------------------------------
  if (lon * nlat > 205120) call                  xit('WPKPHS4', - 1)
  master = 0
  sz     = ilat * lon * mpi_int_sz
  jx = 1
  lon1 = lon + 1
  !
  do i = 1,8
    kbuf(i) = ibuf(i)
  end do ! loop 30
  !
  do l = 1,nlev
    if (nlev == 1) then
      kbuf(4) = 1
    else if (nlev == lev) then
      if (l == lev) then
        if (kbuf(3) /= nc4to8(" EMD") .and. &
            kbuf(3) /= nc4to8(" EMM")) call     xit('WPKPHS4', - 2)
        kbuf(4) = 0
      else
        kbuf(4) = lh(lev - l)
      end if
    else
      kbuf(4) = lh(l)
    end if
    !
    if (nnode > 1) then
      call wrap_gather(g(jx) , sz,            my_int_type, &
                       xbuf  , sz,            my_int_type, &
                       master, agcm_commwrld, ierr)
    else
      xbuf(1:lon * nlat) = g(jx:jx + ilat * lon - 1)
    end if
    jx = jx + (ilat * lon + 1)

    if (mynode == 0) then
      do j = 1,nlat
        do i = 1,lon
          gbuf((j - 1) * lon1 + i) = xbuf((j - 1) * lon + i)
        end do
        gbuf(j * lon1) = gbuf((j - 1) * lon1 + 1)
      end do
      !
      do i = 1,8
        jbuf(i) = kbuf(i)
      end do ! loop 70
      !
      call cgtorg(xbuf,gbuf,jbuf(5),jbuf(6))
      call putfld2(nf,xbuf,jbuf,jbuf(5) * jbuf(6))
    end if
  end do ! loop 100
  !
  do i = 1,8
    ibuf(i) = kbuf(i)
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
