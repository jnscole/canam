!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getace(nf,ebbtpak,eobtpak,ebftpak,eoftpak, &
                  esdtpak,esitpak,esstpak,esotpak,esptpak, &
                  esrtpak,ijpak,nlon,nlat,gg)
  !
  !     * knut von salzen - feb 07,2009. new routine for gcm15h to read in
  !     *                                ebbt,eobt,ebft,eoft
  !     *                                (used to be done before in
  !     *                                getchem2).
  !
  implicit none
  integer, intent(in) :: ijpak
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer :: nc4to8
  !
  !     * invariant fields:
  !
  real, intent(in), dimension(ijpak) :: ebbtpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: eobtpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: ebftpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: eoftpak !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ijpak) :: esdtpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: esitpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: esstpak !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ijpak) :: esotpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: esptpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: esrtpak !< Variable description\f$[units]\f$
  !
  !     * work field:
  !
  real, intent(in), dimension(1) :: gg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  !     * grids read from file nf.

  rewind nf

  call getggbx(ebbtpak,nc4to8("EBBT"),nf,nlon,nlat,0,1,gg)
  call getggbx(eobtpak,nc4to8("EOBT"),nf,nlon,nlat,0,1,gg)
  call getggbx(ebftpak,nc4to8("EBFT"),nf,nlon,nlat,0,1,gg)
  call getggbx(eoftpak,nc4to8("EOFT"),nf,nlon,nlat,0,1,gg)
  !
  call getggbx(esdtpak,nc4to8("ESDT"),nf,nlon,nlat,0,1,gg)
  call getggbx(esitpak,nc4to8("ESIT"),nf,nlon,nlat,0,1,gg)
  call getggbx(esstpak,nc4to8("ESST"),nf,nlon,nlat,0,1,gg)
  call getggbx(esotpak,nc4to8("ESOT"),nf,nlon,nlat,0,1,gg)
  call getggbx(esptpak,nc4to8("ESPT"),nf,nlon,nlat,0,1,gg)
  call getggbx(esrtpak,nc4to8("ESRT"),nf,nlon,nlat,0,1,gg)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

