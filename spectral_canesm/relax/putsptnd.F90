!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putsptnd(nf ,ps,p,c,t,es, &
                          ref_ps,ref_p,ref_c,ref_t,ref_es, &
                          kount,la,lrlmt,ilev,levs,ls,lh, &
                          lsrtotal,latotal,lmtotal,gll)

  !     * nov 04/03 - m.lazare. new version to support mpi, which uses
  !     *                       new routine "PUTSPN" and work array "GLL".
  !     * may 29/95 - m.lazare. previous version putstg8.
  !
  !     * saves global spectral forecast on sequential file nf.
  !     * all fields are written unpacked.
  !     * ls,lh = output label values for full,half levels.
  !
  implicit none
  integer :: ibuf2
  integer :: idiv
  integer :: idivr
  integer :: ies
  integer :: iesr
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ilnsp
  integer :: ilnspr
  integer :: imdh
  integer :: ipio
  integer :: isavdts
  integer :: itmp
  integer :: itmpr
  integer :: ivort
  integer :: ivortr
  integer :: iyear
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: lmtotal
  integer, intent(in) :: lrlmt
  integer :: max
  integer :: myrssti
  integer :: n
  integer :: nc4to8
  integer, intent(in) :: nf

  complex, intent(inout) :: ps(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: t(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: p(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: c(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: es(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ref_ps(la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ref_t(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ref_p(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ref_c(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ref_es(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: gll(latotal) !< Variable description\f$[units]\f$

  integer, intent(in) :: lsrtotal(2,lmtotal+1) !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: ls !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  integer, dimension(8) :: ibuf !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  logical :: ok !<

  common /ipario/  ipio
  common /keeptim/ iyear,imdh,myrssti,isavdts
  !--------------------------------------------------------------------
  max=2*latotal
  !
  ilnsp=nc4to8("LSPN")
  itmp=nc4to8("TMPN")
  ivort=nc4to8("VORN")
  idiv=nc4to8("DIVN")
  ies=nc4to8(" ESN")
  ilnspr=nc4to8("LSPR")
  itmpr=nc4to8("TMPR")
  ivortr=nc4to8("VORR")
  idivr=nc4to8("DIVR")
  iesr=nc4to8(" ESR")

  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts/=0) then
    !        * in 32-bit, this only works until iyear=2147 !
    ibuf2=1000000*iyear + imdh
  else
    ibuf2=kount
  end if
  !
  call setlab(ibuf,nc4to8("SPEC"),ibuf2,-1,1,-1,1,lrlmt,0)
  !
  !     * surface geopotential (mountains) are saved every time.
  !
  ! ccc      ibuf(3)=iphis
  ! ccc      call putspn (nf,phis,la,
  ! ccc     1             lsrtotal,latotal,lmtotal,gll,
  ! ccc     2             ibuf,max,ipio,ok)
  !
  !     * save ln(ps) in pascals.
  !
  ibuf(3)=ilnsp
  call putspn (nf,ps,la, &
                   lsrtotal,latotal,lmtotal,gll, &
                   ibuf,max,ipio,ok)
  !
  !     * save temperature for ilev levels.
  !
  ibuf(3)=itmp
  do l=1,ilev
    ibuf(4)=lh(l)
    call putspn (nf,t(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 310
  !
  !     * save vorticity and divergence in pairs for each level.
  !
  do l=1,ilev
    ibuf(4)=ls(l)

    ibuf(3)=ivort
    call putspn (nf,p(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)

    ibuf(3)=idiv
    call putspn (nf,c(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 410
  !
  !     * moisture variable saved for levs levels.
  !
  if (levs==0) return
  ibuf(3)=ies
  do n=1,levs
    l=(ilev-levs)+n
    ibuf(4)=lh(l)
    call putspn (nf,es(1,n),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 510
  !
  !     * save ln(ps) in pascals.
  !
  ibuf(3)=ilnspr
  call putspn (nf,ref_ps,la, &
                   lsrtotal,latotal,lmtotal,gll, &
                   ibuf,max,ipio,ok)
  !
  !     * save temperature for ilev levels.
  !
  ibuf(3)=itmpr
  do l=1,ilev
    ibuf(4)=lh(l)
    call putspn (nf,ref_t(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 315
  !
  !     * save vorticity and divergence in pairs for each level.
  !
  do l=1,ilev
    ibuf(4)=ls(l)

    ibuf(3)=ivortr
    call putspn (nf,ref_p(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)

    ibuf(3)=idivr
    call putspn (nf,ref_c(1,l),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 415
  !
  !     * moisture variable saved for levs levels.
  !
  if (levs==0) return
  ibuf(3)=iesr
  do n=1,levs
    l=(ilev-levs)+n
    ibuf(4)=lh(l)
    call putspn (nf,ref_es(1,n),la, &
                      lsrtotal,latotal,lmtotal,gll, &
                      ibuf,max,ipio,ok)
  end do ! loop 515
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
