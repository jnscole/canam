!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module em_defs

  !--- mike lazare ...30 apr 2018. new routine based on vtau, but for surface co2 emissions.

  !-------------------------------------------------------------
  !--- set up namelists for surface co2 emissions.
  !-------------------------------------------------------------

  !--- flag to indicate how values are to be interpolated
  !--- this corresponds to parmsub parameter "em_scn_mode"
  !--- which has the following meaning:
  !---         =   -1     continuously update stratospheric aerosols form data in the file "em"
  !---         =  yyyymm  use a constant value from yyyy/mm in the file "em"
  !---         = -yyyymm  use an annual cycle from year abs(yyyymm)/100 in the file "em"
  implicit none
  integer :: em_scn_mode=-1 !<

  !--- namelist used to read in parmsub parameters
  !--- this namelist is read in init_em_co2_scenario
  namelist /em_config/ em_scn_mode

end module em_defs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
