#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * apr 20/2019 - s.kharin    - fix bug ioffz->ioff in *_ext_gcm_sa_* fields
!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtvipas/xtviros for sample burdens.
!     * aug 23/2018 - vivek arora - remove pfhc and pexf
!     * aug 14/2018 - m.lazare.     remove maskpak,gtpal.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * nov 01/2017 - j. cole.     - update for cmip6 stratospheric aerosols
!     * aug 09/2017 - m.lazare.    - add fnpat/fnrot.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     * aug 07/2017 - m.lazare.    - initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstrol,cltrol} removed.
!     *                            - add (for nemo/cice support):
!     *                              hsearol,rainsrol,snowsrol
!     *                              obegrol,obwgrol,begorol,bwgorol,
!     *                              begirol,hflirol,
!     *                              hsearol,rainsrol,snowsrol.
!     *                            - remove: ftilpak/ftilrow.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class):
!     *                              thlwpat/thlwrot.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvpat/algdvrot, algdnpat/algdnrot,
!     *                              algwvpat/algwvrot, algwnpat/algwnrot,
!     *                              socipat/socirot.
!     *                            - add (for fractional land/water/ice):
!     *                              salbpat/salbrot, csalpat/csalrot,
!     *                              emispak/emisrow, emispat/emisrot,
!     *                              wrkapal/wrkarol, wrkbpal/wrkbrol,
!     *                              snopako/snorowo.
!     *                            - add (for pla):
!     *                              psvvpak/psvvrow, pdevpak/pdevrow,
!     *                              pdivpak/pdivrow, prevpak/prevrow,
!     *                              privpak/privrow.
!     *                            - bugfixes for isccp fields and additions
!     *                              for calipso and modis.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - flakpak,flndpak,gtpat,licnpak added.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "PAT"/"ROT" arrays.
!     * jul 10/2013 - m.lazare/    previous version unpack9 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "IM" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well: {fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     *                            - "xtraconv" cpp option moved to before
!     *                              "xtrachem", to be consistent with others.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 18/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr but only for
!     *                              kount>kstart.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - add fare, thlq, thic. change selected
!     *                              row variables to tile versions (rot).
!     *                            - add selected rot variables in
!     *                              addition to their row counterparts.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 03/2012 - m.lazare/    previous version unpack8 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     *                            - "FLGROL_R" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "FDLROL_R-SIGMA*T**4".
!     *                            - include "ISEED" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version unpack7i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("FSOL").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "COUPLER_CTEM".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "HISTEMI"
!     *                              converted to cpp:
!     *                              "TRANSIENT_AEROSOL_EMISSIONS" with
!     *                              further choice of cpp directives
!     *                              "HISTEMI" for historical or
!     *                              "EMISTS" for future emissions.
!     *                              the "HISTEMI" fields are the
!     *                              same as previously. for "EMISTS",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("B" for black
!     *                              carbon, "O" for organic carbon and
!     *                              "S" for sulfur), while for each of
!     *                              these, there are "AIR" for aircraft,
!     *                              "SFC" for surface, "STK" for stack
!     *                              and "FIR" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 19/2009 - m.lazare.    previous version unpack7h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%DF EXPLVOL". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version unpack7g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2006 - j.cole/      previous version unpack7f for gcm15f:
!     *               m.lazare.    - add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    new version for gcm15f:
!     *                            - two extra new fields ("DMC" and "SMC")
!     *                              under control of "%IF DEF,XTRACONV".
!     * may 07/2006 - m.lazare.    previous version unpack7e for gcm15e.
!========================================================================
!     * general physics arrays.
!
do l=1,ilev
  do i=il1,il2
    almxrow(i,l)  = almxpak(ioff+i,l)
    almcrow(i,l)  = almcpak(ioff+i,l)
    cicrow (i,l)  = cicpak (ioff+i,l)
    clcvrow(i,l)  = clcvpak(ioff+i,l)
    cldrow (i,l)  = cldpak (ioff+i,l)
    clwrow (i,l)  = clwpak (ioff+i,l)
    cvarrow(i,l)  = cvarpak(ioff+i,l)
    cvmcrow(i,l)  = cvmcpak(ioff+i,l)
    cvsgrow(i,l)  = cvsgpak(ioff+i,l)
    hrlrow (i,l)  = hrlpak (ioff+i,l)
    hrsrow (i,l)  = hrspak (ioff+i,l)
    hrlcrow (i,l) = hrlcpak (ioff+i,l)
    hrscrow (i,l) = hrscpak (ioff+i,l)
    ometrow(i,l)  = ometpak(ioff+i,l)
    qwf0rol(i,l)  = qwf0pal(ioff+i,l)
    qwfmrol(i,l)  = qwfmpal(ioff+i,l)
    rhrow  (i,l)  = rhpak  (ioff+i,l)
    scdnrow(i,l)  = scdnpak(ioff+i,l)
    sclfrow(i,l)  = sclfpak(ioff+i,l)
    slwcrow(i,l)  = slwcpak(ioff+i,l)
    tacnrow(i,l)  = tacnpak(ioff+i,l)
    zdetrow(i,l)  = zdetpak(ioff+i,l)
  end do
end do
!
do n=1,ntrac
  do l=1,ilev
    do i=il1,il2
      sfrcrol(i,l,n) = sfrcpal(ioff+i,l,n)
    end do
  end do
end do
do l=1,ilev
  do i=il1,il2
    altirow(i,l)   = altipak(ioff+i,l)
  end do
end do
!
do l=1,levoz
  do i=il1,il2
    ozrol  (i,l) = ozpal  (ioff+i,l)
    ozrow  (i,l) = ozpak  (ioff+i,l)
  end do
end do
!
do l=1,levozc
  do i=il1,il2
    o3crol (i,l) = o3cpal (ioff+i,l)
    o3crow (i,l) = o3cpak (ioff+i,l)
  end do
end do
!
csalrol(il1:il2,1:nbs)     =csalpal(ioff+il1:ioff+il2,1:nbs)
csalrot(il1:il2,1:im,1:nbs)=csalpat(ioff+il1:ioff+il2,1:im,1:nbs)
salbrol(il1:il2,1:nbs)     =salbpal(ioff+il1:ioff+il2,1:nbs)
salbrot(il1:il2,1:im,1:nbs)=salbpat(ioff+il1:ioff+il2,1:im,1:nbs)
csdrol(il1:il2) = csdpal(ioff+il1:ioff+il2)
csfrol(il1:il2) = csfpal(ioff+il1:ioff+il2)
!
do i=il1,il2
  alphrow(i) = alphpak(ioff+i)
  begirol(i) = begipal(ioff+i)
  begkrol(i) = begkpal(ioff+i)
  beglrol(i) = beglpal(ioff+i)
  begorol(i) = begopal(ioff+i)
  begrol (i) = begpal (ioff+i)
  bwgirol(i) = bwgipal(ioff+i)
  bwgkrol(i) = bwgkpal(ioff+i)
  bwglrol(i) = bwglpal(ioff+i)
  bwgorol(i) = bwgopal(ioff+i)
  bwgrol (i) = bwgpal (ioff+i)
  cbmfrol(i) = cbmfpal(ioff+i)
  chfxrow(i) = chfxpak(ioff+i)
  cictrow(i) = cictpak(ioff+i)
  clbrol (i) = clbpal (ioff+i)
  cldtrow(i) = cldtpak(ioff+i)
  cldorow(i) = cldopak(ioff+i)
  cldorol(i) = cldopal(ioff+i)
  cldarow(i) = cldapak(ioff+i)
  cldarol(i) = cldapal(ioff+i)
  clwtrow(i) = clwtpak(ioff+i)
  cqfxrow(i) = cqfxpak(ioff+i)
  csbrol (i) = csbpal (ioff+i)
  cszrol (i) = cszpal (ioff+i)
  deltrow(i) = deltpak(ioff+i)
  dmsorol(i) = dmsopal(ioff+i)
  dmsorow(i) = dmsopak(ioff+i)
  edmsrol(i) = edmspal(ioff+i)
  edmsrow(i) = edmspak(ioff+i)
  emisrow(i) = emispak(ioff+i)
  envrow (i) = envpak (ioff+i)
  fdlcrow(i) = fdlcpak(ioff+i)
  fdlrow (i) = fdlpak (ioff+i)
  flamrol(i) = flampal(ioff+i)
  flamrow(i) = flampak(ioff+i)
  flanrow(i) = flanpak(ioff+i)
  flanrol(i) = flanpal(ioff+i)
  flgcrow(i) = flgcpak(ioff+i)
  flgrow (i) = flgpak (ioff+i)
  flkrrow(i) = flkrpak(ioff+i)
  flkurow(i) = flkupak(ioff+i)
  flndrow(i) = flndpak(ioff+i)
  fsamrol(i) = fsampal(ioff+i)
  fsamrow(i) = fsampak(ioff+i)
  fsanrol(i) = fsanpal(ioff+i)
  fsanrow(i) = fsanpak(ioff+i)
  fsdcrow(i) = fsdcpak(ioff+i)
  fsdrol (i) = fsdpal (ioff+i)
  fsdrow (i) = fsdpak (ioff+i)
  fsfrol (i) = fsfpal (ioff+i)
  fsgcrow(i) = fsgcpak(ioff+i)
  fsgirol(i) = fsgipal(ioff+i)
  fsgorol(i) = fsgopal(ioff+i)
  fsgrow (i) = fsgpak (ioff+i)
  fsirol (i) = fsipal (ioff+i)
  fslorol(i) = fslopal(ioff+i)
  fslorow(i) = fslopak(ioff+i)
  fsorow (i) = fsopak (ioff+i)
  fsrcrow(i) = fsrcpak(ioff+i)
  fsrrow (i) = fsrpak (ioff+i)
  fsscrow(i) = fsscpak(ioff+i)
  fssrow (i) = fsspak (ioff+i)
  fsvrol (i) = fsvpal (ioff+i)
  fsvrow (i) = fsvpak (ioff+i)
  gamrow (i) = gampak (ioff+i)
  gicnrow(i) = gicnpak(ioff+i)
  gtarow (i) = gtapak (ioff+i)
  gtrow  (i) = gtpak  (ioff+i)
  hflrow (i) = hflpak (ioff+i)
  hfsrow (i) = hfspak (ioff+i)
  hsearol(i) = hseapal(ioff+i)
  ofsgrol(i) = ofsgpal(ioff+i)
  olrrow (i) = olrpak (ioff+i)
  olrcrow(i) = olrcpak(ioff+i)
  parrol (i) = parpal (ioff+i)
  pblhrow(i) = pblhpak(ioff+i)
  pbltrow(i) = pbltpak(ioff+i)
  pchfrow(i) = pchfpak(ioff+i)
  pcpcrow(i) = pcpcpak(ioff+i)
  pcprow (i) = pcppak (ioff+i)
  pcpsrow(i) = pcpspak(ioff+i)
  phisrow(i) = phispak(ioff+i)
  plhfrow(i) = plhfpak(ioff+i)
  pmslrol(i) = pmslpal(ioff+i)
  psarow (i) = psapak (ioff+i)
  pshfrow(i) = pshfpak(ioff+i)
  psirow (i) = psipak (ioff+i)
  pwatrow(i) = pwatpak(ioff+i)
  pwatrom(i) = pwatpam(ioff+i)
  qfsrow (i) = qfspak (ioff+i)
  qfslrol(i) = qfslpal(ioff+i)
  qfsorol(i) = qfsopal(ioff+i)
  qfxrow (i) = qfxpak (ioff+i)
  qtphrom(i) = qtphpam(ioff+i)
  qtpfrom(i) = qtpfpam(ioff+i)
  qtptrom(i) = qtptpam(ioff+i)
  rainsrol(i)= rainspal(ioff+i)
  sicnrow(i) = sicnpak(ioff+i)
  sicrow (i) = sicpak (ioff+i)
  sigxrow(i) = sigxpak(ioff+i)
  slimrol(i) = slimpal(ioff+i)
  slimrlw(i) = slimplw(ioff+i)
  slimrsh(i) = slimpsh(ioff+i)
  slimrlh(i) = slimplh(ioff+i)
  snorow (i) = snopak (ioff+i)
  snowsrol(i)= snowspal(ioff+i)
  sqrow  (i) = sqpak  (ioff+i)
  srhrow (i) = srhpak (ioff+i)
  srhnrow(i) = srhnpak(ioff+i)
  srhxrow(i) = srhxpak(ioff+i)
  stmnrow(i) = stmnpak(ioff+i)
  stmxrow(i) = stmxpak(ioff+i)
  strow  (i) = stpak  (ioff+i)
  surow  (i) = supak  (ioff+i)
  svrow  (i) = svpak  (ioff+i)
  swarow (i) = swapak (ioff+i)
  swarol (i) = swapal (ioff+i)
  swxrow (i) = swxpak (ioff+i)
  swxurow(i) = swxupak(ioff+i)
  swxvrow(i) = swxvpak(ioff+i)
  tcvrow (i) = tcvpak (ioff+i)
  tfxrow (i) = tfxpak (ioff+i)
  ufsrol (i) = ufspal (ioff+i)
  ufsrow (i) = ufspak (ioff+i)
  ufsirol(i) = ufsipal(ioff+i)
  ufsorol(i) = ufsopal(ioff+i)
  vfsrol (i) = vfspal (ioff+i)
  vfsrow (i) = vfspak (ioff+i)
  vfsirol(i) = vfsipal(ioff+i)
  vfsorol(i) = vfsopal(ioff+i)
end do
snorot(il1:il2,:)=snopat(ioff+il1:ioff+il2,:)
gtrot (il1:il2,:)=gtpat (ioff+il1:ioff+il2,:)
gtrox (il1:il2,:)=gtpax (ioff+il1:ioff+il2,:)
emisrot(il1:il2,:)=emispat(ioff+il1:ioff+il2,:)
xlmrot(il1:il2,:,:)=xlmpat(ioff+il1:ioff+il2,:,:)
!
do l = 1, nbs
  do i = il1, il2
    fsdbrol (i,l)  = fsdbpal (ioff+i,l)
    fsfbrol (i,l)  = fsfbpal (ioff+i,l)
    csdbrol (i,l)  = csdbpal (ioff+i,l)
    csfbrol (i,l)  = csfbpal (ioff+i,l)
    fssbrol (i,l)  = fssbpal (ioff+i,l)
    fsscbrol (i,l) = fsscbpal (ioff+i,l)
    wrkarol (i,l)  = wrkapal (ioff+i,l)
    wrkbrol (i,l)  = wrkbpal (ioff+i,l)
  end do
end do
clbrot (il1:il2,:)=clbpat (ioff+il1:ioff+il2,:)
csbrot (il1:il2,:)=csbpat (ioff+il1:ioff+il2,:)
csdrot (il1:il2,:)=csdpat (ioff+il1:ioff+il2,:)
csfrot (il1:il2,:)=csfpat (ioff+il1:ioff+il2,:)
fdlrot (il1:il2,:)=fdlpat (ioff+il1:ioff+il2,:)
fdlcrot(il1:il2,:)=fdlcpat(ioff+il1:ioff+il2,:)
flgrot (il1:il2,:)=flgpat (ioff+il1:ioff+il2,:)
fsdrot (il1:il2,:)=fsdpat (ioff+il1:ioff+il2,:)
fsfrot (il1:il2,:)=fsfpat (ioff+il1:ioff+il2,:)
fsgrot (il1:il2,:)=fsgpat (ioff+il1:ioff+il2,:)
fsirot (il1:il2,:)=fsipat (ioff+il1:ioff+il2,:)
fsvrot (il1:il2,:)=fsvpat (ioff+il1:ioff+il2,:)
parrot (il1:il2,:)=parpat (ioff+il1:ioff+il2,:)
!
do l = 1, nbs
  do m = 1, im
    do i = il1, il2
      fsdbrot (i,m,l)  = fsdbpat (ioff+i,m,l)
      fsfbrot (i,m,l)  = fsfbpat (ioff+i,m,l)
      csdbrot (i,m,l)  = csdbpat (ioff+i,m,l)
      csfbrot (i,m,l)  = csfbpat (ioff+i,m,l)
      fssbrot (i,m,l)  = fssbpat (ioff+i,m,l)
      fsscbrot(i,m,l)  = fsscbpat(ioff+i,m,l)
    end do
  end do
end do
!
!
!     * land surface arrays.
!
alicrot(il1:il2,:,:)=alicpat(ioff+il1:ioff+il2,:,:)
alvcrot(il1:il2,:,:)=alvcpat(ioff+il1:ioff+il2,:,:)
fcanrot(il1:il2,:,:)=fcanpat(ioff+il1:ioff+il2,:,:)
lnz0rot(il1:il2,:,:)=lnz0pat(ioff+il1:ioff+il2,:,:)
cmasrot(il1:il2,:,:)=cmaspat(ioff+il1:ioff+il2,:,:)
lamnrot(il1:il2,:,:)=lamnpat(ioff+il1:ioff+il2,:,:)
lamxrot(il1:il2,:,:)=lamxpat(ioff+il1:ioff+il2,:,:)
rootrot(il1:il2,:,:)=rootpat(ioff+il1:ioff+il2,:,:)

gflxrow(il1:il2,:)  =gflxpak (ioff+il1:ioff+il2,:)
!
do l=1,ignd
  do i=il1,il2
    hfcgrow(i,l) = hfcgpak(ioff+i,l)
    hmfgrow(i,l) = hmfgpak(ioff+i,l)
    qfvgrow(i,l) = qfvgpak(ioff+i,l)
    tgrow  (i,l) = tgpak  (ioff+i,l)
    wgfrow (i,l) = wgfpak (ioff+i,l)
    wglrow (i,l) = wglpak (ioff+i,l)
  end do
end do
sandrot(il1:il2,:,:)=sandpat(ioff+il1:ioff+il2,:,:)
clayrot(il1:il2,:,:)=claypat(ioff+il1:ioff+il2,:,:)
orgmrot(il1:il2,:,:)=orgmpat(ioff+il1:ioff+il2,:,:)
tgrot  (il1:il2,:,:)=tgpat  (ioff+il1:ioff+il2,:,:)
thlqrot(il1:il2,:,:)=thlqpat(ioff+il1:ioff+il2,:,:)
thicrot(il1:il2,:,:)=thicpat(ioff+il1:ioff+il2,:,:)
if (kount>kstart) then
  dlzwrot(il1:il2,:,:)=dlzwpat(ioff+il1:ioff+il2,:,:)
  porgrot(il1:il2,:,:)=porgpat(ioff+il1:ioff+il2,:,:)
  thfcrot(il1:il2,:,:)=thfcpat(ioff+il1:ioff+il2,:,:)
  thlwrot(il1:il2,:,:)=thlwpat(ioff+il1:ioff+il2,:,:)
  thrrot (il1:il2,:,:)=thrpat (ioff+il1:ioff+il2,:,:)
  thmrot (il1:il2,:,:)=thmpat (ioff+il1:ioff+il2,:,:)
  birot  (il1:il2,:,:)=bipat (ioff+il1:ioff+il2,:,:)
  psisrot(il1:il2,:,:)=psispat(ioff+il1:ioff+il2,:,:)
  grksrot(il1:il2,:,:)=grkspat(ioff+il1:ioff+il2,:,:)
  thrarot(il1:il2,:,:)=thrapat(ioff+il1:ioff+il2,:,:)
  hcpsrot(il1:il2,:,:)=hcpspat(ioff+il1:ioff+il2,:,:)
  tcsrot (il1:il2,:,:)=tcspat (ioff+il1:ioff+il2,:,:)
  psiwrot(il1:il2,:,:)=psiwpat(ioff+il1:ioff+il2,:,:)
  zbtwrot(il1:il2,:,:)=zbtwpat(ioff+il1:ioff+il2,:,:)
  isndrot(il1:il2,:,:)=isndpat(ioff+il1:ioff+il2,:,:)
  !
  algdvrot(il1:il2,:) =algdvpat(ioff+il1:ioff+il2,:)
  algdnrot(il1:il2,:) =algdnpat(ioff+il1:ioff+il2,:)
  algwvrot(il1:il2,:) =algwvpat(ioff+il1:ioff+il2,:)
  algwnrot(il1:il2,:) =algwnpat(ioff+il1:ioff+il2,:)
  igdrrot(il1:il2,:)  =igdrpat(ioff+il1:ioff+il2,:)
end if

!
! * TKE fields
!
do l=1,ilev
  do i=il1,il2
    tkemrow (i,l) = tkempak (ioff+i,l)
    xlmrow  (i,l) = xlmpak  (ioff+i,l)
    svarrow (i,l) = svarpak (ioff+i,l)
  end do
end do

do i=il1,il2
  anrow  (i) = anpak  (ioff+i)
  drrow  (i) = drpak  (ioff+i)
  flggrow(i) = flggpak(ioff+i)
  flgnrow(i) = flgnpak(ioff+i)
  flgvrow(i) = flgvpak(ioff+i)
  fnlarow(i) = fnlapak(ioff+i)
  fnrow  (i) = fnpak  (ioff+i)
  fsggrow(i) = fsggpak(ioff+i)
  fsgnrow(i) = fsgnpak(ioff+i)
  fsgvrow(i) = fsgvpak(ioff+i)
  fvgrow (i) = fvgpak (ioff+i)
  fvnrow (i) = fvnpak (ioff+i)
  gsnorow(i) = gsnopak(ioff+i)
  hfcnrow(i) = hfcnpak(ioff+i)
  hfcvrow(i) = hfcvpak(ioff+i)
  hflgrow(i) = hflgpak(ioff+i)
  hflnrow(i) = hflnpak(ioff+i)
  hflvrow(i) = hflvpak(ioff+i)
  hfsgrow(i) = hfsgpak(ioff+i)
  hfsnrow(i) = hfsnpak(ioff+i)
  hfsvrow(i) = hfsvpak(ioff+i)
  hmfnrow(i) = hmfnpak(ioff+i)
  hmfvrow(i) = hmfvpak(ioff+i)
  mvrow  (i) = mvpak  (ioff+i)
  pcpnrow(i) = pcpnpak(ioff+i)
  pigrow (i) = pigpak (ioff+i)
  pinrow (i) = pinpak (ioff+i)
  pivfrow(i) = pivfpak(ioff+i)
  pivlrow(i) = pivlpak(ioff+i)
  qfgrow (i) = qfgpak (ioff+i)
  qfnrow (i) = qfnpak (ioff+i)
  qfvfrow(i) = qfvfpak(ioff+i)
  qfvlrow(i) = qfvlpak(ioff+i)
  rhonrow(i) = rhonpak(ioff+i)
  rofnrow(i) = rofnpak(ioff+i)
  roforol(i) = rofopal(ioff+i)
  roforow(i) = rofopak(ioff+i)
  rofrol (i) = rofpal (ioff+i)
  rofrow (i) = rofpak (ioff+i)
  rofvrow(i) = rofvpak(ioff+i)
  rovgrow(i) = rovgpak(ioff+i)
  skygrow(i) = skygpak(ioff+i)
  skynrow(i) = skynpak(ioff+i)
  smltrow(i) = smltpak(ioff+i)
  tbasrow(i) = tbaspak(ioff+i)
  tnrow  (i) = tnpak  (ioff+i)
  ttrow  (i) = ttpak  (ioff+i)
  tvrow  (i) = tvpak  (ioff+i)
  wtrgrow(i) = wtrgpak(ioff+i)
  wtrnrow(i) = wtrnpak(ioff+i)
  wtrvrow(i) = wtrvpak(ioff+i)
  wvfrow (i) = wvfpak (ioff+i)
  wvlrow (i) = wvlpak (ioff+i)
  znrow  (i) = znpak  (ioff+i)
  garow  (i) = gapak  (ioff+i)
  hblrow (i) = hblpak (ioff+i)
  ilmorow(i) = ilmopak(ioff+i)
  petrow(i)  = petpak (ioff+i)
  rofbrow(i) = rofbpak(ioff+i)
  rofsrow(i) = rofspak(ioff+i)
  uerow  (i) = uepak  (ioff+i)
  wsnorow(i) = wsnopak(ioff+i)
  wtabrow(i) = wtabpak(ioff+i)
  zpndrow(i) = zpndpak(ioff+i)
  refrow (i) = refpak (ioff+i)
  bcsnrow(i) = bcsnpak(ioff+i)
  depbrow(i) = depbpak(ioff+i)
end do
!
cldt3hrow(il1:il2)=cldt3hpak(ioff+il1:ioff+il2)
hfl3hrow (il1:il2)=hfl3hpak (ioff+il1:ioff+il2)
hfs3hrow (il1:il2)=hfs3hpak (ioff+il1:ioff+il2)
rof3hrol (il1:il2)=rof3hpal (ioff+il1:ioff+il2)
wgl3hrow (il1:il2)=wgl3hpak (ioff+il1:ioff+il2)
wgf3hrow (il1:il2)=wgf3hpak (ioff+il1:ioff+il2)
pcp3hrow (il1:il2)=pcp3hpak (ioff+il1:ioff+il2)
pcpc3hrow(il1:il2)=pcpc3hpak(ioff+il1:ioff+il2)
pcpl3hrow(il1:il2)=pcpl3hpak(ioff+il1:ioff+il2)
pcps3hrow(il1:il2)=pcps3hpak(ioff+il1:ioff+il2)
pcpn3hrow(il1:il2)=pcpn3hpak(ioff+il1:ioff+il2)
fdl3hrow (il1:il2)=fdl3hpak (ioff+il1:ioff+il2)
fdlc3hrow(il1:il2)=fdlc3hpak(ioff+il1:ioff+il2)
flg3hrow (il1:il2)=flg3hpak (ioff+il1:ioff+il2)
fss3hrow (il1:il2)=fss3hpak (ioff+il1:ioff+il2)
fssc3hrow(il1:il2)=fssc3hpak(ioff+il1:ioff+il2)
fsd3hrow (il1:il2)=fsd3hpak (ioff+il1:ioff+il2)
fsg3hrow (il1:il2)=fsg3hpak (ioff+il1:ioff+il2)
fsgc3hrow(il1:il2)=fsgc3hpak(ioff+il1:ioff+il2)
sq3hrow  (il1:il2)=sq3hpak  (ioff+il1:ioff+il2)
st3hrow  (il1:il2)=st3hpak  (ioff+il1:ioff+il2)
su3hrow  (il1:il2)=su3hpak  (ioff+il1:ioff+il2)
sv3hrow  (il1:il2)=sv3hpak  (ioff+il1:ioff+il2)

olr3hrow(il1:il2)  = olr3hpak(ioff+il1:ioff+il2)
fsr3hrow(il1:il2)  = fsr3hpak(ioff+il1:ioff+il2)
olrc3hrow(il1:il2) = olrc3hpak(ioff+il1:ioff+il2)
fsrc3hrow(il1:il2) = fsrc3hpak(ioff+il1:ioff+il2)
fso3hrow(il1:il2)  = fso3hpak(ioff+il1:ioff+il2)
pwat3hrow(il1:il2) = pwat3hpak(ioff+il1:ioff+il2)
clwt3hrow(il1:il2) = clwt3hpak(ioff+il1:ioff+il2)
cict3hrow(il1:il2) = cict3hpak(ioff+il1:ioff+il2)
pmsl3hrow(il1:il2) = pmsl3hpak(ioff+il1:ioff+il2)
!
!  * instantaneous variables for sampling
!
do i=il1,il2
  swrol(i)    = swpal(ioff+i)
  srhrol(i)     = srhpal(ioff+i)
  pcprol(i)     = pcppal(ioff+i)
  pcpnrol(i)    = pcpnpal(ioff+i)
  pcpcrol(i)    = pcpcpal(ioff+i)
  qfsirol(i)    = qfsipal(ioff+i)
  qfnrol(i)     = qfnpal(ioff+i)
  qfvfrol(i)    = qfvfpal(ioff+i)
  ufsinstrol(i) = ufsinstpal(ioff+i)
  vfsinstrol(i) = vfsinstpal(ioff+i)
  hflirol(i)    = hflipal(ioff+i)
  hfsirol(i)    = hfsipal(ioff+i)
  fdlrol(i)     = fdlpal(ioff+i)
  flgrol(i)     = flgpal(ioff+i)
  fssrol(i)     = fsspal(ioff+i)
  fsgrol(i)     = fsgpal(ioff+i)
  fsscrol(i)    = fsscpal(ioff+i)
  fsgcrol(i)    = fsgcpal(ioff+i)
  fdlcrol(i)    = fdlcpal(ioff+i)
  fsorol(i)     = fsopal(ioff+i)
  fsrrol(i)     = fsrpal(ioff+i)
  olrrol(i)     = olrpal(ioff+i)
  olrcrol(i)    = olrcpal(ioff+i)
  fsrcrol(i)    = fsrcpal(ioff+i)
  pwatirol(i)   = pwatipal(ioff+i)
  cldtrol(i)    = cldtpal(ioff+i)
  clwtrol(i)    = clwtpal(ioff+i)
  cictrol(i)    = cictpal(ioff+i)
  baltrol(i)    = baltpal(ioff+i)
  pmslirol(i)   = pmslipal(ioff+i)
  cdcbrol(i)    = cdcbpal(ioff+i)
  cscbrol(i)    = cscbpal(ioff+i)
  gtrol(i)      = gtpal(ioff+i)
  tcdrol(i)     = tcdpal(ioff+i)
  bcdrol(i)     = bcdpal(ioff+i)
  psrol(i)      = pspal(ioff+i)
  strol(i)      = stpal(ioff+i)
  surol(i)      = supal(ioff+i)
  svrol(i)      = svpal(ioff+i)
end do ! i

do l = 1, ilev
  do i = il1, il2
    dmcrol(i,l)  = dmcpal(i+ioff,l)
    tarol(i,l)   = tapal(i+ioff,l)
    uarol(i,l)   = uapal(i+ioff,l)
    varol(i,l)   = vapal(i+ioff,l)
    qarol(i,l)   = qapal(i+ioff,l)
    rhrol(i,l)   = rhpal(i+ioff,l)
    zgrol(i,l)   = zgpal(i+ioff,l)
    rkhrol(i,l)  = rkhpal(i+ioff,l)
    rkmrol(i,l)  = rkmpal(i+ioff,l)
    rkqrol(i,l)  = rkqpal(i+ioff,l)

    ttprol(i,l)  = ttppal(i+ioff,l)
    ttpprol(i,l) = ttpppal(i+ioff,l)
    ttpvrol(i,l) = ttpvpal(i+ioff,l)
    ttplrol(i,l) = ttplpal(i+ioff,l)
    ttpsrol(i,l) = ttpspal(i+ioff,l)
    ttlcrol(i,l) = ttlcpal(i+ioff,l)
    ttscrol(i,l) = ttscpal(i+ioff,l)
    ttpcrol(i,l) = ttpcpal(i+ioff,l)
    ttpmrol(i,l) = ttpmpal(i+ioff,l)

    qtprol(i,l)  = qtppal(i+ioff,l)
    qtpprol(i,l) = qtpppal(i+ioff,l)
    qtpvrol(i,l) = qtpvpal(i+ioff,l)
    qtpcrol(i,l) = qtpcpal(i+ioff,l)
    qtpmrol(i,l) = qtpmpal(i+ioff,l)
  end do ! i
end do ! l

anrot(il1:il2,:)=anpat(ioff+il1:ioff+il2,:)
dpthrot(il1:il2,:)=dpthpat(ioff+il1:ioff+il2,:)
drnrot(il1:il2,:)=drnpat(ioff+il1:ioff+il2,:)
fnrot(il1:il2,:)=fnpat(ioff+il1:ioff+il2,:)
mvrot(il1:il2,:)=mvpat(ioff+il1:ioff+il2,:)
qavrot(il1:il2,:)=qavpat(ioff+il1:ioff+il2,:)
rhonrot(il1:il2,:)=rhonpat(ioff+il1:ioff+il2,:)
socirot(il1:il2,:)=socipat(ioff+il1:ioff+il2,:)
tavrot(il1:il2,:)=tavpat(ioff+il1:ioff+il2,:)
tbasrot(il1:il2,:)=tbaspat(ioff+il1:ioff+il2,:)
tnrot(il1:il2,:)=tnpat(ioff+il1:ioff+il2,:)
tpndrot(il1:il2,:)=tpndpat(ioff+il1:ioff+il2,:)
ttrot(il1:il2,:)=ttpat(ioff+il1:ioff+il2,:)
tvrot(il1:il2,:)=tvpat(ioff+il1:ioff+il2,:)
wsnorot(il1:il2,:)=wsnopat(ioff+il1:ioff+il2,:)
wvfrot(il1:il2,:)=wvfpat(ioff+il1:ioff+il2,:)
wvlrot(il1:il2,:)=wvlpat(ioff+il1:ioff+il2,:)
zpndrot(il1:il2,:)=zpndpat(ioff+il1:ioff+il2,:)
refrot(il1:il2,:)=refpat(ioff+il1:ioff+il2,:)
bcsnrot(il1:il2,:)=bcsnpat(ioff+il1:ioff+il2,:)
!
farerot(il1:il2,:)=farepat(ioff+il1:ioff+il2,:)
tsfsrot(il1:il2,:,:)=tsfspat(ioff+il1:ioff+il2,:,:)
#if defined (agcm_ctem)

      rmatcrot  (il1:il2,:,:,:) = rmatcpat  (ioff+il1:ioff+il2,:,:,:)
      rtctmrot  (il1:il2,:,:,:) = rtctmpat  (ioff+il1:ioff+il2,:,:,:)

      cfcanrot  (il1:il2,:,:) = cfcanpat  (ioff+il1:ioff+il2,:,:)
      zolncrot  (il1:il2,:,:) = zolncpat  (ioff+il1:ioff+il2,:,:)
      ailcrot   (il1:il2,:,:) = ailcpat   (ioff+il1:ioff+il2,:,:)
      cmascrot  (il1:il2,:,:) = cmascpat  (ioff+il1:ioff+il2,:,:)
      calvcrot  (il1:il2,:,:) = calvcpat  (ioff+il1:ioff+il2,:,:)
      calicrot  (il1:il2,:,:) = calicpat  (ioff+il1:ioff+il2,:,:)
      paicrot   (il1:il2,:,:) = paicpat   (ioff+il1:ioff+il2,:,:)
      slaicrot  (il1:il2,:,:) = slaicpat  (ioff+il1:ioff+il2,:,:)
      fcancrot  (il1:il2,:,:) = fcancpat  (ioff+il1:ioff+il2,:,:)
      todfcrot  (il1:il2,:,:) = todfcpat  (ioff+il1:ioff+il2,:,:)
      ailcgrot  (il1:il2,:,:) = ailcgpat  (ioff+il1:ioff+il2,:,:)
      slairot   (il1:il2,:,:) = slaipat   (ioff+il1:ioff+il2,:,:)

      fsnowrtl  (il1:il2,:)     = fsnowptl  (ioff+il1:ioff+il2,:)
      tcanortl  (il1:il2,:)     = tcanoptl  (ioff+il1:ioff+il2,:)
      tcansrtl  (il1:il2,:)     = tcansptl  (ioff+il1:ioff+il2,:)
      tartl     (il1:il2,:)     = taptl     (ioff+il1:ioff+il2,:)
      cfluxcsrot(il1:il2,:)     = cfluxcspat(ioff+il1:ioff+il2,:)
      cfluxcgrot(il1:il2,:)     = cfluxcgpat(ioff+il1:ioff+il2,:)
      co2cg1rot (il1:il2,:,:)   = co2cg1pat (ioff+il1:ioff+il2,:,:)
      co2cg2rot (il1:il2,:,:)   = co2cg2pat (ioff+il1:ioff+il2,:,:)
      co2cs1rot (il1:il2,:,:)   = co2cs1pat (ioff+il1:ioff+il2,:,:)
      co2cs2rot (il1:il2,:,:)   = co2cs2pat (ioff+il1:ioff+il2,:,:)
      ancgrtl   (il1:il2,:,:)   = ancgptl   (ioff+il1:ioff+il2,:,:)
      ancsrtl   (il1:il2,:,:)   = ancsptl   (ioff+il1:ioff+il2,:,:)
      rmlcgrtl  (il1:il2,:,:)   = rmlcgptl  (ioff+il1:ioff+il2,:,:)
      rmlcsrtl  (il1:il2,:,:)   = rmlcsptl  (ioff+il1:ioff+il2,:,:)

      tbarrtl   (il1:il2,:,:)   = tbarptl   (ioff+il1:ioff+il2,:,:)
      tbarcrtl  (il1:il2,:,:)   = tbarcptl  (ioff+il1:ioff+il2,:,:)
      tbarcsrtl (il1:il2,:,:)   = tbarcsptl (ioff+il1:ioff+il2,:,:)
      tbargrtl  (il1:il2,:,:)   = tbargptl  (ioff+il1:ioff+il2,:,:)
      tbargsrtl (il1:il2,:,:)   = tbargsptl (ioff+il1:ioff+il2,:,:)
      thliqcrtl (il1:il2,:,:)   = thliqcptl (ioff+il1:ioff+il2,:,:)
      thliqgrtl (il1:il2,:,:)   = thliqgptl (ioff+il1:ioff+il2,:,:)
      thicecrtl (il1:il2,:,:)   = thicecptl (ioff+il1:ioff+il2,:,:)
!
!     * invariant.
!
      wetfrow   (il1:il2)       = wetfpak   (ioff+il1:ioff+il2)
      wetsrow   (il1:il2)       = wetspak   (ioff+il1:ioff+il2)
      lghtrow   (il1:il2)       = lghtpak   (ioff+il1:ioff+il2)
!
!     * time varying.
!
      soilcrot  (il1:il2,:,:)   = soilcpat  (ioff+il1:ioff+il2,:,:)
      litrcrot  (il1:il2,:,:)   = litrcpat  (ioff+il1:ioff+il2,:,:)
      rootcrot  (il1:il2,:,:)   = rootcpat  (ioff+il1:ioff+il2,:,:)
      stemcrot  (il1:il2,:,:)   = stemcpat  (ioff+il1:ioff+il2,:,:)
      gleafcrot (il1:il2,:,:)   = gleafcpat (ioff+il1:ioff+il2,:,:)
      bleafcrot (il1:il2,:,:)   = bleafcpat (ioff+il1:ioff+il2,:,:)
      fallhrot  (il1:il2,:,:)   = fallhpat  (ioff+il1:ioff+il2,:,:)
      posphrot  (il1:il2,:,:)   = posphpat  (ioff+il1:ioff+il2,:,:)
      leafsrot  (il1:il2,:,:)   = leafspat  (ioff+il1:ioff+il2,:,:)
      growtrot  (il1:il2,:,:)   = growtpat  (ioff+il1:ioff+il2,:,:)
      lastrrot  (il1:il2,:,:)   = lastrpat  (ioff+il1:ioff+il2,:,:)
      lastsrot  (il1:il2,:,:)   = lastspat  (ioff+il1:ioff+il2,:,:)
      thisylrot (il1:il2,:,:)   = thisylpat (ioff+il1:ioff+il2,:,:)
      stemhrot  (il1:il2,:,:)   = stemhpat  (ioff+il1:ioff+il2,:,:)
      roothrot  (il1:il2,:,:)   = roothpat  (ioff+il1:ioff+il2,:,:)
      tempcrot  (il1:il2,:,:)   = tempcpat  (ioff+il1:ioff+il2,:,:)
      ailcbrot  (il1:il2,:,:)   = ailcbpat  (ioff+il1:ioff+il2,:,:)
      bmasvrot  (il1:il2,:,:)   = bmasvpat  (ioff+il1:ioff+il2,:,:)
      veghrot   (il1:il2,:,:)   = veghpat   (ioff+il1:ioff+il2,:,:)
      rootdrot  (il1:il2,:,:)   = rootdpat  (ioff+il1:ioff+il2,:,:)
!
      prefrot   (il1:il2,:,:)   = prefpat   (ioff+il1:ioff+il2,:,:)
      newfrot   (il1:il2,:,:)   = newfpat   (ioff+il1:ioff+il2,:,:)
!
      cvegrot   (il1:il2,:)     = cvegpat   (ioff+il1:ioff+il2,:)
      cdebrot   (il1:il2,:)     = cdebpat   (ioff+il1:ioff+il2,:)
      chumrot   (il1:il2,:)     = chumpat   (ioff+il1:ioff+il2,:)
      fcolrot   (il1:il2,:)     = fcolpat   (ioff+il1:ioff+il2,:)
!
!     * ctem diagnostic output fields.
!     * NOTE THAT SAMPLED FIELDS DON'T HAVE TO BE
!     * unpacked since they are zeroed out in
!     * sfcproc2 prior to accumulation over tiles !
!
      cfnprow(il1:il2) = cfnppak(ioff+il1:ioff+il2)
      cfnerow(il1:il2) = cfnepak(ioff+il1:ioff+il2)
      cfrvrow(il1:il2) = cfrvpak(ioff+il1:ioff+il2)
      cfgprow(il1:il2) = cfgppak(ioff+il1:ioff+il2)
      cfnbrow(il1:il2) = cfnbpak(ioff+il1:ioff+il2)
      cffvrow(il1:il2) = cffvpak(ioff+il1:ioff+il2)
      cffdrow(il1:il2) = cffdpak(ioff+il1:ioff+il2)
      cflvrow(il1:il2) = cflvpak(ioff+il1:ioff+il2)
      cfldrow(il1:il2) = cfldpak(ioff+il1:ioff+il2)
      cflhrow(il1:il2) = cflhpak(ioff+il1:ioff+il2)
      cbrnrow(il1:il2) = cbrnpak(ioff+il1:ioff+il2)
      cfrhrow(il1:il2) = cfrhpak(ioff+il1:ioff+il2)
      cfhtrow(il1:il2) = cfhtpak(ioff+il1:ioff+il2)
      cflfrow(il1:il2) = cflfpak(ioff+il1:ioff+il2)
      cfrdrow(il1:il2) = cfrdpak(ioff+il1:ioff+il2)
      cfrgrow(il1:il2) = cfrgpak(ioff+il1:ioff+il2)
      cfrmrow(il1:il2) = cfrmpak(ioff+il1:ioff+il2)
      cfnlrow(il1:il2) = cfnlpak(ioff+il1:ioff+il2)
      cfnsrow(il1:il2) = cfnspak(ioff+il1:ioff+il2)
      cfnrrow(il1:il2) = cfnrpak(ioff+il1:ioff+il2)
      ch4hrow(il1:il2) = ch4hpak(ioff+il1:ioff+il2)
      ch4nrow(il1:il2) = ch4npak(ioff+il1:ioff+il2)
      wfrarow(il1:il2) = wfrapak(ioff+il1:ioff+il2)
      cw1drow(il1:il2) = cw1dpak(ioff+il1:ioff+il2)
      cw2drow(il1:il2) = cw2dpak(ioff+il1:ioff+il2)
      fcolrow(il1:il2) = fcolpak(ioff+il1:ioff+il2)

!
! instantaneous output (used for subdaily output)
!
      cfgprol(il1:il2) = cfgppal(ioff+il1:ioff+il2)
      cfrvrol(il1:il2) = cfrvpal(ioff+il1:ioff+il2)
      cfrhrol(il1:il2) = cfrhpal(ioff+il1:ioff+il2)
      cfrdrol(il1:il2) = cfrdpal(ioff+il1:ioff+il2)

#endif
#if defined explvol
!
!     * explosive volcano fields.
!
      vtaurow(il1:il2) = vtaupak(ioff+il1:ioff+il2)
      vtaurol(il1:il2) = vtaupal(ioff+il1:ioff+il2)
      troprow(il1:il2) = troppak(ioff+il1:ioff+il2)
      troprol(il1:il2) = troppal(ioff+il1:ioff+il2)

      do ib = 1, nbs
         do l = 1, levsa
	   do i = il1, il2
	      sw_ext_sa_row(i,l,ib) = sw_ext_sa_pak(ioffz+il1z,l,ib)
	      sw_ext_sa_rol(i,l,ib) = sw_ext_sa_pal(ioffz+il1z,l,ib)
	      sw_ssa_sa_row(i,l,ib) = sw_ssa_sa_pak(ioffz+il1z,l,ib)
	      sw_ssa_sa_rol(i,l,ib) = sw_ssa_sa_pal(ioffz+il1z,l,ib)
	      sw_g_sa_row(i,l,ib)   = sw_g_sa_pak(ioffz+il1z,l,ib)
	      sw_g_sa_rol(i,l,ib)   = sw_g_sa_pal(ioffz+il1z,l,ib)
            end do ! i
         end do ! l
      end do ! ib

      do ib = 1, nbl
         do l = 1, levsa
	    do i = il1, il2
	      lw_ext_sa_row(i,l,ib) = lw_ext_sa_pak(ioffz+il1z,l,ib)
	      lw_ext_sa_rol(i,l,ib) = lw_ext_sa_pal(ioffz+il1z,l,ib)
	      lw_ssa_sa_row(i,l,ib) = lw_ssa_sa_pak(ioffz+il1z,l,ib)
	      lw_ssa_sa_rol(i,l,ib) = lw_ssa_sa_pal(ioffz+il1z,l,ib)
	      lw_g_sa_row(i,l,ib)   = lw_g_sa_pak(ioffz+il1z,l,ib)
	      lw_g_sa_rol(i,l,ib)   = lw_g_sa_pal(ioffz+il1z,l,ib)
            end do ! i
         end do ! l
      end do ! ib

      do l = 1, levsa
	 do i = il1, il2
	    w055_ext_sa_row(i,l) = w055_ext_sa_pak(ioffz+il1z,l)
	    w055_ext_sa_rol(i,l) = w055_ext_sa_pal(ioffz+il1z,l)
	    w110_ext_sa_row(i,l) = w110_ext_sa_pak(ioffz+il1z,l)
	    w110_ext_sa_rol(i,l) = w110_ext_sa_pal(ioffz+il1z,l)
	    pressure_sa_row(i,l) = pressure_sa_pak(ioffz+il1z,l)
	    pressure_sa_rol(i,l) = pressure_sa_pal(ioffz+il1z,l)
         end do ! i
      end do ! l

      do i = il1, il2
         w055_vtau_sa_row(i) = w055_vtau_sa_pak(ioff+i)
         w055_vtau_sa_rol(i) = w055_vtau_sa_pal(ioff+i)
         w110_vtau_sa_row(i) = w110_vtau_sa_pak(ioff+i)
         w110_vtau_sa_rol(i) = w110_vtau_sa_pal(ioff+i)
      end do ! i

      do l = 1, ilev
         do i = il1, il2
            w055_ext_gcm_sa_row(i,l) = w055_ext_gcm_sa_pak(ioff+i,l)
            w055_ext_gcm_sa_rol(i,l) = w055_ext_gcm_sa_pal(ioff+i,l)
            w110_ext_gcm_sa_row(i,l) = w110_ext_gcm_sa_pak(ioff+i,l)
            w110_ext_gcm_sa_rol(i,l) = w110_ext_gcm_sa_pal(ioff+i,l)
         end do ! i
      end do ! l

#endif
!
!     * lakes fields.
!
do i=il1,il2
  blakrow(i) = blakpak(ioff+i)
  hlakrow(i) = hlakpak(ioff+i)
  ldmxrow(i) = ldmxpak(ioff+i)
  llakrow(i) = llakpak(ioff+i)
  lrimrow(i) = lrimpak(ioff+i)
  lrinrow(i) = lrinpak(ioff+i)
  luimrow(i) = luimpak(ioff+i)
  luinrow(i) = luinpak(ioff+i)
  lzicrow(i) = lzicpak(ioff+i)
#if defined (cslm)
        delurow(i) = delupak(ioff+i)
        dtmprow(i) = dtmppak(ioff+i)
        expwrow(i) = expwpak(ioff+i)
        gredrow(i) = gredpak(ioff+i)
        nlklrow(i) = nlklpak(ioff+i)
        rhomrow(i) = rhompak(ioff+i)
        t0lkrow(i) = t0lkpak(ioff+i)
        tkelrow(i) = tkelpak(ioff+i)
        flglrow(i) = flglpak(ioff+i)
        fnlrow (i) = fnlpak (ioff+i)
        fsglrow(i) = fsglpak(ioff+i)
        hfclrow(i) = hfclpak(ioff+i)
        hfllrow(i) = hfllpak(ioff+i)
        hfslrow(i) = hfslpak(ioff+i)
        hmflrow(i) = hmflpak(ioff+i)
        pilrow (i) = pilpak (ioff+i)
        qflrow (i) = qflpak (ioff+i)
!
        do l=1,nlklm
          tlakrow(i,l) = tlakpak(ioff+i,l)
        end do
#endif
#if defined (flake)
        lshprow(i) = lshppak(ioff+i)
        ltavrow(i) = ltavpak(ioff+i)
        lticrow(i) = lticpak(ioff+i)
        ltmxrow(i) = ltmxpak(ioff+i)
        ltsnrow(i) = ltsnpak(ioff+i)
        ltwbrow(i) = ltwbpak(ioff+i)
        lzsnrow(i) = lzsnpak(ioff+i)
#endif
end do
!
!     * emission fields.
!
do i=il1,il2
  eostrow(i) = eostpak(ioff+i)
  eostrol(i) = eostpal(ioff+i)
  !
  escvrow(i) = escvpak(ioff+i)
  ehcvrow(i) = ehcvpak(ioff+i)
  esevrow(i) = esevpak(ioff+i)
  ehevrow(i) = ehevpak(ioff+i)
end do
#if defined transient_aerosol_emissions
      do l=1,levwf
      do i=il1,il2
        fbbcrow(i,l) = fbbcpak(ioff+i,l)
        fbbcrol(i,l) = fbbcpal(ioff+i,l)
      end do
      end do
      do l=1,levair
      do i=il1,il2
        fairrow(i,l) = fairpak(ioff+i,l)
        fairrol(i,l) = fairpal(ioff+i,l)
      end do
      end do
#if defined emists
      do i=il1,il2
        sairrow(i) = sairpak(ioff+i)
        ssfcrow(i) = ssfcpak(ioff+i)
        sbiorow(i) = sbiopak(ioff+i)
        sshirow(i) = sshipak(ioff+i)
        sstkrow(i) = sstkpak(ioff+i)
        sfirrow(i) = sfirpak(ioff+i)
        sairrol(i) = sairpal(ioff+i)
        ssfcrol(i) = ssfcpal(ioff+i)
        sbiorol(i) = sbiopal(ioff+i)
        sshirol(i) = sshipal(ioff+i)
        sstkrol(i) = sstkpal(ioff+i)
        sfirrol(i) = sfirpal(ioff+i)
        oairrow(i) = oairpak(ioff+i)
        osfcrow(i) = osfcpak(ioff+i)
        obiorow(i) = obiopak(ioff+i)
        oshirow(i) = oshipak(ioff+i)
        ostkrow(i) = ostkpak(ioff+i)
        ofirrow(i) = ofirpak(ioff+i)
        oairrol(i) = oairpal(ioff+i)
        osfcrol(i) = osfcpal(ioff+i)
        obiorol(i) = obiopal(ioff+i)
        oshirol(i) = oshipal(ioff+i)
        ostkrol(i) = ostkpal(ioff+i)
        ofirrol(i) = ofirpal(ioff+i)
        bairrow(i) = bairpak(ioff+i)
        bsfcrow(i) = bsfcpak(ioff+i)
        bbiorow(i) = bbiopak(ioff+i)
        bshirow(i) = bshipak(ioff+i)
        bstkrow(i) = bstkpak(ioff+i)
        bfirrow(i) = bfirpak(ioff+i)
        bairrol(i) = bairpal(ioff+i)
        bsfcrol(i) = bsfcpal(ioff+i)
        bbiorol(i) = bbiopal(ioff+i)
        bshirol(i) = bshipal(ioff+i)
        bstkrol(i) = bstkpal(ioff+i)
        bfirrol(i) = bfirpal(ioff+i)
      end do
#endif
#endif
#if (defined(pla) && defined(pfrc))
      do i=il1,il2
        bcdpfrow(i) = bcdpfpak(ioff+i)
        bcdpfrol(i) = bcdpfpal(ioff+i)
      end do
      do l=1,ilev
      do i=il1,il2
        amldfrow(i,l) = amldfpak(ioff+i,l)
        amldfrol(i,l) = amldfpal(ioff+i,l)
        reamfrow(i,l) = reamfpak(ioff+i,l)
        reamfrol(i,l) = reamfpal(ioff+i,l)
        veamfrow(i,l) = veamfpak(ioff+i,l)
        veamfrol(i,l) = veamfpal(ioff+i,l)
        fr1frow (i,l) = fr1fpak (ioff+i,l)
        fr1frol (i,l) = fr1fpal (ioff+i,l)
        fr2frow (i,l) = fr2fpak (ioff+i,l)
        fr2frol (i,l) = fr2fpal (ioff+i,l)
        ssldfrow(i,l) = ssldfpak(ioff+i,l)
        ssldfrol(i,l) = ssldfpal(ioff+i,l)
        ressfrow(i,l) = ressfpak(ioff+i,l)
        ressfrol(i,l) = ressfpal(ioff+i,l)
        vessfrow(i,l) = vessfpak(ioff+i,l)
        vessfrol(i,l) = vessfpal(ioff+i,l)
        dsldfrow(i,l) = dsldfpak(ioff+i,l)
        dsldfrol(i,l) = dsldfpal(ioff+i,l)
        redsfrow(i,l) = redsfpak(ioff+i,l)
        redsfrol(i,l) = redsfpal(ioff+i,l)
        vedsfrow(i,l) = vedsfpak(ioff+i,l)
        vedsfrol(i,l) = vedsfpal(ioff+i,l)
        bcldfrow(i,l) = bcldfpak(ioff+i,l)
        bcldfrol(i,l) = bcldfpal(ioff+i,l)
        rebcfrow(i,l) = rebcfpak(ioff+i,l)
        rebcfrol(i,l) = rebcfpal(ioff+i,l)
        vebcfrow(i,l) = vebcfpak(ioff+i,l)
        vebcfrol(i,l) = vebcfpal(ioff+i,l)
        ocldfrow(i,l) = ocldfpak(ioff+i,l)
        ocldfrol(i,l) = ocldfpal(ioff+i,l)
        reocfrow(i,l) = reocfpak(ioff+i,l)
        reocfrol(i,l) = reocfpal(ioff+i,l)
        veocfrow(i,l) = veocfpak(ioff+i,l)
        veocfrol(i,l) = veocfpal(ioff+i,l)
        zcdnfrow(i,l) = zcdnfpak(ioff+i,l)
        zcdnfrol(i,l) = zcdnfpal(ioff+i,l)
        bcicfrow(i,l) = bcicfpak(ioff+i,l)
        bcicfrol(i,l) = bcicfpal(ioff+i,l)
      end do
      end do
#endif
!
!     * tracer arrays.
!
do n=1,ntrac
  do i=il1,il2
    xfsrow (i,n) = xfspak (ioff+i,n)
    xsfxrol(i,n) = xsfxpal(ioff+i,n)
    xsfxrow(i,n) = xsfxpak(ioff+i,n)
    xsrfrow(i,n) = xsrfpak(ioff+i,n)
    xsrfrol(i,n) = xsrfpal(ioff+i,n)
    xtviros(i,n) = xtvipas(ioff+i,n)
    xtvirow(i,n) = xtvipak(ioff+i,n)
    xtvirom(i,n) = xtvipam(ioff+i,n)
    xtpfrom(i,n) = xtpfpam(ioff+i,n)
    xtphrom(i,n) = xtphpam(ioff+i,n)
    xtptrom(i,n) = xtptpam(ioff+i,n)
  end do
  do l=1,ilev
    do i=il1,il2
      xwf0rol(i,l,n) = xwf0pal(ioff+i,l,n)
      xwfmrol(i,l,n) = xwfmpal(ioff+i,l,n)
    end do
  end do
end do
!
do l=1,levox
  do i=il1,il2
    h2o2rol(i,l) = h2o2pal(ioff+i,l)
    h2o2row(i,l) = h2o2pak(ioff+i,l)
    hno3rol(i,l) = hno3pal(ioff+i,l)
    hno3row(i,l) = hno3pak(ioff+i,l)
    nh3rol (i,l) = nh3pal (ioff+i,l)
    nh3row (i,l) = nh3pak (ioff+i,l)
    nh4rol (i,l) = nh4pal (ioff+i,l)
    nh4row (i,l) = nh4pak (ioff+i,l)
    no3rol (i,l) = no3pal (ioff+i,l)
    no3row (i,l) = no3pak (ioff+i,l)
    o3rol  (i,l) = o3pal  (ioff+i,l)
    o3row  (i,l) = o3pak  (ioff+i,l)
    ohrol  (i,l) = ohpal  (ioff+i,l)
    ohrow  (i,l) = ohpak  (ioff+i,l)
  end do
end do 

do l=1,ilev
  do i=il1,il2
    tbndrol(i,l) = tbndpal(ioff+i,l)
    tbndrow(i,l) = tbndpak(ioff+i,l)
    ea55rol(i,l) = ea55pal(ioff+i,l)
    ea55row(i,l) = ea55pak(ioff+i,l)
    rkmrow (i,l) = rkmpak (ioff+i,l)
    rkhrow (i,l) = rkhpak (ioff+i,l)
    rkqrow (i,l) = rkqpak (ioff+i,l)
  end do
end do
!
do l = 1, 4
  do i=il1,il2
    iseedrow(i,l) = iseedpak(ioff+i,l)
  end do ! i
end do ! l
!
do i=il1,il2
  spotrow(i) = spotpak(ioff+i)
  st01row(i) = st01pak(ioff+i)
  st02row(i) = st02pak(ioff+i)
  st03row(i) = st03pak(ioff+i)
  st04row(i) = st04pak(ioff+i)
  st06row(i) = st06pak(ioff+i)
  st13row(i) = st13pak(ioff+i)
  st14row(i) = st14pak(ioff+i)
  st15row(i) = st15pak(ioff+i)
  st16row(i) = st16pak(ioff+i)
  st17row(i) = st17pak(ioff+i)
  suz0row(i) = suz0pak(ioff+i)
  suz0rol(i) = suz0pal(ioff+i)
  pdsfrow(i) = pdsfpak(ioff+i)
  pdsfrol(i) = pdsfpal(ioff+i)
end do
!
!     * conditional arrays.
!
#if defined rad_flux_profs
      do l = 1, ilev+2
         do i = il1, il2
            fsaurow(i,l) = fsaupak(ioff+i,l)
            fsadrow(i,l) = fsadpak(ioff+i,l)
            flaurow(i,l) = flaupak(ioff+i,l)
            fladrow(i,l) = fladpak(ioff+i,l)
            fscurow(i,l) = fscupak(ioff+i,l)
            fscdrow(i,l) = fscdpak(ioff+i,l)
            flcurow(i,l) = flcupak(ioff+i,l)
            flcdrow(i,l) = flcdpak(ioff+i,l)
         end do ! i
      end do ! l
#endif
do l = 1, ilev+2
  do i = il1, il2
    fsaurol(i,l) = fsaupal(ioff+i,l)
    fsadrol(i,l) = fsadpal(ioff+i,l)
    flaurol(i,l) = flaupal(ioff+i,l)
    fladrol(i,l) = fladpal(ioff+i,l)
    fscurol(i,l) = fscupal(ioff+i,l)
    fscdrol(i,l) = fscdpal(ioff+i,l)
    flcurol(i,l) = flcupal(ioff+i,l)
    flcdrol(i,l) = flcdpal(ioff+i,l)
  end do ! i
end do ! l

#if defined radforce

      !--- upward and downward all-sky and clear-sky fluxes
      fsaurow_r(il1:il2,1:ilev+2,1:nrfp) = &
           fsaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fsadrow_r(il1:il2,1:ilev+2,1:nrfp) = &
           fsadpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flaurow_r(il1:il2,1:ilev+2,1:nrfp) = &
           flaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fladrow_r(il1:il2,1:ilev+2,1:nrfp) = &
           fladpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fscurow_r(il1:il2,1:ilev+2,1:nrfp) = &
           fscupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fscdrow_r(il1:il2,1:ilev+2,1:nrfp) = &
           fscdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flcurow_r(il1:il2,1:ilev+2,1:nrfp) = &
           flcupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flcdrow_r(il1:il2,1:ilev+2,1:nrfp) = &
           flcdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)

      fsaurol_r(il1:il2,1:ilev+2,1:nrfp) = &
           fsaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fsadrol_r(il1:il2,1:ilev+2,1:nrfp) = &
           fsadpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flaurol_r(il1:il2,1:ilev+2,1:nrfp) = &
           flaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fladrol_r(il1:il2,1:ilev+2,1:nrfp) = &
           fladpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fscurol_r(il1:il2,1:ilev+2,1:nrfp) = &
           fscupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      fscdrol_r(il1:il2,1:ilev+2,1:nrfp) = &
           fscdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flcurol_r(il1:il2,1:ilev+2,1:nrfp) = &
           flcupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)
      flcdrol_r(il1:il2,1:ilev+2,1:nrfp) = &
           flcdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp)

      !--- temperature perturbation (t_* - t_o) for each perturbation
      rdtrow_r (il1:il2,1:ilev,1:nrfp) = &
           rdtpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)
      rdtrol_r (il1:il2,1:ilev,1:nrfp) = &
           rdtpal_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)
      rdtrom_r (il1:il2,1:ilev,1:nrfp) = &
           rdtpam_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)

      !--- heating rates for each perturbation
      hrsrow_r (il1:il2,1:ilev,1:nrfp) = &
           hrspak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)
      hrlrow_r (il1:il2,1:ilev,1:nrfp) = &
           hrlpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)

      hrscrow_r (il1:il2,1:ilev,1:nrfp) = &
           hrscpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)
      hrlcrow_r (il1:il2,1:ilev,1:nrfp) = &
           hrlcpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp)

      !--- surface fluxes from secondary radiation calls
      csbrol_r(il1:il2,1:nrfp)  = csbpal_r(ioff+il1:ioff+il2,1:nrfp)
      fsgrol_r(il1:il2,1:nrfp)  = fsgpal_r(ioff+il1:ioff+il2,1:nrfp)
      fssrol_r(il1:il2,1:nrfp)  = fsspal_r(ioff+il1:ioff+il2,1:nrfp)
      fsscrol_r(il1:il2,1:nrfp) = fsscpal_r(ioff+il1:ioff+il2,1:nrfp)
      clbrol_r(il1:il2,1:nrfp)  = clbpal_r(ioff+il1:ioff+il2,1:nrfp)
      flgrol_r(il1:il2,1:nrfp)  = flgpal_r(ioff+il1:ioff+il2,1:nrfp)
      fdlrol_r(il1:il2,1:nrfp)  = fdlpal_r(ioff+il1:ioff+il2,1:nrfp)
      fdlcrol_r(il1:il2,1:nrfp) = fdlcpal_r(ioff+il1:ioff+il2,1:nrfp)
      fsrrol_r(il1:il2,1:nrfp)  = fsrpal_r(ioff+il1:ioff+il2,1:nrfp)
      fsrcrol_r(il1:il2,1:nrfp) = fsrcpal_r(ioff+il1:ioff+il2,1:nrfp)
      fsorol_r(il1:il2,1:nrfp)  = fsopal_r(ioff+il1:ioff+il2,1:nrfp)
      olrrol_r(il1:il2,1:nrfp)  = olrpal_r(ioff+il1:ioff+il2,1:nrfp)
      olrcrol_r(il1:il2,1:nrfp) = olrcpal_r(ioff+il1:ioff+il2,1:nrfp)
      fslorol_r(il1:il2,1:nrfp) = fslopal_r(ioff+il1:ioff+il2,1:nrfp)

      do m = 1, im
       csbrot_r(il1:il2,m,1:nrfp) = csbpat_r(ioff+il1:ioff+il2,m,1:nrfp)
       clbrot_r(il1:il2,m,1:nrfp) = clbpat_r(ioff+il1:ioff+il2,m,1:nrfp)
       fsgrot_r(il1:il2,m,1:nrfp) = fsgpat_r(ioff+il1:ioff+il2,m,1:nrfp)
       flgrot_r(il1:il2,m,1:nrfp) = flgpat_r(ioff+il1:ioff+il2,m,1:nrfp)
      end do ! m

      !--- vertical level index of tropopause height
      kthrol(il1:il2) = kthpal(ioff+il1:ioff+il2)
#endif
#if defined qconsav
      do i=il1,il2
          qaddrow(i,1) = qaddpak(ioff+i,1)
          qaddrow(i,2) = qaddpak(ioff+i,2)
          qtphrow(i,1) = qtphpak(ioff+i,1)
          qtphrow(i,2) = qtphpak(ioff+i,2)
      end do
#endif
!
do l=1,ilev
  do i=il1,il2
#if defined tprhs
          ttprow (i,l) = ttppak (ioff+i,l)
#endif
#if defined qprhs
          qtprow (i,l) = qtppak (ioff+i,l)
#endif
#if defined uprhs
          utprow (i,l) = utppak (ioff+i,l)
#endif
#if defined vrphs
          vtprow (i,l) = vtppak (ioff+i,l)
#endif
#if defined qconsav
          qtpfrow(i,l) = qtpfpak(ioff+i,l)
#endif
#if defined tprhsc
          ttpcrow(i,l) = ttpcpak(ioff+i,l)
          ttpkrow(i,l) = ttpkpak(ioff+i,l)
          ttpmrow(i,l) = ttpmpak(ioff+i,l)
          ttpnrow(i,l) = ttpnpak(ioff+i,l)
          ttpprow(i,l) = ttpppak(ioff+i,l)
          ttpvrow(i,l) = ttpvpak(ioff+i,l)
#endif
#if (defined (tprhsc) || defined (radforce))
          ttplrow(i,l) = ttplpak(ioff+i,l)
          ttpsrow(i,l) = ttpspak(ioff+i,l)
          ttscrow(i,l) = ttscpak(ioff+i,l)
          ttlcrow(i,l) = ttlcpak(ioff+i,l)
#endif
#if defined qprhsc
          qtpcrow(i,l) = qtpcpak(ioff+i,l)
          qtpmrow(i,l) = qtpmpak(ioff+i,l)
          qtpprow(i,l) = qtpppak(ioff+i,l)
          qtpvrow(i,l) = qtpvpak(ioff+i,l)
#endif
#if defined uprhsc
          utpcrow(i,l) = utpcpak(ioff+i,l)
          utpgrow(i,l) = utpgpak(ioff+i,l)
          utpnrow(i,l) = utpnpak(ioff+i,l)
          utpsrow(i,l) = utpspak(ioff+i,l)
          utpvrow(i,l) = utpvpak(ioff+i,l)
#endif
#if defined vrphsc
          vtpcrow(i,l) = vtpcpak(ioff+i,l)
          vtpgrow(i,l) = vtpgpak(ioff+i,l)
          vtpnrow(i,l) = vtpnpak(ioff+i,l)
          vtpsrow(i,l) = vtpspak(ioff+i,l)
          vtpvrow(i,l) = vtpvpak(ioff+i,l)
#endif
  end do
end do
!
do n=1,ntrac
#if defined xconsav
        do i=il1,il2
            xaddrow(i,1,n) = xaddpak(ioff+i,1,n)
            xaddrow(i,2,n) = xaddpak(ioff+i,2,n)
            xtphrow(i,1,n) = xtphpak(ioff+i,1,n)
            xtphrow(i,2,n) = xtphpak(ioff+i,2,n)
        end do
#endif
  do l=1,ilev
    do i=il1,il2
#if defined xprhs
            xtprow (i,l,n) = xtppak (ioff+i,l,n)
#endif
#if defined xconsav
            xtpfrow(i,l,n) = xtpfpak(ioff+i,l,n)
#endif
#if defined xprhsc
            xtpcrow(i,l,n) = xtpcpak(ioff+i,l,n)
            xtpmrow(i,l,n) = xtpmpak(ioff+i,l,n)
            xtpprow(i,l,n) = xtpppak(ioff+i,l,n)
            xtpvrow(i,l,n) = xtpvpak(ioff+i,l,n)
#endif
#if defined x01
            x01row (i,l,n) = x01pak (ioff+i,l,n)
#endif
#if defined x02
            x02row (i,l,n) = x02pak (ioff+i,l,n)
#endif
#if defined x03
            x03row (i,l,n) = x03pak (ioff+i,l,n)
#endif
#if defined x04
            x04row (i,l,n) = x04pak (ioff+i,l,n)
#endif
#if defined x05
            x05row (i,l,n) = x05pak (ioff+i,l,n)
#endif
#if defined x06
            x06row (i,l,n) = x06pak (ioff+i,l,n)
#endif
#if defined x07
            x07row (i,l,n) = x07pak (ioff+i,l,n)
#endif
#if defined x08
            x08row (i,l,n) = x08pak (ioff+i,l,n)
#endif
#if defined x09
            x09row (i,l,n) = x09pak (ioff+i,l,n)
#endif
    end do
  end do
end do
#if defined xtraconv
      do i=il1,il2
        bcdrow (i) = bcdpak (ioff+i)
        bcsrow (i) = bcspak (ioff+i)
        caperow(i) = capepak(ioff+i)
        cdcbrow(i) = cdcbpak(ioff+i)
        cinhrow(i) = cinhpak(ioff+i)
        cscbrow(i) = cscbpak(ioff+i)
        tcdrow (i) = tcdpak (ioff+i)
        tcsrow (i) = tcspak (ioff+i)
      end do
!
      do l=1,ilev
      do i=il1,il2
          dmcrow (i,l) = dmcpak (ioff+i,l)
          smcrow (i,l) = smcpak (ioff+i,l)
          dmcdrow(i,l) = dmcdpak(ioff+i,l)
          dmcurow(i,l) = dmcupak(ioff+i,l)
      end do
      end do
#endif
#if defined xtrachem
      do i=il1,il2
        dd4row (i) = dd4pak (ioff+i)
        dox4row(i) = dox4pak(ioff+i)
        doxdrow(i) = doxdpak(ioff+i)
        esdrow (i) = esdpak (ioff+i)
        esfsrow(i) = esfspak(ioff+i)
        eaisrow(i) = eaispak(ioff+i)
        estsrow(i) = estspak(ioff+i)
        efisrow(i) = efispak(ioff+i)
        esfbrow(i) = esfbpak(ioff+i)
        eaibrow(i) = eaibpak(ioff+i)
        estbrow(i) = estbpak(ioff+i)
        efibrow(i) = efibpak(ioff+i)
        esforow(i) = esfopak(ioff+i)
        eaiorow(i) = eaiopak(ioff+i)
        estorow(i) = estopak(ioff+i)
        efiorow(i) = efiopak(ioff+i)
        edslrow(i) = edslpak(ioff+i)
        edsorow(i) = edsopak(ioff+i)
        esvcrow(i) = esvcpak(ioff+i)
        esverow(i) = esvepak(ioff+i)
        noxdrow(i) = noxdpak(ioff+i)
        wdd4row(i) = wdd4pak(ioff+i)
        wdl4row(i) = wdl4pak(ioff+i)
        wds4row(i) = wds4pak(ioff+i)
      end do
#ifndef pla
      do i=il1,il2
        dddrow (i) = dddpak (ioff+i)
        ddbrow (i) = ddbpak (ioff+i)
        ddorow (i) = ddopak (ioff+i)
        ddsrow (i) = ddspak (ioff+i)
        wdldrow(i) = wdldpak(ioff+i)
        wdlbrow(i) = wdlbpak(ioff+i)
        wdlorow(i) = wdlopak(ioff+i)
        wdlsrow(i) = wdlspak(ioff+i)
        wdddrow(i) = wdddpak(ioff+i)
        wddbrow(i) = wddbpak(ioff+i)
        wddorow(i) = wddopak(ioff+i)
        wddsrow(i) = wddspak(ioff+i)
        wdsdrow(i) = wdsdpak(ioff+i)
        wdsbrow(i) = wdsbpak(ioff+i)
        wdsorow(i) = wdsopak(ioff+i)
        wdssrow(i) = wdsspak(ioff+i)
        dd6row (i) = dd6pak (ioff+i)
        sdhprow(i) = sdhppak(ioff+i)
        sdo3row(i) = sdo3pak(ioff+i)
        slhprow(i) = slhppak(ioff+i)
        slo3row(i) = slo3pak(ioff+i)
        sshprow(i) = sshppak(ioff+i)
        sso3row(i) = sso3pak(ioff+i)
        wdd6row(i) = wdd6pak(ioff+i)
        wdl6row(i) = wdl6pak(ioff+i)
        wds6row(i) = wds6pak(ioff+i)
      end do
!
      do l=1,ilev
        do i=il1,il2
           phdrow (i,l) = phdpak (ioff+i,l)
           phlrow (i,l) = phlpak (ioff+i,l)
           phsrow (i,l) = phspak (ioff+i,l)
        end do
      end do
#endif
#endif
#if defined (xtradust)
      do i=il1,il2
        duwdrow(i) = duwdpak(ioff+i)
        dustrow(i) = dustpak(ioff+i)
        duthrow(i) = duthpak(ioff+i)
        usmkrow(i) = usmkpak(ioff+i)
        fallrow(i) = fallpak(ioff+i)
        fa10row(i) = fa10pak(ioff+i)
         fa2row(i) =  fa2pak(ioff+i)
         fa1row(i) =  fa1pak(ioff+i)
        gustrow(i) = gustpak(ioff+i)
        zspdrow(i) = zspdpak(ioff+i)
        vgfrrow(i) = vgfrpak(ioff+i)
        smfrrow(i) = smfrpak(ioff+i)
      end do
#endif
#if (defined(pla) && defined(pam))
      do l=1,ilev
      do i=il1,il2
        ssldrow(i,l)   = ssldpak(ioff+i,l)
        ressrow(i,l)   = resspak(ioff+i,l)
        vessrow(i,l)   = vesspak(ioff+i,l)
        dsldrow(i,l)   = dsldpak(ioff+i,l)
        redsrow(i,l)   = redspak(ioff+i,l)
        vedsrow(i,l)   = vedspak(ioff+i,l)
        bcldrow(i,l)   = bcldpak(ioff+i,l)
        rebcrow(i,l)   = rebcpak(ioff+i,l)
        vebcrow(i,l)   = vebcpak(ioff+i,l)
        ocldrow(i,l)   = ocldpak(ioff+i,l)
        reocrow(i,l)   = reocpak(ioff+i,l)
        veocrow(i,l)   = veocpak(ioff+i,l)
        amldrow(i,l)   = amldpak(ioff+i,l)
        reamrow(i,l)   = reampak(ioff+i,l)
        veamrow(i,l)   = veampak(ioff+i,l)
        fr1row (i,l)   = fr1pak (ioff+i,l)
        fr2row (i,l)   = fr2pak (ioff+i,l)
        zcdnrow(i,l)   = zcdnpak(ioff+i,l)
        bcicrow(i,l)   = bcicpak(ioff+i,l)
        oednrow(i,l,:) = oednpak(ioff+i,l,:)
        oercrow(i,l,:) = oercpak(ioff+i,l,:)
        oidnrow(i,l)   = oidnpak(ioff+i,l)
        oircrow(i,l)   = oircpak(ioff+i,l)
        svvbrow(i,l)   = svvbpak(ioff+i,l)
        psvvrow(i,l)   = psvvpak(ioff+i,l)
        svmbrow(i,l,:) = svmbpak(ioff+i,l,:)
        svcbrow(i,l,:) = svcbpak(ioff+i,l,:)
        pnvbrow(i,l,:)  =pnvbpak(ioff+i,l,:)
        pnmbrow(i,l,:,:)=pnmbpak(ioff+i,l,:,:)
        pncbrow(i,l,:,:)=pncbpak(ioff+i,l,:,:)
        psvbrow(i,l,:,:)  =psvbpak(ioff+i,l,:,:)
        psmbrow(i,l,:,:,:)=psmbpak(ioff+i,l,:,:,:)
        pscbrow(i,l,:,:,:)=pscbpak(ioff+i,l,:,:,:)
        qnvbrow(i,l,:)  =qnvbpak(ioff+i,l,:)
        qnmbrow(i,l,:,:)=qnmbpak(ioff+i,l,:,:)
        qncbrow(i,l,:,:)=qncbpak(ioff+i,l,:,:)
        qsvbrow(i,l,:,:)  =qsvbpak(ioff+i,l,:,:)
        qsmbrow(i,l,:,:,:)=qsmbpak(ioff+i,l,:,:,:)
        qscbrow(i,l,:,:,:)=qscbpak(ioff+i,l,:,:,:)
        qgvbrow(i,l)   = qgvbpak(ioff+i,l)
        qgmbrow(i,l,:) = qgmbpak(ioff+i,l,:)
        qgcbrow(i,l,:) = qgcbpak(ioff+i,l,:)
        qdvbrow(i,l)   = qdvbpak(ioff+i,l)
        qdmbrow(i,l,:) = qdmbpak(ioff+i,l,:)
        qdcbrow(i,l,:) = qdcbpak(ioff+i,l,:)
        onvbrow(i,l,:)  =onvbpak(ioff+i,l,:)
        onmbrow(i,l,:,:)=onmbpak(ioff+i,l,:,:)
        oncbrow(i,l,:,:)=oncbpak(ioff+i,l,:,:)
        osvbrow(i,l,:,:)  =osvbpak(ioff+i,l,:,:)
        osmbrow(i,l,:,:,:)=osmbpak(ioff+i,l,:,:,:)
        oscbrow(i,l,:,:,:)=oscbpak(ioff+i,l,:,:,:)
        ogvbrow(i,l)   = ogvbpak(ioff+i,l)
        ogmbrow(i,l,:) = ogmbpak(ioff+i,l,:)
        ogcbrow(i,l,:) = ogcbpak(ioff+i,l,:)
        devbrow(i,l,:) = devbpak(ioff+i,l,:)
        pdevrow(i,l,:) = pdevpak(ioff+i,l,:)
        dembrow(i,l,:,:)=dembpak(ioff+i,l,:,:)
        decbrow(i,l,:,:)=decbpak(ioff+i,l,:,:)
        divbrow(i,l)   = divbpak(ioff+i,l)
        pdivrow(i,l)   = pdivpak(ioff+i,l)
        dimbrow(i,l,:) = dimbpak(ioff+i,l,:)
        dicbrow(i,l,:) = dicbpak(ioff+i,l,:)
        revbrow(i,l,:) = revbpak(ioff+i,l,:)
        prevrow(i,l,:) = prevpak(ioff+i,l,:)
        rembrow(i,l,:,:)=rembpak(ioff+i,l,:,:)
        recbrow(i,l,:,:)=recbpak(ioff+i,l,:,:)
        rivbrow(i,l)   = rivbpak(ioff+i,l)
        privrow(i,l)   = privpak(ioff+i,l)
        rimbrow(i,l,:) = rimbpak(ioff+i,l,:)
        ricbrow(i,l,:) = ricbpak(ioff+i,l,:)
        sulirow(i,l)   = sulipak(ioff+i,l)
        rsuirow(i,l)   = rsuipak(ioff+i,l)
        vsuirow(i,l)   = vsuipak(ioff+i,l)
        f1surow(i,l)   = f1supak(ioff+i,l)
        f2surow(i,l)   = f2supak(ioff+i,l)
        bclirow(i,l)   = bclipak(ioff+i,l)
        rbcirow(i,l)   = rbcipak(ioff+i,l)
        vbcirow(i,l)   = vbcipak(ioff+i,l)
        f1bcrow(i,l)   = f1bcpak(ioff+i,l)
        f2bcrow(i,l)   = f2bcpak(ioff+i,l)
        oclirow(i,l)   = oclipak(ioff+i,l)
        rocirow(i,l)   = rocipak(ioff+i,l)
        vocirow(i,l)   = vocipak(ioff+i,l)
        f1ocrow(i,l)   = f1ocpak(ioff+i,l)
        f2ocrow(i,l)   = f2ocpak(ioff+i,l)
      end do
      end do
#if defined (xtrapla1)
      do l=1,ilev
      do i=il1,il2
        sncnrow(i,l) = sncnpak(ioff+i,l)
        ssunrow(i,l) = ssunpak(ioff+i,l)
        scndrow(i,l) = scndpak(ioff+i,l)
        sgsprow(i,l) = sgsppak(ioff+i,l)
        ccnrow (i,l) = ccnpak (ioff+i,l)
        cc02row(i,l) = cc02pak(ioff+i,l)
        cc04row(i,l) = cc04pak(ioff+i,l)
        cc08row(i,l) = cc08pak(ioff+i,l)
        cc16row(i,l) = cc16pak(ioff+i,l)
        ccnerow(i,l) = ccnepak(ioff+i,l)
        acasrow(i,l) = acaspak(ioff+i,l)
        acoarow(i,l) = acoapak(ioff+i,l)
        acbcrow(i,l) = acbcpak(ioff+i,l)
        acssrow(i,l) = acsspak(ioff+i,l)
        acmdrow(i,l) = acmdpak(ioff+i,l)
        ntrow  (i,l) = ntpak(ioff+i,l)
        n20row (i,l) = n20pak(ioff+i,l)
        n50row (i,l) = n50pak(ioff+i,l)
        n100row(i,l) = n100pak(ioff+i,l)
        n200row(i,l) = n200pak(ioff+i,l)
        wtrow  (i,l) = wtpak(ioff+i,l)
        w20row (i,l) = w20pak(ioff+i,l)
        w50row (i,l) = w50pak(ioff+i,l)
        w100row(i,l) = w100pak(ioff+i,l)
        w200row(i,l) = w200pak(ioff+i,l)
        rcrirow(i,l) = rcripak(ioff+i,l)
        supsrow(i,l) = supspak(ioff+i,l)
        henrrow(i,l) = henrpak(ioff+i,l)
        o3frrow(i,l) = o3frpak(ioff+i,l)
        h2o2frrow(i,l) = h2o2frpak(ioff+i,l)
        wparrow(i,l) = wparpak(ioff+i,l)
        pm25row(i,l) = pm25pak(ioff+i,l)
        pm10row(i,l) = pm10pak(ioff+i,l)
        dm25row(i,l) = dm25pak(ioff+i,l)
        dm10row(i,l) = dm10pak(ioff+i,l)
      end do
      end do
      do i=il1,il2
        vncnrow(i) = vncnpak(ioff+i)
        vasnrow(i) = vasnpak(ioff+i)
        vscdrow(i) = vscdpak(ioff+i)
        vgsprow(i) = vgsppak(ioff+i)
        voaerow(i) = voaepak(ioff+i)
        vbcerow(i) = vbcepak(ioff+i)
        vaserow(i) = vasepak(ioff+i)
        vmderow(i) = vmdepak(ioff+i)
        vsserow(i) = vssepak(ioff+i)
        voawrow(i) = voawpak(ioff+i)
        vbcwrow(i) = vbcwpak(ioff+i)
        vaswrow(i) = vaswpak(ioff+i)
        vmdwrow(i) = vmdwpak(ioff+i)
        vsswrow(i) = vsswpak(ioff+i)
        voadrow(i) = voadpak(ioff+i)
        vbcdrow(i) = vbcdpak(ioff+i)
        vasdrow(i) = vasdpak(ioff+i)
        vmddrow(i) = vmddpak(ioff+i)
        vssdrow(i) = vssdpak(ioff+i)
        voagrow(i) = voagpak(ioff+i)
        vbcgrow(i) = vbcgpak(ioff+i)
        vasgrow(i) = vasgpak(ioff+i)
        vmdgrow(i) = vmdgpak(ioff+i)
        vssgrow(i) = vssgpak(ioff+i)
        voacrow(i) = voacpak(ioff+i)
        vbccrow(i) = vbccpak(ioff+i)
        vascrow(i) = vascpak(ioff+i)
        vmdcrow(i) = vmdcpak(ioff+i)
        vsscrow(i) = vsscpak(ioff+i)
        vasirow(i) = vasipak(ioff+i)
        vas1row(i) = vas1pak(ioff+i)
        vas2row(i) = vas2pak(ioff+i)
        vas3row(i) = vas3pak(ioff+i)
        vccnrow(i) = vccnpak(ioff+i)
        vcnerow(i) = vcnepak(ioff+i)
      end do
#endif
#if defined (xtrapla2)
      do l=1,ilev
      do i=il1,il2
        cornrow(i,l) = cornpak(ioff+i,l)
        cormrow(i,l) = cormpak(ioff+i,l)
        rsn1row(i,l) = rsn1pak(ioff+i,l)
        rsm1row(i,l) = rsm1pak(ioff+i,l)
        rsn2row(i,l) = rsn2pak(ioff+i,l)
        rsm2row(i,l) = rsm2pak(ioff+i,l)
        rsn3row(i,l) = rsn3pak(ioff+i,l)
        rsm3row(i,l) = rsm3pak(ioff+i,l)
        do isf=1,isdnum
          sdnurow(i,l,isf) = sdnupak(ioff+i,l,isf)
          sdmarow(i,l,isf) = sdmapak(ioff+i,l,isf)
          sdacrow(i,l,isf) = sdacpak(ioff+i,l,isf)
          sdcorow(i,l,isf) = sdcopak(ioff+i,l,isf)
          sssnrow(i,l,isf) = sssnpak(ioff+i,l,isf)
          smdnrow(i,l,isf) = smdnpak(ioff+i,l,isf)
          sianrow(i,l,isf) = sianpak(ioff+i,l,isf)
          sssmrow(i,l,isf) = sssmpak(ioff+i,l,isf)
          smdmrow(i,l,isf) = smdmpak(ioff+i,l,isf)
          sewmrow(i,l,isf) = sewmpak(ioff+i,l,isf)
          ssumrow(i,l,isf) = ssumpak(ioff+i,l,isf)
          socmrow(i,l,isf) = socmpak(ioff+i,l,isf)
          sbcmrow(i,l,isf) = sbcmpak(ioff+i,l,isf)
          siwmrow(i,l,isf) = siwmpak(ioff+i,l,isf)
        end do
      end do
      end do
      do i=il1,il2
        do isf=1,isdiag
          sdvlrow(i,isf) = sdvlpak(ioff+i,isf)
        end do
        vrn1row(i) = vrn1pak(ioff+i)
        vrm1row(i) = vrm1pak(ioff+i)
        vrn2row(i) = vrn2pak(ioff+i)
        vrm2row(i) = vrm2pak(ioff+i)
        vrn3row(i) = vrn3pak(ioff+i)
        vrm3row(i) = vrm3pak(ioff+i)
      end do
      do i=il1,il2
        defarow(i) = defapak(ioff+i)
        defcrow(i) = defcpak(ioff+i)
        do isf=1,isdust
          dmacrow(i,isf) = dmacpak(ioff+i,isf)
          dmcorow(i,isf) = dmcopak(ioff+i,isf)
          dnacrow(i,isf) = dnacpak(ioff+i,isf)
          dncorow(i,isf) = dncopak(ioff+i,isf)
        end do
        do isf=1,isdiag
          defxrow(i,isf) = defxpak(ioff+i,isf)
          defnrow(i,isf) = defnpak(ioff+i,isf)
        end do
      end do
      do i=il1,il2
        tnssrow(i) = tnsspak(ioff+i)
        tnmdrow(i) = tnmdpak(ioff+i)
        tniarow(i) = tniapak(ioff+i)
        tmssrow(i) = tmsspak(ioff+i)
        tmmdrow(i) = tmmdpak(ioff+i)
        tmocrow(i) = tmocpak(ioff+i)
        tmbcrow(i) = tmbcpak(ioff+i)
        tmsprow(i) = tmsppak(ioff+i)
        do n=1,isdnum
          snssrow(i,n) = snsspak(ioff+i,n)
          snmdrow(i,n) = snmdpak(ioff+i,n)
          sniarow(i,n) = sniapak(ioff+i,n)
          smssrow(i,n) = smsspak(ioff+i,n)
          smmdrow(i,n) = smmdpak(ioff+i,n)
          smocrow(i,n) = smocpak(ioff+i,n)
          smbcrow(i,n) = smbcpak(ioff+i,n)
          smsprow(i,n) = smsppak(ioff+i,n)
          siwhrow(i,n) = siwhpak(ioff+i,n)
          sewhrow(i,n) = sewhpak(ioff+i,n)
        end do
      end do
#endif
#endif
#if defined xtrals
      do l=1,ilev
      do i=il1,il2
        aggrow (i,l) = aggpak (ioff+i,l)
        autrow (i,l) = autpak (ioff+i,l)
        cndrow (i,l) = cndpak (ioff+i,l)
        deprow (i,l) = deppak (ioff+i,l)
        evprow (i,l) = evppak (ioff+i,l)
        frhrow (i,l) = frhpak (ioff+i,l)
        frkrow (i,l) = frkpak (ioff+i,l)
        frsrow (i,l) = frspak (ioff+i,l)
        mltirow(i,l) = mltipak(ioff+i,l)
        mltsrow(i,l) = mltspak(ioff+i,l)
        raclrow(i,l) = raclpak(ioff+i,l)
        rainrow(i,l) = rainpak(ioff+i,l)
        sacirow(i,l) = sacipak(ioff+i,l)
        saclrow(i,l) = saclpak(ioff+i,l)
        snowrow(i,l) = snowpak(ioff+i,l)
        subrow (i,l) = subpak (ioff+i,l)
        sedirow(i,l) = sedipak(ioff+i,l)
        rliqrow(i,l) = rliqpak(ioff+i,l)
        ricerow(i,l) = ricepak(ioff+i,l)
        cliqrow(i,l) = cliqpak(ioff+i,l)
        cicerow(i,l) = cicepak(ioff+i,l)
        rlncrow(i,l) = rlncpak(ioff+i,l)

        rliqrol(i,l) = rliqpal(ioff+i,l)
        ricerol(i,l) = ricepal(ioff+i,l)
        cliqrol(i,l) = cliqpal(ioff+i,l)
        cicerol(i,l) = cicepal(ioff+i,l)
        rlncrol(i,l) = rlncpal(ioff+i,l)

      end do
      end do
!
      do i=il1,il2
        vaggrow(i) = vaggpak(ioff+i)
        vautrow(i) = vautpak(ioff+i)
        vcndrow(i) = vcndpak(ioff+i)
        vdeprow(i) = vdeppak(ioff+i)
        vevprow(i) = vevppak(ioff+i)
        vfrhrow(i) = vfrhpak(ioff+i)
        vfrkrow(i) = vfrkpak(ioff+i)
        vfrsrow(i) = vfrspak(ioff+i)
        vmlirow(i) = vmlipak(ioff+i)
        vmlsrow(i) = vmlspak(ioff+i)
        vrclrow(i) = vrclpak(ioff+i)
        vscirow(i) = vscipak(ioff+i)
        vsclrow(i) = vsclpak(ioff+i)
        vsubrow(i) = vsubpak(ioff+i)
        vsedirow(i)  = vsedipak(ioff+i)
        reliqrow(i)  = reliqpak(ioff+i)
        reicerow(i)  = reicepak(ioff+i)
        cldliqrow(i) = cldliqpak(ioff+i)
        cldicerow(i) = cldicepak(ioff+i)
        ctlncrow(i)  = ctlncpak(ioff+i)
        cllncrow(i)  = cllncpak(ioff+i)

        reliqrol(i)  = reliqpal(ioff+i)
        reicerol(i)  = reicepal(ioff+i)
        cldliqrol(i) = cldliqpal(ioff+i)
        cldicerol(i) = cldicepal(ioff+i)
        ctlncrol(i)  = ctlncpal(ioff+i)
        cllncrol(i)  = cllncpal(ioff+i)
      end do
#endif
#if defined (aodpth)
       do i=il1,il2
!
        exb1row(i) = exb1pak(ioff+i)
        exb2row(i) = exb2pak(ioff+i)
        exb3row(i) = exb3pak(ioff+i)
        exb4row(i) = exb4pak(ioff+i)
        exb5row(i) = exb5pak(ioff+i)
        exbtrow(i) = exbtpak(ioff+i)
        odb1row(i) = odb1pak(ioff+i)
        odb2row(i) = odb2pak(ioff+i)
        odb3row(i) = odb3pak(ioff+i)
        odb4row(i) = odb4pak(ioff+i)
        odb5row(i) = odb5pak(ioff+i)
        odbtrow(i) = odbtpak(ioff+i)
        odbvrow(i) = odbvpak(ioff+i)
        ofb1row(i) = ofb1pak(ioff+i)
        ofb2row(i) = ofb2pak(ioff+i)
        ofb3row(i) = ofb3pak(ioff+i)
        ofb4row(i) = ofb4pak(ioff+i)
        ofb5row(i) = ofb5pak(ioff+i)
        ofbtrow(i) = ofbtpak(ioff+i)
        abb1row(i) = abb1pak(ioff+i)
        abb2row(i) = abb2pak(ioff+i)
        abb3row(i) = abb3pak(ioff+i)
        abb4row(i) = abb4pak(ioff+i)
        abb5row(i) = abb5pak(ioff+i)
        abbtrow(i) = abbtpak(ioff+i)
!
        exb1rol(i) = exb1pal(ioff+i)
        exb2rol(i) = exb2pal(ioff+i)
        exb3rol(i) = exb3pal(ioff+i)
        exb4rol(i) = exb4pal(ioff+i)
        exb5rol(i) = exb5pal(ioff+i)
        exbtrol(i) = exbtpal(ioff+i)
        odb1rol(i) = odb1pal(ioff+i)
        odb2rol(i) = odb2pal(ioff+i)
        odb3rol(i) = odb3pal(ioff+i)
        odb4rol(i) = odb4pal(ioff+i)
        odb5rol(i) = odb5pal(ioff+i)
        odbtrol(i) = odbtpal(ioff+i)
        odbvrol(i) = odbvpal(ioff+i)
        ofb1rol(i) = ofb1pal(ioff+i)
        ofb2rol(i) = ofb2pal(ioff+i)
        ofb3rol(i) = ofb3pal(ioff+i)
        ofb4rol(i) = ofb4pal(ioff+i)
        ofb5rol(i) = ofb5pal(ioff+i)
        ofbtrol(i) = ofbtpal(ioff+i)
        abb1rol(i) = abb1pal(ioff+i)
        abb2rol(i) = abb2pal(ioff+i)
        abb3rol(i) = abb3pal(ioff+i)
        abb4rol(i) = abb4pal(ioff+i)
        abb5rol(i) = abb5pal(ioff+i)
        abbtrol(i) = abbtpal(ioff+i)
!
        exs1row(i) = exs1pak(ioff+i)
        exs2row(i) = exs2pak(ioff+i)
        exs3row(i) = exs3pak(ioff+i)
        exs4row(i) = exs4pak(ioff+i)
        exs5row(i) = exs5pak(ioff+i)
        exstrow(i) = exstpak(ioff+i)
        ods1row(i) = ods1pak(ioff+i)
        ods2row(i) = ods2pak(ioff+i)
        ods3row(i) = ods3pak(ioff+i)
        ods4row(i) = ods4pak(ioff+i)
        ods5row(i) = ods5pak(ioff+i)
        odstrow(i) = odstpak(ioff+i)
        odsvrow(i) = odsvpak(ioff+i)
        ofs1row(i) = ofs1pak(ioff+i)
        ofs2row(i) = ofs2pak(ioff+i)
        ofs3row(i) = ofs3pak(ioff+i)
        ofs4row(i) = ofs4pak(ioff+i)
        ofs5row(i) = ofs5pak(ioff+i)
        ofstrow(i) = ofstpak(ioff+i)
        abs1row(i) = abs1pak(ioff+i)
        abs2row(i) = abs2pak(ioff+i)
        abs3row(i) = abs3pak(ioff+i)
        abs4row(i) = abs4pak(ioff+i)
        abs5row(i) = abs5pak(ioff+i)
        abstrow(i) = abstpak(ioff+i)
!
        exs1rol(i) = exs1pal(ioff+i)
        exs2rol(i) = exs2pal(ioff+i)
        exs3rol(i) = exs3pal(ioff+i)
        exs4rol(i) = exs4pal(ioff+i)
        exs5rol(i) = exs5pal(ioff+i)
        exstrol(i) = exstpal(ioff+i)
        ods1rol(i) = ods1pal(ioff+i)
        ods2rol(i) = ods2pal(ioff+i)
        ods3rol(i) = ods3pal(ioff+i)
        ods4rol(i) = ods4pal(ioff+i)
        ods5rol(i) = ods5pal(ioff+i)
        odstrol(i) = odstpal(ioff+i)
        odsvrol(i) = odsvpal(ioff+i)
        ofs1rol(i) = ofs1pal(ioff+i)
        ofs2rol(i) = ofs2pal(ioff+i)
        ofs3rol(i) = ofs3pal(ioff+i)
        ofs4rol(i) = ofs4pal(ioff+i)
        ofs5rol(i) = ofs5pal(ioff+i)
        ofstrol(i) = ofstpal(ioff+i)
        abs1rol(i) = abs1pal(ioff+i)
        abs2rol(i) = abs2pal(ioff+i)
        abs3rol(i) = abs3pal(ioff+i)
        abs4rol(i) = abs4pal(ioff+i)
        abs5rol(i) = abs5pal(ioff+i)
        abstrol(i) = abstpal(ioff+i)
!
      end do
#endif
#if defined use_cosp
! cosp input
      do l = 1, ilev
         do i = il1, il2
            rmixrow(i,l)  = rmixpak(ioff+i,l)
            smixrow(i,l)  = smixpak(ioff+i,l)
            rrefrow(i,l)  = rrefpak(ioff+i,l)
            srefrow(i,l)  = srefpak(ioff+i,l)
         end do ! i
      end do ! l


! cosp output
      do i = il1, il2

! isccp fields
         if (lalbisccp) &
         albisccp(i) = o_albisccp(ioff+i)

         if (ltauisccp) &
         tauisccp(i) = o_tauisccp(ioff+i)

         if (lpctisccp) &
         pctisccp(i) = o_pctisccp(ioff+i)

         if (lcltisccp) &
         cltisccp(i) = o_cltisccp(ioff+i)


         if (lmeantbisccp) &
         meantbisccp(i) = o_meantbisccp(ioff+i)

         if (lmeantbclrisccp) &
         meantbclrisccp(i) = o_meantbclrisccp(ioff+i)

         if (lisccp_sim) &
         sunl(i) = o_sunl(ioff+i)

! calipso fields

         if (lclhcalipso) then
            clhcalipso(i)    = o_clhcalipso(ioff+i)
            cnt_clhcalipso(i) = ocnt_clhcalipso(ioff+i)
         end if

         if (lclmcalipso) then
            clmcalipso(i)    = o_clmcalipso(ioff+i)
            cnt_clmcalipso(i) = ocnt_clmcalipso(ioff+i)
         end if

         if (lcllcalipso) then
            cllcalipso(i)    = o_cllcalipso(ioff+i)
            cnt_cllcalipso(i) = ocnt_cllcalipso(ioff+i)
         end if

         if (lcltcalipso) then
            cltcalipso(i)     = o_cltcalipso(ioff+i)
            cnt_cltcalipso(i) = ocnt_cltcalipso(ioff+i)
         end if

         if (lclhcalipsoliq) then
            clhcalipsoliq(i)    = o_clhcalipsoliq(ioff+i)
            cnt_clhcalipsoliq(i) = ocnt_clhcalipsoliq(ioff+i)
         end if

         if (lclmcalipsoliq) then
            clmcalipsoliq(i)    = o_clmcalipsoliq(ioff+i)
            cnt_clmcalipsoliq(i) = ocnt_clmcalipsoliq(ioff+i)
         end if

         if (lcllcalipsoliq) then
            cllcalipsoliq(i)    = o_cllcalipsoliq(ioff+i)
            cnt_cllcalipsoliq(i) = ocnt_cllcalipsoliq(ioff+i)
         end if

         if (lcltcalipsoliq) then
            cltcalipsoliq(i)     = o_cltcalipsoliq(ioff+i)
            cnt_cltcalipsoliq(i) = ocnt_cltcalipsoliq(ioff+i)
         end if

         if (lclhcalipsoice) then
            clhcalipsoice(i)    = o_clhcalipsoice(ioff+i)
            cnt_clhcalipsoice(i) = ocnt_clhcalipsoice(ioff+i)
         end if

         if (lclmcalipsoice) then
            clmcalipsoice(i)    = o_clmcalipsoice(ioff+i)
            cnt_clmcalipsoice(i) = ocnt_clmcalipsoice(ioff+i)
         end if

         if (lcllcalipsoice) then
            cllcalipsoice(i)    = o_cllcalipsoice(ioff+i)
            cnt_cllcalipsoice(i) = ocnt_cllcalipsoice(ioff+i)
         end if

         if (lcltcalipsoice) then
            cltcalipsoice(i)     = o_cltcalipsoice(ioff+i)
            cnt_cltcalipsoice(i) = ocnt_cltcalipsoice(ioff+i)
         end if

         if (lclhcalipsoun) then
            clhcalipsoun(i)    = o_clhcalipsoun(ioff+i)
            cnt_clhcalipsoun(i) = ocnt_clhcalipsoun(ioff+i)
         end if

         if (lclmcalipsoun) then
            clmcalipsoun(i)    = o_clmcalipsoun(ioff+i)
            cnt_clmcalipsoun(i) = ocnt_clmcalipsoun(ioff+i)
         end if

         if (lcllcalipsoun) then
            cllcalipsoun(i)    = o_cllcalipsoun(ioff+i)
            cnt_cllcalipsoun(i) = ocnt_cllcalipsoun(ioff+i)
         end if

         if (lcltcalipsoun) then
            cltcalipsoun(i)     = o_cltcalipsoun(ioff+i)
            cnt_cltcalipsoun(i) = ocnt_cltcalipsoun(ioff+i)
         end if

! cloudsat+calipso fields
         if (lcltlidarradar) then
            cltlidarradar(i)    = o_cltlidarradar(ioff+i)
            cnt_cltlidarradar(i) = ocnt_cltlidarradar(ioff+i)
         end if
      end do


! several special fields > 1d

! isccp
      if (lclisccp) then
         do ip = 1, 7           ! nptop
            do it = 1, 7        ! ntaucld
               do i = il1, il2
                  clisccp(i,it,ip) = o_clisccp(ioff+i,it,ip)

               end do
            end do
         end do
      end if

! parasol
      if (lparasolrefl) then
         do ip = 1, parasol_nrefl
            do i = il1, il2
               parasol_refl(i,ip)     =  o_parasol_refl(ioff+i,ip)
               cnt_parasol_refl(i,ip) =  ocnt_parasol_refl(ioff+i,ip)
            end do
         end do
      end if

! 3d fields that might be interpolated to special levels or not
      if (use_vgrid) then       ! interpolate to specific heights
         do n = 1, nlr
            do i = il1, il2
               if (lclcalipso) then
                  clcalipso(i,n)     = o_clcalipso(ioff+i,n)
                  cnt_clcalipso(i,n) = ocnt_clcalipso(ioff+i,n)
               end if

               if (lclcalipsoliq) then
                  clcalipsoliq(i,n)     = o_clcalipsoliq(ioff+i,n)
                  cnt_clcalipsoliq(i,n) = ocnt_clcalipsoliq(ioff+i,n)
               end if

               if (lclcalipsoice) then
                  clcalipsoice(i,n)     = o_clcalipsoice(ioff+i,n)
                  cnt_clcalipsoice(i,n) = ocnt_clcalipsoice(ioff+i,n)
               end if

               if (lclcalipsoun) then
                  clcalipsoun(i,n)     = o_clcalipsoun(ioff+i,n)
                  cnt_clcalipsoun(i,n) = ocnt_clcalipsoun(ioff+i,n)
               end if

               if (lclcalipso2) then
                  clcalipso2(i,n)     = o_clcalipso2(ioff+i,n)
                  cnt_clcalipso2(i,n) = ocnt_clcalipso2(ioff+i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  cosp_height_mask(i,n) = o_cosp_height_mask(ioff+i,n)
               end if
            end do
         end do
      else
         do n = 1, ilev
            do i = il1, il2
               if (lclcalipso) then
                   clcalipso(i,n)     =  o_clcalipso(ioff+i,n)
                   cnt_clcalipso(i,n) = ocnt_clcalipso(ioff+i,n)
               end if

               if (lclcalipso2) then
                   clcalipso2(i,n)     = o_clcalipso2(ioff+i,n)
                   cnt_clcalipso2(i,n) = ocnt_clcalipso2(ioff+i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  cosp_height_mask(i,n) = o_cosp_height_mask(ioff+i,n)
               end if
            end do
         end do
      end if

         do n = 1, lidar_ntemp
            do i = il1, il2
               if (lclcalipsotmp) then
                  clcalipsotmp(i,n)     = o_clcalipsotmp(ioff+i,n)
                  cnt_clcalipsotmp(i,n) = ocnt_clcalipsotmp(ioff+i,n)
               end if

               if (lclcalipsotmpliq) then
                  clcalipsotmpliq(i,n)     = o_clcalipsotmpliq(ioff+i,n)
                  cnt_clcalipsotmpliq(i,n) = &
                                     ocnt_clcalipsotmpliq(ioff+i,n)
               end if

               if (lclcalipsotmpice) then
                  clcalipsotmpice(i,n)     = o_clcalipsotmpice(ioff+i,n)
                  cnt_clcalipsotmpice(i,n) = &
                                     ocnt_clcalipsotmpice(ioff+i,n)
               end if

               if (lclcalipsotmpun) then
                  clcalipsotmpun(i,n)     = o_clcalipsotmpun(ioff+i,n)
                  cnt_clcalipsotmpun(i,n) = &
                                     ocnt_clcalipsotmpun(ioff+i,n)
               end if
            end do ! i
         end do ! n
! cfads (2d histograms)

! calipso

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, nlr
                  do i = il1, il2
                     cfad_lidarsr532(i,n,isr) = &
                     o_cfad_lidarsr532(ioff+i,n,isr)
                  end do
               end do
            end do
         end if
      else
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, ilev
                  do i = il1, il2
                     cfad_lidarsr532(i,n,isr) = &
                     o_cfad_lidarsr532(ioff+i,n,isr)
                  end do
               end do
            end do
         end if
      end if

! cloudsat

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, nlr
                  do i = il1, il2
                     cfad_dbze94(i,n,ize)= &
                     o_cfad_dbze94(ioff+i,n,ize)
                  end do
               end do
            end do
         end if
      else
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, ilev
                  do i = il1, il2
                     cfad_dbze94(i,n,ize) = &
                     o_cfad_dbze94(ioff+i,n,ize)
                  end do
               end do
            end do
         end if
      end if
!
      if (lceres_sim) then
         do ip = 1, nceres
            do i = il1, il2
               ceres_cf(i,ip)    = o_ceres_cf(ioff+i,ip)
               ceres_cnt(i,ip)   = o_ceres_cnt(ioff+i,ip)
               ceres_ctp(i,ip)   = o_ceres_ctp(ioff+i,ip)
               ceres_tau(i,ip)   = o_ceres_tau(ioff+i,ip)
               ceres_lntau(i,ip) = o_ceres_lntau(ioff+i,ip)
               ceres_lwp(i,ip)   = o_ceres_lwp(ioff+i,ip)
               ceres_iwp(i,ip)   = o_ceres_iwp(ioff+i,ip)
               ceres_cfl(i,ip)   = o_ceres_cfl(ioff+i,ip)
               ceres_cfi(i,ip)   = o_ceres_cfi(ioff+i,ip)
               ceres_cntl(i,ip)  = o_ceres_cntl(ioff+i,ip)
               ceres_cnti(i,ip)  = o_ceres_cnti(ioff+i,ip)
               ceres_rel(i,ip)   = o_ceres_rel(ioff+i,ip)
               ceres_cdnc(i,ip)  = o_ceres_cdnc(ioff+i,ip)
               ceres_clm(i,ip)   = o_ceres_clm(ioff+i,ip)
               ceres_cntlm(i,ip) = o_ceres_cntlm(ioff+i,ip)
            end do ! i
         end do ! ip
      end if
      if (lceres_sim .and. lswath) then
         do ip = 1, nceres
            do i = il1, il2
               s_ceres_cf(i,ip)    = os_ceres_cf(ioff+i,ip)
               s_ceres_cnt(i,ip)   = os_ceres_cnt(ioff+i,ip)
               s_ceres_ctp(i,ip)   = os_ceres_ctp(ioff+i,ip)
               s_ceres_tau(i,ip)   = os_ceres_tau(ioff+i,ip)
               s_ceres_lntau(i,ip) = os_ceres_lntau(ioff+i,ip)
               s_ceres_lwp(i,ip)   = os_ceres_lwp(ioff+i,ip)
               s_ceres_iwp(i,ip)   = os_ceres_iwp(ioff+i,ip)
               s_ceres_cfl(i,ip)   = os_ceres_cfl(ioff+i,ip)
               s_ceres_cfi(i,ip)   = os_ceres_cfi(ioff+i,ip)
               s_ceres_cntl(i,ip)  = os_ceres_cntl(ioff+i,ip)
               s_ceres_cnti(i,ip)  = os_ceres_cnti(ioff+i,ip)
               s_ceres_rel(i,ip)   = os_ceres_rel(ioff+i,ip)
               s_ceres_cdnc(i,ip)  = os_ceres_cdnc(ioff+i,ip)
               s_ceres_clm(i,ip)   = os_ceres_clm(ioff+i,ip)
               s_ceres_cntlm(i,ip) = os_ceres_cntlm(ioff+i,ip)
            end do ! i
         end do ! ip
      end if

! misr
      if (lclmisr) then
         do i = il1, il2
            misr_cldarea(i)   = o_misr_cldarea(ioff+i)
            misr_mean_ztop(i) = o_misr_mean_ztop(ioff+i)
         end do ! i

         do ip = 1,misr_n_cth
            do i = il1, il2
               dist_model_layertops(i,ip) = &
                                o_dist_model_layertops(ioff+i,ip)
            end do ! i
         end do ! ip

         ibox = 1
         do ip = 1,misr_n_cth
            do it = 1, 7
               do i = il1, il2
                  fq_misr_tau_v_cth(i,ibox) = &
                                o_fq_misr_tau_v_cth(ioff+i,ibox)
               end do ! i
               ibox = ibox + 1
            end do ! it
         end do ! ip
      end if ! lclmisr

! modis
      if (lcltmodis) then
         do i = il1, il2
            modis_cloud_fraction_total_mean(i) = &
                          o_modis_cloud_fraction_total_mean(ioff+i)
         end do ! i
      end if

      if (lclwmodis) then
         do i = il1, il2
            modis_cloud_fraction_water_mean(i) = &
                         o_modis_cloud_fraction_water_mean(ioff+i)
         end do ! i
      end if

      if (lclimodis) then
         do i = il1, il2
            modis_cloud_fraction_ice_mean(i) = &
                           o_modis_cloud_fraction_ice_mean(ioff+i)
         end do ! i
      end if

      if (lclhmodis) then
         do i = il1, il2
            modis_cloud_fraction_high_mean(i) = &
                           o_modis_cloud_fraction_high_mean(ioff+i)
         end do ! i
      end if

      if (lclmmodis) then
         do i = il1, il2
            modis_cloud_fraction_mid_mean(i) = &
                           o_modis_cloud_fraction_mid_mean(ioff+i)
         end do ! i
      end if

      if (lcllmodis) then
         do i = il1, il2
            modis_cloud_fraction_low_mean(i) = &
                           o_modis_cloud_fraction_low_mean(ioff+i)
         end do ! i
      end if

      if (ltautmodis) then
         do i = il1, il2
            modis_optical_thickness_total_mean(i) = &
                       o_modis_optical_thickness_total_mean(ioff+i)
         end do ! i
      end if

      if (ltauwmodis) then
         do i = il1, il2
            modis_optical_thickness_water_mean(i) = &
                       o_modis_optical_thickness_water_mean(ioff+i)
         end do ! i
      end if

      if (ltauimodis) then
         do i = il1, il2
            modis_optical_thickness_ice_mean(i) = &
                         o_modis_optical_thickness_ice_mean(ioff+i)
         end do ! i
      end if

      if (ltautlogmodis) then
         do i = il1, il2
            modis_optical_thickness_total_logmean(i) = &
                   o_modis_optical_thickness_total_logmean(ioff+i)
         end do ! i
      end if

      if (ltauwlogmodis) then
         do i = il1, il2
            modis_optical_thickness_water_logmean(i) = &
                   o_modis_optical_thickness_water_logmean(ioff+i)
         end do ! i
      end if

      if (ltauilogmodis) then
         do i = il1, il2
            modis_optical_thickness_ice_logmean(i) = &
                     o_modis_optical_thickness_ice_logmean(ioff+i)
         end do ! i
      end if

      if (lreffclwmodis) then
         do i = il1, il2
            modis_cloud_particle_size_water_mean(i) = &
                     o_modis_cloud_particle_size_water_mean(ioff+i)
         end do ! i
      end if

      if (lreffclimodis) then
         do i = il1, il2
            modis_cloud_particle_size_ice_mean(i) = &
                      o_modis_cloud_particle_size_ice_mean(ioff+i)
         end do ! i
      end if

      if (lpctmodis) then
         do i = il1, il2
            modis_cloud_top_pressure_total_mean(i) = &
                      o_modis_cloud_top_pressure_total_mean(ioff+i)
         end do ! i
      end if

      if (llwpmodis) then
         do i = il1, il2
            modis_liquid_water_path_mean(i) = &
                             o_modis_liquid_water_path_mean(ioff+i)
         end do ! i
      end if

      if (liwpmodis) then
         do i = il1, il2
            modis_ice_water_path_mean(i) = &
                              o_modis_ice_water_path_mean(ioff+i)
         end do ! i
      end if

      if (lclmodis) then
         ibox = 1
         do ip = 1, a_nummodispressurebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
          modis_optical_thickness_vs_cloud_top_pressure(i,ibox)= &
      o_modis_optical_thickness_vs_cloud_top_pressure(ioff+i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrimodis) then
         ibox = 1
         do ip = 1, a_nummodisrefficebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                  modis_optical_thickness_vs_reffice(i,ibox)= &
                  o_modis_optical_thickness_vs_reffice(ioff+i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrlmodis) then
         ibox = 1
         do ip = 1, a_nummodisreffliqbins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                  modis_optical_thickness_vs_reffliq(i,ibox)= &
                  o_modis_optical_thickness_vs_reffliq(ioff+i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (llcdncmodis) then
         do i = il1, il2
            modis_liq_cdnc_mean(i) = &
                              o_modis_liq_cdnc_mean(ioff+i)
            modis_liq_cdnc_gcm_mean(i) = &
                              o_modis_liq_cdnc_gcm_mean(ioff+i)
            modis_cloud_fraction_warm_mean(i) = &
                   o_modis_cloud_fraction_warm_mean(ioff+i)
         end do ! i
      end if
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
