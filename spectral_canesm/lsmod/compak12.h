#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 20/2018 - m.lazare.   - ensure eco2pak and eco2pal are defined for all cases.
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtvipas for sampled burdens.
!     * aug 23/2018 - vivek arora - remove pfhc and pexf
!     * aug 21/2018 - m.lazare.     remove {alsw,allw}.
!     * aug 14/2018 - m.lazare.     remove maskpak,gtpal.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * apr 19/2018 - m.lazare.    - add eco2 arrays (current and target).
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}.
!     * nov 01/2017 - j. cole.     - add arrays to support cmip6 stratospheric
!     *                              aerosols.
!     * aug 10/2017 - m.lazare.    - add fnpat.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     *                            - wsnopat dimension changed from
!     *                              to im to support use over
!     *                              sea/lake ice.
!     *                            - add local rflndpak to pass to
!     *                              coupler init for non-ocean fraction.
!     * aug 07/2017 - m.lazare.    initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstpal,cltpal} removed.
!     *                            - add (for nemo/cice support):
!     *                              obegpal,obwgpal,begopal,bwgopal,
!     *                              begipal,hflipal,
!     *                              hseapal,rainspal,snowspal.
!     *                            - remove: ftilpak.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class): thlwpat.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvpat,algdnpat,
!     *                              algwvpat,algwnpat,socipat.
!     *                            - add (for fractional land/water/ice):
!     *                              salbpat,csalpat,emispak,emispat,
!     *                              wrkapal,wrkbpal,snopako.
!     *                            - add (for pla):
!     *                              psvvpak,pdevpak,pdivpak,prevpak,
!     *                              privpak.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - ntld used instead of im for land-only
!     *                              mosaic fields.
!     *                            - flakpak,flndpak,gtpat,licnpak added.
!     *                            - {thlqpak,thicpak} removed.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "PAT" arrays.
!     * jul 10/2013 - m.lazare/    previous version compak11 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "IM" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well: {fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 19/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - for selected variables, add new packed tile
!     *                              versions (pat) or convert from pak to pat.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 07/2012 - m.lazare/    previous version compak10 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     *                            - "FLGROL_R" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "FDLROL_R-SIGMA*T**4".
!     *                            - include "ISEED" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version compak9i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("FSOL").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "COUPLER_CTEM".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "HISTEMI"
!     *                              converted to cpp:
!     *                              "TRANSIENT_AEROSOL_EMISSIONS" with
!     *                              further choice of cpp directives
!     *                              "HISTEMI" for historical or
!     *                              "EMISTS" for future emissions.
!     *                              the "HISTEMI" fields are the
!     *                              same as previously. for "EMISTS",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("B" for black
!     *                              carbon, "O" for organic carbon and
!     *                              "S" for sulfur), while for each of
!     *                              these, there are "AIR" for aircraft,
!     *                              "SFC" for surface, "STK" for stack
!     *                              and "FIR" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 20/2009 - m.lazare.    previous version compak9h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%DF EXPLVOL". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version compak9g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2006 - j.cole/      previous version compak9f for gcm15f:
!     *               m.lazare.    - add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    - two extra new fields ("DMC" and "SMC")
!     *                              under control of "%IF DEF,XTRACONV".
!=========================================================================
!     * general physics arrays.
!
real :: ilslpak !<
real :: jlpak !<
!
real :: luinpak !<
real :: luimpak !<
real :: lrinpak !<
real :: lrimpak !<
real :: ilmopak !<
real :: llakpak !<
real :: ldmxpak !<
real :: lzicpak !<
#if defined (flake)
real :: ltavpak !<
real :: lticpak !<
real :: ltsnpak !<
real :: lshppak !<
real :: ltmxpak !<
real :: ltwbpak !<
real :: lzsnpak !<
#endif
#if defined (cslm)
real :: nlklpak !<
#endif
!
real :: lamnpat !<
real :: lamxpat !<
real :: lnz0pat !<
real :: mvpat !<
real :: mvpak !<
real :: no3pak !<
real :: nh3pak !<
real :: nh4pak !<
real :: no3pal !<
real :: nh3pal !<
real :: nh4pal !<
real :: randompak !<
real*8 :: radjpak !<
real*8 :: wjpak !<
real :: lonupak !<
real :: londpak !<
real :: latupak !<
real :: latdpak !<
real :: lw_ext_sa_pak !<
real :: lw_ext_sa_pal !<
real :: lw_ssa_sa_pak !<
real :: lw_ssa_sa_pal !<
real :: lw_g_sa_pak !<
real :: lw_g_sa_pal !<
real :: ntpak !<
real :: n20pak !<
real :: n50pak !<
real :: n100pak !<
real :: n200pak !<

  real :: abb1pak
  real :: abb1pal
  real :: abb2pak
  real :: abb2pal
  real :: abb3pak
  real :: abb3pal
  real :: abb4pak
  real :: abb4pal
  real :: abb5pak
  real :: abb5pal
  real :: abbtpak
  real :: abbtpal
  real :: abs1pak
  real :: abs1pal
  real :: abs2pak
  real :: abs2pal
  real :: abs3pak
  real :: abs3pal
  real :: abs4pak
  real :: abs4pal
  real :: abs5pak
  real :: abs5pal
  real :: abstpak
  real :: abstpal
  real :: ailcbpat
  real :: ailcgpat
  real :: ailcpat
  real :: algdnpat
  real :: algdvpat
  real :: algwnpat
  real :: algwvpat
  real :: alicpat
  real :: almcpak
  real :: almxpak
  real :: alphpak
  real :: altipak
  real :: alvcpat
  real :: ancgptl
  real :: ancsptl
  real :: anpak
  real :: anpat
  real :: bairpak
  real :: bairpal
  real :: baltpal
  real :: bbiopak
  real :: bbiopal
  real :: bcdpak
  real :: bcdpal
  real :: bcsnpak
  real :: bcsnpat
  real :: bcspak
  real :: bdtfpak
  real :: begipal
  real :: begkpal
  real :: beglpal
  real :: begopal
  real :: begpal
  real :: betfpak
  real :: bfirpak
  real :: bfirpal
  real :: bipat
  real :: blakpak
  real :: bleafcpat
  real :: bmasvpat
  real :: brfrpak
  real :: bsfcpak
  real :: bsfcpal
  real :: bshipak
  real :: bshipal
  real :: bstkpak
  real :: bstkpal
  real :: bwgipal
  real :: bwgkpal
  real :: bwglpal
  real :: bwgopal
  real :: bwgpal
  real :: c3crpak
  real :: c3grpak
  real :: c3pfpak
  real :: c4crpak
  real :: c4grpak
  real :: c4pfpak
  real :: calicpat
  real :: calvcpat
  real :: capepak
  real :: cbmfpal
  real :: cbrnpak
  real :: cdcbpak
  real :: cdcbpal
  real :: cdebpak
  real :: cdebpat
  real :: cfcanpat
  real :: cffdpak
  real :: cffvpak
  real :: cfgppak
  real :: cfgppal
  real :: cfhtpak
  real :: cfldpak
  real :: cflfpak
  real :: cflhpak
  real :: cfluxcgpat
  real :: cfluxcspat
  real :: cflvpak
  real :: cfnbpak
  real :: cfnepak
  real :: cfnlpak
  real :: cfnppak
  real :: cfnrpak
  real :: cfnspak
  real :: cfrdpak
  real :: cfrdpal
  real :: cfrgpak
  real :: cfrhpak
  real :: cfrhpal
  real :: cfrmpak
  real :: cfrvpak
  real :: cfrvpal
  real :: ch4hpak
  real :: ch4npak
  real :: chfxpak
  real :: chumpak
  real :: chumpat
  real :: cicpak
  real :: cict3hpak
  real :: cictpak
  real :: cictpal
  real :: cinhpak
  real :: claipak
  real :: claypat
  real :: clbpal
  real :: clbpat
  real :: clcvpak
  real :: cldapak
  real :: cldapal
  real :: cldopak
  real :: cldopal
  real :: cldpak
  real :: cldt3hpak
  real :: cldtpak
  real :: cldtpal
  real :: clndpak
  real :: clwpak
  real :: clwt3hpak
  real :: clwtpak
  real :: clwtpal
  real :: cmascpat
  real :: cmaspat
  real :: co2cg1pat
  real :: co2cg2pat
  real :: co2cs1pat
  real :: co2cs2pat
  real :: cqfxpak
  real :: crpfpak
  real :: csalpal
  real :: csalpat
  real :: csbpal
  real :: csbpat
  real :: cscbpak
  real :: cscbpal
  real :: csdbpal
  real :: csdbpat
  real :: csdpal
  real :: csdpat
  real :: csfbpal
  real :: csfbpat
  real :: csfpal
  real :: csfpat
  real :: cszpal
  real :: curfpak
  real :: cvarpak
  real :: cvegpak
  real :: cvegpat
  real :: cvglpak
  real :: cvgrpak
  real :: cvgspak
  real :: cvmcpak
  real :: cvsgpak
  real :: cw1dpak
  real :: cw2dpak
  real :: dd4pak
  real :: dd6pak
  real :: ddbpak
  real :: dddpak
  real :: ddopak
  real :: ddspak
  real :: deltpak
  real :: delupak
  real :: depbpak
  real :: dlatpak
  real :: dlonpak
  real :: dlzwpat
  real :: dmcdpak
  real :: dmcpak
  real :: dmcpal
  real :: dmcupak
  real :: dmsopak
  real :: dmsopal
  real :: dox4pak
  real :: doxdpak
  real :: dpthpak
  real :: dpthpat
  real :: drnpat
  real :: drpak
  real :: dtmppak
  real :: dustpak
  real :: duthpak
  real :: duwdpak
  real :: dzgpak
  real :: ea55pak
  real :: ea55pal
  real :: eaibpak
  real :: eaiopak
  real :: eaispak
  real :: eco2pak
  real :: eco2pal
  real :: edmspak
  real :: edmspal
  real :: edslpak
  real :: edsopak
  real :: efibpak
  real :: efiopak
  real :: efispak
  real :: ehcvpak
  real :: ehevpak
  real :: emispak
  real :: emispat
  real :: envpak
  real :: eostpak
  real :: eostpal
  real :: escvpak
  real :: esdpak
  real :: esevpak
  real :: esfbpak
  real :: esfopak
  real :: esfspak
  real :: estbpak
  real :: estopak
  real :: estspak
  real :: esvcpak
  real :: esvepak
  real :: exb1pak
  real :: exb1pal
  real :: exb2pak
  real :: exb2pal
  real :: exb3pak
  real :: exb3pal
  real :: exb4pak
  real :: exb4pal
  real :: exb5pak
  real :: exb5pal
  real :: exbtpak
  real :: exbtpal
  real :: expwpak
  real :: exs1pak
  real :: exs1pal
  real :: exs2pak
  real :: exs2pal
  real :: exs3pak
  real :: exs3pal
  real :: exs4pak
  real :: exs4pal
  real :: exs5pak
  real :: exs5pal
  real :: exstpak
  real :: exstpal
  real :: fa10pak
  real :: fa1pak
  real :: fa2pak
  real :: fairpak
  real :: fairpal
  real :: fallhpat
  real :: fallpak
  real :: farepat
  real :: fbbcpak
  real :: fbbcpal
  real :: fcancpat
  real :: fcanpat
  real :: fcappak
  real :: fcolpak
  real :: fcolpat
  real :: fcoopak
  real :: fdl3hpak
  real :: fdlc3hpak
  real :: fdlcpak
  real :: fdlcpal
  real :: fdlcpat
  real :: fdlpak
  real :: fdlpal
  real :: fdlpat
  real :: fladpal
  real :: flampak
  real :: flampal
  real :: flanpak
  real :: flanpal
  real :: flaupal
  real :: flcdpal
  real :: flcupal
  real :: flg3hpak
  real :: flgcpak
  real :: flggpak
  real :: flglpak
  real :: flgnpak
  real :: flgpak
  real :: flgpal
  real :: flgpat
  real :: flgvpak
  real :: flkrpak
  real :: flkupak
  real :: flndpak
  real :: fnlapak
  real :: fnlpak
  real :: fnpak
  real :: fnpat
  real :: foulgrd
  real :: fsadpal
  real :: fsampak
  real :: fsampal
  real :: fsanpak
  real :: fsanpal
  real :: fsaupal
  real :: fscdpal
  real :: fscupal
  real :: fsd3hpak
  real :: fsdbpal
  real :: fsdbpat
  real :: fsdcpak
  real :: fsdpak
  real :: fsdpal
  real :: fsdpat
  real :: fsfbpal
  real :: fsfbpat
  real :: fsfpal
  real :: fsfpat
  real :: fsg3hpak
  real :: fsgc3hpak
  real :: fsgcpak
  real :: fsgcpal
  real :: fsggpak
  real :: fsgipal
  real :: fsglpak
  real :: fsgnpak
  real :: fsgopal
  real :: fsgpak
  real :: fsgpal
  real :: fsgpat
  real :: fsgvpak
  real :: fsipal
  real :: fsipat
  real :: fslopak
  real :: fslopal
  real :: fsnowptl
  real :: fso3hpak
  real :: fsopak
  real :: fsopal
  real :: fsr3hpak
  real :: fsrc3hpak
  real :: fsrcpak
  real :: fsrcpal
  real :: fsrpak
  real :: fsrpal
  real :: fss3hpak
  real :: fssbpal
  real :: fssbpat
  real :: fssc3hpak
  real :: fsscbpal
  real :: fsscbpat
  real :: fsscpak
  real :: fsscpal
  real :: fsspak
  real :: fsspal
  real :: fsvpak
  real :: fsvpal
  real :: fsvpat
  real :: ftoxpak
  real :: ftoypak
  real :: fvgpak
  real :: fvnpak
  real :: gampak
  real :: gapak
  real :: gflxpak
  real :: gicnpak
  real :: gleafcpat
  real :: gredpak
  real :: grkspat
  real :: growtpat
  real :: grsfpak
  real :: gsnopak
  real :: gtapak
  real :: gtpak
  real :: gtpal
  real :: gtpat
  real :: gtpax
  real :: gustpak
  real :: h2o2pak
  real :: h2o2pal
  real :: hblpak
  real :: hcpspat
  real :: hfcgpak
  real :: hfclpak
  real :: hfcnpak
  real :: hfcvpak
  real :: hfl3hpak
  real :: hflgpak
  real :: hflipal
  real :: hfllpak
  real :: hflnpak
  real :: hflpak
  real :: hflvpak
  real :: hfs3hpak
  real :: hfsgpak
  real :: hfsipal
  real :: hfslpak
  real :: hfsnpak
  real :: hfspak
  real :: hfsvpak
  real :: hlakpak
  real :: hmfgpak
  real :: hmflpak
  real :: hmfnpak
  real :: hmfvpak
  real :: hno3pak
  real :: hno3pal
  real :: hrlcpak
  real :: hrlpak
  real :: hrscpak
  real :: hrspak
  real :: hseapal
  integer :: igdrpat
  integer :: iseedpak
  integer :: isndpat
  integer :: ndtfpak
  integer :: netfpak
  real :: o3cpak
  real :: o3cpal
  real :: o3pak
  real :: o3pal
  real :: oairpak
  real :: oairpal
  real :: obiopak
  real :: obiopal
  real :: odb1pak
  real :: odb1pal
  real :: odb2pak
  real :: odb2pal
  real :: odb3pak
  real :: odb3pal
  real :: odb4pak
  real :: odb4pal
  real :: odb5pak
  real :: odb5pal
  real :: odbtpak
  real :: odbtpal
  real :: odbvpak
  real :: odbvpal
  real :: ods1pak
  real :: ods1pal
  real :: ods2pak
  real :: ods2pal
  real :: ods3pak
  real :: ods3pal
  real :: ods4pak
  real :: ods4pal
  real :: ods5pak
  real :: ods5pal
  real :: odstpak
  real :: odstpal
  real :: odsvpak
  real :: odsvpal
  real :: ofb1pak
  real :: ofb1pal
  real :: ofb2pak
  real :: ofb2pal
  real :: ofb3pak
  real :: ofb3pal
  real :: ofb4pak
  real :: ofb4pal
  real :: ofb5pak
  real :: ofb5pal
  real :: ofbtpak
  real :: ofbtpal
  real :: ofirpak
  real :: ofirpal
  real :: ofs1pak
  real :: ofs1pal
  real :: ofs2pak
  real :: ofs2pal
  real :: ofs3pak
  real :: ofs3pal
  real :: ofs4pak
  real :: ofs4pal
  real :: ofs5pak
  real :: ofs5pal
  real :: ofsgpal
  real :: ofstpak
  real :: ofstpal
  real :: ohpak
  real :: ohpal
  real :: olr3hpak
  real :: olrc3hpak
  real :: olrcpak
  real :: olrcpal
  real :: olrpak
  real :: olrpal
  real :: ometpak
  real :: orgmpat
  real :: osfcpak
  real :: osfcpal
  real :: oshipak
  real :: oshipal
  real :: ostkpak
  real :: ostkpal
  real :: ozpak
  real :: ozpal
  real :: paicpat
  real :: parpal
  real :: parpat
  real :: pblhpak
  real :: pbltpak
  real :: pchfpak
  real :: pcp3hpak
  real :: pcpc3hpak
  real :: pcpcpak
  real :: pcpcpal
  real :: pcpl3hpak
  real :: pcpn3hpak
  real :: pcpnpak
  real :: pcpnpal
  real :: pcppak
  real :: pcppal
  real :: pcps3hpak
  real :: pcpspak
  real :: pdsfpak
  real :: pdsfpal
  real :: petpak
  real :: phdpak
  real :: phispak
  real :: phlpak
  real :: phspak
  real :: pigpak
  real :: pilpak
  real :: pinpak
  real :: pivfpak
  real :: pivlpak
  real :: plhfpak
  real :: pmsl3hpak
  real :: pmslipal
  real :: pmslpal
  real :: porgpak
  real :: porgpat
  real :: posphpat
  real :: prefpat
  real :: pressure_sa_pak
  real :: pressure_sa_pal
  real :: psapak
  real :: pshfpak
  real :: psipak
  real :: psispat
  real :: psiwpat
  real :: pspal
  real :: pwat3hpak
  real :: pwatipal
  real :: pwatpak
  real :: pwatpam
  real :: qapal
  real :: qavpat
  real :: qfgpak
  real :: qflpak
  real :: qfnpak
  real :: qfnpal
  real :: qfsipal
  real :: qfslpal
  real :: qfsopal
  real :: qfspak
  real :: qfvfpak
  real :: qfvfpal
  real :: qfvgpak
  real :: qfvlpak
  real :: qfxpak
  real :: qtpcpal
  real :: qtpfpam
  real :: qtphpam
  real :: qtpmpal
  real :: qtppal
  real :: qtpppal
  real :: qtptpam
  real :: qtpvpal
  real :: qwf0pal
  real :: qwfmpal
  real :: rainspal
  real :: refpak
  real :: refpat
  real :: rflndpak
  real :: rhompak
  real :: rhonpak
  real :: rhonpat
  real :: rhpak
  real :: rhpal
  real :: rivogrd
  real :: rkhpak
  real :: rkhpal
  real :: rkmpak
  real :: rkmpal
  real :: rkqpak
  real :: rkqpal
  real :: rmatcpat
  real :: rmlcgptl
  real :: rmlcsptl
  real :: rof3hpal
  real :: rofbpak
  real :: rofnpak
  real :: rofopak
  real :: rofopal
  real :: rofpak
  real :: rofpal
  real :: rofspak
  real :: rofvpak
  real :: rootcpat
  real :: rootdpat
  real :: roothpat
  real :: rootpat
  real :: rovgpak
  real :: rtctmpat
  real :: rvelgrd
  real :: sairpak
  real :: sairpal
  real :: salbpal
  real :: salbpat
  real :: sandpat
  real :: sbiopak
  real :: sbiopal
  real :: scdnpak
  real :: sclfpak
  real :: sdhppak
  real :: sdo3pak
  real :: sfirpak
  real :: sfirpal
  real :: sfrcpal
  real :: sicnpak
  real :: sicpak
  real :: sigxpak
  real :: skygpak
  real :: skynpak
  real :: slaicpat
  real :: slaipat
  real :: slhppak
  real :: slimpal
  real :: slimplh
  real :: slimplw
  real :: slimpsh
  real :: slo3pak
  real :: slwcpak
  real :: smcpak
  real :: smfrpak
  real :: smltpak
  real :: snopak
  real :: snopat
  real :: snowspal
  real :: so4cpak
  real :: so4cpal
  real :: socipat
  real :: soilcpat
  real :: spotpak
  real :: sq3hpak
  real :: sqpak
  real :: srhnpak
  real :: srhpak
  real :: srhpal
  real :: srhxpak
  real :: ssfcpak
  real :: ssfcpal
  real :: sshipak
  real :: sshipal
  real :: sshppak
  real :: sso3pak
  real :: sstkpak
  real :: sstkpal
  real :: st01pak
  real :: st02pak
  real :: st03pak
  real :: st04pak
  real :: st06pak
  real :: st13pak
  real :: st14pak
  real :: st15pak
  real :: st16pak
  real :: st17pak
  real :: st3hpak
  real :: stemcpat
  real :: stemhpat
  real :: stmnpak
  real :: stmxpak
  real :: stpak
  real :: stpal
  real :: su3hpak
  real :: supak
  real :: supal
  real :: suz0pak
  real :: suz0pal
  real :: sv3hpak
  real :: svpak
  real :: svpal
  real :: sw_ext_sa_pak
  real :: sw_ext_sa_pal
  real :: sw_g_sa_pak
  real :: sw_g_sa_pal
  real :: sw_ssa_sa_pak
  real :: sw_ssa_sa_pal
  real :: swapak
  real :: swapal
  real :: swpal
  real :: swxpak
  real :: swxupak
  real :: swxvpak
  real :: t0lkpak
  real :: tacnpak
  real :: tapal
  real :: taptl
  real :: tavpat
  real :: tbarcptl
  real :: tbarcsptl
  real :: tbargptl
  real :: tbargsptl
  real :: tbarptl
  real :: tbaspak
  real :: tbaspat
  real :: tbndpak
  real :: tbndpal
  real :: tcanoptl
  real :: tcansptl
  real :: tcdpak
  real :: tcdpal
  real :: tcspak
  real :: tcspat
  real :: tcvpak
  real :: tdoxpak
  real :: tdoypak
  real :: tempcpat
  real :: tfxpak
  real :: tgpak
  real :: tgpat
  real :: thfcpat
  real :: thicecptl
  real :: thicpat
  real :: thisylpat
  real :: thliqcptl
  real :: thliqgptl
  real :: thlqpat
  real :: thlwpat
  real :: thmpat
  real :: thrapat
  real :: thrpat
  real :: tkelpak
  real :: tlakpak
  real :: tnpak
  real :: tnpat
  real :: todfcpat
  real :: tpndpat
  real :: treepak
  real :: troppak
  real :: troppal
  real :: tsfspat
  real :: ttlcpal
  real :: ttpak
  real :: ttpat
  real :: ttpcpal
  real :: ttplpal
  real :: ttpmpal
  real :: ttppal
  real :: ttpppal
  real :: ttpspal
  real :: ttpvpal
  real :: ttscpal
  real :: tvpak
  real :: tvpat
  real :: uapal
  real :: uepak
  real :: ufsinstpal
  real :: ufsipal
  real :: ufsopal
  real :: ufspak
  real :: ufspal
  real :: usmkpak
  real :: vapal
  real :: vegfpak
  real :: veghpat
  real :: vfsinstpal
  real :: vfsipal
  real :: vfsopal
  real :: vfspak
  real :: vfspal
  real :: vgfrpak
  real :: vtaupak
  real :: vtaupal
  real :: w055_ext_gcm_sa_pak
  real :: w055_ext_gcm_sa_pal
  real :: w055_ext_sa_pak
  real :: w055_ext_sa_pal
  real :: w055_vtau_sa_pak
  real :: w055_vtau_sa_pal
  real :: w110_ext_gcm_sa_pak
  real :: w110_ext_gcm_sa_pal
  real :: w110_ext_sa_pak
  real :: w110_ext_sa_pal
  real :: w110_vtau_sa_pak
  real :: w110_vtau_sa_pal
  real :: wdd4pak
  real :: wdd6pak
  real :: wddbpak
  real :: wdddpak
  real :: wddopak
  real :: wddspak
  real :: wdl4pak
  real :: wdl6pak
  real :: wdlbpak
  real :: wdldpak
  real :: wdlopak
  real :: wdlspak
  real :: wds4pak
  real :: wds6pak
  real :: wdsbpak
  real :: wdsdpak
  real :: wdsopak
  real :: wdsspak
  real :: wetfpak
  real :: wetspak
  real :: wfrapak
  real :: wgf3hpak
  real :: wgfpak
  real :: wgl3hpak
  real :: wglpak
  real :: wrkapal
  real :: wrkbpal
  real :: wsnopak
  real :: wsnopat
  real :: wtabpak
  real :: wtrgpak
  real :: wtrnpak
  real :: wtrvpak
  real :: wvfpak
  real :: wvfpat
  real :: wvlpak
  real :: wvlpat
  real :: xfspak
  real :: xsfxpak
  real :: xsfxpal
  real :: xsrfpak
  real :: xsrfpal
  real :: xtpfpam
  real :: xtphpam
  real :: xtptpam
  real :: xtvipak
  real :: xtvipam
  real :: xtvipas
  real :: xwf0pal
  real :: xwfmpal
  real :: ydepgrd
  real :: ygwogrd
  real :: ygwsgrd
  real :: yswigrd
  real :: yswogrd
  real :: yswsgrd
  real :: zbtwpat
  real :: zdetpak
  real :: zgpal
  real :: znpak
  real :: zolncpat
  real :: zpndpak
  real :: zpndpat
  real :: zspdpak

common /pak/ almcpak(ip0j,ilev)
common /pak/ almxpak(ip0j,ilev)
common /pak/ cicpak (ip0j,ilev)
common /pak/ clcvpak(ip0j,ilev)
common /pak/ cldpak (ip0j,ilev)
common /pak/ clwpak (ip0j,ilev)
common /pak/ csalpal(ip0j,nbs)
common /pak/ csalpat(ip0j,im,nbs)
common /pak/ cvarpak(ip0j,ilev)
common /pak/ cvmcpak(ip0j,ilev)
common /pak/ cvsgpak(ip0j,ilev)
common /pak/ h2o2pak(ip0j,levox)
common /pak/ h2o2pal(ip0j,levox)
common /pak/ hno3pak(ip0j,levox)
common /pak/ hno3pal(ip0j,levox)
common /pak/ hrlpak (ip0j,ilev)
common /pak/ hrspak (ip0j,ilev)
common /pak/ hrlcpak(ip0j,ilev)
common /pak/ hrscpak(ip0j,ilev)
common /pak/ nh3pak (ip0j,levox)
common /pak/ nh3pal (ip0j,levox)
common /pak/ nh4pak (ip0j,levox)
common /pak/ nh4pal (ip0j,levox)
common /pak/ no3pak (ip0j,levox)
common /pak/ no3pal (ip0j,levox)
common /pak/ o3pak  (ip0j,levox)
common /pak/ o3pal  (ip0j,levox)
common /pak/ o3cpak (ip0j,levozc)
common /pak/ o3cpal (ip0j,levozc)
common /pak/ ohpak  (ip0j,levox)
common /pak/ ohpal  (ip0j,levox)
common /pak/ ometpak(ip0j,ilev)
common /pak/ ozpak  (ip0j,levoz)
common /pak/ ozpal  (ip0j,levoz)
common /pak/ qwf0pal(ip0j,ilev)
common /pak/ qwfmpal(ip0j,ilev)
common /pak/ rhpak  (ip0j,ilev)
common /pak/ salbpal(ip0j,nbs)
common /pak/ salbpat(ip0j,im,nbs)
common /pak/ scdnpak(ip0j,ilev)
common /pak/ sclfpak(ip0j,ilev)
common /pak/ slwcpak(ip0j,ilev)
common /pak/ tacnpak(ip0j,ilev)
common /pak/ tbndpak(ip0j,ilev)
common /pak/ tbndpal(ip0j,ilev)
common /pak/ ea55pak(ip0j,ilev)
common /pak/ ea55pal(ip0j,ilev)
common /pak/ wrkapal(ip0j,nbs)
common /pak/ wrkbpal(ip0j,nbs)
common /pak/ zdetpak(ip0j,ilev)
common /pak/ sfrcpal(ip0j,ilev,ntrac)
common /pak/ altipak (ip0j,ilev)
common /pak/ ftoxpak(ip0j,ilev), ftoypak(ip0j,ilev)
common /pak/ tdoxpak(ip0j,ilev), tdoypak(ip0j,ilev)

common /pak/ dlatpak  (ip0j) ! latitude in degrees
common /pak/ latupak  (ip0j)
common /pak/ latdpak  (ip0j)
common /pak/ lonupak  (ip0j)
common /pak/ londpak  (ip0j)
!
common /pak/ alphpak(ip0j)
common /pak/ begipal(ip0j)
common /pak/ begkpal(ip0j)
common /pak/ beglpal(ip0j)
common /pak/ begopal(ip0j)
common /pak/ begpal (ip0j)
common /pak/ bwgipal(ip0j)
common /pak/ bwgkpal(ip0j)
common /pak/ bwglpal(ip0j)
common /pak/ bwgopal(ip0j)
common /pak/ bwgpal (ip0j)
common /pak/ cbmfpal(ip0j)
common /pak/ chfxpak(ip0j)
common /pak/ cictpak(ip0j)
common /pak/ clbpal (ip0j)
common /pak/ clbpat (ip0j,im)
common /pak/ cldtpak(ip0j)
common /pak/ cldopak(ip0j)
common /pak/ cldopal(ip0j)
common /pak/ cldapak(ip0j)
common /pak/ cldapal(ip0j)
common /pak/ clwtpak(ip0j)
common /pak/ cqfxpak(ip0j)
common /pak/ csbpal (ip0j)
common /pak/ csbpat (ip0j,im)
common /pak/ csdpal (ip0j)
common /pak/ csdpat (ip0j,im)
common /pak/ csfpal (ip0j)
common /pak/ csfpat (ip0j,im)
common /pak/ cszpal (ip0j)
common /pak/ deltpak(ip0j)
common /pak/ dlonpak(ip0j)
common /pak/ dmsopak(ip0j)
common /pak/ dmsopal(ip0j)
common /pak/ eco2pak(ip0j)
common /pak/ eco2pal(ip0j)
common /pak/ edmspak(ip0j)
common /pak/ edmspal(ip0j)
common /pak/ emispak(ip0j)
common /pak/ emispat(ip0j,im)
common /pak/ envpak (ip0j)
common /pak/ fdlpak (ip0j)
common /pak/ fdlpat (ip0j,im)
common /pak/ fdlcpak(ip0j)
common /pak/ fdlcpat(ip0j,im)
common /pak/ flampak(ip0j)
common /pak/ flampal(ip0j)
common /pak/ flanpak(ip0j)
common /pak/ flanpal(ip0j)
common /pak/ flgcpak(ip0j)
common /pak/ flgpak (ip0j)
common /pak/ flgpat (ip0j,im)
common /pak/ flkrpak(ip0j)
common /pak/ flkupak(ip0j)
common /pak/ flndpak(ip0j)
common /pak/ fsampak(ip0j)
common /pak/ fsampal(ip0j)
common /pak/ fsanpak(ip0j)
common /pak/ fsanpal(ip0j)
common /pak/ fsdcpak(ip0j)
common /pak/ fsdpak (ip0j)
common /pak/ fsdpal (ip0j)
common /pak/ fsdpat (ip0j,im)
common /pak/ fsfpal (ip0j)
common /pak/ fsfpat (ip0j,im)
common /pak/ fsgcpak(ip0j)
common /pak/ fsgpak (ip0j)
common /pak/ fsgipal(ip0j)
common /pak/ fsgopal(ip0j)
common /pak/ fsgpat (ip0j,im)
common /pak/ fsipal (ip0j)
common /pak/ fsipat (ip0j,im)
common /pak/ fslopak(ip0j)
common /pak/ fslopal(ip0j)
common /pak/ fsopak (ip0j)
common /pak/ fsrcpak(ip0j)
common /pak/ fsrpak (ip0j)
common /pak/ fsscpak(ip0j)
common /pak/ fsspak (ip0j)
common /pak/ fsvpak (ip0j)
common /pak/ fsvpal (ip0j)
common /pak/ fsvpat (ip0j,im)
common /pak/ fsdbpal (ip0j,nbs)
common /pak/ fsdbpat (ip0j,im,nbs)
common /pak/ fsfbpal (ip0j,nbs)
common /pak/ fsfbpat (ip0j,im,nbs)
common /pak/ csdbpal (ip0j,nbs)
common /pak/ csdbpat (ip0j,im,nbs)
common /pak/ csfbpal (ip0j,nbs)
common /pak/ csfbpat (ip0j,im,nbs)
common /pak/ fssbpal (ip0j,nbs)
common /pak/ fssbpat (ip0j,im,nbs)
common /pak/ fsscbpal(ip0j,nbs)
common /pak/ fsscbpat(ip0j,im,nbs)
common /pak/ gampak (ip0j)
common /pak/ gicnpak(ip0j)
common /pak/ gtapak (ip0j)
common /pak/ gtpak  (ip0j)
common /pak/ gtpat  (ip0j,im)
common /pak/ gtpax  (ip0j,im)
common /pak/ hflpak (ip0j)
common /pak/ hfspak (ip0j)
common /pak/ hseapal(ip0j)
common /pak/ ilslpak(ip0j)
common /pak/ jlpak  (ip0j)
common /pak/ ofsgpal(ip0j)
common /pak/ olrpak (ip0j)
common /pak/ olrcpak(ip0j)
common /pak/ parpal (ip0j)
common /pak/ parpat (ip0j,im)
common /pak/ pblhpak(ip0j)
common /pak/ pbltpak(ip0j)
common /pak/ pchfpak(ip0j)
common /pak/ pcpcpak(ip0j)
common /pak/ pcppak (ip0j)
common /pak/ pcpspak(ip0j)
common /pak/ phispak(ip0j)
common /pak/ plhfpak(ip0j)
common /pak/ pmslpal(ip0j)
common /pak/ psapak (ip0j)
common /pak/ pshfpak(ip0j)
common /pak/ psipak (ip0j)
common /pak/ pwatpak(ip0j)
common /pak/ pwatpam(ip0j)
common /pak/ qfspak (ip0j)
common /pak/ qfslpal(ip0j)
common /pak/ qfsopal(ip0j)
common /pak/ qfxpak (ip0j)
common /pak/ qtpfpam(ip0j)
common /pak/ qtphpam(ip0j)
common /pak/ qtptpam(ip0j)
common /pak/ radjpak(ip0j)
common /pak/ rainspal(ip0j)
common /pak/ sicnpak(ip0j)
common /pak/ sicpak (ip0j)
common /pak/ sigxpak(ip0j)
common /pak/ slimpal(ip0j)
common /pak/ slimplw(ip0j),slimpsh(ip0j),slimplh(ip0j)
common /pak/ snopak (ip0j)
common /pak/ snopat (ip0j,im)
common /pak/ snowspal(ip0j)
common /pak/ sqpak  (ip0j)
common /pak/ srhpak (ip0j)
common /pak/ srhnpak(ip0j)
common /pak/ srhxpak(ip0j)
common /pak/ stmnpak(ip0j)
common /pak/ stmxpak(ip0j)
common /pak/ stpak  (ip0j)
common /pak/ supak  (ip0j)
common /pak/ svpak  (ip0j)
common /pak/ swapak (ip0j)
common /pak/ swapal (ip0j)
common /pak/ swxpak (ip0j)
common /pak/ swxupak(ip0j)
common /pak/ swxvpak(ip0j)
common /pak/ tcvpak (ip0j)
common /pak/ tfxpak (ip0j)
common /pak/ ufspak (ip0j)
common /pak/ ufspal (ip0j)
common /pak/ ufsipal(ip0j)
common /pak/ ufsopal(ip0j)
common /pak/ vfspak (ip0j)
common /pak/ vfspal (ip0j)
common /pak/ vfsipal(ip0j)
common /pak/ vfsopal(ip0j)
common /pak/ wjpak  (ip0j)
common /pak/ rkmpak (ip0j,ilev)  ! momentum eddy diffusivities
common /pak/ rkhpak (ip0j,ilev)  ! temperature eddy diffusivities
common /pak/ rkqpak (ip0j,ilev)  ! water vapour eddy diffusivities

! * tke additional fields.
real :: tkempak, xlmpak, svarpak, xlmpat
common /pak/ tkempak(ip0j,ilev)
common /pak/ xlmpak (ip0j,ilev)
common /pak/ svarpak(ip0j,ilev)
common /pak/ xlmpat (ip0j,im,ilev)
!
!     * 3-hour save fields.
!
common /pak/ cldt3hpak(ip0j)
common /pak/ hfl3hpak (ip0j)
common /pak/ hfs3hpak (ip0j)
common /pak/ rof3hpal (ip0j)
common /pak/ wgl3hpak (ip0j)
common /pak/ wgf3hpak (ip0j)
common /pak/ pcp3hpak (ip0j)
common /pak/ pcpc3hpak(ip0j)
common /pak/ pcps3hpak(ip0j)
common /pak/ pcpl3hpak(ip0j)
common /pak/ pcpn3hpak(ip0j)
common /pak/ fdl3hpak (ip0j)
common /pak/ fdlc3hpak(ip0j)
common /pak/ flg3hpak (ip0j)
common /pak/ fss3hpak (ip0j)
common /pak/ fssc3hpak(ip0j)
common /pak/ fsd3hpak (ip0j)
common /pak/ fsg3hpak (ip0j)
common /pak/ fsgc3hpak(ip0j)
common /pak/ sq3hpak  (ip0j)
common /pak/ st3hpak  (ip0j)
common /pak/ su3hpak  (ip0j)
common /pak/ sv3hpak  (ip0j)

common /pak/ olr3hpak  (ip0j) ! all-sky outgoing thermal toa
common /pak/ fsr3hpak  (ip0j) ! all-sky outgoing solar toa
common /pak/ olrc3hpak (ip0j) ! clear-sky outgoing thermal toa
common /pak/ fsrc3hpak (ip0j) ! clear-sky outgoing solar toa
common /pak/ fso3hpak  (ip0j) ! incident solar at toa
common /pak/ pwat3hpak (ip0j) ! precipitable water
common /pak/ clwt3hpak (ip0j) ! vertically integrated non-precipitating liquid water
common /pak/ cict3hpak (ip0j) ! vertically integrated non-precipitating ice water
common /pak/ pmsl3hpak (ip0j) ! sea-level pressure

!
!     * instantaneous variables for sampling
!

common /pak/ swpal(ip0j)       ! wind speed
common /pak/ srhpal(ip0j)      ! near-surface relative humidity
common /pak/ pcppal(ip0j)      ! total precip
common /pak/ pcpnpal(ip0j)     ! snow precip
common /pak/ pcpcpal(ip0j)     ! convective precip
common /pak/ qfsipal(ip0j)     ! evaporation including sublimation and transpiration
common /pak/ qfnpal(ip0j)      ! surface snow and ice sublimation flux
common /pak/ qfvfpal(ip0j)     !
common /pak/ ufsinstpal(ip0j)  ! surface downward eastward wind stress
common /pak/ vfsinstpal(ip0j)  ! surface downward northward wind stress
common /pak/ hflipal(ip0j)     ! surface upward latent heat flux
common /pak/ hfsipal(ip0j)     ! surface upward sensible heat flux
common /pak/ fdlpal(ip0j)      ! surface downwelling longwave radiation
common /pak/ flgpal(ip0j)      ! net longwave radiation (subtract from fdl to get upward flux)
common /pak/ fsspal(ip0j)      ! surface downwelling shortwave radiation
common /pak/ fsgpal(ip0j)      ! net shortwave radiation (subtract from fss to get upward flux)
common /pak/ fsscpal(ip0j)     ! surface downwelling clear-sky shortwave radiation
common /pak/ fsgcpal(ip0j)     ! surface upwelling clear-sky shortwave radiation (subtract from fssc to get upward flux)
common /pak/ fdlcpal (ip0j)    ! surface downwelling clear-sky longwave radiation
common /pak/ fsopal  (ip0j)    ! toa incident shortwave radiation
common /pak/ fsrpal  (ip0j)    ! top-of-atmosphere outgoing shortwave radiation
common /pak/ olrpal  (ip0j)    ! toa outgoing longwave radiation
common /pak/ olrcpal (ip0j)    ! toa outgoing clear-sky longwave radiation
common /pak/ fsrcpal (ip0j)    ! toa outgoing clear-sky shortwave radiation
common /pak/ pwatipal(ip0j)    ! water vapor path
common /pak/ cldtpal (ip0j)    ! total cloud cover percentage
common /pak/ clwtpal (ip0j)    ! condensed water path (liquid)
common /pak/ cictpal(ip0j)     ! condensed water path (ice)
common /pak/ baltpal(ip0j)     ! net downward radiative flux at top of model
common /pak/ pmslipal(ip0j)    ! sea level pressure
common /pak/ cdcbpal(ip0j)     ! fraction of time convection occurs in cell
common /pak/ cscbpal(ip0j)     ! fraction of time shallow convection occurs
common /pak/ gtpal(ip0j)       ! surface temperature

common /pak/ tcdpal(ip0j)     ! pressure at top of convection
common /pak/ bcdpal(ip0j)     ! pressure at bottom of convection
common /pak/ pspal(ip0j)      ! surface pressure

common /pak/ stpal(ip0j)       ! near surface temperature
common /pak/ supal(ip0j)       ! near surface east wind
common /pak/ svpal(ip0j)       ! near surface north wind

! instantaneous 3d fields
common /pak/ dmcpal(ip0j,ilev) ! net convective mass flux
common /pak/ tapal(ip0j,ilev)  ! temperature (from physics)
common /pak/ uapal(ip0j,ilev)  ! eastward wind (from physics)
common /pak/ vapal(ip0j,ilev)  ! northward wind (from physics)
common /pak/ qapal(ip0j,ilev)  ! specific humidity (from physics)
common /pak/ rhpal(ip0j,ilev)  ! relative humidity (from physics)
common /pak/ zgpal(ip0j,ilev)  ! geopotential height (from physics)
common /pak/ rkhpal(ip0j,ilev)  ! eddy coefficient for temperature
common /pak/ rkmpal(ip0j,ilev)  ! eddy coefficient for momentum
common /pak/ rkqpal(ip0j,ilev)  ! water vapour eddy diffusivities

common /pak/ ttppal(ip0j,ilev)  ! temperature tendency from model physics
common /pak/ ttpppal(ip0j,ilev)  !
common /pak/ ttpvpal(ip0j,ilev)  !
common /pak/ ttplpal(ip0j,ilev)  ! temperature tendency from lw heating
common /pak/ ttpspal(ip0j,ilev)  ! clear-sky temperature tendency from sw heating
common /pak/ ttlcpal(ip0j,ilev)  ! clear-sky temperature tendency from lw heating
common /pak/ ttscpal(ip0j,ilev)  ! temperature tendency from sw heating
common /pak/ ttpcpal(ip0j,ilev)  ! temperature tendency from convection
common /pak/ ttpmpal(ip0j,ilev)  ! temperature tendency from convection

common /pak/ qtpcpal(ip0j,ilev)  ! specific humidity tendency from convection
common /pak/ qtpmpal(ip0j,ilev)  ! specific humidity tendency from convection
common /pak/ qtpppal(ip0j,ilev)  !
common /pak/ qtpvpal(ip0j,ilev)  !
common /pak/ qtppal(ip0j,ilev)  ! specific humidity tendency from model physics

!
!     * others.
!
common /pak/ fcoopak(ip0j)
common /pak/ iseedpak(ip0j,4)
common /pak/ rflndpak(ip0j)
common /pak/ randompak(ip0j)
!
!     * lakes.
!
common /pak/ blakpak(ip0j)
common /pak/ hlakpak(ip0j)
common /pak/ ldmxpak(ip0j)
common /pak/ llakpak(ip0j)
common /pak/ lrimpak(ip0j)
common /pak/ lrinpak(ip0j)
common /pak/ luimpak(ip0j)
common /pak/ luinpak(ip0j)
common /pak/ lzicpak(ip0j)
#if defined (cslm)
      common /pak/ delupak(ip0j)
      common /pak/ dtmppak(ip0j)
      common /pak/ expwpak(ip0j)
      common /pak/ gredpak(ip0j)
      common /pak/ nlklpak(ip0j)
      common /pak/ rhompak(ip0j)
      common /pak/ t0lkpak(ip0j)
      common /pak/ tkelpak(ip0j)
      common /pak/ tlakpak(ip0j,nlklm)
!
      common /pak/ flglpak(ip0j)
      common /pak/ fnlpak (ip0j)
      common /pak/ fsglpak(ip0j)
      common /pak/ hfclpak(ip0j)
      common /pak/ hfllpak(ip0j)
      common /pak/ hfslpak(ip0j)
      common /pak/ hmflpak(ip0j)
      common /pak/ pilpak (ip0j)
      common /pak/ qflpak (ip0j)
#endif
#if defined (flake)
      common /pak/ lshppak(ip0j)
      common /pak/ ltavpak(ip0j)
      common /pak/ lticpak(ip0j)
      common /pak/ ltmxpak(ip0j)
      common /pak/ ltsnpak(ip0j)
      common /pak/ ltwbpak(ip0j)
      common /pak/ lzsnpak(ip0j)
#endif
!
!     * emissions.
!
common /pak/ eostpak(ip0j)
common /pak/ eostpal(ip0j)
!
common /pak/ escvpak(ip0j)
common /pak/ ehcvpak(ip0j)
common /pak/ esevpak(ip0j)
common /pak/ ehevpak(ip0j)
!
#if defined transient_aerosol_emissions
      common /pak/ fbbcpak(ip0j,levwf)
      common /pak/ fbbcpal(ip0j,levwf)
      common /pak/ fairpak(ip0j,levair)
      common /pak/ fairpal(ip0j,levair)
#if defined emists
      common /pak/  sairpak(ip0j)
      common /pak/  ssfcpak(ip0j)
      common /pak/  sbiopak(ip0j)
      common /pak/  sshipak(ip0j)
      common /pak/  sstkpak(ip0j)
      common /pak/  sfirpak(ip0j)
      common /pak/  sairpal(ip0j)
      common /pak/  ssfcpal(ip0j)
      common /pak/  sbiopal(ip0j)
      common /pak/  sshipal(ip0j)
      common /pak/  sstkpal(ip0j)
      common /pak/  sfirpal(ip0j)
      common /pak/  oairpak(ip0j)
      common /pak/  osfcpak(ip0j)
      common /pak/  obiopak(ip0j)
      common /pak/  oshipak(ip0j)
      common /pak/  ostkpak(ip0j)
      common /pak/  ofirpak(ip0j)
      common /pak/  oairpal(ip0j)
      common /pak/  osfcpal(ip0j)
      common /pak/  obiopal(ip0j)
      common /pak/  oshipal(ip0j)
      common /pak/  ostkpal(ip0j)
      common /pak/  ofirpal(ip0j)
      common /pak/  bairpak(ip0j)
      common /pak/  bsfcpak(ip0j)
      common /pak/  bbiopak(ip0j)
      common /pak/  bshipak(ip0j)
      common /pak/  bstkpak(ip0j)
      common /pak/  bfirpak(ip0j)
      common /pak/  bairpal(ip0j)
      common /pak/  bsfcpal(ip0j)
      common /pak/  bbiopal(ip0j)
      common /pak/  bshipal(ip0j)
      common /pak/  bstkpal(ip0j)
      common /pak/  bfirpal(ip0j)
#endif
#endif
#if (defined(pla) && defined(pfrc))
      common /pak/  amldfpak(ip0j,ilev)
      common /pak/  amldfpal(ip0j,ilev)
      common /pak/  reamfpak(ip0j,ilev)
      common /pak/  reamfpal(ip0j,ilev)
      common /pak/  veamfpak(ip0j,ilev)
      common /pak/  veamfpal(ip0j,ilev)
      common /pak/  fr1fpak(ip0j,ilev)
      common /pak/  fr1fpal(ip0j,ilev)
      common /pak/  fr2fpak(ip0j,ilev)
      common /pak/  fr2fpal(ip0j,ilev)
      common /pak/  ssldfpak(ip0j,ilev)
      common /pak/  ssldfpal(ip0j,ilev)
      common /pak/  ressfpak(ip0j,ilev)
      common /pak/  ressfpal(ip0j,ilev)
      common /pak/  vessfpak(ip0j,ilev)
      common /pak/  vessfpal(ip0j,ilev)
      common /pak/  dsldfpak(ip0j,ilev)
      common /pak/  dsldfpal(ip0j,ilev)
      common /pak/  redsfpak(ip0j,ilev)
      common /pak/  redsfpal(ip0j,ilev)
      common /pak/  vedsfpak(ip0j,ilev)
      common /pak/  vedsfpal(ip0j,ilev)
      common /pak/  bcldfpak(ip0j,ilev)
      common /pak/  bcldfpal(ip0j,ilev)
      common /pak/  rebcfpak(ip0j,ilev)
      common /pak/  rebcfpal(ip0j,ilev)
      common /pak/  vebcfpak(ip0j,ilev)
      common /pak/  vebcfpal(ip0j,ilev)
      common /pak/  ocldfpak(ip0j,ilev)
      common /pak/  ocldfpal(ip0j,ilev)
      common /pak/  reocfpak(ip0j,ilev)
      common /pak/  reocfpal(ip0j,ilev)
      common /pak/  veocfpak(ip0j,ilev)
      common /pak/  veocfpal(ip0j,ilev)
      common /pak/  zcdnfpak(ip0j,ilev)
      common /pak/  zcdnfpal(ip0j,ilev)
      common /pak/  bcicfpak(ip0j,ilev)
      common /pak/  bcicfpal(ip0j,ilev)
      common /pak/  bcdpfpak(ip0j)
      common /pak/  bcdpfpal(ip0j)
#endif
!
!     * general tracer arrays.
!
common /pak/ xfspak (ip0j,ntrac)
common /pak/ xsfxpak(ip0j,ntrac)
common /pak/ xsfxpal(ip0j,ntrac)
common /pak/ xsrfpak(ip0j,ntrac)
common /pak/ xsrfpal(ip0j,ntrac)
common /pak/ xtpfpam(ip0j,ntrac)
common /pak/ xtphpam(ip0j,ntrac)
common /pak/ xtptpam(ip0j,ntrac)
common /pak/ xtvipas(ip0j,ntrac)
common /pak/ xtvipak(ip0j,ntrac)
common /pak/ xtvipam(ip0j,ntrac)
common /pak/ xwf0pal(ip0j,ilev,ntrac)
common /pak/ xwfmpal(ip0j,ilev,ntrac)
!
!     * land surface scheme arrays.
!
common /pak/ alicpat(ip0j,ntld,ican + 1)
common /pak/ alvcpat(ip0j,ntld,ican + 1)
common /pak/ claypat(ip0j,ntld,ignd)
common /pak/ cmaspat(ip0j,ntld,ican)
common /pak/ dzgpak (ip0j,ignd)
common /pak/ fcanpat(ip0j,ntld,ican + 1)
common /pak/ gflxpak(ip0j,ignd)
common /pak/ hfcgpak(ip0j,ignd)
common /pak/ hmfgpak(ip0j,ignd)
common /pak/ lamnpat(ip0j,ntld,ican)
common /pak/ lamxpat(ip0j,ntld,ican)
common /pak/ lnz0pat(ip0j,ntld,ican + 1)
common /pak/ orgmpat(ip0j,ntld,ignd)
common /pak/ porgpak(ip0j,ignd)
common /pak/ fcappak(ip0j,ignd)
common /pak/ dlzwpat(ip0j,ntld,ignd)
common /pak/ porgpat(ip0j,ntld,ignd)
common /pak/ thfcpat(ip0j,ntld,ignd)
common /pak/ thlwpat(ip0j,ntld,ignd)
common /pak/ thrpat (ip0j,ntld,ignd)
common /pak/ thmpat (ip0j,ntld,ignd)
common /pak/ bipat  (ip0j,ntld,ignd)
common /pak/ psispat(ip0j,ntld,ignd)
common /pak/ grkspat(ip0j,ntld,ignd)
common /pak/ thrapat(ip0j,ntld,ignd)
common /pak/ hcpspat(ip0j,ntld,ignd)
common /pak/ tcspat (ip0j,ntld,ignd)
common /pak/ psiwpat(ip0j,ntld,ignd)
common /pak/ zbtwpat(ip0j,ntld,ignd)
common /pak/ isndpat(ip0j,ntld,ignd)
common /pak/ qfvgpak(ip0j,ignd)
common /pak/ rootpat(ip0j,ntld,ican)
common /pak/ sandpat(ip0j,ntld,ignd)
common /pak/ tgpak  (ip0j,ignd)
common /pak/ tgpat  (ip0j,ntld,ignd)
common /pak/ thicpat(ip0j,ntld,ignd)
common /pak/ thlqpat(ip0j,ntld,ignd)
common /pak/ wgfpak (ip0j,ignd)
common /pak/ wglpak (ip0j,ignd)
!
common /pak/ anpak  (ip0j)
common /pak/ anpat  (ip0j,im)
common /pak/ dpthpak(ip0j)
common /pak/ dpthpat(ip0j,ntld)
common /pak/ algdvpat(ip0j,ntld)
common /pak/ algdnpat(ip0j,ntld)
common /pak/ algwvpat(ip0j,ntld)
common /pak/ algwnpat(ip0j,ntld)
common /pak/ igdrpat(ip0j,ntld)
common /pak/ drnpat (ip0j,ntld)
common /pak/ drpak  (ip0j)
common /pak/ flggpak(ip0j)
common /pak/ flgnpak(ip0j)
common /pak/ flgvpak(ip0j)
common /pak/ fnlapak(ip0j)
common /pak/ fnpak  (ip0j)
common /pak/ fnpat  (ip0j,im)
common /pak/ fsggpak(ip0j)
common /pak/ fsgnpak(ip0j)
common /pak/ fsgvpak(ip0j)
common /pak/ fvgpak (ip0j)
common /pak/ fvnpak (ip0j)
common /pak/ gapak  (ip0j)
common /pak/ gsnopak(ip0j)
common /pak/ hblpak (ip0j)
common /pak/ hfcnpak(ip0j)
common /pak/ hfcvpak(ip0j)
common /pak/ hflgpak(ip0j)
common /pak/ hflnpak(ip0j)
common /pak/ hflvpak(ip0j)
common /pak/ hfsgpak(ip0j)
common /pak/ hfsnpak(ip0j)
common /pak/ hfsvpak(ip0j)
common /pak/ hmfnpak(ip0j)
common /pak/ hmfvpak(ip0j)
common /pak/ mvpak  (ip0j)
common /pak/ ilmopak(ip0j)
common /pak/ mvpat  (ip0j,ntld)
common /pak/ pcpnpak(ip0j)
common /pak/ petpak (ip0j)
common /pak/ pivfpak(ip0j)
common /pak/ pivlpak(ip0j)
common /pak/ pigpak (ip0j)
common /pak/ pinpak (ip0j)
common /pak/ qfgpak (ip0j)
common /pak/ qfnpak (ip0j)
common /pak/ qfvfpak(ip0j)
common /pak/ qfvlpak(ip0j)
common /pak/ rhonpak(ip0j)
common /pak/ rhonpat(ip0j,im)
common /pak/ rofbpak(ip0j)
common /pak/ refpak(ip0j)
common /pak/ refpat(ip0j,im)
common /pak/ bcsnpak(ip0j)
common /pak/ bcsnpat(ip0j,im)
common /pak/ rofnpak(ip0j)
common /pak/ rofopak(ip0j)
common /pak/ rofopal(ip0j)
common /pak/ rofpak (ip0j)
common /pak/ rofpal (ip0j)
common /pak/ rofspak(ip0j)
common /pak/ rofvpak(ip0j)
common /pak/ rovgpak(ip0j)
common /pak/ skygpak(ip0j)
common /pak/ skynpak(ip0j)
common /pak/ smltpak(ip0j)
common /pak/ socipat(ip0j,ntld)
common /pak/ tbaspak(ip0j)
common /pak/ tbaspat(ip0j,ntld)
common /pak/ tnpak  (ip0j)
common /pak/ tnpat  (ip0j,im)
common /pak/ ttpak  (ip0j)
common /pak/ ttpat  (ip0j,ntld)
common /pak/ tvpak  (ip0j)
common /pak/ tvpat  (ip0j,ntld)
common /pak/ uepak  (ip0j)
common /pak/ wtabpak(ip0j)
common /pak/ wtrgpak(ip0j)
common /pak/ wtrnpak(ip0j)
common /pak/ wtrvpak(ip0j)
common /pak/ wvfpak (ip0j)
common /pak/ wvfpat (ip0j,ntld)
common /pak/ wvlpak (ip0j)
common /pak/ wvlpat (ip0j,ntld)
common /pak/ znpak  (ip0j)
common /pak/ depbpak(ip0j)
common /pak/ tpndpat(ip0j,ntld)
common /pak/ zpndpak(ip0j)
common /pak/ zpndpat(ip0j,ntld)
common /pak/ tavpat (ip0j,ntld)
common /pak/ qavpat (ip0j,ntld)
common /pak/ wsnopak(ip0j)
common /pak/ wsnopat(ip0j,im)

common /pak/ farepat(ip0j,im)
common /pak/ tsfspat(ip0j,ntld,4)
#if defined (agcm_ctem)
!
!     * ctem related variables
!
      common /pak/ cfcanpat  (ip0j,ntld,icanp1)
      common /pak/ calvcpat  (ip0j,ntld,icanp1)
      common /pak/ calicpat  (ip0j,ntld,icanp1)

      common /pak/ zolncpat  (ip0j,ntld,ican)
      common /pak/ cmascpat  (ip0j,ntld,ican)
      common /pak/ rmatcpat  (ip0j,ntld,ican,  ignd)
      common /pak/ rtctmpat  (ip0j,ntld,ictem, ignd)

      common /pak/ ailcpat   (ip0j,ntld,ican)
      common /pak/ paicpat   (ip0j,ntld,ican)
      common /pak/ slaicpat  (ip0j,ntld,ican)
      common /pak/ fcancpat  (ip0j,ntld,ictem)
      common /pak/ todfcpat  (ip0j,ntld,ictem)
      common /pak/ ailcgpat  (ip0j,ntld,ictem)
      common /pak/ slaipat   (ip0j,ntld,ictem)
      common /pak/ co2cg1pat (ip0j,ntld,ictem)
      common /pak/ co2cg2pat (ip0j,ntld,ictem)
      common /pak/ co2cs1pat (ip0j,ntld,ictem)
      common /pak/ co2cs2pat (ip0j,ntld,ictem)
      common /pak/ ancgptl   (ip0j,ntld,ictem)
      common /pak/ ancsptl   (ip0j,ntld,ictem)
      common /pak/ rmlcgptl  (ip0j,ntld,ictem)
      common /pak/ rmlcsptl  (ip0j,ntld,ictem)

      common /pak/ fsnowptl  (ip0j,ntld)
      common /pak/ tcanoptl  (ip0j,ntld)
      common /pak/ tcansptl  (ip0j,ntld)
      common /pak/ taptl     (ip0j,ntld)
      common /pak/ cfluxcspat(ip0j,ntld)
      common /pak/ cfluxcgpat(ip0j,ntld)

      common /pak/ tbarptl   (ip0j,ntld,ignd)
      common /pak/ tbarcptl  (ip0j,ntld,ignd)
      common /pak/ tbarcsptl (ip0j,ntld,ignd)
      common /pak/ tbargptl  (ip0j,ntld,ignd)
      common /pak/ tbargsptl (ip0j,ntld,ignd)
      common /pak/ thliqcptl (ip0j,ntld,ignd)
      common /pak/ thliqgptl (ip0j,ntld,ignd)
      common /pak/ thicecptl (ip0j,ntld,ignd)
!
real :: lghtpak !<
real :: litrcpat !<
real :: leafspat !<
real :: lastrpat !<
real :: lastspat !<
real :: newfpat !<
!
!     * invariant.
!
      common /pak/ wetfpak  (ip0j)
      common /pak/ wetspak  (ip0j)
      common /pak/ lghtpak  (ip0j)
!
!     * time varying.
!
      common /pak/ soilcpat (ip0j,ntld,ictemp1)
      common /pak/ litrcpat (ip0j,ntld,ictemp1)
      common /pak/ rootcpat (ip0j,ntld,ictem)
      common /pak/ stemcpat (ip0j,ntld,ictem)
      common /pak/ gleafcpat(ip0j,ntld,ictem)
      common /pak/ bleafcpat(ip0j,ntld,ictem)
      common /pak/ fallhpat (ip0j,ntld,ictem)
      common /pak/ posphpat (ip0j,ntld,ictem)
      common /pak/ leafspat (ip0j,ntld,ictem)
      common /pak/ growtpat (ip0j,ntld,ictem)
      common /pak/ lastrpat (ip0j,ntld,ictem)
      common /pak/ lastspat (ip0j,ntld,ictem)
      common /pak/ thisylpat(ip0j,ntld,ictem)
      common /pak/ stemhpat (ip0j,ntld,ictem)
      common /pak/ roothpat (ip0j,ntld,ictem)
      common /pak/ tempcpat (ip0j,ntld,2)
      common /pak/ ailcbpat (ip0j,ntld,ictem)
      common /pak/ bmasvpat (ip0j,ntld,ictem)
      common /pak/ veghpat  (ip0j,ntld,ictem)
      common /pak/ rootdpat (ip0j,ntld,ictem)
!
      common /pak/ prefpat  (ip0j,ntld,ictem)
      common /pak/ newfpat  (ip0j,ntld,ictem)
!
      common /pak/ cvegpat  (ip0j,ntld)
      common /pak/ cdebpat  (ip0j,ntld)
      common /pak/ chumpat  (ip0j,ntld)
      common /pak/ fcolpat  (ip0j,ntld)
!
!     * ctem diagnostic output fields.
!
      common /pak/ cvegpak(ip0j)
      common /pak/ cdebpak(ip0j)
      common /pak/ chumpak(ip0j)
      common /pak/ claipak(ip0j)
      common /pak/ cfnppak(ip0j)
      common /pak/ cfnepak(ip0j)
      common /pak/ cfrvpak(ip0j)
      common /pak/ cfgppak(ip0j)
      common /pak/ cfnbpak(ip0j)
      common /pak/ cffvpak(ip0j)
      common /pak/ cffdpak(ip0j)
      common /pak/ cflvpak(ip0j)
      common /pak/ cfldpak(ip0j)
      common /pak/ cflhpak(ip0j)
      common /pak/ cbrnpak(ip0j)
      common /pak/ cfrhpak(ip0j)
      common /pak/ cfhtpak(ip0j)
      common /pak/ cflfpak(ip0j)
      common /pak/ cfrdpak(ip0j)
      common /pak/ cfrgpak(ip0j)
      common /pak/ cfrmpak(ip0j)
      common /pak/ cvglpak(ip0j)
      common /pak/ cvgspak(ip0j)
      common /pak/ cvgrpak(ip0j)
      common /pak/ cfnlpak(ip0j)
      common /pak/ cfnspak(ip0j)
      common /pak/ cfnrpak(ip0j)
      common /pak/ ch4hpak(ip0j)
      common /pak/ ch4npak(ip0j)
      common /pak/ wfrapak(ip0j)
      common /pak/ cw1dpak(ip0j)
      common /pak/ cw2dpak(ip0j)
      common /pak/ fcolpak(ip0j)
      common /pak/ curfpak(ip0j,ictem)

!     additional ctem related cmip6 output

      common /pak/ brfrpak(ip0j)   ! baresoilfrac
      common /pak/ c3crpak(ip0j)   ! cropfracc3
      common /pak/ c4crpak(ip0j)   ! cropfracc4
      common /pak/ crpfpak(ip0j)   ! cropfrac
      common /pak/ c3grpak(ip0j)   ! grassfracc3
      common /pak/ c4grpak(ip0j)   ! grassfracc4
      common /pak/ grsfpak(ip0j)   ! grassfrac
      common /pak/ bdtfpak(ip0j)   ! treefracbdldcd
      common /pak/ betfpak(ip0j)   ! treefracbdlevg
      common /pak/ ndtfpak(ip0j)   ! treefracndldcd
      common /pak/ netfpak(ip0j)   ! treefracndlevg
      common /pak/ treepak(ip0j)   ! treefrac
      common /pak/ vegfpak(ip0j)   ! vegfrac
      common /pak/ c3pfpak(ip0j)   ! c3pftfrac
      common /pak/ c4pfpak(ip0j)   ! c4pftfrac
      common /pak/ clndpak(ip0j)   ! cland

!
! instantaneous output (used for subdaily output)
!
      common /pak/ cfgppal (ip0j) ! gpp
      common /pak/ cfrvpal (ip0j) ! ra
      common /pak/ cfrhpal (ip0j) ! rh
      common /pak/ cfrdpal (ip0j) ! rh

#endif
#if defined explvol
      common /pak/ vtaupak(ip0j)
      common /pak/ vtaupal(ip0j)
      common /pak/ sw_ext_sa_pak(ip0jz,levsa,nbs)
      common /pak/ sw_ext_sa_pal(ip0jz,levsa,nbs)
      common /pak/ sw_ssa_sa_pak(ip0jz,levsa,nbs)
      common /pak/ sw_ssa_sa_pal(ip0jz,levsa,nbs)
      common /pak/ sw_g_sa_pak(ip0jz,levsa,nbs)
      common /pak/ sw_g_sa_pal(ip0jz,levsa,nbs)
      common /pak/ lw_ext_sa_pak(ip0jz,levsa,nbl)
      common /pak/ lw_ext_sa_pal(ip0jz,levsa,nbl)
      common /pak/ lw_ssa_sa_pak(ip0jz,levsa,nbl)
      common /pak/ lw_ssa_sa_pal(ip0jz,levsa,nbl)
      common /pak/ lw_g_sa_pak(ip0jz,levsa,nbl)
      common /pak/ lw_g_sa_pal(ip0jz,levsa,nbl)
      common /pak/ w055_ext_sa_pak(ip0jz,levsa)
      common /pak/ w055_ext_sa_pal(ip0jz,levsa)
      common /pak/ w055_vtau_sa_pak(ip0j)
      common /pak/ w055_vtau_sa_pal(ip0j)
      common /pak/ w055_ext_gcm_sa_pak(ip0j,ilev)
      common /pak/ w055_ext_gcm_sa_pal(ip0j,ilev)
      common /pak/ w110_ext_sa_pak(ip0jz,levsa)
      common /pak/ w110_ext_sa_pal(ip0jz,levsa)
      common /pak/ w110_vtau_sa_pak(ip0j)
      common /pak/ w110_vtau_sa_pal(ip0j)
      common /pak/ w110_ext_gcm_sa_pak(ip0j,ilev)
      common /pak/ w110_ext_gcm_sa_pal(ip0j,ilev)
      common /pak/ pressure_sa_pak(ip0jz,levsa)
      common /pak/ pressure_sa_pal(ip0jz,levsa)
      common /pak/ troppak(ip0j)
      common /pak/ troppal(ip0j)
#endif

common /pak/ spotpak(ip0j)
common /pak/ st01pak(ip0j)
common /pak/ st02pak(ip0j)
common /pak/ st03pak(ip0j)
common /pak/ st04pak(ip0j)
common /pak/ st06pak(ip0j)
common /pak/ st13pak(ip0j)
common /pak/ st14pak(ip0j)
common /pak/ st15pak(ip0j)
common /pak/ st16pak(ip0j)
common /pak/ st17pak(ip0j)
common /pak/ suz0pak(ip0j)
common /pak/ suz0pal(ip0j)
common /pak/ pdsfpak(ip0j)
common /pak/ pdsfpal(ip0j)
#if defined (xtradust)
      common /pak/ duwdpak(ip0j)
      common /pak/ dustpak(ip0j)
      common /pak/ duthpak(ip0j)
      common /pak/ usmkpak(ip0j)
      common /pak/ fallpak(ip0j)
      common /pak/ fa10pak(ip0j)
      common /pak/ fa2pak(ip0j)
      common /pak/ fa1pak(ip0j)
      common /pak/ gustpak(ip0j)
      common /pak/ zspdpak(ip0j)
      common /pak/ vgfrpak(ip0j)
      common /pak/ smfrpak(ip0j)
#endif

#if defined rad_flux_profs
! note that for vertical profiles
! level 1 is the top of atmosphere
! level 2 is the top of the model
! level ilev+2 is the surface
      common /pak/ fsaupak (ip0j,ilev + 2) ! upward all-sky shortwave flux profile
      common /pak/ fsadpak (ip0j,ilev + 2) ! downward all-sky shortwave flux profile
      common /pak/ flaupak (ip0j,ilev + 2) ! upward all-sky longwave flux profile
      common /pak/ fladpak (ip0j,ilev + 2) ! downward all-sky longwave flux profile
      common /pak/ fscupak (ip0j,ilev + 2) ! upward clear-sky shortwave flux profile
      common /pak/ fscdpak (ip0j,ilev + 2) ! downward clear-sky shortwave flux profile
      common /pak/ flcupak (ip0j,ilev + 2) ! upward clear-sky longwave flux profile
      common /pak/ flcdpak (ip0j,ilev + 2) ! downward clear-sky longwave flux profile
#endif
common /pak/ fsaupal (ip0j,ilev + 2) ! upward all-sky shortwave flux profile
common /pak/ fsadpal (ip0j,ilev + 2) ! downward all-sky shortwave flux profile
common /pak/ flaupal (ip0j,ilev + 2) ! upward all-sky longwave flux profile
common /pak/ fladpal (ip0j,ilev + 2) ! downward all-sky longwave flux profile
common /pak/ fscupal (ip0j,ilev + 2) ! upward clear-sky shortwave flux profile
common /pak/ fscdpal (ip0j,ilev + 2) ! downward clear-sky shortwave flux profile
common /pak/ flcupal (ip0j,ilev + 2) ! upward clear-sky longwave flux profile
common /pak/ flcdpal (ip0j,ilev + 2) ! downward clear-sky longwave flux profile

#if defined radforce
      common /pak/ fsaupak_r (ip0j,ilev + 2,nrfp) ! upward all-sky shortwave flux profile
      common /pak/ fsadpak_r (ip0j,ilev + 2,nrfp) ! downward all-sky shortwave flux profile
      common /pak/ flaupak_r (ip0j,ilev + 2,nrfp) ! upward all-sky longwave flux profile
      common /pak/ fladpak_r (ip0j,ilev + 2,nrfp) ! downward all-sky longwave flux profile
      common /pak/ fscupak_r (ip0j,ilev + 2,nrfp) ! upward clear-sky shortwave flux profile
      common /pak/ fscdpak_r (ip0j,ilev + 2,nrfp) ! downward clear-sky shortwave flux profile
      common /pak/ flcupak_r (ip0j,ilev + 2,nrfp) ! upward clear-sky longwave flux profile
      common /pak/ flcdpak_r (ip0j,ilev + 2,nrfp) ! downward clear-sky longwave flux profile

      common /pak/ fsaupal_r (ip0j,ilev + 2,nrfp) ! upward all-sky shortwave flux profile
      common /pak/ fsadpal_r (ip0j,ilev + 2,nrfp) ! downward all-sky shortwave flux profile
      common /pak/ flaupal_r (ip0j,ilev + 2,nrfp) ! upward all-sky longwave flux profile
      common /pak/ fladpal_r (ip0j,ilev + 2,nrfp) ! downward all-sky longwave flux profile
      common /pak/ fscupal_r (ip0j,ilev + 2,nrfp) ! upward clear-sky shortwave flux profile
      common /pak/ fscdpal_r (ip0j,ilev + 2,nrfp) ! downward clear-sky shortwave flux profile
      common /pak/ flcupal_r (ip0j,ilev + 2,nrfp) ! upward clear-sky longwave flux profile
      common /pak/ flcdpal_r (ip0j,ilev + 2,nrfp) ! downward clear-sky longwave flux profile

      common /pak/ rdtpak_r  (ip0j,ilev,nrfp)
      common /pak/ rdtpal_r  (ip0j,ilev,nrfp)
      common /pak/ rdtpam_r  (ip0j,ilev,nrfp)
      common /pak/ hrspak_r  (ip0j,ilev,nrfp)
      common /pak/ hrlpak_r  (ip0j,ilev,nrfp)
      common /pak/ hrscpak_r (ip0j,ilev,nrfp)
      common /pak/ hrlcpak_r (ip0j,ilev,nrfp)
      common /pak/ csbpal_r  (ip0j,nrfp)
      common /pak/ fsgpal_r  (ip0j,nrfp)
      common /pak/ fsspal_r  (ip0j,nrfp)
      common /pak/ fsscpal_r (ip0j,nrfp)
      common /pak/ clbpal_r  (ip0j,nrfp)
      common /pak/ flgpal_r  (ip0j,nrfp)
      common /pak/ fdlpal_r  (ip0j,nrfp)
      common /pak/ fdlcpal_r (ip0j,nrfp)
      common /pak/ fsrpal_r  (ip0j,nrfp)
      common /pak/ fsrcpal_r (ip0j,nrfp)
      common /pak/ fsopal_r  (ip0j,nrfp)
      common /pak/ olrpal_r  (ip0j,nrfp)
      common /pak/ olrcpal_r (ip0j,nrfp)
      common /pak/ fslopal_r (ip0j,nrfp)
      common /pak/ csbpat_r  (ip0j,im,nrfp)
      common /pak/ clbpat_r  (ip0j,im,nrfp)
      common /pak/ fsgpat_r  (ip0j,im,nrfp)
      common /pak/ flgpat_r  (ip0j,im,nrfp)
      common /pak/ kthpal    (ip0j)
#endif
#if defined tprhs
!
!     * tprhs defined
      common /pak/ ttppak (ip0j,ilev)
#endif
#if defined qprhs
!
!     * qprhs defined
      common /pak/ qtppak (ip0j,ilev)
#endif
#if defined uprhs
!
!     * uprhs defined
      common /pak/ utppak (ip0j,ilev)
#endif
#if defined vprhs
!
!     * vprhs defined
      common /pak/ vtppak (ip0j,ilev)
#endif
#if defined qconsav
!
!     * qconsav defined
      common /pak/ qaddpak(ip0j,2)
      common /pak/ qtphpak(ip0j,2)
      common /pak/ qtpfpak(ip0j,ilev)
#endif
#if defined tprhsc
!
!     * tprhsc defined
      common /pak/ ttpcpak(ip0j,ilev)
      common /pak/ ttpkpak(ip0j,ilev)
      common /pak/ ttpmpak(ip0j,ilev)
      common /pak/ ttpnpak(ip0j,ilev)
      common /pak/ ttpppak(ip0j,ilev)
      common /pak/ ttpvpak(ip0j,ilev)
#endif
#if (defined (tprhsc) || defined (radforce))
      common /pak/ ttplpak(ip0j,ilev)
      common /pak/ ttpspak(ip0j,ilev)
      common /pak/ ttscpak(ip0j,ilev)
      common /pak/ ttlcpak(ip0j,ilev)
#endif
#if defined qprhsc
!
!     * qprhsc defined
      common /pak/ qtpcpak(ip0j,ilev)
      common /pak/ qtpmpak(ip0j,ilev)
      common /pak/ qtpppak(ip0j,ilev)
      common /pak/ qtpvpak(ip0j,ilev)
#endif
#if defined uprhsc
!
!     * uprhsc defined
      common /pak/ utpcpak(ip0j,ilev)
      common /pak/ utpgpak(ip0j,ilev)
      common /pak/ utpnpak(ip0j,ilev)
      common /pak/ utpspak(ip0j,ilev)
      common /pak/ utpvpak(ip0j,ilev)
#endif
#if defined vprhsc
!
!     * vprhsc defined
      common /pak/ vtpcpak(ip0j,ilev)
      common /pak/ vtpgpak(ip0j,ilev)
      common /pak/ vtpnpak(ip0j,ilev)
      common /pak/ vtpspak(ip0j,ilev)
      common /pak/ vtpvpak(ip0j,ilev)
#endif
#if defined xconsav
!
!     * xconsav defined
      common /pak/ xaddpak(ip0j,2,ntrac)
      common /pak/ xtphpak(ip0j,2,ntrac)
      common /pak/ xtpfpak(ip0j,ilev,ntrac)
#endif
#if defined xprhs
!
!     * xprhs defined
      common /pak/ xtppak(ip0j,ilev,ntrac)
#endif
#if defined xprhsc
!
!     * xprhsc defined
      common /pak/ xtpcpak(ip0j,ilev,ntrac)
      common /pak/ xtpmpak(ip0j,ilev,ntrac)
      common /pak/ xtpppak(ip0j,ilev,ntrac)
      common /pak/ xtpvpak(ip0j,ilev,ntrac)

#endif
#if defined xtraconv
!
!     * xtraconv defined
!
      common /pak/ bcdpak (ip0j)
      common /pak/ bcspak (ip0j)
      common /pak/ capepak(ip0j)
      common /pak/ cdcbpak(ip0j)
      common /pak/ cinhpak(ip0j)
      common /pak/ cscbpak(ip0j)
      common /pak/ dmcpak (ip0j,ilev)
      common /pak/ dmcdpak(ip0j,ilev)
      common /pak/ dmcupak(ip0j,ilev)
      common /pak/ smcpak (ip0j,ilev)
      common /pak/ tcdpak (ip0j)
      common /pak/ tcspak (ip0j)
#endif
#if defined xtrachem
real :: noxdpak !<
!
      common /pak/ dd4pak (ip0j)
      common /pak/ dox4pak(ip0j)
      common /pak/ doxdpak(ip0j)
      common /pak/ esdpak (ip0j)
      common /pak/ esfspak(ip0j)
      common /pak/ eaispak(ip0j)
      common /pak/ estspak(ip0j)
      common /pak/ efispak(ip0j)
      common /pak/ esfbpak(ip0j)
      common /pak/ eaibpak(ip0j)
      common /pak/ estbpak(ip0j)
      common /pak/ efibpak(ip0j)
      common /pak/ esfopak(ip0j)
      common /pak/ eaiopak(ip0j)
      common /pak/ estopak(ip0j)
      common /pak/ efiopak(ip0j)
      common /pak/ edslpak(ip0j)
      common /pak/ edsopak(ip0j)
      common /pak/ esvcpak(ip0j)
      common /pak/ esvepak(ip0j)
      common /pak/ noxdpak(ip0j)
      common /pak/ wdd4pak(ip0j)
      common /pak/ wdl4pak(ip0j)
      common /pak/ wds4pak(ip0j)
#ifndef pla
      common /pak/ dddpak (ip0j)
      common /pak/ ddbpak (ip0j)
      common /pak/ ddopak (ip0j)
      common /pak/ ddspak (ip0j)
      common /pak/ wdldpak(ip0j)
      common /pak/ wdlbpak(ip0j)
      common /pak/ wdlopak(ip0j)
      common /pak/ wdlspak(ip0j)
      common /pak/ wdddpak(ip0j)
      common /pak/ wddbpak(ip0j)
      common /pak/ wddopak(ip0j)
      common /pak/ wddspak(ip0j)
      common /pak/ wdsdpak(ip0j)
      common /pak/ wdsbpak(ip0j)
      common /pak/ wdsopak(ip0j)
      common /pak/ wdsspak(ip0j)
      common /pak/ dd6pak (ip0j)
      common /pak/ phdpak (ip0j,ilev)
      common /pak/ phlpak (ip0j,ilev)
      common /pak/ phspak (ip0j,ilev)
      common /pak/ sdhppak(ip0j)
      common /pak/ sdo3pak(ip0j)
      common /pak/ slhppak(ip0j)
      common /pak/ slo3pak(ip0j)
      common /pak/ sshppak(ip0j)
      common /pak/ sso3pak(ip0j)
      common /pak/ wdd6pak(ip0j)
      common /pak/ wdl6pak(ip0j)
      common /pak/ wds6pak(ip0j)
#endif
#endif
#if (defined(pla) && defined(pam))
      common /pak/ ssldpak (ip0j,ilev)
      common /pak/ resspak (ip0j,ilev)
      common /pak/ vesspak (ip0j,ilev)
      common /pak/ dsldpak (ip0j,ilev)
      common /pak/ redspak (ip0j,ilev)
      common /pak/ vedspak (ip0j,ilev)
      common /pak/ bcldpak (ip0j,ilev)
      common /pak/ rebcpak (ip0j,ilev)
      common /pak/ vebcpak (ip0j,ilev)
      common /pak/ ocldpak (ip0j,ilev)
      common /pak/ reocpak (ip0j,ilev)
      common /pak/ veocpak (ip0j,ilev)
      common /pak/ amldpak (ip0j,ilev)
      common /pak/ reampak (ip0j,ilev)
      common /pak/ veampak (ip0j,ilev)
      common /pak/ fr1pak  (ip0j,ilev)
      common /pak/ fr2pak  (ip0j,ilev)
      common /pak/ zcdnpak (ip0j,ilev)
      common /pak/ bcicpak (ip0j,ilev)
      common /pak/ oednpak (ip0j,ilev,kextt)
      common /pak/ oercpak (ip0j,ilev,kextt)
      common /pak/ oidnpak (ip0j,ilev)
      common /pak/ oircpak (ip0j,ilev)
      common /pak/ svvbpak (ip0j,ilev)
      common /pak/ psvvpak (ip0j,ilev)
      common /pak/ svmbpak (ip0j,ilev,nrmfld)
      common /pak/ svcbpak (ip0j,ilev,nrmfld)
      common /pak/ pnvbpak (ip0j,ilev,isaintt)
      common /pak/ pnmbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ pncbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ psvbpak (ip0j,ilev,isaintt,kintt)
      common /pak/ psmbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ pscbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ qnvbpak (ip0j,ilev,isaintt)
      common /pak/ qnmbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ qncbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ qsvbpak (ip0j,ilev,isaintt,kintt)
      common /pak/ qsmbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ qscbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ qgvbpak (ip0j,ilev)
      common /pak/ qgmbpak (ip0j,ilev,nrmfld)
      common /pak/ qgcbpak (ip0j,ilev,nrmfld)
      common /pak/ qdvbpak (ip0j,ilev)
      common /pak/ qdmbpak (ip0j,ilev,nrmfld)
      common /pak/ qdcbpak (ip0j,ilev,nrmfld)
      common /pak/ onvbpak (ip0j,ilev,isaintt)
      common /pak/ onmbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ oncbpak (ip0j,ilev,isaintt,nrmfld)
      common /pak/ osvbpak (ip0j,ilev,isaintt,kintt)
      common /pak/ osmbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ oscbpak (ip0j,ilev,isaintt,kintt,nrmfld)
      common /pak/ ogvbpak (ip0j,ilev)
      common /pak/ ogmbpak (ip0j,ilev,nrmfld)
      common /pak/ ogcbpak (ip0j,ilev,nrmfld)
      common /pak/ devbpak (ip0j,ilev,kextt)
      common /pak/ pdevpak (ip0j,ilev,kextt)
      common /pak/ dembpak (ip0j,ilev,kextt,nrmfld)
      common /pak/ decbpak (ip0j,ilev,kextt,nrmfld)
      common /pak/ divbpak (ip0j,ilev)
      common /pak/ pdivpak (ip0j,ilev)
      common /pak/ dimbpak (ip0j,ilev,nrmfld)
      common /pak/ dicbpak (ip0j,ilev,nrmfld)
      common /pak/ revbpak (ip0j,ilev,kextt)
      common /pak/ prevpak (ip0j,ilev,kextt)
      common /pak/ rembpak (ip0j,ilev,kextt,nrmfld)
      common /pak/ recbpak (ip0j,ilev,kextt,nrmfld)
      common /pak/ rivbpak (ip0j,ilev)
      common /pak/ privpak (ip0j,ilev)
      common /pak/ rimbpak (ip0j,ilev,nrmfld)
      common /pak/ ricbpak (ip0j,ilev,nrmfld)
      common /pak/ sulipak (ip0j,ilev)
      common /pak/ rsuipak (ip0j,ilev)
      common /pak/ vsuipak (ip0j,ilev)
      common /pak/ f1supak (ip0j,ilev)
      common /pak/ f2supak (ip0j,ilev)
      common /pak/ bclipak (ip0j,ilev)
      common /pak/ rbcipak (ip0j,ilev)
      common /pak/ vbcipak (ip0j,ilev)
      common /pak/ f1bcpak (ip0j,ilev)
      common /pak/ f2bcpak (ip0j,ilev)
      common /pak/ oclipak (ip0j,ilev)
      common /pak/ rocipak (ip0j,ilev)
      common /pak/ vocipak (ip0j,ilev)
      common /pak/ f1ocpak (ip0j,ilev)
      common /pak/ f2ocpak (ip0j,ilev)
#if defined (xtrapla1)
      common /pak/ sncnpak (ip0j,ilev)
      common /pak/ ssunpak (ip0j,ilev)
      common /pak/ scndpak (ip0j,ilev)
      common /pak/ sgsppak (ip0j,ilev)
      common /pak/ ccnpak  (ip0j,ilev)
      common /pak/ cc02pak (ip0j,ilev)
      common /pak/ cc04pak (ip0j,ilev)
      common /pak/ cc08pak (ip0j,ilev)
      common /pak/ cc16pak (ip0j,ilev)
      common /pak/ ccnepak (ip0j,ilev)
      common /pak/ acaspak (ip0j,ilev)
      common /pak/ acoapak (ip0j,ilev)
      common /pak/ acbcpak (ip0j,ilev)
      common /pak/ acsspak (ip0j,ilev)
      common /pak/ acmdpak (ip0j,ilev)
      common /pak/ ntpak   (ip0j,ilev)
      common /pak/ n20pak  (ip0j,ilev)
      common /pak/ n50pak  (ip0j,ilev)
      common /pak/ n100pak (ip0j,ilev)
      common /pak/ n200pak (ip0j,ilev)
      common /pak/ wtpak   (ip0j,ilev)
      common /pak/ w20pak  (ip0j,ilev)
      common /pak/ w50pak  (ip0j,ilev)
      common /pak/ w100pak (ip0j,ilev)
      common /pak/ w200pak (ip0j,ilev)
      common /pak/ rcripak (ip0j,ilev)
      common /pak/ supspak (ip0j,ilev)
      common /pak/ henrpak (ip0j,ilev)
      common /pak/ o3frpak (ip0j,ilev)
      common /pak/ h2o2frpak(ip0j,ilev)
      common /pak/ wparpak (ip0j,ilev)
      common /pak/ pm25pak (ip0j,ilev)
      common /pak/ pm10pak (ip0j,ilev)
      common /pak/ dm25pak (ip0j,ilev)
      common /pak/ dm10pak (ip0j,ilev)
      common /pak/ vncnpak (ip0j)
      common /pak/ vasnpak (ip0j)
      common /pak/ vscdpak (ip0j)
      common /pak/ vgsppak (ip0j)
      common /pak/ voaepak (ip0j)
      common /pak/ vbcepak (ip0j)
      common /pak/ vasepak (ip0j)
      common /pak/ vmdepak (ip0j)
      common /pak/ vssepak (ip0j)
      common /pak/ voawpak (ip0j)
      common /pak/ vbcwpak (ip0j)
      common /pak/ vaswpak (ip0j)
      common /pak/ vmdwpak (ip0j)
      common /pak/ vsswpak (ip0j)
      common /pak/ voadpak (ip0j)
      common /pak/ vbcdpak (ip0j)
      common /pak/ vasdpak (ip0j)
      common /pak/ vmddpak (ip0j)
      common /pak/ vssdpak (ip0j)
      common /pak/ voagpak (ip0j)
      common /pak/ vbcgpak (ip0j)
      common /pak/ vasgpak (ip0j)
      common /pak/ vmdgpak (ip0j)
      common /pak/ vssgpak (ip0j)
      common /pak/ voacpak (ip0j)
      common /pak/ vbccpak (ip0j)
      common /pak/ vascpak (ip0j)
      common /pak/ vmdcpak (ip0j)
      common /pak/ vsscpak (ip0j)
      common /pak/ vasipak (ip0j)
      common /pak/ vas1pak (ip0j)
      common /pak/ vas2pak (ip0j)
      common /pak/ vas3pak (ip0j)
      common /pak/ vccnpak (ip0j)
      common /pak/ vcnepak (ip0j)
#endif
#if defined (xtrapla2)
      common /pak/ cornpak (ip0j,ilev)
      common /pak/ cormpak (ip0j,ilev)
      common /pak/ rsn1pak (ip0j,ilev)
      common /pak/ rsm1pak (ip0j,ilev)
      common /pak/ rsn2pak (ip0j,ilev)
      common /pak/ rsm2pak (ip0j,ilev)
      common /pak/ rsn3pak (ip0j,ilev)
      common /pak/ rsm3pak (ip0j,ilev)
      common /pak/ sdnupak (ip0j,ilev,isdnum)
      common /pak/ sdmapak (ip0j,ilev,isdnum)
      common /pak/ sdacpak (ip0j,ilev,isdnum)
      common /pak/ sdcopak (ip0j,ilev,isdnum)
      common /pak/ sssnpak (ip0j,ilev,isdnum)
      common /pak/ smdnpak (ip0j,ilev,isdnum)
      common /pak/ sianpak (ip0j,ilev,isdnum)
      common /pak/ sssmpak (ip0j,ilev,isdnum)
      common /pak/ smdmpak (ip0j,ilev,isdnum)
      common /pak/ sewmpak (ip0j,ilev,isdnum)
      common /pak/ ssumpak (ip0j,ilev,isdnum)
      common /pak/ socmpak (ip0j,ilev,isdnum)
      common /pak/ sbcmpak (ip0j,ilev,isdnum)
      common /pak/ siwmpak (ip0j,ilev,isdnum)
      common /pak/ sdvlpak (ip0j,isdiag)
      common /pak/ vrn1pak (ip0j)
      common /pak/ vrm1pak (ip0j)
      common /pak/ vrn2pak (ip0j)
      common /pak/ vrm2pak (ip0j)
      common /pak/ vrn3pak (ip0j)
      common /pak/ vrm3pak (ip0j)
      common /pak/ defapak(ip0j)
      common /pak/ defcpak(ip0j)
      common /pak/ dmacpak(ip0j,isdust)
      common /pak/ dmcopak(ip0j,isdust)
      common /pak/ dnacpak(ip0j,isdust)
      common /pak/ dncopak(ip0j,isdust)
      common /pak/ defxpak(ip0j,isdiag)
      common /pak/ defnpak(ip0j,isdiag)
      common /pak/ tnsspak(ip0j)
      common /pak/ tnmdpak(ip0j)
      common /pak/ tniapak(ip0j)
      common /pak/ tmsspak(ip0j)
      common /pak/ tmmdpak(ip0j)
      common /pak/ tmocpak(ip0j)
      common /pak/ tmbcpak(ip0j)
      common /pak/ tmsppak(ip0j)
      common /pak/ snsspak(ip0j,isdnum)
      common /pak/ snmdpak(ip0j,isdnum)
      common /pak/ sniapak(ip0j,isdnum)
      common /pak/ smsspak(ip0j,isdnum)
      common /pak/ smmdpak(ip0j,isdnum)
      common /pak/ smocpak(ip0j,isdnum)
      common /pak/ smbcpak(ip0j,isdnum)
      common /pak/ smsppak(ip0j,isdnum)
      common /pak/ siwhpak(ip0j,isdnum)
      common /pak/ sewhpak(ip0j,isdnum)
#endif
#endif
#if defined xtrals
real :: mltipak !<
real :: mltspak !<
      common /pak/ aggpak (ip0j,ilev)
      common /pak/ autpak (ip0j,ilev)
      common /pak/ cndpak (ip0j,ilev)
      common /pak/ deppak (ip0j,ilev)
      common /pak/ evppak (ip0j,ilev)
      common /pak/ frhpak (ip0j,ilev)
      common /pak/ frkpak (ip0j,ilev)
      common /pak/ frspak (ip0j,ilev)
      common /pak/ mltipak(ip0j,ilev)
      common /pak/ mltspak(ip0j,ilev)
      common /pak/ raclpak(ip0j,ilev)
      common /pak/ rainpak(ip0j,ilev)
      common /pak/ sacipak(ip0j,ilev)
      common /pak/ saclpak(ip0j,ilev)
      common /pak/ snowpak(ip0j,ilev)
      common /pak/ subpak (ip0j,ilev)
      common /pak/ sedipak(ip0j,ilev)
      common /pak/ rliqpak(ip0j,ilev)
      common /pak/ ricepak(ip0j,ilev)
      common /pak/ rlncpak(ip0j,ilev)
      common /pak/ cliqpak(ip0j,ilev)
      common /pak/ cicepak(ip0j,ilev)

      common /pak/ rliqpal(ip0j,ilev)
      common /pak/ ricepal(ip0j,ilev)
      common /pak/ rlncpal(ip0j,ilev)
      common /pak/ cliqpal(ip0j,ilev)
      common /pak/ cicepal(ip0j,ilev)
!
      common /pak/ vaggpak(ip0j)
      common /pak/ vautpak(ip0j)
      common /pak/ vcndpak(ip0j)
      common /pak/ vdeppak(ip0j)
      common /pak/ vevppak(ip0j)
      common /pak/ vfrhpak(ip0j)
      common /pak/ vfrkpak(ip0j)
      common /pak/ vfrspak(ip0j)
      common /pak/ vmlipak(ip0j)
      common /pak/ vmlspak(ip0j)
      common /pak/ vrclpak(ip0j)
      common /pak/ vscipak(ip0j)
      common /pak/ vsclpak(ip0j)
      common /pak/ vsubpak(ip0j)
      common /pak/ vsedipak(ip0j)
      common /pak/ reliqpak(ip0j)
      common /pak/ reicepak(ip0j)
      common /pak/ cldliqpak(ip0j)
      common /pak/ cldicepak(ip0j)
      common /pak/ ctlncpak(ip0j)
      common /pak/ cllncpak(ip0j)

      common /pak/ reliqpal(ip0j)
      common /pak/ reicepal(ip0j)
      common /pak/ cldliqpal(ip0j)
      common /pak/ cldicepal(ip0j)
      common /pak/ ctlncpal(ip0j)
      common /pak/ cllncpal(ip0j)
#endif
#if defined use_cosp
! cosp input
      common /pak/ rmixpak(ip0j,ilev)
      common /pak/ smixpak(ip0j,ilev)
      common /pak/ rrefpak(ip0j,ilev)
      common /pak/ srefpak(ip0j,ilev)
#endif
#if defined (aodpth)
!
!     exxx: extinction
!     odxx: optical depth
!     ofxx: fine mode optical depth (bulk), of06: angstrom coefficient (pla)
!     abxx: absorption
!
!     xxbx: at band 1
!     xxsx: at 550 nm
!
!     xxx1: so4,
!     xxx2: ss,
!     xxx3: du,
!     xxx4: bc,
!     xxx5: pom
!     xxxt: total
!
      common /pak/ exb1pak(ip0j)
      common /pak/ exb2pak(ip0j)
      common /pak/ exb3pak(ip0j)
      common /pak/ exb4pak(ip0j)
      common /pak/ exb5pak(ip0j)
      common /pak/ exbtpak(ip0j)
      common /pak/ odb1pak(ip0j)
      common /pak/ odb2pak(ip0j)
      common /pak/ odb3pak(ip0j)
      common /pak/ odb4pak(ip0j)
      common /pak/ odb5pak(ip0j)
      common /pak/ odbtpak(ip0j)
      common /pak/ odbvpak(ip0j)
      common /pak/ ofb1pak(ip0j)
      common /pak/ ofb2pak(ip0j)
      common /pak/ ofb3pak(ip0j)
      common /pak/ ofb4pak(ip0j)
      common /pak/ ofb5pak(ip0j)
      common /pak/ ofbtpak(ip0j)
      common /pak/ abb1pak(ip0j)
      common /pak/ abb2pak(ip0j)
      common /pak/ abb3pak(ip0j)
      common /pak/ abb4pak(ip0j)
      common /pak/ abb5pak(ip0j)
      common /pak/ abbtpak(ip0j)
!
      common /pak/ exb1pal(ip0j)
      common /pak/ exb2pal(ip0j)
      common /pak/ exb3pal(ip0j)
      common /pak/ exb4pal(ip0j)
      common /pak/ exb5pal(ip0j)
      common /pak/ exbtpal(ip0j)
      common /pak/ odb1pal(ip0j)
      common /pak/ odb2pal(ip0j)
      common /pak/ odb3pal(ip0j)
      common /pak/ odb4pal(ip0j)
      common /pak/ odb5pal(ip0j)
      common /pak/ odbtpal(ip0j)
      common /pak/ odbvpal(ip0j)
      common /pak/ ofb1pal(ip0j)
      common /pak/ ofb2pal(ip0j)
      common /pak/ ofb3pal(ip0j)
      common /pak/ ofb4pal(ip0j)
      common /pak/ ofb5pal(ip0j)
      common /pak/ ofbtpal(ip0j)
      common /pak/ abb1pal(ip0j)
      common /pak/ abb2pal(ip0j)
      common /pak/ abb3pal(ip0j)
      common /pak/ abb4pal(ip0j)
      common /pak/ abb5pal(ip0j)
      common /pak/ abbtpal(ip0j)
!
      common /pak/ exs1pak(ip0j)
      common /pak/ exs2pak(ip0j)
      common /pak/ exs3pak(ip0j)
      common /pak/ exs4pak(ip0j)
      common /pak/ exs5pak(ip0j)
      common /pak/ exstpak(ip0j)
      common /pak/ ods1pak(ip0j)
      common /pak/ ods2pak(ip0j)
      common /pak/ ods3pak(ip0j)
      common /pak/ ods4pak(ip0j)
      common /pak/ ods5pak(ip0j)
      common /pak/ odstpak(ip0j)
      common /pak/ odsvpak(ip0j)
      common /pak/ ofs1pak(ip0j)
      common /pak/ ofs2pak(ip0j)
      common /pak/ ofs3pak(ip0j)
      common /pak/ ofs4pak(ip0j)
      common /pak/ ofs5pak(ip0j)
      common /pak/ ofstpak(ip0j)
      common /pak/ abs1pak(ip0j)
      common /pak/ abs2pak(ip0j)
      common /pak/ abs3pak(ip0j)
      common /pak/ abs4pak(ip0j)
      common /pak/ abs5pak(ip0j)
      common /pak/ abstpak(ip0j)
!
      common /pak/ exs1pal(ip0j)
      common /pak/ exs2pal(ip0j)
      common /pak/ exs3pal(ip0j)
      common /pak/ exs4pal(ip0j)
      common /pak/ exs5pal(ip0j)
      common /pak/ exstpal(ip0j)
      common /pak/ ods1pal(ip0j)
      common /pak/ ods2pal(ip0j)
      common /pak/ ods3pal(ip0j)
      common /pak/ ods4pal(ip0j)
      common /pak/ ods5pal(ip0j)
      common /pak/ odstpal(ip0j)
      common /pak/ odsvpal(ip0j)
      common /pak/ ofs1pal(ip0j)
      common /pak/ ofs2pal(ip0j)
      common /pak/ ofs3pal(ip0j)
      common /pak/ ofs4pal(ip0j)
      common /pak/ ofs5pal(ip0j)
      common /pak/ ofstpal(ip0j)
      common /pak/ abs1pal(ip0j)
      common /pak/ abs2pal(ip0j)
      common /pak/ abs3pal(ip0j)
      common /pak/ abs4pal(ip0j)
      common /pak/ abs5pal(ip0j)
      common /pak/ abstpal(ip0j)
!
#endif
#if defined x01
      common /pak/ x01pak (ip0j,ilev,ntrac)
#endif
#if defined x02
      common /pak/ x02pak (ip0j,ilev,ntrac)
#endif
#if defined x03
      common /pak/ x03pak (ip0j,ilev,ntrac)
#endif
#if defined x04
      common /pak/ x04pak (ip0j,ilev,ntrac)
#endif
#if defined x05
      common /pak/ x05pak (ip0j,ilev,ntrac)
#endif
#if defined x06
      common /pak/ x06pak (ip0j,ilev,ntrac)
#endif
#if defined x07
      common /pak/ x07pak (ip0j,ilev,ntrac)
#endif
#if defined x08
      common /pak/ x08pak (ip0j,ilev,ntrac)
#endif
#if defined x09
      common /pak/ x09pak (ip0j,ilev,ntrac)
#endif
#if defined agcm_river_routing
!
!     * river routing fields passed through restart.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
      common /grdfull/ ygwogrd(lonsl + 1,nlat)
      common /grdfull/ ygwsgrd(lonsl + 1,nlat)
      common /grdfull/ yswogrd(lonsl + 1,nlat)
      common /grdfull/ yswsgrd(lonsl + 1,nlat)
      common /grdfull/ yswigrd(lonsl + 1,nlat)
      common /grdfull/ ydepgrd(lonsl + 1,nlat)
!
!     * river routing fields saved to history file.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
      common /grdfull/ rvelgrd(lonsl + 1,nlat)
      common /grdfull/ foulgrd(lonsl + 1,nlat)
#endif
!
!     * river routing fields saved to history file **and**
!     * written to coupler.
!     * note !! ! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
!     *         at some point.
!
common /grdfull/ rivogrd(lonsl + 1,nlat)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
