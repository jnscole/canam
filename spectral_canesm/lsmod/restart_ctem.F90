#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine restart_ctem(in, lu, &
                              lct, lgt, lctem, lctg, lctemg, &
                              lev, ibuf, idat, kount, khem, gll)
  !
  !     * may 21/2016 - m.lazare.    fcancpat removed (not used).
  !     * feb 14/2016 - m.lazare.    new version for gcm19, based on
  !     *                            combination of rstarth, ctem_init and
  !     *                            ctem_write_ts to make generalized
  !     *                            for ctem now in class.
  !     ==================================================================
  !     * read/write ts file
  !     ==================================================================

  !     * read or write re-start ctem file for the ccrn gcm.
  !     * in = +1, read-in
  !     * in = -1, write-out
  !
  use psizes_19
#if (defined(pla) && defined(pam))
      use compar
#endif
  !
  implicit none
  integer :: ibufbad
  integer, intent(in) :: in
  integer :: itime
  integer, intent(in) :: khem
  integer :: kk
  integer :: klen
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: lctemx
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lu
  integer :: nc4to8
  integer :: ntlc
  integer :: ntlc1
  !
#include "compak12.h"
  !     * common block to hold information pertinent to mpi.
  !     * in particular, this is shared with the i/o routines (for input).
  !     * these variables are defined in section 0.
  !
  integer*4 :: mynode !<
  common /mpinfo/ mynode

  integer, dimension(8) :: jbuf !<
  integer, intent(in) :: lct(ntld*icanp1) !< Variable description\f$[units]\f$
  integer, intent(in) :: lgt(ntld*ignd) !< Variable description\f$[units]\f$
  integer, intent(in) :: lctem(ntld*ictemp1) !< Variable description\f$[units]\f$
  integer, intent(in) :: lctg(ntld*ignd*ican) !< Variable description\f$[units]\f$
  integer, intent(in) :: lctemg(ntld*ignd*ictem) !< Variable description\f$[units]\f$
  !
  !     * work field for i/o.
  !
  real, intent(in) :: gll(*) !< Variable description\f$[units]\f$
  !
  !     * input/output work arrays (must be contiguous).
  !
  integer, intent(inout), dimension(8) :: ibuf !< Variable description\f$[units]\f$
  integer, intent(in), dimension(1) :: idat !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  logical :: ok !<
  !-----------------------------------------------------------------------------------
  if (in==+1) then

    !.........read the label at the current file pointer then reposition the
    !         pointer at the start of this label
    !
    call fbuffin (-lu,jbuf,-8,kk,klen)
    if (kk>=0) then
      ibufbad=jbuf(3)
      go to 500
    end if
    itime=jbuf(2)
    backspace (lu)

    !         *************************************************************
    !         * read the packed ctem fields *******************************
    !         *************************************************************

    call setlab(ibuf,nc4to8("GRID"),itime,-1,-1,lonsl+1,nlat,khem,0)

    ntlc1=ntld*ictemp1
    ibuf(3) = nc4to8("CSOC")
    call rpkphs4(lu,soilcpat, ntlc1,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("CLTR")
    call rpkphs4(lu,litrcpat, ntlc1,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ntlc=ntld*ictem
    ibuf(3) = nc4to8("CRTM")
    call rpkphs4(lu,rootcpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("CSTM")
    call rpkphs4(lu,stemcpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("CGLF")
    call rpkphs4(lu,gleafcpat, ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("CBLF")
    call rpkphs4(lu,bleafcpat, ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HFHL")
    call rpkphs4(lu,fallhpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HPAD")
    call rpkphs4(lu,posphpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HLST")
    call rpkphs4(lu,leafspat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HGEF")
    call rpkphs4(lu,growtpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HCST")
    call rpkphs4(lu,lastspat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HCRT")
    call rpkphs4(lu,lastrpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ibuf(3) = nc4to8("HLAI")
    call rpkphs4(lu,thisylpat, ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if
    !
    !         * THE FOLLOWING TWO SETS ARE FOR BARE-SOIL PFT'S (6,7) ONLY.
    !
    stemhpat=0.
    ntlc=ntld*2
    lctemx=ntld*5+1
    ibuf(3) = nc4to8("HSHL")
    call rpkphs4(lu,stemhpat(1,1,6),ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem(lctemx),gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    roothpat=0.
    ibuf(3) = nc4to8("HRHL")
    call rpkphs4(lu,roothpat(1,1,6),ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem(lctemx),gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if
    !
    !         * the following set is for two cold days only (1,2).
    !
    tempcpat=0.
    ibuf(3) = nc4to8("HTMP")
    call rpkphs4(lu,tempcpat,  ntlc,ibuf,lonsl,nlat,ilat,lev, &
                       lctem,gll,ok)
    if (.not.ok) then
      ibufbad=ibuf(3)
      go to 500
    end if

    ntlc=ntld*ictem
    !
    !         * initialize seven ctem extra fields which may not be in the restart.
    !
    cfluxcspat= 0.
    cfluxcgpat= 0.
    co2cg1pat = 0.
    co2cg2pat = 0.
    co2cs1pat = 0.
    co2cs2pat = 0.
    fcolpat   = 0.
    !
    !         * read the label at the current file pointer then reposition the
    !         * pointer at the start of this label.
    !
    call fbuffin (-lu,jbuf,-8,kk,klen)
    if (kk>=0) go to 500
    backspace (lu)
    if (jbuf(3)/=nc4to8("ARCS")) then
      go to 410
    else

      ibuf(3) = nc4to8("ARCS")
      call rpkphs4(lu,cfluxcspat,ntld,ibuf,lonsl,nlat,ilat,lev, &
                         lct,gll,ok)
      !
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ibuf(3) = nc4to8("ARCG")
      call rpkphs4(lu,cfluxcgpat,ntld,ibuf,lonsl,nlat,ilat,lev, &
                         lct,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ntlc=ntld*ictem
      ibuf(3) = nc4to8("CCG1")
      call rpkphs4(lu,co2cg1pat,ntlc,ibuf,lonsl,nlat,ilat,lev, &
                         lctem,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ibuf(3) = nc4to8("CCG2")
      call rpkphs4(lu,co2cg2pat,ntlc,ibuf,lonsl,nlat,ilat,lev, &
                         lctem,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ibuf(3) = nc4to8("CCS1")
      call rpkphs4(lu,co2cs1pat,ntlc,ibuf,lonsl,nlat,ilat,lev, &
                         lctem,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ibuf(3) = nc4to8("CCS2")
      call rpkphs4(lu,co2cs2pat,ntlc,ibuf,lonsl,nlat,ilat,lev, &
                         lctem,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

      ibuf(3) = nc4to8("FCOL")
      call rpkphs4(lu,fcolpat,   ntld,ibuf,lonsl,nlat,ilat,lev, &
                         lct,gll,ok)
      if (.not.ok) then
        ibufbad=ibuf(3)
        go to 500
      end if

    end if
410 continue

    500     if (.not.ok) then
      print *, '0BAD RESTART ORDER FOR FIELD: ',ibufbad
      call xit('RESTART_CTEM',-50)
    end if

    close(lu)
    !----------------------------------------------------------------------
  else if (in==-1) then

    !                         ========================
    !                         ========================
    !                         **** end of the job ****
    !                         ========================
    !                         ========================

    open(lu,file='NEWTS',form='UNFORMATTED')
    rewind lu

    !         *************************************************************
    !         * write the packed physics **********************************
    !         *************************************************************

    call setlab(ibuf,nc4to8("GRID"),kount,-1,-1,lonsl+1,nlat,khem,0)

    ntlc1=ntld*ictemp1
    ibuf(3) = nc4to8("CSOC")
    call wpkphs4(lu,soilcpat, ntlc1,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("CLTR")
    call wpkphs4(lu,litrcpat, ntlc1,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ntlc=ntld*ictem
    ibuf(3) = nc4to8("CRTM")
    call wpkphs4(lu,rootcpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("CSTM")
    call wpkphs4(lu,stemcpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("CGLF")
    call wpkphs4(lu,gleafcpat, ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("CBLF")
    call wpkphs4(lu,bleafcpat, ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HFHL")
    call wpkphs4(lu,fallhpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HPAD")
    call wpkphs4(lu,posphpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HLST")
    call wpkphs4(lu,leafspat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HGEF")
    call wpkphs4(lu,growtpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HCST")
    call wpkphs4(lu,lastspat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HCRT")
    call wpkphs4(lu,lastrpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ibuf(3) = nc4to8("HLAI")
    call wpkphs4(lu,thisylpat, ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)
    !
    !         * THE FOLLOWING TWO SETS ARE FOR BARE-SOIL PFT'S (6,7) ONLY.
    !
    ntlc=ntld*2
    lctemx=ntld*5+1
    ibuf(3) = nc4to8("HSHL")
    call wpkphs4(lu,stemhpat(1,1,6),ntlc,ibuf,lonsl,nlat,ilat, &
                       ilev,lev,lctem(lctemx))

    ibuf(3) = nc4to8("HRHL")
    call wpkphs4(lu,roothpat(1,1,6),ntlc,ibuf,lonsl,nlat,ilat, &
                       ilev,lev,lctem(lctemx))
    !
    !         * the following set is for two cold days only (1,2).
    !
    ibuf(3) = nc4to8("HTMP")
    call wpkphs4(lu,tempcpat,  ntlc,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lctem)

    ntlc=ntld*ictem
    ibuf(3) = nc4to8("ARCS")
    call wpkphs4(lu,cfluxcspat,ntld,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lct)

    ibuf(3) = nc4to8("ARCG")
    call wpkphs4(lu,cfluxcgpat,ntld,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lct)

    ntlc=ntld*ictem
    ibuf(3) = nc4to8("CCG1")
    call wpkphs4(lu,co2cg1pat,ntlc,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lctem)

    ibuf(3) = nc4to8("CCG2")
    call wpkphs4(lu,co2cg2pat,ntlc,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lctem)

    ibuf(3) = nc4to8("CCS1")
    call wpkphs4(lu,co2cs1pat,ntlc,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lctem)

    ibuf(3) = nc4to8("CCS2")
    call wpkphs4(lu,co2cs2pat,ntlc,ibuf,lonsl,nlat,ilat,ilev, &
                       lev,lctem)

    ibuf(3) = nc4to8("FCOL")
    call wpkphs4(lu,fcolpat,   ntld,ibuf,lonsl,nlat,ilat,ilev,lev, &
                       lct)

    close(lu)

  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

