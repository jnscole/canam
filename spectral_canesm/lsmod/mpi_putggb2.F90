#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine mpi_putggb2(ggpak,ilg1,ilat,khem,npgg,kount, &
                             nf,name,lev,gll,wrks,tag)
  !
  !     * apr 26/10 - m.lazare.     new version for gcm15i:
  !     *                           - remove unnecessary call
  !     *                             to taginfo_h.
  !     * jan 09/04 - d.robitaille. original version mpi_putggb:
  !     *                           added mpi calls for coupler
  !     *                           to putggb
  !
  !     * unpacks a gaussian grid in ggpak row by row from the gcm.
  !     * the unpacked grid is saved on file nf.
  !     * the first point in each row is copied into the last point.
  !
  !     * level 2,ggpak,gll
  use com_cpl, only: send_fld

  implicit none
  integer :: i
  integer :: igrid
  integer :: ij
  integer :: ilg
  integer, intent(in) :: ilg1  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: ioff
  integer :: j
  integer :: kfld
  integer, intent(in) :: khem
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: name
  integer, intent(in) :: nf
  integer :: nlatx
  integer :: npack
  integer, intent(in) :: npgg
  integer :: nc4to8

  include 'mpif.h'
  integer, intent(in), dimension(3) :: tag !< Variable description\f$[units]\f$

#include "coupling_h.h"
#if defined use_canam_32bit
integer*4, intent(in) :: ilat !<
integer*4 :: lonsl !<
integer*4 :: nlat !<
integer*4 :: ilev !<
integer*4 :: levs !<
integer*4 :: lrlmt !<
integer*4 :: icoord !<
integer*4 :: lay !<
integer*4 :: moist !<
real*4 :: ptoit !<
#endif
  common /sizes/  lonsl,nlat,ilev,levs,lrlmt,icoord,lay,ptoit,moist

  integer*4 :: mynode !<
  common /mpinfo/ mynode

  !     * common block to hold the number of nodes and communicator.

  integer*4 :: nnode !<
  integer*4 :: agcm_commwrld !<
  integer*4 :: my_cmplx_type !<
  integer*4 :: my_real_type !<
  integer*4 :: my_int_type !<
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  real, intent(in) :: ggpak(ilat*(ilg1-1)) !< Variable description\f$[units]\f$
  real, intent(in), dimension(1) :: wrks !< Variable description\f$[units]\f$
  integer, dimension(8) :: ibuf !<

  real, intent(inout), target  :: gll(ilg1*ilat) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !     real, target  :: xpak(nlat*ilg1)

  !     * hard code "XPAK" dimension due to problem with "NANS" initialization
  !     * UNDER AIX. IT'S SET TO HANDLE UP TO 641X320 (T213 GRID).

  real, target  , dimension(205120) :: xpak !<
  real, pointer , dimension(:) :: gbuf !<

  real, dimension(205120) :: gg !<

  integer*4 :: sz !<
  integer*4 :: master !<
  !---------------------------------------------------------------------
  if (ilg1*nlat>205120) call                xit('MPI_PUTGGB2',-1)
  master = 0
  sz     = ilat*ilg1
  igrid=nc4to8("GRID")
  !
  !     * store values of ggpak into gll, adding cyclic longitude.
  !
  ilg=ilg1-1
  do j=1,ilat
    ij   = (j-1)*ilg1
    ioff = (j-1)*ilg
    do i=1,ilg
      gll(ij+i)=ggpak(ioff+i)
    end do ! loop 200
    gll(ij+ilg1)=gll(ij+1)
  end do ! loop 210
  !
  !     * gather gll into gbuf (=>xbuf) from all nbr nodes.
  !
  if (nnode  > 1) then
    gbuf => xpak(1:ilg1*nlat)
#if defined use_canam_32bit
          call wrap_gather(gll   , sz,            mpi_double_precision, &
                      gbuf  , sz,            mpi_double_precision, &
                      master, agcm_commwrld, ierr)
#else
          call wrap_gather(gll   , sz,            my_real_type, &
                      gbuf  , sz,            my_real_type, &
                      master, agcm_commwrld, ierr)
#endif
  else
    gbuf => gll
  end if
  !
  kfld=igrid
  npack=0
  if (mynode == 0) then
    nlatx=nlat
    call cgtorg_cpl(gg,gbuf,ilg1,nlatx)

    call setlab(ibuf,kfld,kount,tag(2),lev,ilg1,nlat,khem,0)
    call send_fld (gg, ibuf, coupler, tag)

  end if
  return
  !---------------------------------------------------------------------
end

subroutine cgtorg_cpl(pako,paki,lon1,ilat)

  !     * april 08, 2003 - m.lazare.
  !
  !     * this routine takes an ordered s-n pair grid field, "PAKI"
  !     * and converts it into a normal grid, "PAKO",
  !     * which is subsequently written to output.
  !     * this routine is the opposite operation of "RGTOCG".
  !
  implicit none
  integer :: i
  integer, intent(in) :: ilat
  integer :: ilath
  integer :: ilatq
  integer :: j
  integer :: jne
  integer :: jnp
  integer :: jse
  integer :: jsp
  integer, intent(in) :: lon1
  integer :: ni
  !
  real, intent(inout) :: pako(lon1,ilat) !<
  real, intent(in) :: paki(lon1*ilat) !<
  !==================================================================
  ni=0
  if (mod(ilat,4)/=0) call                    xit('CGTORG_CPL',-1)
  ilath=ilat/2
  ilatq=ilat/4
  !
  do j=1,ilatq
    jsp=j
    do i=1,lon1
      ni=ni+1
      pako(i,jsp) = paki(ni)
    end do
    !
    jnp=ilat-j+1
    do i=1,lon1
      ni=ni+1
      pako(i,jnp) = paki(ni)
    end do
    !
    jse=ilath-j+1
    do i=1,lon1
      ni=ni+1
      pako(i,jse) = paki(ni)
    end do
    !
    jne=ilath+j
    do i=1,lon1
      ni=ni+1
      pako(i,jne) = paki(ni)
    end do
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
