!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module relax_parm_defs

  !--- slava kharin  ...20 sep 2018

  !-------------------------------------------------------------
  !--- set up namelists to hold relaxation parameters
  !-------------------------------------------------------------

  !--- this module will hold a list of relaxation parameters
  !--- used in the agcm.

  !--- define defaults.
  implicit none
  integer :: relax_spc !<
  integer :: relax_spc_bc !<
  integer :: relax_tg1 !<
  integer :: relax_tg1_bc !<
  integer :: relax_wg1 !<
  integer :: relax_wg1_bc !<
  integer :: relax_ocn !<
  integer :: relax_ocn_bc !<
  integer :: iperiod_spc !<
  integer :: iperiod_lnd !<
  integer :: iperiod_lnd_clm !<
  integer :: iperiod_ocn !<
  integer :: iperiod_ocn_clm !<
  integer :: ntrunc_p !<
  integer :: ntrunc_c !<
  integer :: ntrunc_t !<
  integer :: ntrunc_es !<
  integer :: ntrunc_ps !<
  integer :: itrunc_tnd !<
  integer :: itrunc_anl !<
  integer :: ivfp !<

  !===============================================================
  ! Vars added during syntax upgrade to satisfy implicit none
  integer :: li
  integer :: irx3
  integer :: irx3_lnd
  integer :: irx3_lnd_clm
  integer :: irx3_ocn
  integer :: irx3_ocn_clm
  integer :: iyr0
  integer :: imn0
  integer :: idy0
  integer :: ihr0
  integer :: iymdh0
  integer :: iymdh0_clm
  integer :: iymdh0_spc
  integer :: iymdh0_spc_clm
  integer :: iymdh0_lnd
  integer :: iymdh0_lnd_clm
  integer :: iymdh0_ocn
  integer :: iymdh0_ocn_clm
  integer :: iymdh0_ocn_sicn
  integer :: iymdh0_ocn_sic
  integer :: irx1
  integer :: irx2
  integer :: irx1_ocn
  integer :: irx2_ocn
  integer :: irx1_clm
  integer :: irx2_clm
  integer :: irx1_lnd
  integer :: irx2_lnd
  integer :: irx1_ocn_clm
  integer :: irx2_ocn_clm
  integer :: irx1_lnd_clm
  integer :: irx2_lnd_clm
  integer :: irelax_kount_lnd_clm
  integer :: kchunk
  integer :: kchunk_ocn
  integer :: kchunk_clm
  integer :: kchunk_lnd
  integer :: kchunk_ocn_clm
  integer :: kchunk_lnd_clm
  integer :: kount_rs
  integer :: kount_rs_ocn
  integer :: kount_rs_clm
  integer :: kount_rs_lnd
  integer :: kount_rs_ocn_clm
  integer :: kount_rs_lnd_clm
  integer :: kount_re
  integer :: kount_re_ocn
  integer :: kount_re_clm
  integer :: kount_re_lnd
  integer :: kount_re_ocn_clm
  integer :: kount_re_lnd_clm
  integer :: kount_offset_lnd_clm
  real :: cp
  real :: cp_ua
  real :: depth
  real :: t_frz
  real :: hf_fct
  real :: fct_t_p
  real :: fct_t_c
  real :: fct_t_t
  real :: fct_a_p
  real :: fct_a_c
  real :: fct_a_t
  real :: fct_t_es
  real :: fct_a_es
  real :: fct_t_ps
  real :: fct_a_ps
  real :: fct_sst
  real :: fct_sic
  real :: fct_edge
  real :: fct_tg1
  real :: fct_wg1
  real :: fmw
  real :: fmi
  real :: fmo
  real :: fow
  real :: foi
  real :: fww
  real :: fii
  real :: fwi
  real :: rwgl
  real :: rwgf
  real :: rlx1
  real :: rlx1_ocn
  real :: rlx1_lnd
  real :: rlx1_lnd_clm
  real :: rlx2
  real :: rlx2_ocn
  real :: rlx2_lnd
  real :: rlx2_lnd_clm
  real :: rkchunk
  real :: rkchunk_ocn
  real :: rkchunk_clm
  real :: rkchunk_lnd
  real :: rkchunk_ocn_clm
  real :: rkchunk_lnd_clm
  real :: rho_ref
  real :: wglf
  real :: wglf2vfsm
  real :: vfsm
  real :: taui_p
  real :: taui_c
  real :: taui_t
  real :: taui_es
  real :: taui_ps
  real :: sic_015
  real :: sic_100
  real :: sic_min
  real :: sghalft_p
  real :: sghalft_c
  real :: shhalft_t
  real :: shhalft_e
  real :: sghalfb_p
  real :: sghalfb_c
  real :: shhalfb_t
  real :: shhalfb_e
  real :: taui_sst
  real :: taui_sic
  real :: taui_edge
  real :: taui_tg1
  real :: taui_wg1

  !===============================================================

  real :: relax_p !<
  real :: relax_c !<
  real :: relax_t !<
  real :: relax_es !<
  real :: relax_ps !<
  real :: tauh_p !<
  real :: tauh_c !<
  real :: tauh_t !<
  real :: tauh_es !<
  real :: tauh_ps !<
  real :: sgtop !<
  real :: shtop !<
  real :: sgfullt_p !<
  real :: sgfullt_c !<
  real :: shfullt_t !<
  real :: shfullt_e !<
  real :: sgdampt_p !<
  real :: sgdampt_c !<
  real :: shdampt_t !<
  real :: shdampt_e !<
  real :: sgbot !<
  real :: shbot !<
  real :: sgfullb_p !<
  real :: sgfullb_c !<
  real :: shfullb_t !<
  real :: shfullb_e !<
  real :: sgdampb_p !<
  real :: sgdampb_c !<
  real :: shdampb_t !<
  real :: shdampb_e !<
  real :: tauh_tg1 !<
  real :: tauh_wg1 !<
  real :: tauh_sst !<
  real :: tauh_sic !<
  real :: tauh_edge !<
  real :: rlx_sst !<
  real :: rlx_sic !<
  real :: rlx_tbeg !<
  real :: rlx_tbei !<
  real :: rlx_tbes !<
  real :: rlc_tbeg !<
  real :: rlc_tbei !<
  real :: rlc_tbes !<
  real :: rlx_sbeg !<
  real :: rlx_sbei !<
  real :: rlx_sbes !<
  real :: rlc_sbeg !<
  real :: rlc_sbei !<
  real :: rlc_sbes !<

  !--- namelist used to read in parmsub parameters
  !--- this namelist is read in init_relax_parm

  namelist /relax_parm/ &
      relax_spc, relax_spc_bc, &
      relax_tg1, relax_tg1_bc, &
      relax_wg1, relax_wg1_bc, &
      relax_ocn, relax_ocn_bc, &
      iperiod_spc, &
      iperiod_lnd,iperiod_lnd_clm, &
      iperiod_ocn,iperiod_ocn_clm, &
      ntrunc_p,ntrunc_c,ntrunc_t,ntrunc_es,ntrunc_ps, &
      itrunc_tnd,itrunc_anl,ivfp, &
      relax_p,relax_c,relax_t,relax_es,relax_ps, &
      tauh_p, tauh_c, tauh_t, tauh_es, tauh_ps, &
      sgtop,shtop, &
      sgfullt_p,sgfullt_c,shfullt_t,shfullt_e, &
      sgdampt_p,sgdampt_c,shdampt_t,shdampt_e, &
      sgbot,shbot, &
      sgfullb_p,sgfullb_c,shfullb_t,shfullb_e, &
      sgdampb_p,sgdampb_c,shdampb_t,shdampb_e, &
      tauh_tg1,tauh_wg1, &
      tauh_sst,tauh_sic,tauh_edge, &
      rlx_sst,       rlx_sic, &
      rlx_tbeg,      rlx_tbei,      rlx_tbes, &
      rlc_tbeg,      rlc_tbei,      rlc_tbes, &
      rlx_sbeg,      rlx_sbei,      rlx_sbes, &
      rlc_sbeg,      rlc_sbei,      rlc_sbes

end module relax_parm_defs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
