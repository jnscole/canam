#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * nov 22/2018 - m.lazare.    put back in ico2 redefinition for specifiec co2.
!     * nov 05/2018 - m.lazare.    added 3hr save fields.
!     * aug 19/2018 - m.lazare.    remove ico2 redefinition for specified co2
!     *                            since now done in revised rstarth.
!     * aug 14/2018 - m.lazare.    remove maskpak.
!     * jul 27/2019 - m.lazare.    remove {com,csp,csn} parmsub variables.
!     * nov 08/2017 - m.lazare.    add cmplx intinsic specification for "TRAC"
!     *                            ico2 redefinition for specified co2.
!     * aug 10/2017 - m.lazare.    - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     * aug 07/2017 - m.lazare.    initial git verison.
!     * feb 04/2014 - m.lazare.    new version for gcm18:
!     *                            - added flnd,flak,licn to invariant section.
!     * jul 10/2013 - m.lazare/    previous version saveinp4 for gcm17:
!     *               k.vonsalzen. - "xtraplafrc" fields added.
!     *                            - removed dzg,porg and added wsno/zpnd
!     *                              for new class version.
!     *                            - odst and abst are calculated as
!     *                              instantaneous results in the radiation
!     *                              so removed here and put in saveinp4
!     *                              instead.
!     * may 07/2012 - m.lazare.    previous version saveinp3 for gcm16:
!     *                            - save hrs and hrl.
!     * may 04/2010 - m.lazare.    previous version saveinp2i for gcm15i:
!     *                            - "OMET" now saved since is true
!     *                              vertical velocity.
!     *                            - "CLDS" removed and "ZDET","CLCV",
!     *                              "SFRC" saving commented out to
!     *                              save space.
!     *                            - if running "SPECIFIED_CO2" experiment
!     *                              for carbon cycyle, store the mmr
!     *                              value of co2 into the (0,0) coefficient
!     *                              of the tracer array (carbon portion) and
!     *                              zero out the other coefficients. this
!     *                              will allow all the diagnostics to work
!     *                              in the same manner with/without the
!     *                              "SPECIFIED_CO2" switch.
!     * mar 24/2009 - m.lazare/    previous version saveinp2h for gcm15h:
!     *               k.vonsalzen. - new saved fields "VTAU" and "TROP",
!     *                              under control of "%IF DEF,EXPLVOL".
!     *                            - new saved field "SFRCPAL" under
!     *                              control of "%IF DEF,XTRACONV".
!     *                            - save maskpak in invariant section.
!     *                            - save dpthpak in invariant section.
!     * jan 17/2008 - m.lazare/    previous version saveinp2g for gcm15g:
!     *               k.vonsalzen. - saves new almx,almc fields.
!     *                            - code related to s/l removed.
!     *                            - removes saving of: sclf,scdn,slwc,
!     *                              ebc,eoc,eocf,edms,dmso.
!     * sep 11/2006 - m.lazare.    previous version saveinp2f for gcm15f:
!     *                            - calls "NAME2" instead of "NAME".
!     * dec 15/2005 - m.lazare/    previous version saveinp2d for gcm15d/e:
!     *               k.vonsalzen. - add cvar for new statistical
!     *                              cloud scheme.
!     *                            - add scdn,sclf,slwc for shallow-cloud
!     *                              effects on radiation.
!     * feb 15,2005 - m.lazare.    previous version for gcm15c:
!     *                           - add multi-level diagnostic terms
!     *                             for large-scale under control
!     *                             of %if def,xtrals.
!     *                            - rewind nuinv at kount=0 so
!     *                              invariant fields saved only once.
!     *                            - save new input fields chfx,cqfx.
!     * apr 01,2004 - j.scinocca.  final version of previous saveinp2b:
!     *                            calls new putnatr3 in support of
!     *                            {itrlvs,itrlvf} subset of vertical
!     *                            domain for non-advected tracers.
!     * feb 16,2004 - m.lazare.    "SALB" and ""omet" SAVING COMMENTED
!     *                            out and "ZDET","CLCV" saving now
!     *                            optional under control of "%IF DEF,
!     *                            xtraconv" (ALL THESE GENERALLY NOT
!     *                            needed).
!     * dec 20,2003 - m.lazare/    new version for gcm15b (saveinp2b):
!     *               k.vonsalzen. - salbpal added and alswpal,allwpal
!     *                              removed.
!     *                            - add qwf0,qwfm,xwf0,xwfm fields
!     *                              for new conservation.
!     *                            - add new fields: tfx,qfx,cbmf,zdet,
!     *                              clcv,omet,clds,etc.
!     *                            - new input arrays saved under control
!     *                              of update directive "XTRASULF".
!     * jan 05/2004 - m.lazare.    previous version saveinpd for gcm13d.
!===================================================================
!     * save invariant fields at first timestep.
!
if (kount==kstart) then
  k=0
  rewind nuinv
  call putgg(dpthpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("DPTH"),1,gll,wrks)
  call putgg(flkrpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("FLKR"),1,gll,wrks)
  call putgg(flkupak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("FLKU"),1,gll,wrks)
  call putgg(blakpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("BLAK"),1,gll,wrks)
  call putgg(hlakpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("HLAK"),1,gll,wrks)
  call putgg(llakpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("LLAK"),1,gll,wrks)
#if defined (cslm)
          call putgg(nlklpak,lon1,ilat,khem,npgg,k,nuinv, &
                  nc4to8("NLAK"),1,gll,wrks)
#endif
  call putgg(flndpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("FLND"),1,gll,wrks)
  call putgg(gicnpak,lon1,ilat,khem,npgg,k,nuinv, &
                     nc4to8("GICN"),1,gll,wrks)
end if
!
!     * save spectral fields every issp timesteps.
!
if (lssp) then
  call putstg9(nupr,ps,p,c,t,es,phis, &
                     kount,la,lrlmt,ilev,levspec,ls,lh, &
                     lsrtotal,latotal,lmtotal,gll)
end if
!
!     * save tracer fields every istr timesteps.
!
if (ntraca>0) then
#if defined carbon
#if defined specified_co2
!  * IF RUNNING INVERSE EXPERIMENT FOR CARBON CYCLE, WE WANT TO STORE
!  * THE MMR VALUE OF CO2 INTO THE (0,0) COEFFICIENT OF THE TRACER
!  * ARRAY (FOR THE CARBON PORTION) AND ZERO OUT THE OTHER COEFFICIENTS.
!  * THIS WILL ENABLE ALL ASSOCIATED DIAGNOSTICS TO WORK THE SAME WITH/
!  * WITHOUT THE INVERSE CALCULATION.
	do na = 1,ntrspec
	  n=indxa(na)
	  if(n == ico2) then
	    if( itrvar == nc4to8("   Q") .or. (itrvar == nc4to8("QHYB") .and. xref(n) == 0.) ) then
	      real_zero = sqrt(2.)*rmco2
	      real_rest = 0.
	    else
	      pinv  = 1./xpow(n)
	      xxref = xref(n)/((1.+xpow(n)*log(xref(n)/rmco2))**pinv)
	      real_zero = sqrt(2.)*xxref
	      real_rest = xmin(n)
            end if
	    do l = 1,ilev
	      do i = 1,la
	        mnk = (l-1)*la+i
	        trac(mnk,na) = cmplx(real_rest,0.)
	      enddo
	      if(mynode == 0) then
	        mnk = (l-1)*la+1
	        trac(mnk,na) = cmplx(real_zero,0.)
	      end if
	    end do
	  end if
	end do
#endif
#endif
  call putrac4x(nupr,trac,la,lrlmt,ilev,lh,kount,lstr, &
                lsrtotal,latotal,lmtotal,gll, &
                itrspec,ntrspec,ntrac,itrnam,indxa)
end if

!
! * generally, we now save non-advected fields. change isavna
! * via updates if this is not desired.
!
isavna=1
if(ntracn > 0 .and. isavna /= 0) then
   call putnatr3(nupr,tracna(1,inp),lon1,ilat,ip0j,ilev, &
                 lh,kount,lstr,khem,ntrac, &
                 itrnam,itrlvs,itrlvf,indxna,ntracn,gll,wrks)
end if
!
if (ls3hr) then
  !
  !        * save selected fields on file nu3hr every is3hr (3-hour) steps.
  !
  if (isavdts/=0) then
    ibuf2=iymdh
  else
    ibuf2=kount
  end if
  !
  call setlab(ibuf,nc4to8("SPEC"),ibuf2,-1,1,-1,1,lrlmt,0)
  ibuf(3)=nc4to8("LNSP")
  call putspn(nu3hr,ps,la, &
                     lsrtotal,latotal,lmtotal,gll, &
                     ibuf,maxx,ipio,ok)

end if
!
if (lsgg) then
  !
  !        * save i/o grids on file nupr every isgg steps, if requested.
  !
  k=kount
#if defined explvol
!
         call putgg(vtaupak,lon1,ilat,khem,npgg,k,nupr,nc4to8("VTAU") &
                        ,1,gll,wrks)
         call putgg(troppak,lon1,ilat,khem,npgg,k,nupr,nc4to8("TROP") &
                        ,1,gll,wrks)
#endif
  !
  do l=1,ilev
    call putgg(cldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" CLD"),lh(l),gll,wrks)
    call putgg(tacnpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("TACN"),lh(l),gll,wrks)
    call putgg(rhpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("  RH"),lh(l),gll,wrks)
    call putgg(clwpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" CLW"),lh(l),gll,wrks)
    call putgg(cicpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" CIC"),lh(l),gll,wrks)
    call putgg(cvarpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("CVAR"),lh(l),gll,wrks)
    call putgg(almxpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("ALMX"),lh(l),gll,wrks)
    call putgg(almcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("ALMC"),lh(l),gll,wrks)
    call putgg(ometpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("OMET"),lh(l),gll,wrks)
    call putgg(hrlpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" HRL"),lh(l),gll,wrks)
    call putgg(hrspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" HRS"),lh(l),gll,wrks)
#if defined qconsav
            call putgg(qwf0pal(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("QWF0"),lh(l),gll,wrks)
            call putgg(qwfmpal(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("QWFM"),lh(l),gll,wrks)
#endif
  end do
  !
  ! * tke fields.
  !
  if(itke == 1) then
    do l=1,ilev
      call putgg(xlmpak(1,l), lon1,ilat,khem,npgg,k,nupr, &
                              nc4to8(" XLM"),lh(l),gll,wrks)
      call putgg(svarpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                              nc4to8("SVAR"),lh(l),gll,wrks)
    end do
  end if
#if defined xconsav
         do n=1,ntrac
            call name2(nam,'W0',n)
            do l=1,ilev
               call putgg(xwf0pal(1,l,n),lon1,ilat,khem,npgg,k,nupr, &
                                      nam,lh(l),gll,wrks)
            end do
!
            call name2(nam,'WM',n)
            do l=1,ilev
               call putgg(xwfmpal(1,l,n),lon1,ilat,khem,npgg,k,nupr, &
                                      nam,lh(l),gll,wrks)
            end do
         end do
#endif
#if defined xtraconv

!        do l=1,ilev
!           call putgg(zdetpak(1,l),lon1,ilat,khem,npgg,k,nupr,
!    1                                nc4to8(" DET"),lh(l),gll,wrks)
!           call putgg(clcvpak(1,l),lon1,ilat,khem,npgg,k,nupr,
!    1                                nc4to8(" CCV"),lh(l),gll,wrks)
!        end do
!        do n=1,ntrac
!           CALL NAME2(NAM,'SF',N)
!           do l=1,ilev
!              call putgg(sfrcpal(1,l,n),lon1,ilat,khem,npgg,k,nupr,
!    1                                     nam,lh(l),gll,wrks)
!           end do
!        end do
#endif
#if defined xtrasulf

         do l=1,ilev

            call putgg(ohpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("  OH"),lh(l),gll,wrks)

            call putgg(h2o2pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("H2O2"),lh(l),gll,wrks)

            call putgg(o3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("  O3"),lh(l),gll,wrks)

            call putgg(no3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8(" NO3"),lh(l),gll,wrks)

            call putgg(hno3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("HNO3"),lh(l),gll,wrks)

            call putgg(nh3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8(" NH3"),lh(l),gll,wrks)

            call putgg(nh4pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8(" NH4"),lh(l),gll,wrks)

            call putgg(so4cpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("SO4C"),lh(l),gll,wrks)
         end do
#endif
end if
if (lshf) then
  ! * save high-frequency fields on file nupr every isgg steps, if requested.
  if(itke == 1) then
    k=kount
    do l=1,ilev
      call putgg(tkempak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                            nc4to8("TKEM"),lh(l),gll,wrks)
    end do
  end if
end if

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
