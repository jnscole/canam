!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * jul 27/19 - m.lazare. remove {com,csp,csn} parmsub variables.
!     * nov 28/03 - m.lazare/ new version for gcm13c, with gcmparm
!     *            l.solheim. variables replaced by explicit parameter
!     *                       statement values.
!     * may 26/03 - m.lazare. new common deck (com13db) containing declarations
!     *                       pertaining to non-linear dynamics.
!     *                       longitude and level are explicitly split
!     *                       to facilitate conversion to/from pointers.
!======================================================================
!     * grid slice fields.

!     * the following fields are mapped into the "FOUR2" work array
!     * in mhanl7.
!==================================================================
! physical (adjustable) parameters
!
! define and document here any adjustable parameters.
! this should be variable described using the doxygen format above as
! well as a description of its minimum/default/maximum.
!
! here is an example,
!
! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
!           !! It is compute differently when using bulk or PAM aerosols.
!           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
!==================================================================

common /grd/ tvtg  (ilg_td,ilev)
common /grd/ svtg  (ilg_td,levs)
common /grd/ xvtg  (ilg_td,ilev,ntraca)
common /grd/ utmp  (ilg_td,ilev)
common /grd/ vtmp  (ilg_td,ilev)

!     * the following fields are mapped into the "FOUR3" work array
!     * in mhanl7.

common /grd/ tutg  (ilg_td,ilev)
common /grd/ sutg  (ilg_td,levs)
common /grd/ xutg  (ilg_td,ilev,ntraca)
common /grd/ eg    (ilg_td,ilev)

!     * the following fields are mapped into the "FOUR1" work array
!     * in mhanl7.

common /grd/ ttgd   (ilg_td,ilev)
common /grd/ estgd  (ilg_td,levs)
common /grd/ tractgd(ilg_td,ilev,ntraca)
common /grd/ vtgd   (ilg_td,ilev)
common /grd/ utgd   (ilg_td,ilev)
common /grd/ pstgd  (ilg_td)
!
common /grd/ pressgd(ilg_td)
common /grd/ pgd    (ilg_td,ilev),cgd    (ilg_td,ilev)
common /grd/ tgd    (ilg_td,ilev),esgd   (ilg_td,levs)
common /grd/ tracgd (ilg_td,ilev,ntraca)
common /grd/ psdpgd (ilg_td)
common /grd/ ugd    (ilg_td,ilev),vgd    (ilg_td,ilev)
common /grd/ psdlgd (ilg_td)
!$omp threadprivate (/grd/)
!
!     * the fields "DSGJ" and "DSHJ" are used in both the physics
!     * and non-linear dynamics.
!
common /gr1/ dsgj (idlm), dshj (idlm), dlnsgj(idlm)
common /gr1/ d1sgj(idlm), a1sgj(idlm), b1sgj (idlm)
common /gr1/ d2sgj(idlm), a2sgj(idlm), b2sgj (idlm)
!$omp threadprivate (/gr1/)
!=====================================================================
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
