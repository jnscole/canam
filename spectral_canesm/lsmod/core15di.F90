!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine core15di(j, joff, &
                          four1d,   nlevi, &
                          itrspec, ntrspec, &
                          alfmod,    sref,    spow,    smin,   qmin, &
                          fourd1,  fourd2,  fourd3,  nlevo1, nlevo2,  nlevo3)
  !
  !     * jul 27/2019 - m.lazare. remove {com,csp,csn} parmsub variables.
  !     * may 04/2010 - m.lazare. new version for gcm15i:
  !     *                         - call to new com15i.
  !     * feb 23/2009 - m.lazare. previous version core15dh for gcm15h:
  !     *                         - call to new com15h.
  !     * dec 21/2007 - m.lazare. previous version core15dg for gcm15g:
  !     *                         - revised call to new hscala5.
  !     *                         - remove code related to s/l.
  !     *                         - call to new com15g.
  !     * jun 13/2006 - m.lazare. previous version core15df for gcm15f:
  !     *                         - qmin passed in as well.
  !     *                         - calls "COM15F" instead of "COM13C".
  !     *                         - calls new hscala4 with no ah,bh,
  !     *                           tgd,pressgd.
  !     * may 07/2006 - m.lazare. previous version core15dc for gcm15e:
  !     *                         - based on core15db except calls
  !     *                           hscala3 instead of hscala2.
  !     * feb 21/2004 - m.lazare. previous version core15db for gcm15d.
  !
  !     * this routine does the spectral transform (sp->gg) component
  !     * of the non-linear dynamics core. it is executed in parallel.
  !     * work arrays had to be made private for each processor.
  !     * the use of dynamic memory allocation for these arrays provides
  !     * that and also a better use of the memory. some work arrays are
  !     * still declared by common blocks but the use of "THREADPRIVATE"
  !     * makes them private too.
  !
  !     * dictionary for the arguments of this routine
  !     * --------------------------------------------
  !
  !            ***  input arguments ***
  !                 ---------------
  !
  !     -------------------------------------------------------------------
  !
  !         name       type    signification
  !     * ---------  -------   -------------
  !     * j        : integer  : current latitude set index.
  !     *
  !     * four1d   : real     : fourier components of prognostic variables
  !     *                       for this node latitude set (size
  !     *                       (2,nlevi,ilatd,ilhd)
  !     * joff     : integer  : offset into node latitude space for a
  !     *                       given task.
  !     * itrspec  : integer  : switch for spectral tracers.
  !     * ntrspec  : integer  : number of spectrally-advected tracers.
  !     * alfmod   : real     : weight use in vertical discretization.
  !     * sref     : real     : reference moisture value for qhyb.
  !     * spow     : real     : reference power    value for qhyb.
  !     * smin     : real     : minimum   moisture value for qhyb.

  !            ***  output  arguments ***
  !                 -----------------

  !     * fourd1   : real     : fourier components of variable set #1
  !     *                       for this node latitude set (size
  !     *                       (2,nlevo1,ilatd,ilhd)
  !     * fourd2   : real     : fourier components of variable set #2
  !     *                       for this node latitude set (size
  !     *                       (2,nlevo2,ilatd,ilhd)
  !     * fourd3   : real     : fourier components of variable set #3
  !     *                       for this node latitude set (size
  !     *                       (2,nlevo3,ilatd,ilhd)
  !
  !     -------------------------------------------------------------------
  !
  use msizes
  !
  implicit none
  real :: a
  real :: a1sgj
  real :: a2sgj
  real :: acg
  real :: ach
  real :: ag
  real :: ah
  real, intent(in) :: alfmod
  real :: asq
  real :: b1sgj
  real :: b2sgj
  real :: bcg
  real :: bch
  real :: bg
  real :: bh
  real :: bwgk
  real :: cgd
  real :: cpres
  real :: cpresv
  real :: d1sgj
  real :: d2b
  real :: d2sgj
  real :: daylnt
  real :: db
  real :: delt
  real :: disc
  real :: dises
  real :: disp
  real :: dist
  real :: disx
  real :: dlnsgj
  real :: dsgj
  real :: dshj
  real :: eg
  real :: esgd
  real :: estgd
  real :: fc
  real :: fp
  real :: fps
  real :: fs
  real :: ft
  real :: fx
  real :: grav
  real :: hs
  real :: hv
  integer :: i
  integer :: icemod
  integer :: iclw
  integer :: icoord
  integer :: icsw
  integer :: id
  integer :: iday
  integer :: idiv
  integer :: ie
  integer :: iepr
  integer :: ifax
  integer :: ifaxd
  integer :: ifdiff
  integer :: ilevx
  integer :: imdh
  integer :: incd
  integer :: iocean
  integer :: ir
  integer :: isavdts
  integer :: isbeg
  integer :: isen
  integer :: isgg
  integer :: ishf
  integer :: ismon
  integer :: israd
  integer :: issp
  integer :: isst
  integer :: istr
  integer :: itrac
  integer :: itrlvf
  integer :: itrlvs
  integer, intent(in) :: itrspec
  integer :: ixd
  integer :: ixp
  integer :: iyear
  integer, intent(in) :: j
  integer :: jlatpr
  integer, intent(in) :: joff
  integer :: kstart
  integer :: ktotal
  integer :: l
  integer :: lay
  integer :: lday
  integer :: len_ie
  integer :: len_ir
  integer :: len_maxd
  integer :: len_maxp
  integer :: levsx
  integer :: lh
  integer :: lonslx
  integer :: lrlmt
  integer :: ls
  integer :: lsr
  integer :: lsrtotal
  integer :: mday
  integer :: mdayt
  integer :: mindex
  integer :: moist
  integer :: myrssti
  integer :: n
  integer :: ndays
  integer :: newrun
  integer :: ni
  integer :: nj
  integer :: nlatx
  integer, intent(in) :: nlevi
  integer, intent(in) :: nlevo1
  integer, intent(in) :: nlevo2
  integer, intent(in) :: nlevo3
  integer :: nsecs
  integer, intent(in) :: ntrspec
  real :: pgd
  real :: pi
  real :: pressgd
  real :: psdlgd
  real :: psdpgd
  real :: pstgd
  real :: ptoit
  real :: qfso
  real, intent(in) :: qmin
  real :: qscl0
  real :: qsclm
  real :: qsrc0
  real :: qsrcm
  real :: qsrcrat
  real :: qsrcrm1
  real :: rgas
  real :: rgasv
  real :: rgoasq
  real :: rgocp
  real :: rvord
  real :: scrit
  real :: sg
  real :: sgb
  real :: sh
  real :: shb
  real, intent(in) :: smin
  real, intent(in) :: spow
  real, intent(in) :: sref
  real :: sutg
  real :: svtg
  real :: tficqm
  real :: tficxm
  real :: tfrez
  real :: tgd
  real :: tmin
  real :: tracgd
  real :: tracna
  real :: tractgd
  real :: trigs
  real :: trigsd
  real :: ttgd
  real :: tutg
  real :: tvtg
  real :: tw
  real :: ugd
  real :: utgd
  real :: utmp
  real :: vgd
  real :: vtgd
  real :: vtmp
  real :: wp0
  real :: wpm
  real :: wpp
  real :: ww
  real :: xmin
  real :: xp0
  real :: xpm
  real :: xpow
  real :: xpp
  real :: xref
  real :: xscl0
  real :: xsclm
  real :: xsfx
  real :: xsrc0
  real :: xsrcm
  real :: xsrcrat
  real :: xsrcrm1
  real :: xtvi
  real :: xutg
  real :: xvtg
  !
  !     * output fourier arrays (passed and used as real).
  !
  real, intent(in), dimension(2,nlevo1,ilatd,ilhd) :: fourd1 !<
  real, intent(in), dimension(2,nlevo2,ilatd,ilhd) :: fourd2 !<
  real, intent(in), dimension(2,nlevo3,ilatd,ilhd) :: fourd3 !<
  !
  !     * input fourier array (passed and used as real).
  !
  real, intent(in), dimension(2,nlevi, ilatd,ilhd) :: four1d !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !     * dynamic memory pointer for scratch array "SC".

  real, allocatable, dimension(:) :: sc !<
  !
  !     * associated "ROW" vectors for multi-latitude formulation.
  !
  real*8 , dimension(ilg_td) :: cosjd !<

#include "com15i.h"
#include "com13dc.h"
  !---------------------------------------------------------------------
  !     * get memory dynamically for the scratch array "SC".

  allocate(sc(len_maxd))
  !
  !     * calculate cosjd for each point in a multi-latitude formulation.
  !     * this is used for conversion from model <-> real :: winds.
  !     * *** note !! ! *** NO CHAINING DONE FOR DYNAMICS !! !
  !
  ni = 0
  do nj = 1,nlatjd
    do i = 1,lonsld
      ni = ni + 1
      cosjd(ni) = cld(nj,j)
    end do ! loop 100
  end do ! loop 110
  !
  !     * zero out tendencies before starting to accumulate.
  !
  do l = 1,ilev
    do i = 1,ilgd
      ttgd(i,l) = 0.
      utgd(i,l) = 0.
      vtgd(i,l) = 0.
    end do
  end do ! loop 260

  do l = 1,levs
    do i = 1,ilgd
      estgd(i,l) = 0.
    end do
  end do ! loop 265

  do n = 1,ntrspec
    do l = 1,ilev
      do i = 1,ilgd
        tractgd(i,l,n) = 0.
      end do
    end do
  end do ! loop 270
  !
  !     * compute grid point values from spectral coeff.
  !     * convert wind images to real :: winds for non-linear dynamics.
  !
  call fourf(pressgd,four1d,nlevi, &
                 ilhd,lonsld,lmtotal, &
                 ifaxd,trigsd,nlatjd,ilatd,joff, &
                 sc(ixd(1)),sc(ixd(2))          )
  call imvrai2 (ugd,vgd,psdlgd,psdpgd,cosjd,ilgd,lond,ilev,a)
  !
  do i = 1,lond
    pressgd(i) = exp(pressgd(i))
  end do ! loop 320
  !
  !     * main calculations.
  !
  call sigcalh ( &
                    dsgj,dshj,dlnsgj, &
                    d1sgj,a1sgj,b1sgj, &
                    d2sgj,a2sgj,b2sgj, &
                    ilgd, lond, ilev, &
                    ptoit, &
                    pressgd,ag,bg,ah,bh,alfmod &
                    )
  !
  call hscala5 ( &
                    sc(id(16)),esgd,1,lond,ilgd,ilev,levs, &
                    moist,.true.,sref,spow,qmin,smin)

  call dyncal4d( &
                    utgd, vtgd, ttgd, estgd,  pstgd, tractgd, &
                    xutg, xvtg, eg,   tutg,   tvtg, &
                    sutg, svtg, &
                    ilgd, lond, ilev, levs,itrspec,  ntrspec, &
                    pgd,  cgd,  ugd,  vgd,     tgd,  esgd, &
                    pressgd, psdlgd, psdpgd, tracgd, &
                    sc(id(1)),sc(id(2)),sc(id(3)),sc(id(4)), &
                    sc(id(5)),sc(id(16)),sc(id(17)),sc(id(18)), &
                    sc(id(19)), dsgj, dshj, dlnsgj, &
                    d1sgj, a1sgj, b1sgj, d2sgj, a2sgj, b2sgj, &
                    db, d2b, 0., rgas,rgasv,cpres,cpresv,grav,j)
  !
  !     * rescale tendency terms in old tradition.
  !
  call vraima3(vtgd,utgd,tutg,tvtg,eg,ttgd,sutg,svtg,xutg,xvtg, &
                   itrspec,ntrspec,ilgd,lond,ilev,levs,cosjd,a)
  !
  !     * fourier transform of dynamics grid tendencies.
  !
  call fourb(fourd1,ttgd,nlevo1, &
                 ilhd,nlatjd,ilatd,joff,lmtotal, &
                 lonsld,ifaxd,trigsd, &
                 sc(ixd(1)),sc(ixd(2))               )
  call fourb(fourd2,tvtg,nlevo2, &
                 ilhd,nlatjd,ilatd,joff,lmtotal, &
                 lonsld,ifaxd,trigsd, &
                 sc(ixd(1)),sc(ixd(2))               )
  call fourb(fourd3,tutg,nlevo3, &
                 ilhd,nlatjd,ilatd,joff,lmtotal, &
                 lonsld,ifaxd,trigsd, &
                 sc(ixd(1)),sc(ixd(2))               )

  deallocate(sc)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
