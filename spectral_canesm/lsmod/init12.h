#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * Jul 09/2020 - M.Lazare.   - Generalization of GTPAT to GTPAK instead of 0.
!     *                             This was causing problems when initialization
!     *                             program produced SICN=0 and then coupler had
!     *                             SICN>0. In that case, GTPAT=0. found its way into
!     *                             routine DRCOEF for ice condition, which led to a
!     *                             NaN for WSTAR, etc. (when GTGAT=0.).
!     * May 06/2020 - M.Lazare.   - Remove IF condition on NTLK>1 since always done.
!     * aug 14/2018 - m.lazare.   - remove maskpak and use flnd instead.
!     * aug 07/2018 - m.lazare.   - remove zeroing of fcoo since done in gcm18.dk.
!     *                           - added zeroing of a number of fields existing in
!     *                             restarting which are defined/used in physics. most,
!     *                             like the radiation fields, are defined at gmt=0
!     *                             so these will be overwritten but still need to exist
!     *                             in restart read.
!     *                           - added zeroing of {cldtpal,cldopal} since needed in physics.
!     *                           - added zeroing of other
!     * aug 01/2018 - m.lazare.   - remove unused gc.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     *                           - moved accumulated fields from init12 to zeroacc4.
!     * may 12/2018 - m.lazare.    correct resetting of sand to -3. so
!     *                            that it is not being done over water.
!     * aug 09/2017 - m.lazare.    major changes in initialization of tiled
!     *                            fields.
!     * aug 07/2017 - m.lazare.    new git verison.
!     * mar 07/2015 - m.lazare/    final version for gcm18:
!     *               k.vonsalzen. - define gcpak for saving at kount=0.
!     *                            - define fractional area arrays.
!     *                            - initialize csalpat,salbpat,emispat.
!     *                            - initialize begipal,begopal,
!     *                              bwgopal,hflipal,hseapal,obegpal,
!     *                              obwgpal.
!     *                            - initialize certain pla arrays.
!     * apr 17/2014 - m.lazare.    interim version for gcm18:
!     *                            - ntld used instead of im for land-only
!     *                              mosaic fields.
!     *                            - flakpak,flndpak,licnpak added.
!     *                            - remove maskpak from setting land tile
!     *                              soil properties (unnecessary because
!     *                              already has to be land).
!     *                            - {xfspak,xsrfpak,xtvipak} moved to new
!     *                              zeroacc4.
!     *                            - "use_cosp", "pla", fields removed
!     *                              to new zeroacc4.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "PAT" arrays.
!     * jul 10/2013 - m.lazare/    previous version init11 for gcm17:
!     *               k.vonsalzen/ - certain pla fields initialized to zero.
!     *               e.chan.      - remove porg and only use a temporary
!     *                              scalar dzg for soil moisture conversion.
!     *                              the calculation remains for classb
!     *                              which is saved for these two invariant
!     *                              fields (along with newly-saved field
!     *                              capacity fcap) **after** the physics.
!     *                            - modified for use with packed tile
!     *                              variables (pat)
!     *                            - added setting of fare (temporary).
!     *                            - added setting/computation of pak
!     *                              variables based on the associated pat
!     *                              versions, particularly for output at
!     *                              kount=0.
!     *                            - revised to support arbitrary number of
!     *                              soil layers.
!     *                            - define soil layers as in classb.
!     *                            - added tpnd,zpnd,tav,qav,wsno,tsfs.
!     *                            - set sno values below 1.e-3 to 0.
!     *                            - initialized required ctem fields.
!     * may 08/2012 - m.lazare.    previou version init10 for gcm16:
!     *                            - fdlpal_r replaced by flgpal_r for
!     *                              radiative forcing.
!     *                            - remove {fsa,fla,fstc,fltc}.
!     *                            - add {fsam,flam} fields.
!     * may 04/2010 - m.lazare.    previous version init9i for gcm15i:
!     *                            - add/remove fields (see other
!     *                              common decks for details).
!     *                            - update directives changed to
!     *                              cpp directives.
!     * feb 19/2009 - m.lazare.    previous version init9h for gcm15h:
!     *                            - bugfix to initialize
!     *                              csal in the same way we do for salb.
!     *                            - remove the initialization of all the
!     *                              solar "PAL" arrays which is
!     *                              unnecessary since this is already done
!     *                              at the beginning of the radiation.
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud field
!     *                              (using optical depth cutoff): cldo.
!     * apr 21/2008 - l.solheim/   previous version init9g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - remove unused qtpn,tsem.
!     * jan 11/2006 - j.cole/      previous version init9f for gcm15f:
!     *               m.lazare.    add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    two extra new fields ("DMC" and "SMC")
!     *                            under control of "%IF DEF,XTRACONV".
!     * jun 20/2006 - m.lazare.    new version for gcm15f:
!     *                            - use variables instead of constants
!     *                              in intrinsics such as "MAX" or
!     *                              "MIN", to enable compiling with
!     *                              implicit real*8 :: in 32-bit mode.
!     * may 07/2006 - m.lazare.    previous version init9e for gcm15e:
!     *                            - cvdu->cvmc.
!     * dec 15/2005 - m.lazare/    previous version init9d for gcm15d.
!     *               k.vonsalzen.
!======================================================================
!
!     * initialize selected fields.
!
tzeroc = 273.16
!----------------------------------------------------------------------
!     * reset farepat if running with fractional glaciers
!     * and re-define ground properties to those of glaciers
!     * for this tile.
!     * note that one just has to redefine the sand for
!     * the first layer; there is code below to re-define
!     * the other layers, and tg/thlq/thic, based on this.
! ****************************************************************
!     * note !! ! For now, the code below will only work when
!     *         running with "general" and "land glacier" tiles,
!     *         ie ntld=2. to generalize requires more thought
!     *         and possibly changes to initialization.
!*****************************************************************
!
!--------------------------------------------------------------
!     * define fractional area arrays.
!
!     * note that this overrides what is currently in program
!     * initg14 and that should be removed when this works
!     * (and this comment !). also to be removed are the
!     * reading of "FARE" and "MASK" in getgg14.
!
!     * important !! !! THE USER MUST HAND-CODE CHANGES HERE
!     *               when tiling changes !! !
!
if (ntld > 0) then
  do n = 1,ntld
    farepat(:,n) = flndpak(:)
  end do
end if
!
if (ntlk > 0) then
  do nt = 1,ntlk
    n = ntld + nt
    if(iktnam(nt) == nc4to8(" ULK")) then
      farepat(:,n)=flkupak(:)
    else if(iktnam(nt).eq.nc4to8(" RWT")) then
      farepat(:,n)=(1.-lrinpak(:))*flkrpak(:)
    else if(iktnam(nt).eq.nc4to8(" RIC")) then
      farepat(:,n)=lrinpak(:)*flkrpak(:)
    else
      call xit('INIT12', - 4)
    end if
  end do
  !
  ldmxpak = 0.
  lzicpak = 0.
#if defined (cslm)
        delupak = 0.
        dtmppak = 0.
        expwpak = 0.
        gredpak = 0.
        nlklpak = 0.
        rhompak = 0.
        t0lkpak = 0.
        tkelpak = 0.
        do l = 1,nlklm
          tlakpak(:,l) = 0.
        end do
#endif
#if defined (flake)
        lshppak = 0.
        ltavpak = 0.
        lticpak = 0.
        ltmxpak = 0.
        ltsnpak = 0.
        ltwbpak = 0.
        lzsnpak = 0.
#endif
  where (flkupak /= 0.)
    lzicpak = luimpak/913.
#if defined (cslm)
          nlklpak = real(nint(hlakpak/0.50))    ! delzlk=0.5
          delupak = 0.
          t0lkpak = max(gtpak,277.16)
          tkelpak = 1.0e-12                ! tkemin=1.0e-12
          ldmxpak(:) = 0.5 * nint(nlklpak(:)) - 1
          dtmppak = 0.
!
!         * initial mixed layer temp (celsius), density, expansivity,
!         * and reduced gravity
!
          rhompak(:) = 999.975 * &
                (1. - 8.2545e-6 * ((t0lkpak(:) - tfrez) - 3.9816) * &
                ((t0lkpak(:) - tfrez) - 3.9816))
          expwpak(:) = - 1.0 * ( - 2. * 999.975 * 8.2545e-6 * &
                ((t0lkpak(:) - tfrez) - 3.9816))/rhompak(:)
          gredpak(:) = grav * abs(rhow - rhompak(:))/rhow
#endif
#if defined (flake)
          hlakpak = min(hlakpak,60.)
          ldmxpak = min(0.25 * hlakpak,10.)
          lshppak = 0.5              ! the value used for c_t_min in flake.
          ltavpak = 283.15
          lticpak = 273.15
          ltmxpak = 283.15
          ltsnpak = 273.15
          ltwbpak = 283.15
#endif
  end where
#if defined (cslm)
!
!       * abort conditions for cslm.
!
        do i = 1,lonsl * ilat
          if (flkupak(i) > 0.) then
            if (nlklpak(i) > real(nlklm)) then
              print * , '0Bad NLAK value: I,NLKLPAK(I),NLKLM = ', &
                   i,nlklpak(i),nlklm
              call xit('GCM18', - 12)
            end if
!
            if (gredpak(i) <= 0.) then
              call xit('GCM18', - 14)
            end if
!
            do l = 1,nlklpak(i)
              tlakpak(i,l) = max(gtpak(i),277.16)
            end do
          end if
        end do
#endif
end if ! ntlk>0
!
do nt = 1,ntwt
  n = ntld + ntlk + nt
  if (ntwt > 1) then
    if (iwtnam(nt) == nc4to8(" WAT")) then
      farepat(:,n) = (1. - sicnpak(:)) * &
                           (1. - flndpak(:) - flkupak(:) - flkrpak(:))
    else if (iwtnam(nt) == nc4to8(" SIC")) then
      farepat(:,n) = sicnpak(:) * &
                           (1. - flndpak(:) - flkupak(:) - flkrpak(:))
    else
      call xit('INIT12', - 1)
    end if
  else
    call xit('INIT12', - 2)
  end if
end do
!--------------------------------------------------------------
!     * the following code can be used as a template in case
!     * running with glaciers as a tile. it might require
!     * further modifications.
!
!     if (ntld>1) then
!       do n=1,ntld
!         if (iltnam(n)==nc4to8(" GIC"))    then
!            farepat(:,n)=gicnpak*flndpak
!            sandpat(:,n,:)=-4.
!            fcanpat(:,n,:)=0.
!            snopat (:,n)=min(snopat(:,n),60.)
!         else
!            if (ntld==2)                   then
!              farepat(:,n)=(1.-gicnpak)*flndpak
!            else
!              CALL XIT('INIT12',-1)
!            end if
!         end if
!       end do
!     end if
!-------------------------------------------------------------
!
where (sandpat(:,1,1) == - 4. .and. snopak > 60.) snopak = 60.
!
!     * zero out values of sno below 1.e-3.
!
where (snopak > 1.e-3)
  rhonpak = 300.
  anpak = 0.84
  fnpak = 1.
  refpak = 5.45256270447e-05
  bcsnpak = 0.
else where
  snopak = 0.
  rhonpak = 0.
  anpak = 0.
  fnpak = 0.
  refpak = 0.
  bcsnpak = 0.
end where
emispak = 1.
!
tnpak = min(gtpak,tzeroc)
!
do i = 1,lonsl * ilat
  !
  if (flndpak(i) > 0.) then
    if (gtpak(i) > tzeroc) then
      ttpat(i,:) = 1.
    else
      ttpat(i,:) = 0.
    end if
    tvpat(i,:) = gtpak(i)
  else
    ttpat(i,:) = 2.
    tvpat(i,:) = 0.
  end if
  !
  !        * ensure total vegetation cover is less than/equal to 1.
  !
  do m = 1,ntld
    fveg = 0.0
    do l = 1,icanp1
      fveg = fveg + fcanpat(i,m,l)
    end do
    if (fveg > 1.0) then
      do l = 1,icanp1
        fcanpat(i,m,l) = fcanpat(i,m,l)/fveg
      end do
    end if
  end do
  !
  !        * set liquid/frozen water contents for rock layers
  !        * and glaciers and set tg for glaciers.
  !
  do m = 1,ntld
    do l = 1,ignd
      sandx = sandpat(i,m,l)
      depthx = zbot(l) - delz(l) + rlim
      if (sandpat(i,m,1) == - 4.) then
        sandpat(i,m,l) = - 4.
        thlqpat(i,m,l) = 0.
        thicpat(i,m,l) = 1.
        tgpat(i,m,l) = min(tgpat(i,m,l),tzeroc)
      else if (sandx /= - 3. .and. dpthpat(i,m) < depthx) then
        sandpat(i,m,l) = - 3.
        thlqpat(i,m,l) = 0.
        thicpat(i,m,l) = 0.
      end if
    end do
  end do
end do
!
!     * Initialization of grid-average (untiled) albedoes.
!
where (sicnpak >= 0.15 .or. snopak > 0.)
  salbpal(:,1) = 0.778
  salbpal(:,2) = 0.443
  salbpal(:,3) = 0.055
  salbpal(:,4) = 0.036
  csalpal(:,1) = 0.778
  csalpal(:,2) = 0.443
  csalpal(:,3) = 0.055
  csalpal(:,4) = 0.036
elsewhere
  salbpal(:,1) = 0.15
  salbpal(:,2) = 0.15
  salbpal(:,3) = 0.15
  salbpal(:,4) = 0.15
  csalpal(:,1) = 0.15
  csalpal(:,2) = 0.15
  csalpal(:,3) = 0.15
  csalpal(:,4) = 0.15
end where
!
!     * other tiled fields which exist over all tiles.
!
do i = 1,lonsl * ilat
  !
  !       * general initialization for all tiles to start.
  !
  do m = 1,im
    emispat(i,m) = 0.
    csalpat(i,m,1) = 0.
    salbpat(i,m,1) = 0.
    csalpat(i,m,2) = 0.
    salbpat(i,m,2) = 0.
    csalpat(i,m,3) = 0.
    salbpat(i,m,3) = 0.
    csalpat(i,m,4) = 0.
    salbpat(i,m,4) = 0.
    !
    gtpat  (i,m) = gtpak(i)
    snopat (i,m) = 0.
    anpat  (i,m) = 0.
    fnpat  (i,m) = 0.
    tnpat  (i,m) = 0.
    rhonpat(i,m) = 0.
    refpat (i,m) = 0.
    bcsnpat(i,m) = 0.
  end do
  !
  !       * land tiles.
  !
  do m = 1,ntld
    if (flndpak(i) > 0.) then
      !
      ! * set pat fields from initialized read-in pak fields (getgg13)
      ! * and above.
      !
      emispat(i,m) = 1.
      csalpat(i,m,1)=0.25
      salbpat(i,m,1)=0.25
      csalpat(i,m,2)=0.25
      salbpat(i,m,2)=0.25
      csalpat(i,m,3)=0.25
      salbpat(i,m,3)=0.25
      csalpat(i,m,4)=0.25
      salbpat(i,m,4)=0.25

      if (snopak(i) > 0.) then
        snopat (i,m) = snopak (i)
        anpat  (i,m) = anpak  (i)
        fnpat  (i,m) = fnpak  (i)
        tnpat  (i,m) = tnpak  (i)
        rhonpat(i,m) = rhonpak(i)
        refpat (i,m) = refpak (i)
        bcsnpat(i,m) = bcsnpak(i)
      else
        snopat (i,m) = 0.
        anpat  (i,m) = 0.
        tnpat  (i,m) = 0.
        rhonpat(i,m) = 0.
        refpat (i,m) = 0.
        bcsnpat(i,m) = 0.
      end if
    end if
  end do
  !
  !       * unresolved lakes.
  !
  if (flkupak(i) > 0.) then
    if (luimpak(i) > 0.) then
      gtpat  (i,iulak)  = 273.15
      emispat(i,iulak)  = 1.
      csalpat(i,iulak,1) = 0.778
      salbpat(i,iulak,1) = 0.778
      csalpat(i,iulak,2) = 0.443
      salbpat(i,iulak,2) = 0.443
      csalpat(i,iulak,3) = 0.055
      salbpat(i,iulak,3) = 0.055
      csalpat(i,iulak,4) = 0.036
      salbpat(i,iulak,4) = 0.036
    else
      gtpat  (i,iulak)  = gtpak(i)
      emispat(i,iulak)  = 0.97   ! emsw
      csalpat(i,iulak,1)=0.05
      salbpat(i,iulak,1)=0.05
      csalpat(i,iulak,2)=0.05
      salbpat(i,iulak,2)=0.05
      csalpat(i,iulak,3)=0.05
      salbpat(i,iulak,3)=0.05
      csalpat(i,iulak,4)=0.05
      salbpat(i,iulak,4)=0.05
    end if
  end if
  !
  !       * resolved lakes (assumed no initial snow and all ice-covered).
  !
  if (flkrpak(i) > 0.) then
    if (lrinpak(i) > 0.) then
      gtpat  (i,irlic)  = 273.15
      emispat(i,irlic)  = 1.
      csalpat(i,irlic,1) = 0.778
      salbpat(i,irlic,1) = 0.778
      csalpat(i,irlic,2) = 0.443
      salbpat(i,irlic,2) = 0.443
      csalpat(i,irlic,3) = 0.055
      salbpat(i,irlic,3) = 0.055
      csalpat(i,irlic,4) = 0.036
      salbpat(i,irlic,4) = 0.036
    end if
    if (lrinpak(i) < 1.) then
      gtpat  (i,irlwt)  = gtpak(i)
      emispat(i,irlwt)  = 0.97   ! emsw
      csalpat(i,irlwt,1)=0.05
      salbpat(i,irlwt,1)=0.05
      csalpat(i,irlwt,2)=0.05
      salbpat(i,irlwt,2)=0.05
      csalpat(i,irlwt,3)=0.05
      salbpat(i,irlwt,3)=0.05
      csalpat(i,irlwt,4)=0.05
      salbpat(i,irlwt,4)=0.05
    end if
  end if
  !
  !       * ocean tiles (assume no snow and ice-covered).
  !
  fwat = 1. - flndpak(i) - flkupak(i) - flkrpak(i)
  if (fwat > 0.) then
    if (sicnpak(i) > 0.) then
      gtpat  (i,iosic)  = 273.15
      emispat(i,iosic)  = 1.
      csalpat(i,iosic,1) = 0.778
      salbpat(i,iosic,1) = 0.778
      csalpat(i,iosic,2) = 0.443
      salbpat(i,iosic,2) = 0.443
      csalpat(i,iosic,3) = 0.055
      salbpat(i,iosic,3) = 0.055
      csalpat(i,iosic,4) = 0.036
      salbpat(i,iosic,4) = 0.036
    end if
    if (sicnpak(i) < 1.) then
      gtpat  (i,iowat)  = gtpak(i)
      emispat(i,iowat)  = 0.97   ! emsw
      csalpat(i,iowat,1)=0.05
      salbpat(i,iowat,1)=0.05
      csalpat(i,iowat,2)=0.05
      salbpat(i,iowat,2)=0.05
      csalpat(i,iowat,3)=0.05
      salbpat(i,iowat,3)=0.05
      csalpat(i,iowat,4)=0.05
      salbpat(i,iowat,4)=0.05
    end if
  end if
end do
!
!     * set tt for glaciers.
!
where(sandpat(:,:,1) == - 4.) ttpat = - 2.
!
!     * initialize the bedrock temperature, the temperature and humidity
!     * of the air in the canopy space, and the vegetation canopy mass.
!
tbaspat = tgpat(:,:,ignd)
tavpat = tvpat
qavpat = 0.5e-4
mvpat = 0.
!
!     * initialize the temperature of the ground/snow surface
!     * for each of the 4 subareas.
!
tsfspat(:,:,1:2) = tzeroc
tsfspat(:,:,3) = tgpat(:,:,1)
tsfspat(:,:,4) = tgpat(:,:,1)
!
pbltpak = real(ilev)
tcvpak  = real(ilev)
gtapak  = gtpak
stmxpak = gtpak
stmnpak = gtpak
!----------------------------------------------------------------------
!
!     * initialize remaining packed arrays to zero.
!
call pkzeros2(clbpat,ijpak,  im)
call pkzeros2(csbpat,ijpak,  im)
call pkzeros2(csdpat,ijpak,  im)
call pkzeros2(csfpat,ijpak,  im)
call pkzeros2(fdlpat,ijpak,  im)
call pkzeros2(fdlcpat,ijpak,  im)
call pkzeros2(flgpat,ijpak,  im)
call pkzeros2(fsdpat,ijpak,  im)
call pkzeros2(fsfpat,ijpak,  im)
call pkzeros2(fsgpat,ijpak,  im)
call pkzeros2(fsipat,ijpak,  im)
call pkzeros2(fsvpat,ijpak,  im)
call pkzeros2(parpat,ijpak,  im)
call pkzeros2(fsdbpat,ijpak,ntbs)
call pkzeros2(fsfbpat,ijpak,ntbs)
call pkzeros2(fssbpat,ijpak,ntbs)
call pkzeros2(csdbpat,ijpak,ntbs)
call pkzeros2(csfbpat,ijpak,ntbs)
call pkzeros2(fsscbpat,ijpak,ntbs)
!
call pkzeros2(fsopal,ijpak,   1)
call pkzeros2(csalpal,ijpak, nbs)
call pkzeros2(salbpal,ijpak, nbs)
call pkzeros2(csdpal,ijpak,   1)
call pkzeros2(csfpal,ijpak,   1)
call pkzeros2(fsdbpal,ijpak, nbs)
call pkzeros2(fsfbpal,ijpak, nbs)
call pkzeros2(fssbpal,ijpak, nbs)
call pkzeros2(csdbpal,ijpak, nbs)
call pkzeros2(csfbpal,ijpak, nbs)
call pkzeros2(fsscbpal,ijpak, nbs)
call pkzeros2(wrkapal,ijpak, nbs)
call pkzeros2(wrkbpal,ijpak, nbs)
call pkzeros2(fsgpal,ijpak,   1)
call pkzeros2(fsdpal,ijpak,   1)
call pkzeros2(fsfpal,ijpak,   1)
call pkzeros2(fsvpal,ijpak,   1)
call pkzeros2(fsipal,ijpak,   1)
call pkzeros2(fsrpal,ijpak,   1)
call pkzeros2(fsrcpal,ijpak,   1)
call pkzeros2(fdlpal,ijpak,   1)
call pkzeros2(fdlcpal,ijpak,   1)
call pkzeros2(flgpal,ijpak,   1)
call pkzeros2(fslopal,ijpak,   1)
call pkzeros2(parpal,ijpak,   1)
call pkzeros2(olrpal,ijpak,   1)
call pkzeros2(olrcpal,ijpak,   1)
call pkzeros2(csbpal,ijpak,   1)
call pkzeros2(clbpal,ijpak,   1)
call pkzeros2(flampal,ijpak,   1)
call pkzeros2(fsampal,ijpak,   1)
call pkzeros2(flanpal,ijpak,   1)
call pkzeros2(fsanpal,ijpak,   1)
!
call pkzeros2(pblhpak,ijpak,   1)
call pkzeros2(clwtpal,ijpak,   1)
call pkzeros2(cictpal,ijpak,   1)
call pkzeros2(wvlpak,ijpak,   1)
call pkzeros2(wvfpak,ijpak,   1)
!
call pkzeros2(pwatpam,ijpak,   1)
call pkzeros2(qtphpam,ijpak,   1)
call pkzeros2(qtpfpam,ijpak,   1)
call pkzeros2(qtptpam,ijpak,   1)
!
call pkzeros2(cldtpal,ijpak,   1)
call pkzeros2(cldopal,ijpak,   1)
!
call pkzeros2(tfxpak,ijpak,   1)
call pkzeros2(qfxpak,ijpak,   1)
call pkzeros2(chfxpak,ijpak,   1)
call pkzeros2(cqfxpak,ijpak,   1)
call pkzeros2(cbmfpal,ijpak,   1)
!
call pkzeros2(hrspak,ijpak,ilev)
call pkzeros2(hrlpak,ijpak,ilev)
call pkzeros2(cldpak,ijpak,ilev)
call pkzeros2(tacnpak,ijpak,ilev)
call pkzeros2(rhpak,ijpak,ilev)
call pkzeros2(clwpak,ijpak,ilev)
call pkzeros2(cicpak,ijpak,ilev)
call pkzeros2(cvarpak,ijpak,ilev)
call pkzeros2(cvmcpak,ijpak,ilev)
call pkzeros2(cvsgpak,ijpak,ilev)
call pkzeros2(almxpak,ijpak,ilev)
call pkzeros2(almcpak,ijpak,ilev)

!
! * TKE.
!
tkempak=0.
xlmpak=0.
svarpak=0.
xlmpat=0.
!
! * OTHERS.
!

wsnopak = 0.
wtabpak = 0.
zpndpak = 0.
depbpak = 0.
!
tpndpat = 0.
wsnopat = 0.
wvlpat = 0.
wvfpat = 0.
zpndpat = 0.

call pkzeros2(sclfpak,ijpak,ilev)
call pkzeros2(scdnpak,ijpak,ilev)
call pkzeros2(slwcpak,ijpak,ilev)
call pkzeros2(ometpak,ijpak,ilev)
call pkzeros2(zdetpak,ijpak,ilev)
call pkzeros2(clcvpak,ijpak,ilev)
call pkzeros2(qwf0pal,ijpak,ilev)
call pkzeros2(qwfmpal,ijpak,ilev)

#if defined (agcm_ctem)
!
!     * time varying.
!
      soilcpat  = 0.
      litrcpat  = 0.
      rootcpat  = 0.
      stemcpat  = 0.
      gleafcpat = 0.
      bleafcpat = 0.
      fallhpat  = 0.
      posphpat  = 0.
      leafspat  = 0.
      growtpat  = 0.
      lastrpat  = 0.
      lastspat  = 0.
      thisylpat = 0.
      stemhpat  = 0.
      roothpat  = 0.
      tempcpat  = 0.
      ailcbpat  = 0.
      bmasvpat  = 0.
      veghpat   = 0.
      rootdpat  = 0.
!
      prefpat   = 0.
      newfpat   = 0.
!
      cvegpat   = 0.
      cdebpat   = 0.
      chumpat   = 0.
#endif

sfrcpal = 1.
qsrcrat = 1.
qsrcrm1 = 1.
qsrcm   = 1.
qsrc0   = 1.
qscl0   = 1.
qsclm   = 1.
do l = 1,ilev
  qsrcratl(l) = 0.
end do
!
do n = 1,ntrac
  xsrcrat(n) = 1.
  xsrcrm1(n) = 1.
  xsrc0  (n) = 1.
  xsrcm  (n) = 1.
  xscl0  (n) = 1.
  xsclm  (n) = 1.
  do l = 1,ilev
    xsrcratl(l,n) = 0.
  end do
  !
  call pkzeros2(xsfxpak(1,n)  ,ijpak,   1)
  call pkzeros2(xsfxpal(1,n)  ,ijpak,   1)
  call pkzeros2(xsrfpal(1,n)  ,ijpak,   1)
  call pkzeros2(xtpfpam(1,n)  ,ijpak,   1)
  call pkzeros2(xtphpam(1,n)  ,ijpak,   1)
  call pkzeros2(xtvipam(1,n)  ,ijpak,   1)
  call pkzeros2(xtptpam(1,n)  ,ijpak,   1)
  call pkzeros2(xwf0pal(1,1,n),ijpak,ilev)
  call pkzeros2(xwfmpal(1,1,n),ijpak,ilev)
  !
end do
#if (defined(pla) && defined(pam))
      call pkzeros2(ssldpak,ijpak,ilev)
      call pkzeros2(resspak,ijpak,ilev)
      call pkzeros2(vesspak,ijpak,ilev)
      call pkzeros2(dsldpak,ijpak,ilev)
      call pkzeros2(redspak,ijpak,ilev)
      call pkzeros2(vedspak,ijpak,ilev)
      call pkzeros2(bcldpak,ijpak,ilev)
      call pkzeros2(rebcpak,ijpak,ilev)
      call pkzeros2(vebcpak,ijpak,ilev)
      call pkzeros2(ocldpak,ijpak,ilev)
      call pkzeros2(reocpak,ijpak,ilev)
      call pkzeros2(veocpak,ijpak,ilev)
      call pkzeros2(amldpak,ijpak,ilev)
      call pkzeros2(reampak,ijpak,ilev)
      call pkzeros2(veampak,ijpak,ilev)
      call pkzeros2(fr1pak ,ijpak,ilev)
      call pkzeros2(fr2pak ,ijpak,ilev)
      call pkzeros2(zcdnpak,ijpak,ilev)
      call pkzeros2(bcicpak,ijpak,ilev)
      do isf = 1,kextt
        oednpak(:,:,isf) = yna
        oercpak(:,:,isf) = yna
      end do
      oidnpak = yna
      oircpak = yna
      call pkzeros2(svvbpak,ijpak,ilev)
      psvvpak = yna
      call pkzeros2(qgvbpak,ijpak,ilev)
      call pkzeros2(qdvbpak,ijpak,ilev)
      call pkzeros2(ogvbpak,ijpak,ilev)
      do isi = 1,isaintt
        call pkzeros2(pnvbpak(1,1,isi),ijpak,ilev)
        call pkzeros2(qnvbpak(1,1,isi),ijpak,ilev)
        call pkzeros2(onvbpak(1,1,isi),ijpak,ilev)
        do k = 1,kintt
          call pkzeros2(psvbpak(1,1,isi,k),ijpak,ilev)
          call pkzeros2(qsvbpak(1,1,isi,k),ijpak,ilev)
          call pkzeros2(osvbpak(1,1,isi,k),ijpak,ilev)
        end do
      end do
      do isf = 1,nrmfld
        call pkzeros2(svmbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(qgmbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(qdmbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(ogmbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(svcbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(qgcbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(qdcbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(ogcbpak(1,1,isf),ijpak,ilev)
        do isi = 1,isaintt
          call pkzeros2(pnmbpak(1,1,isi,isf),ijpak,ilev)
          call pkzeros2(qnmbpak(1,1,isi,isf),ijpak,ilev)
          call pkzeros2(onmbpak(1,1,isi,isf),ijpak,ilev)
          call pkzeros2(pncbpak(1,1,isi,isf),ijpak,ilev)
          call pkzeros2(qncbpak(1,1,isi,isf),ijpak,ilev)
          call pkzeros2(oncbpak(1,1,isi,isf),ijpak,ilev)
          do k = 1,kintt
            call pkzeros2(psmbpak(1,1,isi,k,isf),ijpak,ilev)
            call pkzeros2(qsmbpak(1,1,isi,k,isf),ijpak,ilev)
            call pkzeros2(osmbpak(1,1,isi,k,isf),ijpak,ilev)
            call pkzeros2(pscbpak(1,1,isi,k,isf),ijpak,ilev)
            call pkzeros2(qscbpak(1,1,isi,k,isf),ijpak,ilev)
            call pkzeros2(oscbpak(1,1,isi,k,isf),ijpak,ilev)
          end do
        end do
      end do
      do isf = 1,kextt
        call pkzeros2(devbpak(1,1,isf),ijpak,ilev)
        pdevpak(:,:,isf) = yna
        do isg = 1,nrmfld
          call pkzeros2(dembpak(1,1,isf,isg),ijpak,ilev)
          call pkzeros2(decbpak(1,1,isf,isg),ijpak,ilev)
        end do
      end do
      call pkzeros2(divbpak,ijpak,ilev)
      pdivpak = yna
      do isf = 1,nrmfld
        call pkzeros2(dimbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(dicbpak(1,1,isf),ijpak,ilev)
      end do
      do isf = 1,kextt
        revbpak(:,:,isf) = yna
        prevpak(:,:,isf) = yna
        do isg = 1,nrmfld
          call pkzeros2(rembpak(1,1,isf,isg),ijpak,ilev)
          call pkzeros2(recbpak(1,1,isf,isg),ijpak,ilev)
        end do
      end do
      rivbpak = yna
      privpak = yna
      do isf = 1,nrmfld
        call pkzeros2(rimbpak(1,1,isf),ijpak,ilev)
        call pkzeros2(ricbpak(1,1,isf),ijpak,ilev)
      end do
#if defined radforce
      call pkzeros2(sulipak,ijpak,ilev)
      call pkzeros2(rsuipak,ijpak,ilev)
      call pkzeros2(vsuipak,ijpak,ilev)
      call pkzeros2(f1supak,ijpak,ilev)
      call pkzeros2(f2supak,ijpak,ilev)
      call pkzeros2(bclipak,ijpak,ilev)
      call pkzeros2(rbcipak,ijpak,ilev)
      call pkzeros2(vbcipak,ijpak,ilev)
      call pkzeros2(f1bcpak,ijpak,ilev)
      call pkzeros2(f2bcpak,ijpak,ilev)
      call pkzeros2(oclipak,ijpak,ilev)
      call pkzeros2(rocipak,ijpak,ilev)
      call pkzeros2(vocipak,ijpak,ilev)
      call pkzeros2(f1ocpak,ijpak,ilev)
      call pkzeros2(f2ocpak,ijpak,ilev)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
