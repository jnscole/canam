#include "cppdef_config.h"
!! --- NEW: #define with_MPI_

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine spec_fwd_xfer2(nl2, ilat, nlat, lm, lmtotal, xx, yy, &
                                sbuf,sz,soffset,roffset)
  !
  !     * sep 12/2006 - m.lazare. new version for gcm15f, based on old
  !     *                         spec_fwd,xfer, using native reals
  !     *                         instead of explicit real(8), to
  !     *                         support 32-bit driver.
  !     * jan 23/2004 - r.mclay.  previous version spec_fwd_xfer
  !     *                         developed for conversion to ibm.
  !
  implicit none
  integer(4) :: mynode !<
  common /mpinfo/mynode
  !
  !     * common block to hold the number of nodes and communicator.
  !
  integer(4) :: nnode !<
  integer(4) :: agcm_commwrld !<
  integer(4) :: my_cmplx_type !<
  integer(4) :: my_real_type !<
  integer(4) :: my_int_type !<
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  integer(4) :: ierr !<
  integer(4) :: offset !<
  integer(4) :: inode !<
  integer(4), intent(inout) :: sz(0:nnode-1) !< Variable description\f$[units]\f$
  integer(4), intent(inout) :: soffset(0:nnode-1) !< Variable description\f$[units]\f$
  integer(4), intent(inout) :: roffset(0:nnode-1) !< Variable description\f$[units]\f$
  integer, intent(in)    :: nl2 !< Variable description\f$[units]\f$
  integer, intent(in)    :: ilat !< Variable description\f$[units]\f$
  integer, intent(in)    :: nlat !< Variable description\f$[units]\f$
  integer, intent(in)    :: lm !< Variable description\f$[units]\f$
  integer, intent(in)    :: lmtotal !<
  integer    :: lmhalf !<
  integer    :: mm !<
  integer    :: jj !<
  integer    :: j !<
  real, intent(in), dimension(nl2, nlat, lm) :: xx !< Variable description\f$[units]\f$
  real, intent(in)       :: yy(nl2, ilat, lm,   0:nnode-1) !< Variable description\f$[units]\f$
  real, intent(inout)       :: sbuf(nl2, ilat, lm/2, 0:nnode-1) !< Variable description\f$[units]\f$
  real       :: val !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !===================================================================
#ifdef with_MPI_
      lmhalf = lm/2

      offset = 0
      do inode = 0, nnode - 1
        sz(inode)      = nl2*ilat*lmhalf
        soffset(inode) = offset
        roffset(inode) = offset
        offset         = offset + sz(inode)
      end do

      do mm = 1, lmhalf
        jj = 1
        do inode = 0, nnode - 1
          sbuf(:, : , mm, inode) = xx(:, jj:jj+ilat-1, 2*mm-1)
          jj = jj + ilat
        end do
      end do

      call mpi_alltoallv(sbuf(1,1,1,0), sz, soffset, my_real_type, &
                     yy(1,1,1,0),   sz, roffset, my_real_type, &
                     agcm_commwrld, ierr)


      do inode = 0, nnode - 1
        roffset(nnode - inode - 1) = offset
        offset                     = offset + sz(inode)
      end do

      do mm = 1, lmhalf
        jj = 1
        do inode = 0, nnode - 1
          sbuf(:, :, mm, inode) = xx(:, jj:jj+ilat-1, lm - 2*mm + 2)
          jj = jj + ilat
        end do
      end do

      call mpi_alltoallv(sbuf(1,1,1,0), sz, soffset, my_real_type, &
                     yy(1,1,1,0),   sz, roffset, my_real_type, &
                     agcm_commwrld, ierr)
#endif
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
