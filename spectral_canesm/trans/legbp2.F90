!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine legbp2(spec,four1, &
                        alp,dalp, &
                        wl,wocsl,ilev,levs,nlat, &
                        itrac,ntrac, &
                        m,ntot)
  !
  !     * sep 11/06 - m. lazare. new version for gcm15f:
  !     *                        - calls new vfastx2 to support 32-bit driver.
  !     * oct 29/03 - m. lazare. previous version legbp from ibm conversion:
  !     * oct 29/03 - m. lazare. new routine to do exclusively the backward
  !     *                        legendre transform for physics, based on
  !     *                        old code in mhanlp_ and using dgemm.
  !     *
  !     * transform fourier coefficients tendencies to spectral tendencies
  !     * for this (possibly chained) gaussian latitude.
  !     * tracer variable transforms done only if itrac/=0.
  !
  !     * this is called for a given zonal wavenumber index "M", ie inside
  !     * a loop over m.
  !     *
  !     *    alp = legendre polynomials *gaussian weight
  !     *   dalp = n-s derivative of leg.poly. *gauss.weight/sin(lat)
  !     *
  !     * input fields
  !     * ------------
  !     *
  !     * this routine is called within a loop over zonal index "M" and
  !     * thus accesses the input for that particular zonal index from
  !     * the calling array.
  !     *
  !     * ttg,estg,tractg,vtg,utg --> four1(2,nlev1,nlat)
  !     *
  !     * output fields
  !     * -------------
  !     *
  !     * the output data fields are the spectral tendencies work field,
  !     * accumulated into the following data structure:
  !     *
  !     *           spec(2,nlev1,ntot)
  !     *
  !
  implicit none
  real :: beta
  real :: bi
  real :: f1tmp
  real :: fms
  real :: fsq
  integer :: i
  integer :: iestf
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: itractf
  integer :: ittf
  integer :: iutf
  integer :: iutmp
  integer :: ivtf
  integer :: ivtmp
  integer :: lat
  integer :: lev
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: m
  integer :: n
  integer :: nilev
  integer, intent(in) :: nlat
  integer :: nlev1
  integer :: nlev2
  integer, intent(in) :: ntot
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  !     * input fourier coefficients.
  !
  real, intent(inout) :: four1(2,3*ilev+levs+itrac*ntrac*ilev,nlat) !< Variable description\f$[units]\f$
  !
  !     * output spectral tendencies.
  !
  real, intent(inout), target :: spec(2,3*ilev+levs+itrac*ntrac*ilev,ntot) !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real*8, intent(in) :: alp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) :: dalp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wl !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wocsl !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * work arrays.
  !
  real, allocatable, dimension(:,:,:) :: four2 !<
  real, allocatable, dimension(:,:,:) :: spec2 !<
  !-----------------------------------------------------------------------
  fms = real(m-1)
  !
  !     * calculate number of parallel transforms.
  !
  nilev = 0
  if (itrac /= 0) nilev=ilev*ntrac
  nlev1 = 3*ilev + levs + nilev
  nlev2 = 2*ilev
  !
  !     * initialize pointers to variables in "FOUR" arrays.
  !     * these constants are equal to the number of levels that
  !     * must be skipped to access the data for a particular variable.
  !
  ittf      = 0
  iestf     = ittf  + ilev
  itractf   = iestf + levs
  ivtf      = itractf
  if (itrac /= 0) then
    ivtf    = ivtf  + nilev
  end if
  iutf      = ivtf  + ilev

  iutmp     = 0
  ivtmp     = iutmp + ilev
  !
  !     * copy the fourier * "VTF" and "UTF" fields from "FOUR1" into "FOUR2".
  !     * note that the ordering of "UTMP" and "VTMP" in "FOUR2" is reversed
  !     * relative to the ordering of "VTF" and "UTF" in "FOUR1".
  !     * this is the ordering required for calculating the cross-product
  !     * contributions to the "PT" and "CT" tendencies.
  !
  allocate(four2(2,2*ilev,nlat))

  do lat=1,nlat
    do i=1,2*ilev
      four2(i,iutmp+1,lat) =  four1(i,iutf +1,lat)
      four2(i,ivtmp+1,lat) =  four1(i,ivtf +1,lat)
    end do
  end do ! loop 100
  !
  !     * north - south derivatives using dalp.
  !     * note that "SPEC2" *must* be initialized to zero to avoid
  !     * accumulating spurious data.
  !
  allocate(spec2(2,2*ilev,ntot))
  !
  beta=0.
  call vfastx2(four2,spec2,dalp, &
                   beta,nlev2,nlat,ntot)
  !
  deallocate(four2)
  !
  !     * east - west derivative of fourier components.
  !     * special note:  note that "PUTF" and "PVTF" map to "VTG" and "UTG"
  !     * respectively in the call to "MHANL8".  this mapping is expressed
  !     * using the "iutf" and "ivtf" pointers below.
  !
  do lat=1,nlat
    fsq   = wocsl(lat)/wl(lat)
    bi    = fsq*fms
    do lev=ivtf+1,ivtf+ilev
      f1tmp                =      four1(1, lev, lat)
      four1(1, lev, lat)   =   bi*four1(2, lev, lat)
      four1(2, lev, lat)   =  -bi*f1tmp
    end do ! loop 324
    !
    do lev=iutf+1,iutf+ilev
      f1tmp                =      four1(1, lev, lat)
      four1(1, lev, lat)   =  -bi*four1(2, lev, lat)
      four1(2, lev, lat)   =   bi*f1tmp
    end do ! loop 326
  end do ! loop 330
  !
  !     * do the direct legendre transforms.
  !     * this presumes that the grid slices and spectral arrays
  !     * are lined up in memory.
  !     * number = ilev*3 (for putf,pvtf,peetf),
  !     *         +levs*1 (for estf),
  !     *         +ilev*ntrac (for tractf, if itrac/=0)
  !
  beta=0.
  call vfastx2(four1,spec,alp, &
                   beta,nlev1,nlat,ntot)
  !
  !     * accumulate contribution for ct from array "SPEC2".
  !     * the "IUTF" pointer corresponds to the same range of levels
  !     * as for the "CT" spectral tendency and "IVTF" similarily for
  !     * "PT".
  !
  do n=1,ntot
    do i=1,2*ilev
      spec(i,iutf+1,n) = spec(i,iutf+1,n) + spec2(i,ivtmp+1,n)
      spec(i,ivtf+1,n) = spec(i,ivtf+1,n) + spec2(i,iutmp+1,n)
    end do
  end do ! loop 500
  deallocate(spec2)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
