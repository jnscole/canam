!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vfastx2(f,s,wlp, &
                         beta,nlev,nlat,ntot)

  !     * sep 11/2006 - f.majaess/ new version for gcm15f:
  !     *               m.lazare.  revised to call "sgemm" for r4i4 mode.
  !     * oct 29/2003 - r.mclay/  previous version vfastx from ibm conversion:
  !     *               m.lazare. new legendre transform, like vfast except
  !     *                         uses vendor-supplied dgemm.
  !     *                         ** note ** : wlp order must be reversed
  !     *                         to use this !
  !     * oct 30/92. -  a.j.stacey. previous version vfast.
  !
  !     * note: the usual input to routines such as blas is 32-bit reals
  !     *       and integers. since, however, we are using "dgemm", the
  !     *       input is 64-bit reals and 32-bit integers. hence, the
  !     *       required interface changes are to use 4-byte integers for
  !     *       any integers passed to "dgemm".
  !     *
  !     * also note that since "S" and "F" are complex :: in the calling
  !     * routine and since the level dimension is the innermost, all
  !     * sizes referring to level are passed as "2*...".
  !
  implicit none
  real, intent(in) :: s(2*nlev,ntot) !< Variable description\f$[units]\f$
  real, intent(in) :: f(2*nlev,nlat) !< Variable description\f$[units]\f$
  real*8, intent(in)  :: wlp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real :: alpha !<
  real, intent(in) :: beta !< Variable description\f$[units]\f$

  !
  !     * internal work array for 32-bit case.
  !     * this is required since sgemm requires 32-bit arrays and
  !     * "wlp" is 64-bit from agcm driver !
  !
  real*4  :: wlp4(nlat,ntot+1) !<
  !
  integer*4 :: ntot4 !<
  integer*4 :: nlat4 !<
  integer*4 :: nlev4 !<
  integer, intent(in) :: nlev !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat !< Variable description\f$[units]\f$
  integer, intent(in) :: ntot !< Variable description\f$[units]\f$
  integer :: machine !<
  integer :: intsize !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !-----------------------------------------------------------------------
  alpha  = 1.
  ntot4  = ntot
  nlat4  = nlat
  nlev4  = 2*nlev
  !
  wlp4(1:nlat,1:ntot+1)=wlp(1:nlat,1:ntot+1)
  !
  if (machine==2) then
    call sgemm('N', 'N', nlev4 , ntot4, nlat4, alpha, f, &
                  nlev4, wlp4, nlat4, beta, s, nlev4)
  else
    !$$$       call dgemm('N', 'N', nlev4 , ntot4, nlat4, alpha, f,
    !$$$     1           nlev4, wlp, nlat4, beta, s, nlev4)
    call dgemm('N', 'N', nlev*2 , ntot, nlat, alpha, f, &
                  nlev*2, wlp, nlat, beta, s, nlev*2)
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
