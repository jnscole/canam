!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swaptf(s,wrks,wrks1,la,ilev,hoskf)

  !     * dec 10/2004 - j.scinocca. add hoskins spectral filter of
  !     *                           tendencies.
  !     * jun 25/2003 - m.lazare. previous version swapt.
  !     *                         like routine swaps1 except
  !     *                         adds "WRKS" into "S" instead
  !     *                         of overwriting "S". this is
  !     *                         now used in the post-nec
  !     *                         code where the conversion
  !     *                         from "SPEC3" to the /sp/ common
  !     *                         block is done following the global
  !     *                         sums.
  !
  !     * update s(2,la,ilev) with transpose from wrks(2,ilev,la)
  !
  implicit none
  real :: flt
  integer :: i
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: j
  integer, intent(in) :: la
  real, intent(inout), dimension(2,la,ilev) :: s !< Variable description\f$[units]\f$
  real, intent(in), dimension(2,ilev,la) :: wrks !< Variable description\f$[units]\f$
  real, intent(inout) :: wrks1(2,ilev,la+1) !< Variable description\f$[units]\f$
  real, intent(in), dimension(2,la) :: hoskf !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !     * first determine if la is an exact multiple of 32 to
  !     * see if bank conflicts may be a problem.
  !
  if (mod(la,32)/=0) then
    do j=1,ilev
      do i=1,la
        flt=hoskf(1,i)
        s(1,i,j) = s(1,i,j) + wrks(1,j,i)*flt
        s(2,i,j) = s(2,i,j) + wrks(2,j,i)*flt
      end do
    end do ! loop 10
  else
    do i=1,la
      do j=1,ilev
        wrks1(1,j,i) = wrks(1,j,i)
        wrks1(2,j,i) = wrks(2,j,i)
      end do
    end do ! loop 20
    !
    do j=1,ilev
      do i=1,la
        flt=hoskf(1,i)
        s(1,i,j) = s(1,i,j) + wrks1(1,j,i)*flt
        s(2,i,j) = s(2,i,j) + wrks1(2,j,i)*flt
      end do
    end do ! loop 30
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
