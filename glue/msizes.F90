#include "cppdef_config.h"
#include "cppdef_sizes.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
module msizes
  !=======================================================================
  !     * aug 14,2018 - m. lazare. - remove unused iflm.
  !     * nov 1,2017  - j. cole.   - add variables to handle zonal fields
  !     * apr 18,2015 - f. majaess.- add "ntlkd" definition to better
  !     *                            handle no lake tiles in trinfo10
  !     *                            namelists.
  !     * feb 12,2015 - m. lazare. - add tiling dimensions based on
  !                                  namelist output (for gcm18+).
  !     *                          - remove ilevp2 (unused), backstitched.
  !     * dec 15,2003 - m. lazare. - parms13b split into msizes,psizes.
  !     * dec 10,2003 - m. lazare. - nintsw now hard-coded.
  !     *                          - $nnode$ -> $nnode_a$.
  !     *                          - ntracn,ntraca added.
  !     *                          - ntraca used instead of ntrac in nlev1e.
  !     * dec 03,2003 - l. solheim - consolodation of "parmsub" parameters
  !                                  into a single module
  !=======================================================================
  !
  !     * number of smp nodes ...use mpi for nnode>1
  implicit none
  integer, parameter :: nnodex = _PAR_NNODE_A

  !     * number of tracers
  integer, parameter :: ntrac = _PAR_NTRAC
  integer, parameter :: ntraca = _PAR_NTRACA
  integer, parameter :: ntracn = _PAR_NTRACN
  !
  !     * number of mosaic tiles per grid cell and
  !     * total number of tiles.
  !
  integer, parameter :: ntld = _PAR_NTLD
  integer, parameter :: ntlk = _PAR_NTLK
  integer, parameter :: ntwt = _PAR_NTWT
  integer, parameter :: im = ntld + ntlk + ntwt
  integer, parameter :: ntlkd = max(ntlk,1)

  !.....physical dimension sizes

  !     * number of model levels
  integer, parameter :: ilev = _PAR_ILEV
  integer, parameter :: ilevp1 = ilev + 1

  !     * number of moisture levels
  integer, parameter :: levs = _PAR_LEVS

  !     * number of longitudes on the physics grid
  integer, parameter :: lonsl = _PAR_LONSL
  integer, parameter :: ilgsl = lonsl + 2

  !     * number of longitudes on the zonal gird (obviously 1)
  integer, parameter :: lonsz = 1

  !     * number of longitudes on the dynamics grid
  integer, parameter :: lonsld = _PAR_LONSLD
  integer, parameter :: ilgsld = lonsld + 2

  !     * number of gaussian latitudes on physics grid
  integer, parameter :: nlat = _PAR_NLAT
  integer, parameter :: ilat = nlat/nnodex

  !     * number of gaussian latitudes on dymanics grid
  integer, parameter :: nlatd = _PAR_NLATD
  integer, parameter :: ilatd = nlatd/nnodex

  !.....parameters related to using multi- or sub- latitudes

  !     * grid points per slice in physics
  integer, parameter :: lonp = _PAR_LON

  !     * grid points per slice in dynamics
  integer, parameter :: lond = _PAR_LOND

  !     * number of chained physics latitudes or 1 for sublatitude cases
  integer, parameter :: nlatj = max(lonp/lonsl,1)

  !     * number of chained dymanics latitudes or 1 for sublatitude cases
  integer, parameter :: nlatjd = max(lond/lonsld,1)

  integer, parameter :: nlatjm = max(nlatj,nlatjd)

  !     * physics "chained latitude" dimension
  !     * ilg = lonp+1        for sublatitude case
  !     * ilg = ilgsl*nlatj+1 for chained latitude case
  integer, parameter :: ilg = _PAR_ILG

  !     * zonal mean latitude dimension
  integer, parameter :: ilgz = lonsz + 1

  !
  !     * physics tile gathered array dimension
  !
  integer, parameter :: ilgl = ilg * ntld
  integer, parameter :: ilgk = ilg * ntlk
  integer, parameter :: ilgw = ilg * ntwt
  integer, parameter :: ilgm = max(ilgl,ilgk,ilgw)

  !     * dynamics "chained latitude" dimension
  !     * ilgd = lond+1          for sublatitude case
  !     * ilgd = ilgsld*nlatjd+1 for chained latitude case
  integer, parameter :: ilgd = _PAR_ILGD

  !     * physics "chained latitude" dimension
  integer, parameter :: ilg_tp = ilgsl * nlatj + 1

  !     * dynamics "chained latitude" dimension
  integer, parameter :: ilg_td = ilgsld * nlatjd + 1

  !     * used to dimension arrays in "GR1" common
  integer, parameter :: idlm = max(ilg * ilev,ilgd * ilev,ilg_tp * ilev)

  !     * first dimension of physics "pak" arrays
  integer, parameter :: ip0j = lonsl * ilat + 1

  !     * first dimension of zonal "pak" arrays
  integer, parameter :: ip0jz = lonsz * ilat + 1

  !     * first dimension of dynamics "pak" arrays
  integer, parameter :: dp0j = lonsld * ilatd + 1

  integer, parameter :: lon_tp = lonsl * nlatj

  integer, parameter :: lon_td = lonsld * nlatjd

  !     * number of itterations in physics lattitude loop
  integer, parameter :: ntask_p = lonsl * ilat/lonp

  !     * number of itterations in dynamics lattitude loop
  integer, parameter :: ntask_d = lonsld * ilatd/lond

  integer, parameter :: ntask_m = max(ntask_p,ntask_d)

  !     * number of itterations in physics lattitude loop for sublatitude case
  integer, parameter :: ntask_tp = ilat/nlatj

  !     * number of itterations in dynamics lattitude loop for sublatitude case
  integer, parameter :: ntask_td = ilatd/nlatjd

  !.....spectral parameters

  !     * first dimension of complex (fourier) arrays
  integer, parameter :: ilh = ilgsl/2
  integer, parameter :: ilhd = ilgsld/2

  !     * total spectral triangle sizes
  integer, parameter :: lmtotal = _PAR_LMTOTAL
  integer, parameter :: latotal = (lmtotal + 1) * lmtotal/2
  integer, parameter :: iram = latotal + lmtotal

  !     * local (to each node) spectral sizes
  integer, parameter :: lm = lmtotal/nnodex
  integer, parameter :: lmp1 = lm + 1
  integer, parameter :: la = latotal/nnodex

  !     * complex :: work space dimensions (local to each node)
  integer, parameter :: rl = la * ilev
  integer, parameter :: rs = la * levs

  !     * length of idat data buffer in common/icom/
  !ignoreLint(5)
  integer, parameter :: ip0f = max(max(ilgsld * nlatd,2 * latotal),20000) + 8

  !     * size of gll work array
  integer, parameter :: ngll = max(ilgsld * nlatd,((ilev + 2) ** 2) * nlatd)
  !
  !     * ctem parameters from base file.
  !
  !
  !     lndcvrmod ! = 12345 means continuously update land cover
  !                   from 1850 to 2100
  !               ! = 1990 or any other year means use land cover
  !               !   for july of that year all the time.
  !
  !     lndcvr_offset ! = offset to add to model year to get calendar
  !                   !   year to use for land cover when lndcvrmod = 12345
  !
  !     initpool  ! = -ve means, initialize pools from coupler restart file
  !               ! = +ve means, initialize pools as below even if there are
  !                   values in the restart file.
  !               ! = 0 means, initialize pools from zero
  !               ! = 1850 or any other value means initialize pools from that year.
  !               !   of course, the relevant file must be present.
  !
  integer, parameter :: initpool = _PAR_INITPOOL
  integer, parameter :: lndcvrmod = _PAR_LNDCVRMOD
  integer, parameter :: lndcvr_offset = _PAR_LNDCVR_OFFSET

end module msizes
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
