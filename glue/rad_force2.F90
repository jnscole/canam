#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module rad_force2
  !
  !     * feb 07/2011 - m.lazare. unused "curr_time" removed.
  !     * may 08/2010 - m.lazare. radforce update directive changed to cpp.
  !     * feb 15/2009 - l.solheim. previous version radforce.
  implicit none
#if defined radforce

      use msizes, only : ip0j

      !--- character variable to identify the set of perturbations to use
      character(len = 10) :: rf_scenario = " "

      !--- rf_adj and rf_inst flag the use of adjusted or instantaneous
      !--- radiative forcing, respectively
      logical :: rf_adj
      logical :: rf_inst

      !--- trop_idx is the model level index at which the tropopause is
      !--- assumed to exist. trop_idx=0 means use instantaneous radiative
      !--- forcing while trop_idx>0 means use adjusted radiative forcing
      integer :: trop_idx = 0

      !--- number of years to offset years_ghg array
      !--- so that the kount value at the start of the model run
      !--- corresponds with start_year_r
      integer :: start_year_r = 1850

      !--- character variables used to form variable names for output
      character(len = 4) :: rfname = "    "
      character(len = 2) :: rfp_id

      !--- found_rf is used to flag existence of certain files
      logical :: found_rf

      !--- number of ghg constituents
      integer, parameter :: nghg = 7

      !--- current values of time dependent ghg constituents
      real, dimension(nghg) :: curr_ghg

      !--- length of ghg time series
      integer, parameter :: ntghg = 22

      !--- time (in years) for ghg time series
      real*8 , dimension(ntghg) :: years_ghg
      data years_ghg / &
     0.0,  50.0,  70.0,  80.0,  90.0, 100.0, &
   110.0, 120.0, 130.0, 140.0, 150.0, 160.0, &
   170.0, 180.0, 190.0, 200.0, 210.0, 220.0, &
   230.0, 240.0, 250.0, 251.0/

      !--- space for ghg time series (ntghg values for each constituent)
      real, dimension(nghg,ntghg) :: ghgts

      !---------------------------------------------------------
      !--- ghg time series for ipcc ar4 scenario a1b (1850-2100)
      !---------------------------------------------------------

      real, dimension(5,ntghg) :: ghgts_a1b

      !--- co2 -- ar4 scenario a1b ---
      data ghgts_a1b(1,:) / &
   288.e-6, 296.e-6, 302.e-6, 305.e-6, 308.e-6, 311.e-6, &
   316.e-6, 325.e-6, 337.e-6, 352.e-6, 367.e-6, 388.e-6, &
   418.e-6, 447.e-6, 483.e-6, 522.e-6, 563.e-6, 601.e-6, &
   639.e-6, 674.e-6, 703.e-6, 706.e-6/

      !--- ch4 -- ar4 scenario a1b ---
      data ghgts_a1b(2,:) / &
   0.792e-6, 0.879e-6, 0.978e-6, 1.036e-6, 1.088e-6, 1.147e-6, &
   1.247e-6, 1.420e-6, 1.570e-6, 1.700e-6, 1.760e-6, 1.871e-6, &
   2.026e-6, 2.202e-6, 2.337e-6, 2.400e-6, 2.386e-6, 2.301e-6, &
   2.191e-6, 2.078e-6, 1.974e-6, 1.9636e-6/

      !--- n2o -- ar4 scenario a1b ---
      data ghgts_a1b(3,:) / &
  0.2755e-6, 0.2800e-6, 0.2815e-6, 0.2830e-6, 0.2850e-6, 0.2870e-6, &
  0.2910e-6, 0.2950e-6, 0.3010e-6, 0.3080e-6, 0.3160e-6, 0.3240e-6, &
  0.3310e-6, 0.3380e-6, 0.3440e-6, 0.3500e-6, 0.3560e-6, 0.3600e-6, &
  0.3650e-6, 0.3680e-6, 0.3720e-6, 0.3724e-6/

      !--- cfc11 -- ar4 scenario a1b ---
      data ghgts_a1b(4,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0175e-9, 0.050e-9, 0.164e-9, 0.258e-9,  0.267e-9, 0.246e-9, &
  0.214e-9,  0.180e-9, 0.149e-9, 0.123e-9,  0.101e-9, 0.083e-9, &
  0.068e-9,  0.056e-9, 0.045e-9, 0.0439e-9/

      !--- cfc12 -- ar4 scenario a1b ---
      data ghgts_a1b(5,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0303e-9, 0.109e-9, 0.290e-9, 0.467e-9,  0.535e-9, 0.527e-9, &
  0.486e-9,  0.441e-9, 0.400e-9, 0.362e-9,  0.328e-9, 0.298e-9, &
  0.270e-9,  0.245e-9, 0.222e-9, 0.2197e-9/

      !--------------------------------------------------------
      !--- ghg time series for ipcc ar4 scenario a2 (1850-2100)
      !--------------------------------------------------------

      real, dimension(5,ntghg) :: ghgts_a2

      !--- co2 -- ar4 scenario a2 ---
      data ghgts_a2(1,:) / &
  288.e-6, 296.e-6, 302.e-6, 305.e-6, 308.e-6, 311.e-6, &
  316.e-6, 325.e-6, 337.e-6, 352.e-6, 367.e-6, 386.e-6, &
  414.e-6, 444.e-6, 481.e-6, 522.e-6, 568.e-6, 620.e-6, &
  682.e-6, 754.e-6, 836.e-6, 844.2e-6/

      !--- ch4 -- ar4 scenario a2 ---
      data ghgts_a2(2,:) / &
  0.792e-6, 0.879e-6, 0.978e-6, 1.036e-6, 1.088e-6, 1.147e-6, &
  1.247e-6, 1.420e-6, 1.570e-6, 1.700e-6, 1.760e-6, 1.861e-6, &
  1.997e-6, 2.163e-6, 2.357e-6, 2.562e-6, 2.779e-6, 3.011e-6, &
  3.252e-6, 3.493e-6, 3.731e-6, 3.7548e-6/

      !--- n2o -- ar4 scenario a2 ---
      data ghgts_a2(3,:) / &
  0.2755e-6, 0.2800e-6, 0.2815e-6, 0.2830e-6, 0.2850e-6, 0.2870e-6, &
  0.2910e-6, 0.2950e-6, 0.3010e-6, 0.3080e-6, 0.3160e-6, 0.3250e-6, &
  0.3350e-6, 0.3470e-6, 0.3600e-6, 0.3730e-6, 0.3870e-6, 0.4010e-6, &
  0.4160e-6, 0.4320e-6, 0.4470e-6, 0.4485e-6/

      !--- cfc11 -- ar4 scenario a2 ---
      data ghgts_a2(4,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0175e-9, 0.050e-9, 0.164e-9, 0.258e-9,  0.267e-9, 0.246e-9, &
  0.214e-9,  0.180e-9, 0.149e-9, 0.123e-9,  0.101e-9, 0.083e-9, &
  0.068e-9,  0.056e-9, 0.045e-9, 0.0439e-9/

      !--- cfc12 -- ar4 scenario a2 ---
      data ghgts_a2(5,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0303e-9, 0.109e-9, 0.290e-9, 0.467e-9,  0.535e-9, 0.527e-9, &
  0.486e-9,  0.441e-9, 0.400e-9, 0.362e-9,  0.328e-9, 0.298e-9, &
  0.270e-9,  0.245e-9, 0.222e-9, 0.2197e-9/

      !--------------------------------------------------------
      !--- ghg time series for ipcc ar4 scenario b1 (1850-2100)
      !--------------------------------------------------------

      real, dimension(5,ntghg) :: ghgts_b1

      !--- co2 -- ar4 scenario b1 ---
      data ghgts_b1(1,:) / &
  288.e-6, 296.e-6, 302.e-6, 305.e-6, 308.e-6, 311.e-6, &
  316.e-6, 325.e-6, 337.e-6, 352.e-6, 367.e-6, 386.e-6, &
  410.e-6, 432.e-6, 457.e-6, 482.e-6, 503.e-6, 518.e-6, &
  530.e-6, 538.e-6, 540.e-6, 540.2e-6/

      !--- ch4 -- ar4 scenario b1 ---
      data ghgts_b1(2,:) / &
  0.792e-6, 0.879e-6, 0.978e-6, 1.036e-6, 1.088e-6, 1.147e-6, &
  1.247e-6, 1.420e-6, 1.570e-6, 1.700e-6, 1.760e-6, 1.827e-6, &
  1.891e-6, 1.927e-6, 1.919e-6, 1.881e-6, 1.836e-6, 1.797e-6, &
  1.741e-6, 1.663e-6, 1.574e-6, 1.5651e-6/

      !--- n2o -- ar4 scenario b1 ---
      data ghgts_b1(3,:) / &
  0.2755e-6, 0.2800e-6, 0.2815e-6, 0.2830e-6, 0.2850e-6, 0.2870e-6, &
  0.2910e-6, 0.2950e-6, 0.3010e-6, 0.3080e-6, 0.3160e-6, 0.3240e-6, &
  0.3330e-6, 0.3410e-6, 0.3490e-6, 0.3570e-6, 0.3630e-6, 0.3680e-6, &
  0.3710e-6, 0.3740e-6, 0.3750e-6, 0.3751e-6/

      !--- cfc11 -- ar4 scenario b1 ---
      data ghgts_b1(4,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0175e-9, 0.050e-9, 0.164e-9, 0.258e-9,  0.267e-9, 0.246e-9, &
  0.214e-9,  0.180e-9, 0.149e-9, 0.123e-9,  0.101e-9, 0.083e-9, &
  0.068e-9,  0.056e-9, 0.045e-9, 0.0439e-9/

      !--- cfc12 -- ar4 scenario b1 ---
      data ghgts_b1(5,:) / &
  0.,        0.,       0.,       0.,        0.,       0., &
  0.0303e-9, 0.109e-9, 0.290e-9, 0.467e-9,  0.535e-9, 0.527e-9, &
  0.486e-9,  0.441e-9, 0.400e-9, 0.362e-9,  0.328e-9, 0.298e-9, &
  0.270e-9,  0.245e-9, 0.222e-9, 0.2197e-9/

      !--- namelist used to modify run time parameters
      namelist /rf_rtp/ rf_scenario,trop_idx,start_year_r
#else
      !--- stub for module used with radiative forcing option
#endif
end module rad_force2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
