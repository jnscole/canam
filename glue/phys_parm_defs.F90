!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module phys_parm_defs

!--- Jason Cole  ...8 Dec 2017

!-------------------------------------------------------------
!--- Set up namelists to hold physical parameters
!-------------------------------------------------------------

!--- This module will hold a list of physical parameters
!--- used in the AGCM.

!--- Define defaults.
implicit none

real ::  pp_solar_const !< Solar "constant", aka the total solar irradiance at 1 AU \f$[Wm^{-2}]\f$
integer :: pp_rdm_num_pert !< Perturbation to the random number generator "seed" \f$[unitless]\f$
real :: pp_mam_damp_gw_time !< Period of time to damp gravity waves at beginning of simulation \f$[s]\f$
real :: pp_vtau_background !< Target background stratospheric optical depth at 550 nm \f$[unitless]\f$
real :: pp_ext_sa_background !< Target background stratospheric extinction at 550 nm \f$[m^{-1}]\f$
real :: pp_e_folding_strat_aerosol !< e-folding for decay of stratospheric aerosols \f$[s]\f$

! For now "switches" are being added to this module.  To be updated later.
logical :: switch_decay_strat_aerosol !< Switch to control if the stratospheric aerosols decay from initial value \f$[unitless]\f$
logical :: switch_solar_refraction    !< Switch to control if the direct-beam is refracted in solar radiative transfer \f$[unitless]\f$
logical :: switch_zero_thermal_source !< Switch to control if the thermal sources in thermal radiative transfer \f$[unitless]\f$
logical :: switch_gas_chem !< Switch to run with trace gas chemistry \f$[T,F]\f$

real :: ap_uicefac
real :: ap_facaut
real :: ap_facacc
real :: ap_cdnc_fac
real :: ap_cdnc_exp
real :: ap_cdnc_scale
real :: ap_ct
real :: ap_scale_scmbf
real :: ap_weight
real*8 :: ap_fmax
real :: ap_alf
real :: ap_taus1
real :: ap_c0fac
real :: ap_rkxmin
real :: ap_rkmn
real :: ap_rkhmin
real :: ap_rkqmin
real :: ap_xlmin
real :: ap_almc_min
real :: ap_csigma
real :: ap_scale_cvsg
real :: ap_drngat

!--- namelist used to read in parmsub parameters
!--- This namelist is read in init_phys_parm

namelist /phys_parm/ pp_solar_const,       &
                     pp_rdm_num_pert,      &
                     pp_mam_damp_gw_time,  &
                     pp_vtau_background,   &
                     pp_ext_sa_background, &
                     pp_e_folding_strat_aerosol

namelist /switches/ switch_decay_strat_aerosol, &
                    switch_solar_refraction,    &
                    switch_zero_thermal_source, &
                    switch_gas_chem

namelist /adjustable_parm/ ap_uicefac,     &
                           ap_facaut,      &
                           ap_facacc,      &
                           ap_cdnc_fac,    &
                           ap_cdnc_exp,    &
                           ap_cdnc_scale,  &
                           ap_ct,          &
                           ap_scale_scmbf, &
                           ap_weight,      &
                           ap_fmax,        &
                           ap_alf,         &
                           ap_taus1,       &
                           ap_c0fac,       &
                           ap_rkxmin,      &
                           ap_rkmn,        &
                           ap_rkhmin,      &
                           ap_rkqmin,      &
                           ap_xlmin,       &
                           ap_almc_min,    &
                           ap_csigma,      &
                           ap_scale_cvsg,  &
                           ap_drngat

end module phys_parm_defs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
