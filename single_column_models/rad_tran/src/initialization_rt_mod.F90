!>\file
!>\brief Routines needed to initialize input for the radiative transfer.
!!
!! @author Jason Cole
!

module initialization_rt_mod
  implicit none
contains
  
!===============================================================================
!===============================================================================
  subroutine init_rng(cfg)
    
    ! Initialize the seeds for the kissvec random number generator.
    
    use read_config_file_mod, only : cfg_values
    use arrays_mod, only : iseedrow
    
    implicit none
    
    ! input
    type(cfg_values), intent(in) :: cfg !< Configuration 
    
    ! local
    integer :: il     ! Counter over columns
    integer :: iloop  ! Counter

    real(kind=8), dimension(cfg%ilg) :: randumb

    iseedrow(:,1) = cfg%iseedrow
    iseedrow(:,2) = cfg%iseedrow
    iseedrow(:,3) = cfg%iseedrow
    iseedrow(:,4) = cfg%iseedrow
    
    ! "Prime" the random number generator
    do iloop = 1, 100
      call kissvec(iseedrow(1,1), & ! Inout
                   iseedrow(1,2), &
                   iseedrow(1,3), &
                   iseedrow(1,4), &
                   randumb,       & ! Output
                   cfg%il1,       & ! Input
                   cfg%il2,       &
                   cfg%ilg        )
    end do ! iloop
        
    return
    
  end subroutine init_rng
  
!===============================================================================
!===============================================================================
  subroutine init_common_blocks(cfg)
    
    ! Initialize the common blocks needed for the radiative transfer
    
    use read_config_file_mod, only : cfg_values
    use parameters_mod, only : p_thresh
    
    implicit none
    
    ! input
    type(cfg_values), intent(in) :: cfg !< Configuration

    ! local
    real :: pii
    real :: fvort
    real :: rrsq
    real :: cosd
    real :: sind

    common /eccent/ rrsq,cosd,sind

    ! Common block for absorber mixing ratios.

    real :: rmco2, rmch4, rmn2o, rmo2, rmf11, rmf12, rmf113, rmf114
    common /trace / rmco2, rmch4, rmn2o, rmo2, rmf11, rmf12, rmf113, rmf114
    
    call xcw2_data(cfg%idist)
    call radcons4()
    call spwcon11(fvort,pii)
    call ckdsw4
    call ckdlw4
    call datcldop3
    
  end subroutine init_common_blocks
  
end module initialization_rt_mod

!> \file
!> Routines to read in the inputs needed for the radiative transfer 
!! calculations.
