!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine albval2(alb,ib,tau_in,csz_in,wind_in,chl_in, &
                   nstart,nend,nval)
  !***********************************************************************
  ! j.cole    ...april 17, 2018.  limit the cosine of solar zenith angle to
  !                               max and min in table.
  ! m.lazare  ...jul 20, 2014. modified to pass in start and stop indices.
  ! l.solheim ...apr 2,2004.   prevous version albval.
  !
  ! given nval values for optical depth (tau_in), cosine of solar zenith
  ! angle (csz_in), wind speed (wind_in) and chlorophyll concentration
  ! (chl_in), return nval surface albedo values for band ib. these albedo
  ! values are determined using a lookup table produces by z. jin and
  ! thomas charlock at nasa.
  !
  ! quadra-linear interpolation is done over the nodal values for tau,
  ! csz, wind and chl. extrapolation is done on values outside of the
  ! nodal range. this produces reasonable values as long as the point
  ! is "close" to an end point and is efficient because no checks need
  ! to be made on out of range values. however, linear extrapolation is
  ! generally not good for points not "close" to the end points. therefore
  ! care should be taken so that this routine is not called with
  ! interpolation points that fall outside of the nodal point range.
  !
  ! input:
  !  ib            ...frequency band for which albedos are to be evaluated
  !  tau_in (nval) ...optical depths
  !  csz_in (nval) ...cosines of solar zenith angle
  !  wind_in(nval) ...wind speeds (m/s)
  !  chl_in (nval) ...chlorophyll concentrations (mg/m**3)
  !  nstart        ...starting index number.
  !  nend          ... end     index number
  !  nval          ...number of albedo values requested
  !
  ! output:
  !  alb(nval) ...surface albedo values corresponding to input values of
  !               optical depth, zenith angle, wind speed and chlorophyll
  !               concentration for band ib
  !***********************************************************************

  implicit none

  !--- input/output parameters
  integer, intent(in)     :: ib !< Variable description\f$[units]\f$
  integer, intent(in)     :: nval !< Variable description\f$[units]\f$
  integer, intent(in)     :: nstart !< Variable description\f$[units]\f$
  integer, intent(in)     :: nend !< Variable description\f$[units]\f$
  real, intent(in),  dimension(nval) :: tau_in !< Variable description\f$[units]\f$
  real, intent(in),  dimension(nval) :: csz_in !< Variable description\f$[units]\f$
  real, intent(in),  dimension(nval) :: wind_in !< Variable description\f$[units]\f$
  real, intent(in),  dimension(nval) :: chl_in !< Variable description\f$[units]\f$

  real, intent(inout),  dimension(nval) :: alb !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !--- local work space
  real, dimension(2) :: wt !<
  real, dimension(2) :: ws !<
  real, dimension(2) :: ww !<
  real, dimension(2) :: wc !<
  real, dimension(nval) :: csz_loc !<

  integer :: i, it, is, iw, ic, itt, iss, iww, icc, mvidx
  real :: wtt

  !--- nodal values found in lookup table

  !--- table dimensions
  ! nb ...number of bands
  !       4 currently: 0.20-0.69, 0.69-1.19, 1.19-2.38, 2.38-4.00 um
  ! nt ...number of aerosol/cloud optical depths
  ! ns ...number of solar zenith angles
  ! nw ...number of wind speeds
  ! nc ...number of chlorophyll concentration values
  
  integer, parameter :: nb=4
  integer, parameter :: nt=16
  integer, parameter :: ns=15
  integer, parameter :: nw=7
  integer, parameter :: nc=5

  !--- aerosol/cloud optical depth: tau<1.0 aerosol, tau>1.0 cloud
  real,   parameter, dimension(nt) :: tau=(/ 0.00, 0.05, 0.10, 0.16, 0.24, 0.35,  0.5,  0.7,0.99, 1.30, 1.80, 2.50, 5.00, 9.00, 15.0, 25.0 /) !<

  !--- cosine of solar zenith angle
  real,   parameter, dimension(ns) :: csz=(/ 0.05, 0.09, 0.15, 0.21, 0.27, 0.33, 0.39, 0.45,0.52, 0.60, 0.68, 0.76, 0.84, 0.92, 1.0 /) !<

  !--- wind speeds in m/s
  real,   parameter, dimension(nw) :: wind=(/ 0.0, 3.0, 6.0, 9.0, 12.0, 15.0, 18.0 /) !<

  !--- ocean chlorophyll concentrations in mg/m**3
  real,   parameter, dimension(nc) :: chl=(/ 0.0, 0.1, 0.5, 2.0, 12.0 /) !<

  !--- common space for lookup table data
  real, dimension(nb,nc,nw,ns,nt) :: atable
  integer :: init_table
  common /salbtab/ atable, init_table
  !

  !.....abort if atable has not been initialized
  if (init_table/=1) then
    write(6,*)'albval: Lookup table has not been initialized'
    call xit('ALBVAL',-1)
  end if

  !.....ensure ib is valid
  if (ib<1.or.ib>nb) then
    write(6,*)'albval: ib is out of range. ib = ',ib
    call xit('ALBVAL',-2)
  end if

  !....bound input csz to be within min and max of lookup table
  do i=nstart,nend
    csz_loc(i) = max(min(csz_in(i),csz(ns)),csz(1))
  end do

  do i=nstart,nend
    !.......calculate weigths
    it=mvidx(tau,nt,tau_in(i))
    is=mvidx(csz,ns,csz_loc(i))
    iw=mvidx(wind,nw,wind_in(i))
    ic=mvidx(chl,nc,chl_in(i))
    wt(2) = (tau_in(i)-tau(it))/(tau(it+1)-tau(it))
    wt(1) = 1.0-wt(2)
    ws(2) = (csz_loc(i)-csz(is))/(csz(is+1)-csz(is))
    ws(1) = 1.0-ws(2)
    ww(2) = (wind_in(i)-wind(iw))/(wind(iw+1)-wind(iw))
    ww(1) = 1.0-ww(2)
    wc(2) = (chl_in(i)-chl(ic))/(chl(ic+1)-chl(ic))
    wc(1) = 1.0-wc(2)

    !.......calculate alb(i) for band ib, using quadra-linear interpolation
    alb(i)=0.0
    do itt=it,it+1
      do iss=is,is+1
        do iww=iw,iw+1
          do icc=ic,ic+1
            wtt=wt(itt-it+1)*ws(iss-is+1)*ww(iww-iw+1)*wc(icc-ic+1)
            alb(i) = alb(i) + wtt*atable(ib,icc,iww,iss,itt)
          end do
        end do
      end do
    end do
  end do
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
