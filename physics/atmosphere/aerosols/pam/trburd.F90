subroutine trburd (voabrd,vbcbrd,vasbrd,vmdbrd,vssbrd,xrow,iaind, &
                   dshj,pressg,grav,ntrac,ntracp,il1,il2,ilg,ilev, &
                   ilevp1,msg)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !
  !
  !     history:
  !     --------
  !     * feb 10/2015 - k.vonsalzen  correction of vertical levels
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  !
  implicit none
  real, intent(in) :: grav
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilevp1
  integer, intent(in) :: ilg
  integer :: imlex
  integer :: imlin
  integer :: ioff
  integer :: is
  integer :: ist
  integer :: isx
  integer :: kx
  integer :: l
  integer, intent(in) :: msg
  integer, intent(in) :: ntrac
  integer, intent(in) :: ntracp
  !
  integer, intent(in), dimension(ntracp) :: iaind !<
  real, intent(in), dimension(ilg) :: pressg !<
  real, intent(in), dimension(ilg,ilev) :: dshj !<
  real, intent(in), dimension(ilg,ilevp1,ntrac) :: xrow !<
  real, intent(out), dimension(ilg) :: voabrd !<
  real, intent(out), dimension(ilg) :: vbcbrd !<
  real, intent(out), dimension(ilg) :: vasbrd !<
  real, intent(out), dimension(ilg) :: vmdbrd !<
  real, intent(out), dimension(ilg) :: vssbrd !<
  !
  !     * initialization.
  !
  ioff = ilevp1 - ilev
  voabrd = 0.
  vbcbrd = 0.
  vasbrd = 0.
  vmdbrd = 0.
  vssbrd = 0.
  !
  !     * organic carbon burden.
  !
  if (isextoc > 0) then
    do isx = 1,isextoc
      is = iexoc(isx)
      imlex = iaind(sextf%isaer(is)%ism)
      do il = il1,il2
        do l = msg + 1,ilev
          voabrd(il) = voabrd(il) &
                       + xrow(il,l + ioff,imlex) * pressg(il) * dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintoc > 0) then
    do isx = 1,isintoc
      is = iinoc(isx)
      do ist = 1,sintf%isaer(is)%itypt
        kx = sintf%isaer(is)%ityp(ist)
        if (kx == kintoc) then
          imlin = iaind(sintf%isaer(is)%ism(ist))
          do il = il1,il2
            do l = msg + 1,ilev
              voabrd(il) = voabrd(il) &
                           + xrow(il,l + ioff,imlin) * pressg(il) * dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     * black carbon burden.
  !
  if (isextbc > 0) then
    do isx = 1,isextbc
      is = iexbc(isx)
      imlex = iaind(sextf%isaer(is)%ism)
      do il = il1,il2
        do l = msg + 1,ilev
          vbcbrd(il) = vbcbrd(il) &
                       + xrow(il,l + ioff,imlex) * pressg(il) * dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintbc > 0) then
    do isx = 1,isintbc
      is = iinbc(isx)
      do ist = 1,sintf%isaer(is)%itypt
        kx = sintf%isaer(is)%ityp(ist)
        if (kx == kintbc) then
          imlin = iaind(sintf%isaer(is)%ism(ist))
          do il = il1,il2
            do l = msg + 1,ilev
              vbcbrd(il) = vbcbrd(il) &
                           + xrow(il,l + ioff,imlin) * pressg(il) * dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     * sulphate burden.
  !
  if (isextso4 > 0) then
    do isx = 1,isextso4
      is = iexso4(isx)
      imlex = iaind(sextf%isaer(is)%ism)
      do il = il1,il2
        do l = msg + 1,ilev
          vasbrd(il) = vasbrd(il) &
                       + xrow(il,l + ioff,imlex) * pressg(il) * dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintso4 > 0) then
    do isx = 1,isintso4
      is = iinso4(isx)
      do ist = 1,sintf%isaer(is)%itypt
        kx = sintf%isaer(is)%ityp(ist)
        if (kx == kintso4) then
          imlin = iaind(sintf%isaer(is)%ism(ist))
          do il = il1,il2
            do l = msg + 1,ilev
              vasbrd(il) = vasbrd(il) &
                           + xrow(il,l + ioff,imlin) * pressg(il) * dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     * mineral dust burden.
  !
  if (isextmd > 0) then
    do isx = 1,isextmd
      is = iexmd(isx)
      imlex = iaind(sextf%isaer(is)%ism)
      do il = il1,il2
        do l = msg + 1,ilev
          vmdbrd(il) = vmdbrd(il) &
                       + xrow(il,l + ioff,imlex) * pressg(il) * dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintmd > 0) then
    do isx = 1,isintmd
      is = iinmd(isx)
      do ist = 1,sintf%isaer(is)%itypt
        kx = sintf%isaer(is)%ityp(ist)
        if (kx == kintmd) then
          imlin = iaind(sintf%isaer(is)%ism(ist))
          do il = il1,il2
            do l = msg + 1,ilev
              vmdbrd(il) = vmdbrd(il) &
                           + xrow(il,l + ioff,imlin) * pressg(il) * dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     * sea salt burden.
  !
  if (isextss > 0) then
    do isx = 1,isextss
      is = iexss(isx)
      imlex = iaind(sextf%isaer(is)%ism)
      do il = il1,il2
        do l = msg + 1,ilev
          vssbrd(il) = vssbrd(il) &
                       + xrow(il,l + ioff,imlex) * pressg(il) * dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintss > 0) then
    do isx = 1,isintss
      is = iinss(isx)
      do ist = 1,sintf%isaer(is)%itypt
        kx = sintf%isaer(is)%ityp(ist)
        if (kx == kintss) then
          imlin = iaind(sintf%isaer(is)%ism(ist))
          do il = il1,il2
            do l = msg + 1,ilev
              vssbrd(il) = vssbrd(il) &
                           + xrow(il,l + ioff,imlin) * pressg(il) * dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
end subroutine trburd
