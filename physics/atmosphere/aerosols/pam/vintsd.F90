subroutine vintsd (vvol,pen0,pephi0,pepsi,pedphi0,pephis0, &
                   pewetrc,penum,pemas,pin0,piphi0,pipsi, &
                   pidphi0,piphis0,piwetrc,pinum,pimas,vitrm, &
                   rads,rade,isdiag,ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     volume aerosol size distribution using bin scheme - for diagnostic
  !     purposes.
  !
  !     history:
  !     --------
  !     * feb 10/2010 - k.vonsalzen   newly installed.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  !
  implicit none
  real :: fdphi0s
  real :: fphie
  real :: fphis
  integer :: il
  integer, intent(in) :: ilga
  integer :: is
  integer, intent(in) :: isdiag
  integer :: kx
  integer :: l
  integer, intent(in) :: leva
  integer :: nfilt
  integer :: nsub
  real, intent(in) :: rade
  real, intent(in) :: rads
  !
  real, intent(in), dimension(ilga,leva) :: vitrm !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !<
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(out), dimension(ilga,isdiag) :: vvol !<
  real, dimension(ilga,leva,isdiag) :: fvolo !<
  real, dimension(ilga,leva,isdiag) :: fivol !<
  real, dimension(ilga,leva,isdiag) :: fevol !<
  real, dimension(ilga,leva,isdiag) :: fphis0 !<
  real, dimension(ilga,leva,isdiag) :: fdphi0 !<
  !
  !-----------------------------------------------------------------------
  !     * width of diagnostic sections.
  !
  fphis = log(rads/r0)
  fphie = log(rade/r0)
  fdphi0s = (fphie - fphis)/real(isdiag)
  fdphi0 = fdphi0s
  !
  !     * particle sizes corresponding to section boundaries.
  !
  fphis0(:,:,1) = fphis
  do is = 2,isdiag
    fphis0(:,:,is) = fphis0(:,:,1) + real(is - 1) * fdphi0(:,:,is - 1)
  end do
  !
  !-----------------------------------------------------------------------
  !     * strength of filter. no filter, if zero. the particle size
  !     * range of the filter is always smaller than the smallest
  !     * section width.
  !
  if (isaint > 0) then
    nsub = int(aintf%dpstar/fdphi0s)
  end if
  if (isaext > 0) then
    do kx = 1,kext
      nsub = min(nsub,int(aextf%tp(kx)%dpstar/fdphi0s))
    end do
  end if
  !      nfilt=int(0.5*(real(nsub)-1.))
  nfilt = 1
  !
  !-----------------------------------------------------------------------
  !     * integrate concentrations over sub-sections and filter.
  !
  fvolo = 0.
  if (isaint > 0) then
    call intsd(fivol,fphis0,fdphi0,pinum,pimas,pin0,piphi0, &
               pipsi,piphis0,pidphi0,piwetrc,pidryrc, &
               isdiag,nfilt,ilga,leva,isaint)
    fvolo = fvolo + fivol
  end if
  if (isaext > 0) then
    call intsd(fevol,fphis0,fdphi0,penum,pemas,pen0,pephi0, &
               pepsi,pephis0,pedphi0,pewetrc,pedryrc, &
               isdiag,nfilt,ilga,leva,isaext)
    fvolo = fvolo + fevol
  end if
  !
  !     * vertically integrated volume size distribution.
  !
  do il = 1,ilga
    vvol(il,:) = 0.
    do l = 1,leva
      vvol(il,:) = vvol(il,:) + fvolo(il,l,:) * vitrm(il,l)
    end do
  end do
  !
end subroutine vintsd
