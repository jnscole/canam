subroutine ccnc(cecdnc,cerci,cicdnc,circi,cewetrb,ceddnsl, &
                ceddnis,cenuio,cemolw,cekappa,ciwetrb,ciddnsl, &
                ciddnis,cinuio,cimolw,cikappa,ciepsm,pen0, &
                pephi0,pepsi,pephis0,pedphi0,pin0,piphi0, &
                pipsi,piphis0,pidphi0,zclf,clwc,sv,ta,zcthr, &
                ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     cloud condensation number nuclei concentration for given
  !     maximum supersaturation in cloud layer.
  !
  !     history:
  !     --------
  !     * jun 10/2014 - k.vonsalzen   code clean-up: external cloud
  !     *                             threshold, replace calculations of
  !     *                             cloud droplet number and critical
  !     *                             size by instantaneous values
  !     * feb 27/2014 - k.vonsalzen   more efficient gathering
  !                                   of cloud layers
  !     * feb 10/2010 - k.vonsalzen   newly installed
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use scparm
  !
  implicit none
  integer :: il
  integer, intent(in) :: ilga
  integer :: ilgatt
  integer :: ilgt
  integer :: ilo
  integer :: ismax
  integer :: ismin
  integer :: kx
  integer :: l
  integer, intent(in) :: leva
  real :: rmax
  real :: rmin
  real, intent(in) :: zcthr
  !
  real, intent(inout), dimension(ilga,leva,kext) :: cecdnc !<
  real, intent(inout), dimension(ilga,leva,kext) :: cerci !<
  real, intent(inout), dimension(ilga,leva)      :: cicdnc !<
  real, intent(inout), dimension(ilga,leva)      :: circi !<
  real, intent(inout), dimension(ilga,leva) :: sv !<
  real, intent(in), dimension(ilga,leva) :: zclf !<
  real, intent(in), dimension(ilga,leva) :: clwc !<
  real, intent(in), dimension(ilga,leva) :: ta !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in), dimension(ilga,leva,isextb) :: cewetrb !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnsl !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnis !<
  real, intent(in), dimension(ilga,leva,isextb) :: cenuio !<
  real, intent(in), dimension(ilga,leva,isextb) :: cemolw !<
  real, intent(in), dimension(ilga,leva,isextb) :: cekappa !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnsl !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnis !<
  real, intent(in), dimension(ilga,leva,isintb) :: cinuio !<
  real, intent(in), dimension(ilga,leva,isintb) :: cimolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: cikappa !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciepsm !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: tephis0 !<
  real, allocatable, dimension(:,:,:) :: tedphi0 !<
  real, allocatable, dimension(:,:,:) :: terci !<
  real, allocatable, dimension(:,:,:) :: tecdnc !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tiphis0 !<
  real, allocatable, dimension(:,:,:) :: tidphi0 !<
  real, allocatable, dimension(:,:) :: tirci !<
  real, allocatable, dimension(:,:) :: ticdnc !<
  real, allocatable, dimension(:,:,:) :: tewetrb !<
  real, allocatable, dimension(:,:,:) :: teddnsl !<
  real, allocatable, dimension(:,:,:) :: teddnis !<
  real, allocatable, dimension(:,:,:) :: tenuio !<
  real, allocatable, dimension(:,:,:) :: temolw !<
  real, allocatable, dimension(:,:,:) :: tekappa !<
  real, allocatable, dimension(:,:,:) :: terc !<
  real, allocatable, dimension(:,:,:) :: tesc !<
  real, allocatable, dimension(:,:,:) :: tiwetrb !<
  real, allocatable, dimension(:,:,:) :: tiddnsl !<
  real, allocatable, dimension(:,:,:) :: tiddnis !<
  real, allocatable, dimension(:,:,:) :: tinuio !<
  real, allocatable, dimension(:,:,:) :: timolw !<
  real, allocatable, dimension(:,:,:) :: tikappa !<
  real, allocatable, dimension(:,:,:) :: tirc !<
  real, allocatable, dimension(:,:,:) :: tisc !<
  real, allocatable, dimension(:,:,:) :: tiepsm !<
  real, allocatable, dimension(:,:) :: svt !<
  real, allocatable, dimension(:,:) :: tat !<
  integer, allocatable, dimension(:) :: ilgat !<
  integer, allocatable, dimension(:) :: lb !<
  integer, allocatable, dimension(:) :: lt !<
  integer, allocatable, dimension(:) :: l0 !<
  logical, allocatable, dimension(:,:) :: klclc !<
  !
  !     * parameters.
  !
  integer, parameter :: levat = 1 !<
  integer, parameter :: loffs = 1 !<
  logical :: kcld !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory.
  !
  allocate(klclc(ilga,leva))
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  if (kext > 0) then
    cecdnc = yna
    do kx = 1,kext
      cerci(:,:,kx) = yna
    end do
  end if
  if (kint > 0) then
    cicdnc = yna
    circi = yna
  end if
  do l = 1,leva
    do il = 1,ilga
      if ( (1. - zclf(il,l)) < zcthr .and. clwc(il,l) > ysmall) then
        klclc(il,l) = .true.
      else
        klclc(il,l) = .false.
      end if
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * find bottom and top index for cloud layers.
  !
  ilgatt = 0
  do il = 1,ilga
    kcld = .false.
    !
    !       * determine total number of cloud layers.
    !
    if (klclc(il,leva) ) then
      kcld = .true.
      ilgatt = ilgatt + 1
    end if
    do l = leva,2, - 1
      if ( .not.kcld .and. klclc(il,l - 1) ) then
        kcld = .true.
        ilgatt = ilgatt + 1
      else if (kcld .and. .not.klclc(il,l - 1) ) then
        kcld = .false.
      end if
    end do
    if (kcld .and. klclc(il,1) ) then
      kcld = .false.
    end if
  end do
  !
  !     * allocate arrays to save cloud layer location and level indices.
  !
  if (ilgatt > 0) then
    allocate(ilgat(ilgatt))
    allocate(lb(ilgatt))
    allocate(lt(ilgatt))
    allocate(l0(ilgatt))
    !
    ilgt = 0
    do il = 1,ilga
      kcld = .false.
      !
      !         * detect bottom and top of each cloud layer.
      !
      if (klclc(il,leva) ) then
        kcld = .true.
        ilgt = ilgt + 1
        lb(ilgt) = leva
        l0(ilgt) = leva
        ilgat(ilgt) = il
      end if
      do l = leva,2, - 1
        if ( .not.kcld .and. klclc(il,l - 1) ) then
          kcld = .true.
          ilgt = ilgt + 1
          lb(ilgt) = l - 1
          l0(ilgt) = min(lb(ilgt) + loffs,leva)
          ilgat(ilgt) = il
        else if (kcld .and. .not.klclc(il,l - 1) ) then
          kcld = .false.
          lt(ilgt) = l
        end if
      end do
      if (kcld .and. klclc(il,1) ) then
        kcld = .false.
        lt(ilgt) = 1
      end if
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * allocation of arrays.
  !
  if (ilgatt > 0) then
    allocate(svt(ilgatt,levat))
    allocate(tat(ilgatt,levat))
    if (isextb > 0) then
      allocate(teddnsl(ilgatt,levat,isextb))
      allocate(teddnis(ilgatt,levat,isextb))
      allocate(tenuio (ilgatt,levat,isextb))
      allocate(tekappa(ilgatt,levat,isextb))
      allocate(temolw (ilgatt,levat,isextb))
      allocate(tewetrb(ilgatt,levat,isextb))
      allocate(terc   (ilgatt,levat,isextb))
      allocate(tesc   (ilgatt,levat,isextb))
    end if
    if (isaext > 0) then
      allocate(ten0   (ilgatt,levat,isaext))
      allocate(tephi0 (ilgatt,levat,isaext))
      allocate(tepsi  (ilgatt,levat,isaext))
      allocate(tephis0(ilgatt,levat,isaext))
      allocate(tedphi0(ilgatt,levat,isaext))
    end if
    if (kext > 0) then
      allocate(tecdnc (ilgatt,levat,kext))
      allocate(terci  (ilgatt,levat,kext))
    end if
    if (isintb > 0) then
      allocate(tiddnsl(ilgatt,levat,isintb))
      allocate(tiddnis(ilgatt,levat,isintb))
      allocate(tinuio (ilgatt,levat,isintb))
      allocate(tikappa(ilgatt,levat,isintb))
      allocate(timolw (ilgatt,levat,isintb))
      allocate(tiwetrb(ilgatt,levat,isintb))
      allocate(tirc   (ilgatt,levat,isintb))
      allocate(tisc   (ilgatt,levat,isintb))
      allocate(tiepsm (ilgatt,levat,isintb))
    end if
    if (isaint > 0) then
      allocate(tin0   (ilgatt,levat,isaint))
      allocate(tiphi0 (ilgatt,levat,isaint))
      allocate(tipsi  (ilgatt,levat,isaint))
      allocate(tiphis0(ilgatt,levat,isaint))
      allocate(tidphi0(ilgatt,levat,isaint))
    end if
    if (kint > 0) then
      allocate(ticdnc (ilgatt,levat))
      allocate(tirci  (ilgatt,levat))
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * initialization of air and aerosol properties for subcloud layer.
  !
  do il = 1,ilgatt
    ilo = ilgat(il)
    svt(il,1) = sv(ilo,lb(il))
    tat(il,1) = ta(ilo,lb(il))
    if (isextb > 0) then
      teddnsl (il,1,:) = ceddnsl(ilo,l0(il),:)
      teddnis (il,1,:) = ceddnis(ilo,l0(il),:)
      tenuio  (il,1,:) = cenuio (ilo,l0(il),:)
      tekappa (il,1,:) = cekappa(ilo,l0(il),:)
      temolw  (il,1,:) = cemolw (ilo,l0(il),:)
      tewetrb (il,1,:) = cewetrb(ilo,l0(il),:)
    end if
    if (isaext > 0) then
      ten0   (il,1,:) = pen0   (ilo,l0(il),:)
      tephi0 (il,1,:) = pephi0 (ilo,l0(il),:)
      tepsi  (il,1,:) = pepsi  (ilo,l0(il),:)
      tephis0(il,1,:) = pephis0(ilo,l0(il),:)
      tedphi0(il,1,:) = pedphi0(ilo,l0(il),:)
    end if
    if (isintb > 0) then
      tiddnsl (il,1,:) = ciddnsl(ilo,l0(il),:)
      tiddnis (il,1,:) = ciddnis(ilo,l0(il),:)
      tinuio  (il,1,:) = cinuio (ilo,l0(il),:)
      tikappa (il,1,:) = cikappa(ilo,l0(il),:)
      timolw  (il,1,:) = cimolw (ilo,l0(il),:)
      tiwetrb (il,1,:) = ciwetrb(ilo,l0(il),:)
      tiepsm  (il,1,:) = ciepsm (ilo,l0(il),:)
    end if
    if (isaint > 0) then
      tin0   (il,1,:) = pin0   (ilo,l0(il),:)
      tiphi0 (il,1,:) = piphi0 (ilo,l0(il),:)
      tipsi  (il,1,:) = pipsi  (ilo,l0(il),:)
      tiphis0(il,1,:) = piphis0(ilo,l0(il),:)
      tidphi0(il,1,:) = pidphi0(ilo,l0(il),:)
    end if
  end do
  !
  !     * diagnose ccn mass mixing ratio and critical radius.
  !
  if (ilgatt > 0) then
    call ccnpar(terc,tesc,tirc,tisc,tat,teddnsl,teddnis, &
                tenuio,tekappa,temolw,tiddnsl,tiddnis,tinuio, &
                tikappa,timolw,tiepsm,svt,ilgatt,levat, &
                iexkap,iinkap)
    call cncdnca(tecdnc,terci,ticdnc,tirci,tewetrb,terc,tesc, &
                 tiwetrb,tirc,tisc,ten0,tephi0,tepsi,tephis0, &
                 tedphi0,tin0,tiphi0,tipsi,tiphis0,tidphi0,svt, &
                 ilgatt,levat)
  end if
  !
  !     * apply results to entire cloud layer.
  !
  if (kext > 0) then
    do kx = 1,kext
      do il = 1,ilgatt
        ilo = ilgat(il)
        cecdnc(ilo,lt(il):lb(il),kx) = tecdnc(il,1,kx)
        cerci (ilo,lt(il):lb(il),kx) = terci (il,1,kx)
      end do
    end do
  end if
  if (kint > 0) then
    do il = 1,ilgatt
      ilo = ilgat(il)
      cicdnc(ilo,lt(il):lb(il)) = ticdnc(il,1)
      circi (ilo,lt(il):lb(il)) = tirci (il,1)
    end do
  end if
  do il = 1,ilgatt
    ilo = ilgat(il)
    sv(ilo,lt(il):lb(il)) = svt(il,1)
  end do
  !
  !     * deallocation of arrays.
  !
  if (ilgatt > 0) then
    deallocate(ilgat)
    deallocate(lb)
    deallocate(lt)
    deallocate(l0)
    deallocate(svt)
    deallocate(tat)
    if (isextb > 0) then
      deallocate(teddnsl)
      deallocate(teddnis)
      deallocate(tenuio)
      deallocate(tekappa)
      deallocate(temolw)
      deallocate(tewetrb)
      deallocate(terc)
      deallocate(tesc)
    end if
    if (isaext > 0) then
      deallocate(ten0)
      deallocate(tephi0)
      deallocate(tepsi)
      deallocate(tephis0)
      deallocate(tedphi0)
    end if
    if (kext > 0) then
      deallocate(tecdnc)
      deallocate(terci)
    end if
    if (isintb > 0) then
      deallocate(tiddnsl)
      deallocate(tiddnis)
      deallocate(tinuio)
      deallocate(tikappa)
      deallocate(timolw)
      deallocate(tiwetrb)
      deallocate(tirc)
      deallocate(tisc)
      deallocate(tiepsm)
    end if
    if (isaint > 0) then
      deallocate(tin0)
      deallocate(tiphi0)
      deallocate(tipsi)
      deallocate(tiphis0)
      deallocate(tidphi0)
    end if
    if (kint > 0) then
      deallocate(ticdnc)
      deallocate(tirci)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * limit range for critical radius results.
  !
  if (kext > 0) then
    do kx = 1,kext
      ismin = peismnk(kx)
      ismax = peismxk(kx)
      rmin = pedryr(ismin)%vl
      rmax = pedryr(ismax)%vr
      where (abs(cerci(:,:,kx) - yna) > ytiny)
        cerci(:,:,kx) = min(max(cerci(:,:,kx),rmin),rmax)
      end where
    end do
  end if
  if (kint > 0) then
    rmin = pidryr(1)%vl
    rmax = pidryr(isaint)%vr
    where (abs(circi - yna) > ytiny)
      circi = min(max(circi,rmin),rmax)
    end where
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate memory.
  !
  deallocate(klclc)
  !
end subroutine ccnc
