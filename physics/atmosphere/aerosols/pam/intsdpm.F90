subroutine intsdpm(fmass,fmassd,pnum,pmas,pn0,phi0,psi, &
                   phis0,dphi0,drydn,wetrc,dryrc,radc, &
                   rhoa,isn,ilga,leva,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     pm2.5 and pm10 concentrations.
  !
  !     history:
  !     --------
  !     * mar 5/2018 - k.vonsalzen   new
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use sdcode
  !
  implicit none
  real :: fdphid
  real :: fgr
  real :: fphied
  real :: fphis
  real :: fphisd
  integer :: il
  integer, intent(in) :: ilga
  integer :: is
  integer :: isd
  integer, intent(in) :: isec
  integer, intent(in) :: isn
  integer :: l
  integer, intent(in) :: leva
  real :: rmom3
  real :: tcutd
  !
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !<
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !<
  real, intent(in), dimension(ilga,leva,isec) :: pnum !<
  real, intent(in), dimension(ilga,leva,isec) :: pmas !<
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real, intent(in), dimension(ilga,leva,isec) :: psi !<
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !<
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !<
  real, intent(in), dimension(ilga,leva,isec) :: drydn !<
  real, intent(in), dimension(isec)           :: dryrc !<
  real, intent(in), dimension(ilga,leva)      :: rhoa !<
  real, intent(in), dimension(isn)            :: radc !<
  real, intent(out), dimension(ilga,leva,isn) :: fmass !<
  real, intent(out), dimension(ilga,leva,isn) :: fmassd !<
  !
  real, dimension(ilga,leva,isn) :: fmom3 !<
  real, dimension(ilga,leva,isn) :: fmom3d !<
  real, dimension(:,:,:), allocatable :: tphis0 !<
  real, dimension(:,:,:), allocatable :: tphi0 !<
  real, dimension(:,:,:), allocatable :: wetdn !<
  real, dimension(:,:,:), allocatable :: alfgr !<
  real, dimension(isn) :: tphic !<
  !
  !-----------------------------------------------------------------------
  !     * integrate pla size distribution over given sub-sections.
  !
  tphic = log(radc/r0)
  rmom3 = 3.
  fmass = 0.
  fmassd = 0.
  fmom3 = 0.
  fmom3d = 0.
  if (isec > 0) then
    !        allocate(tphis0(ilga,leva,isec))
    !        allocate(tphi0(ilga,leva,isec))
    !        allocate(wetdn(ilga,leva,isec))
    allocate(alfgr(ilga,leva,isec))
    !
    !       * section boundaries and density for wet size distribution.
    !
    do is = 1,isec
      do l = 1,leva
        do il = 1,ilga
          !          tphis0(il,l,is)=phis0(il,l,is)
          !          tphi0(il,l,is)=phi0(il,l,is)
          !          wetdn(il,l,is)=drydn(il,l,is)
          alfgr(il,l,is) = 0.
          if (pnum(il,l,is) > ytiny .and. pmas(il,l,is) > ytiny &
              .and. abs(psi(il,l,is) - yna) > ytiny) then
            fgr = wetrc(il,l,is)/dryrc(is)
            alfgr(il,l,is) = log(fgr)
            !            tphis0(il,l,is)=tphis0(il,l,is)+alfgr(il,l,is)
            !            tphi0(il,l,is)=tphi0(il,l,is)+alfgr(il,l,is)
            !            fgr3i=1./fgr(il,l,is)**3
            !            wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
          end if
        end do
      end do
    end do
    !
    !       * integrate dry mass size distribution between smallest
    !       * available particle radius and dry cutoff size.
    !
    do isd = 1,isn
      do is = 1,isec
        do l = 1,leva
          do il = 1,ilga
            fphis = phis0(il,l,is) + alfgr(il,l,is)
            fphisd = phis0(il,l,is)
            !
            !         * this assumes that the air in the instrument inlet is
            !         * not preheated and particles are collected at ambient
            !         * relative humdity. filter samples are subject to
            !         * to gravitmetric analysis in a dry laboratory.
            !
            if (tphic(isd) >= fphis) then
              tcutd = tphic(isd) - alfgr(il,l,is)
              fphied = min(phis0(il,l,is) + dphi0(il,l,is), &
                       tcutd)
              fdphid = max(fphied - fphisd,0.)
              if (abs(psi(il,l,is) - yna) > ytiny) then
                fmom3(il,l,isd) = fmom3(il,l,isd) &
                                  + ycnst * drydn(il,l,is) * pn0(il,l,is) &
                                  * sdintb(phi0(il,l,is),psi(il,l,is), &
                                  rmom3,fphisd,fdphid)
              end if
            end if
            !
            !         * this assumes that the air in the instrument inlet is
            !         * preheated so that only dry particles are sampled.
            !
            if (tphic(isd) >= fphisd) then
              fphied = min(phis0(il,l,is) + dphi0(il,l,is), &
                       tphic(isd))
              fdphid = max(fphied - fphisd,0.)
              if (abs(psi(il,l,is) - yna) > ytiny) then
                fmom3d(il,l,isd) = fmom3d(il,l,isd) &
                                   + ycnst * drydn(il,l,is) * pn0(il,l,is) &
                                   * sdintb(phi0(il,l,is),psi(il,l,is), &
                                   rmom3,fphisd,fdphid)
                !
                !              * total wet mass, including aerosol water
                !
                !              fphie=min(tphis0(il,l,is)+dphi0(il,l,is),tphic(isd))
                !              fdphi=max(fphie-fphis,0.)
                !              fmom3(il,l,isd)=fmom3(il,l,isd) &
                !                      +ycnst*wetdn(il,l,is)*pn0(il,l,is) &
                !                      *sdintb(tphi0(il,l,is),psi(il,l,is), &
                !                              rmom3,fphis,fdphi)
              end if
            end if
          end do
        end do
      end do
    end do
    !        deallocate(tphis0)
    !        deallocate(tphi0)
    !        deallocate(wetdn)
    deallocate(alfgr)
  end if
  !
  !     * convert from mass mixing ratio to concentration.
  !
  do is = 1,isn
    fmass(:,:,is) = fmom3(:,:,is) * rhoa
    fmassd(:,:,is) = fmom3d(:,:,is) * rhoa
  end do
  !
end subroutine intsdpm
