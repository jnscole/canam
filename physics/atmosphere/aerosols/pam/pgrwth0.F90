subroutine pgrwth0 (tnum,resn,i0l,i0r,dp0l,dp0r,dp0s, &
                    pn0,ilga,leva,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     particle growth for particle number.
  !
  !     it is assumed that sections initially have the same size and that
  !     growth leads to a an equal or smaller width of each section.
  !     the subroutine treats growth in particle size space based on
  !     a lagrangian approach.
  !
  !     history:
  !     --------
  !     * feb 20/2005 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use fpdef
  !
  implicit none
  integer :: il
  integer :: is
  integer :: isi
  integer :: l
  !
  real, intent(out), dimension(ilga,leva,isec) :: tnum !<
  real, intent(inout), dimension(ilga,leva) :: resn !<
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0s !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  integer, intent(in), dimension(ilga,leva,isec) :: i0l !<
  integer, intent(in), dimension(ilga,leva,isec) :: i0r !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  tnum = 0.
  !
  !-----------------------------------------------------------------------
  !     * calculate number and mass in each section.
  !
  do isi = 1,isec
    do l = 1,leva
      do il = 1,ilga
        !
        !        * calculate contribution in each section from the left-hand
        !        * side part of the modified section.
        !
        is = i0l(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            ) then
          !
          !          * number size distribution from n(phi(t))=n(phi(t=0))
          !
          tnum(il,l,is) = tnum(il,l,is) + pn0(il,l,isi) * dp0l(il,l,isi)
        end if
        !
        !        * calculate contribution in each section from the right-hand
        !        * side part of the modified section.
        !
        is = i0r(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            .and. i0l(il,l,isi) /= is) then
          !
          !          * number size distribution from n(phi(t))=n(phi(t=0))
          !
          tnum(il,l,is) = tnum(il,l,is) + pn0(il,l,isi) * dp0r(il,l,isi)
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * residuum due to particles growing to sizes larger than
  !     * the spectrum cutoff.
  !
  do isi = 1,isec
    do l = 1,leva
      do il = 1,ilga
        if ( (i0r(il,l,isi) == (isec + 1) .or. i0l(il,l,isi) == 0) &
            .and. pn0(il,l,isi) > ytiny) then
          !
          !          * number size distribution from n(phi(t))=n(phi(t=0))
          !
          resn(il,l) = resn(il,l) + pn0(il,l,isi) * dp0s(il,l,isi)
        end if
      end do
    end do
  end do
  !
end subroutine pgrwth0
