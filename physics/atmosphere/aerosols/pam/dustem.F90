subroutine dustem (pedndt,pedmdt,pidfdt,pidndt,pidmdt,pimas, &
                   pedphi0,pephis0,pidphi0,piphis0, &
                   pifrc,dpa,dt,grav,ilga,leva, &
                   flnd,gt,smfr,fn,zspd,ustard, &
                   suz0,slai,spot,st02,st03,st04,st06, &
                   st13,st14,st15,st16,st17, &
                   isvdust,defa,defc, &
                   fall,fa10,fa2,fa1, &
                   duwd,dust,duth,usmk, &
                   diagmas,diagnum,isdust,desize,deflux,defnum, &
                   isdiag)

  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     mineral dust emissions.
  !
  !     history:
  !     --------
  !     * feb 05/2015 - m.lazare/     new version for gcm18:
  !     *               k.vonsalzen.  - flnd passed in instead of {gc,mask},
  !     *                               for fractional land mask support.
  !     *                             - {zspd,ustard} passed in instead of {sfcu,sfcv,gust}
  !     *                               because this screen-level quantity
  !     *                               already calculated in earlier routines.
  !     *                               this changes and simplifies the calculation of
  !     *                               ustar.
  !     *                             - adjust total concentrations after emission of
  !     *                               min. dust to account for lack of emissions from
  !     *                               non-land fraction of grid cell. emissions are
  !     *                               done only over the land fraction of the grid square.
  !     * aug 25/2009 - k.vonsalzen   modified for aerocom size parameters
  !                                   and emission flux from bulk scheme.
  !     * jun 14/2007 - k.vonsalzen   new
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use duparm1
  !
  implicit none
  integer :: il
  integer, intent(in) :: ilga
  integer :: is
  integer, intent(in) :: isdiag
  integer, intent(in) :: isdust
  integer, intent(in) :: isvdust
  integer, intent(in) :: leva
  !
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !<
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !<
  real, intent(out), dimension(ilga,isdust * 2) :: diagmas !<
  real, intent(out), dimension(ilga,isdust * 2) :: diagnum !<
  real, intent(out), dimension(ilga,isdiag) :: desize !<
  real, intent(out), dimension(ilga,isdiag) :: deflux !<
  real, intent(out), dimension(ilga,isdiag) :: defnum !<
  real, intent(in) :: dt !<
  real, intent(in) :: grav !<
  real, intent(in),  dimension(ilga) :: flnd !<
  real, intent(in),  dimension(ilga) :: gt !<
  real, intent(in),  dimension(ilga) :: smfr !<
  real, intent(in),  dimension(ilga) :: fn !<
  real, intent(in),  dimension(ilga) :: zspd !<
  real, intent(in),  dimension(ilga) :: ustard !<
  real, intent(in),  dimension(ilga) :: suz0 !<
  real, intent(in),  dimension(ilga) :: slai !<
  real, intent(in),  dimension(ilga) :: spot !<
  real, intent(in),  dimension(ilga) :: st02 !<
  real, intent(in),  dimension(ilga) :: st03 !<
  real, intent(in),  dimension(ilga) :: st04 !<
  real, intent(in),  dimension(ilga) :: st06 !<
  real, intent(in),  dimension(ilga) :: st13 !<
  real, intent(in),  dimension(ilga) :: st14 !<
  real, intent(in),  dimension(ilga) :: st15 !<
  real, intent(in),  dimension(ilga) :: st16 !<
  real, intent(in),  dimension(ilga) :: st17 !<
  real, intent(in),  dimension(ilga,leva) :: dpa !<
  real, intent(in),  dimension(ilga,leva,isaext) :: pedphi0 !<
  real, intent(in),  dimension(ilga,leva,isaext) :: pephis0 !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in),  dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in),  dimension(ilga,leva,isaint,kint) :: pifrc !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pimas !<
  real, allocatable, dimension(:) :: fedphis !<
  real, allocatable, dimension(:) :: fephiss !<
  real, allocatable, dimension(:) :: feddns !<
  real, allocatable, dimension(:) :: fidphis !<
  real, allocatable, dimension(:) :: fiphiss !<
  real, allocatable, dimension(:) :: fiddns !<
  real, allocatable, dimension(:,:) :: aephi0 !<
  real, allocatable, dimension(:,:) :: aen0 !<
  real, allocatable, dimension(:,:) :: aepsi !<
  real, allocatable, dimension(:,:) :: aiphi0 !<
  real, allocatable, dimension(:,:) :: ain0 !<
  real, allocatable, dimension(:,:) :: aipsi !<
  real, allocatable, dimension(:,:) :: cimast !<
  real, allocatable, dimension(:,:,:) :: fephi0 !<
  real, allocatable, dimension(:,:,:) :: fen0 !<
  real, allocatable, dimension(:,:,:) :: fepsi !<
  real, allocatable, dimension(:,:,:) :: feddn !<
  real, allocatable, dimension(:,:,:) :: fedphi0 !<
  real, allocatable, dimension(:,:,:) :: fephis0 !<
  real, allocatable, dimension(:,:,:) :: fenum !<
  real, allocatable, dimension(:,:,:) :: femas !<
  real, allocatable, dimension(:,:,:) :: cenum !<
  real, allocatable, dimension(:,:,:) :: cemas !<
  real, allocatable, dimension(:,:,:) :: fiphi0 !<
  real, allocatable, dimension(:,:,:) :: fin0 !<
  real, allocatable, dimension(:,:,:) :: fipsi !<
  real, allocatable, dimension(:,:,:) :: fiddn !<
  real, allocatable, dimension(:,:,:) :: fidphi0 !<
  real, allocatable, dimension(:,:,:) :: fiphis0 !<
  real, allocatable, dimension(:,:,:) :: finum !<
  real, allocatable, dimension(:,:,:) :: fimas !<
  real, allocatable, dimension(:,:,:) :: cinum !<
  real, allocatable, dimension(:,:,:) :: cimas !<
  real, allocatable, dimension(:,:,:) :: cimaf !<
  !
  real, intent(out), dimension(ilga) :: defa !<
  real, intent(out), dimension(ilga) :: defc !<
  real, intent(out), dimension(ilga) :: fall !<
  real, intent(out), dimension(ilga) :: fa10 !<
  real, intent(out), dimension(ilga) :: fa2 !<
  real, intent(out), dimension(ilga) :: fa1 !<
  real, intent(out), dimension(ilga) :: duwd !<
  real, intent(out), dimension(ilga) :: dust !<
  real, intent(out), dimension(ilga) :: duth !<
  real, intent(out), dimension(ilga) :: usmk !<
  !
  !-----------------------------------------------------------------------
  !     * allocation of work arrays.
  !
  if (isextmd > 0) then
    allocate (fedphis(isextmd))
    allocate (fephiss(isextmd))
    allocate (feddns (isextmd))
    allocate (aephi0 (ilga,isextmd))
    allocate (aen0   (ilga,isextmd))
    allocate (aepsi  (ilga,isextmd))
    allocate (fephi0 (ilga,1,isextmd))
    allocate (fen0   (ilga,1,isextmd))
    allocate (fepsi  (ilga,1,isextmd))
    allocate (feddn  (ilga,1,isextmd))
    allocate (fedphi0(ilga,1,isextmd))
    allocate (fephis0(ilga,1,isextmd))
    allocate (fenum  (ilga,1,isextmd))
    allocate (femas  (ilga,1,isextmd))
    allocate (cenum  (ilga,1,isextmd))
    allocate (cemas  (ilga,1,isextmd))
  end if
  if (isintmd > 0) then
    allocate (fidphis(isintmd))
    allocate (fiphiss(isintmd))
    allocate (fiddns (isintmd))
    allocate (aiphi0 (ilga,isintmd))
    allocate (ain0   (ilga,isintmd))
    allocate (aipsi  (ilga,isintmd))
    allocate (fiphi0 (ilga,1,isintmd))
    allocate (fin0   (ilga,1,isintmd))
    allocate (fipsi  (ilga,1,isintmd))
    allocate (fiddn  (ilga,1,isintmd))
    allocate (fidphi0(ilga,1,isintmd))
    allocate (fiphis0(ilga,1,isintmd))
    allocate (finum  (ilga,1,isintmd))
    allocate (fimas  (ilga,1,isintmd))
    allocate (cinum  (ilga,1,isintmd))
    allocate (cimas  (ilga,1,isintmd))
    allocate (cimast (ilga,isintmd))
  end if
  if (isaint > 0) then
    allocate (cimaf  (ilga,isaint,kint))
  end if
  !
  !     * pla section information.
  !
  if (kextmd > 0) then
    do is = 1,isextmd
      fedphi0(:,1,is) = pedphi0(:,leva,iexmd(is))
      fephis0(:,1,is) = pephis0(:,leva,iexmd(is))
      fedphis(is) = pedphis(iexmd(is))
      fephiss(is) = pephiss(iexmd(is))
      feddns (is) = aextf%tp(kextmd)%dens
    end do
  end if
  if (kintmd > 0) then
    do is = 1,isintmd
      fidphi0(:,1,is) = pidphi0(:,leva,iinmd(is))
      fiphis0(:,1,is) = piphis0(:,leva,iinmd(is))
      fidphis(is) = pidphis(iinmd(is))
      fiphiss(is) = piphiss(iinmd(is))
      fiddns (is) = aintf%tp(kintmd)%dens
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * mineral dust flux in terms of pla parameters.
  !
  if (kextmd > 0) then
    call dustp(aen0,aephi0,aepsi,ilga,isextmd, &
               flnd,gt,smfr,fn,zspd,ustard, &
               suz0,slai,spot,st02,st03,st04,st06, &
               st13,st14,st15,st16,st17, &
               isvdust,defa,defc, &
               fall,fa10,fa2,fa1, &
               duwd,dust,duth,usmk, &
               diagmas,diagnum,isdust)

  end if
  if (kintmd > 0) then
    call dustp(ain0,aiphi0,aipsi,ilga,isintmd, &
               flnd,gt,smfr,fn,zspd,ustard, &
               suz0,slai,spot,st02,st03,st04,st06, &
               st13,st14,st15,st16,st17, &
               isvdust,defa,defc, &
               fall,fa10,fa2,fa1, &
               duwd,dust,duth,usmk, &
               diagmas,diagnum,isdust)
  end if
  !
  !     * scale diagnostic output arrays by fraction of land.
  !
  do il = 1,ilga
    diagmas(il,:) = flnd(il) * diagmas(il,:)
    diagnum(il,:) = flnd(il) * diagnum(il,:)
  end do
  !
  !-----------------------------------------------------------------------
  !     * convert results in preparation of conversion from
  !     * pla parameters to aerosol number and mass concentrations.
  !
  if (kextmd > 0) then
    fen0   (:,1,:) = aen0 (:,:)
    fephi0 (:,1,:) = aephi0(:,:)
    fepsi  (:,1,:) = aepsi (:,:)
    do is = 1,isextmd
      feddn(:,1,is) = aextf%tp(kextmd)%dens
    end do
    !
    if (isvdust > 0) then
      call dplaout(fen0,fephi0,fepsi,feddn, &
                   isdiag,ilga,desize,deflux,defnum)
    end if
    !
  end if
  if (kintmd > 0) then
    fin0   (:,1,:) = ain0 (:,:)
    fiphi0 (:,1,:) = aiphi0(:,:)
    fipsi  (:,1,:) = aipsi (:,:)
    do is = 1,isintmd
      fiddn(:,1,is) = aintf%tp(kintmd)%dens
    end do
  end if
  !
  !     * mass and number fluxes (kg/m2/sec, resp. 1/m2/sec).
  !
  if (kextmd > 0) then
    call pla2nm(fenum,femas,fen0,fephi0,fepsi,fephis0,fedphi0, &
                feddn,ilga,1,isextmd)
  end if
  if (kintmd > 0) then
    call pla2nm(finum,fimas,fin0,fiphi0,fipsi,fiphis0,fidphi0, &
                fiddn,ilga,1,isintmd)
  end if
  !
  !     * convert fluxes to change in concentrations in first model
  !     * layer.
  !
  if (kextmd > 0) then
    do is = 1,isextmd
      where (abs(fenum(:,1,is) - yna) > ytiny &
          .and. abs(femas(:,1,is) - yna) > ytiny)
        cenum(:,1,is) = fenum(:,1,is) * grav * dt/dpa(:,leva)
        cemas(:,1,is) = femas(:,1,is) * grav * dt/dpa(:,leva)
      else where
        cenum(:,1,is) = 0.
        cemas(:,1,is) = 0.
      end where
    end do
  end if
  if (kintmd > 0) then
    do is = 1,isintmd
      where (abs(finum(:,1,is) - yna) > ytiny &
          .and. abs(fimas(:,1,is) - yna) > ytiny)
        cinum(:,1,is) = finum(:,1,is) * grav * dt/dpa(:,leva)
        cimas(:,1,is) = fimas(:,1,is) * grav * dt/dpa(:,leva)
      else where
        cinum(:,1,is) = 0.
        cimas(:,1,is) = 0.
      end where
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * mass and number tendencies for externally mixed aerosol.
  !
  if (isaext > 0) then
    pedndt = 0.
    pedmdt = 0.
  end if
  do is = 1,isextmd
    pedndt(:,leva,iexmd(is)) = cenum(:,1,is)/dt
    pedmdt(:,leva,iexmd(is)) = cemas(:,1,is)/dt
  end do
  !
  !     * scale output arrays for externally mixed aerosols
  !     * by fraction of land.
  !
  do il = 1,ilga
    pedndt(il,:,:) = flnd(il) * pedndt(il,:,:)
    pedmdt(il,:,:) = flnd(il) * pedmdt(il,:,:)
  end do
  !
  !-----------------------------------------------------------------------
  !
  !     * adjust total concentrations after emission of min. dust to
  !     * account for lack of emissions from non-land fraction of grid cell.
  !
  if (kintmd > 0) then
    do is = 1,isintmd
      cimast(:,is) = pimas(:,leva,iinmd(is)) + flnd(:) * cimas(:,1,is)
    end do
  end if
  !
  !     * new mineral dust fraction for internally mixed types of
  !     * aerosol.
  !
  if (kint > 0) then
    cimaf = pifrc(:,leva,:,:)
  end if
  if (kintmd > 0) then
    do is = 1,isintmd
      do il = 1,ilga
        if (cimast(il,is) > ytiny) then
          cimaf(il,iinmd(is),:) = cimaf(il,iinmd(is),:) &
                                  * pimas(il,leva,iinmd(is))/cimast(il,is)
          cimaf(il,iinmd(is),kintmd) = cimaf(il,iinmd(is),kintmd) &
                                       + flnd(il) * cimas(il,1,is)/cimast(il,is)
        else
          cimaf(il,iinmd(is),:) = pifrc(il,leva,iinmd(is),:)
        end if
      end do
    end do
  end if
  !
  !     * mass, number, and mass fraction tendencies for internally
  !     * mixed types of aerosol.
  !
  if (isaint > 0) then
    pidndt = 0.
    pidmdt = 0.
    pidfdt = 0.
  end if
  if (kint > 0) then
    pidfdt(:,leva,:,:) = (cimaf - pifrc(:,leva,:,:))/dt
  end if
  if (kintmd > 0) then
    do is = 1,isintmd
      pidndt(:,leva,iinmd(is)) = cinum(:,1,is)/dt
      pidmdt(:,leva,iinmd(is)) = cimas(:,1,is)/dt
    end do
  end if
  !
  !     * scale output arrays for inernally mixed aerosols
  !     * by fraction of land.
  !
  do il = 1,ilga
    pidndt(il,:,:) = flnd(il) * pidndt(il,:,:)
    pidmdt(il,:,:) = flnd(il) * pidmdt(il,:,:)
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocation.
  !
  if (isextmd > 0) then
    deallocate (fedphis)
    deallocate (fephiss)
    deallocate (feddns)
    deallocate (aephi0)
    deallocate (aen0)
    deallocate (aepsi)
    deallocate (fephi0)
    deallocate (fen0)
    deallocate (fepsi)
    deallocate (feddn)
    deallocate (fedphi0)
    deallocate (fephis0)
    deallocate (fenum)
    deallocate (femas)
    deallocate (cenum)
    deallocate (cemas)
  end if
  if (isintmd > 0) then
    deallocate (fidphis)
    deallocate (fiphiss)
    deallocate (fiddns)
    deallocate (aiphi0)
    deallocate (ain0)
    deallocate (aipsi)
    deallocate (fiphi0)
    deallocate (fin0)
    deallocate (fipsi)
    deallocate (fiddn)
    deallocate (fidphi0)
    deallocate (fiphis0)
    deallocate (finum)
    deallocate (fimas)
    deallocate (cinum)
    deallocate (cimas)
    deallocate (cimast)
  end if
  if (isaint > 0) then
    deallocate (cimaf)
  end if
  !
end subroutine dustem
