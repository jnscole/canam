subroutine intfrc (amldfrow,reamfrow,veamfrow,fr1frow,fr2frow, &
                   ssldfrow,ressfrow,vessfrow,dsldfrow,redsfrow, &
                   vedsfrow,bcldfrow,rebcfrow,vebcfrow,ocldfrow, &
                   reocfrow,veocfrow,zcdnfrow,bcicfrow,bcdpfrow, &
                   amldfrol,reamfrol,veamfrol,fr1frol,fr2frol, &
                   ssldfrol,ressfrol,vessfrol,dsldfrol,redsfrol, &
                   vedsfrol,bcldfrol,rebcfrol,vebcfrol,ocldfrol, &
                   reocfrol,veocfrol,zcdnfrol,bcicfrol,bcdpfrol, &
                   il1,il2,ilg,ilev,delt,gmt,iday,mday)
  !
  !     * apr  3/13 - k.von salzen. new
  !     *                       interpolation of forcing fields.
  !
  !     * xxrow  = chemistry field    for previous timestep.
  !     * xxrol  = chemistry field    at next physics time.
  !
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * mday   = date of target sst.
  !
  implicit none
  real :: day
  real :: daysm
  real, intent(in) :: delt
  real :: fmday
  real, intent(in) :: gmt
  integer :: i
  integer, intent(in) :: iday
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: istepsm
  integer :: l
  integer, intent(in) :: mday
  real :: stepsm
  !
  !     * single-level fields.
  !
  real, intent(inout), dimension(ilg) :: bcdpfrow !<
  real, intent(in), dimension(ilg) :: bcdpfrol !<
  !
  !     * multi-level fields.
  !
  real, intent(inout), dimension(ilg,ilev) :: amldfrow !<
  real, intent(in), dimension(ilg,ilev) :: amldfrol !<
  real, intent(inout), dimension(ilg,ilev) :: reamfrow !<
  real, intent(in), dimension(ilg,ilev) :: reamfrol !<
  real, intent(inout), dimension(ilg,ilev) :: veamfrow !<
  real, intent(in), dimension(ilg,ilev) :: veamfrol !<
  real, intent(inout), dimension(ilg,ilev) :: fr1frow !<
  real, intent(in), dimension(ilg,ilev) :: fr1frol !<
  real, intent(inout), dimension(ilg,ilev) :: fr2frow !<
  real, intent(in), dimension(ilg,ilev) :: fr2frol !<
  real, intent(inout), dimension(ilg,ilev) :: ssldfrow !<
  real, intent(in), dimension(ilg,ilev) :: ssldfrol !<
  real, intent(inout), dimension(ilg,ilev) :: ressfrow !<
  real, intent(in), dimension(ilg,ilev) :: ressfrol !<
  real, intent(inout), dimension(ilg,ilev) :: vessfrow !<
  real, intent(in), dimension(ilg,ilev) :: vessfrol !<
  real, intent(inout), dimension(ilg,ilev) :: dsldfrow !<
  real, intent(in), dimension(ilg,ilev) :: dsldfrol !<
  real, intent(inout), dimension(ilg,ilev) :: redsfrow !<
  real, intent(in), dimension(ilg,ilev) :: redsfrol !<
  real, intent(inout), dimension(ilg,ilev) :: vedsfrow !<
  real, intent(in), dimension(ilg,ilev) :: vedsfrol !<
  real, intent(inout), dimension(ilg,ilev) :: bcldfrow !<
  real, intent(in), dimension(ilg,ilev) :: bcldfrol !<
  real, intent(inout), dimension(ilg,ilev) :: rebcfrow !<
  real, intent(in), dimension(ilg,ilev) :: rebcfrol !<
  real, intent(inout), dimension(ilg,ilev) :: vebcfrow !<
  real, intent(in), dimension(ilg,ilev) :: vebcfrol !<
  real, intent(inout), dimension(ilg,ilev) :: ocldfrow !<
  real, intent(in), dimension(ilg,ilev) :: ocldfrol !<
  real, intent(inout), dimension(ilg,ilev) :: reocfrow !<
  real, intent(in), dimension(ilg,ilev) :: reocfrol !<
  real, intent(inout), dimension(ilg,ilev) :: veocfrow !<
  real, intent(in), dimension(ilg,ilev) :: veocfrol !<
  real, intent(inout), dimension(ilg,ilev) :: zcdnfrow !<
  real, intent(in), dimension(ilg,ilev) :: zcdnfrol !<
  real, intent(inout), dimension(ilg,ilev) :: bcicfrow !<
  real, intent(in), dimension(ilg,ilev) :: bcicfrol !<
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  day=real(iday)+gmt/86400.
  fmday=real(mday)
  if (fmday<day) fmday=fmday+365.
  daysm=fmday-day
  istepsm=nint(daysm*86400./delt)
  stepsm=real(istepsm)
  !
  !     * general interpolation.
  !
  do i=il1,il2
    bcdpfrow(i)=((stepsm-1.)*bcdpfrow(i)+bcdpfrol(i))/stepsm
  end do ! loop 210
  !
  do l=1,ilev
    do i=il1,il2
      amldfrow(i,l)=((stepsm-1.)*amldfrow(i,l)+amldfrol(i,l))/stepsm
      reamfrow(i,l)=((stepsm-1.)*reamfrow(i,l)+reamfrol(i,l))/stepsm
      veamfrow(i,l)=((stepsm-1.)*veamfrow(i,l)+veamfrol(i,l))/stepsm
      fr1frow (i,l)=((stepsm-1.)*fr1frow (i,l)+fr1frol (i,l))/stepsm
      fr2frow (i,l)=((stepsm-1.)*fr2frow (i,l)+fr2frol (i,l))/stepsm
      ssldfrow(i,l)=((stepsm-1.)*ssldfrow(i,l)+ssldfrol(i,l))/stepsm
      ressfrow(i,l)=((stepsm-1.)*ressfrow(i,l)+ressfrol(i,l))/stepsm
      vessfrow(i,l)=((stepsm-1.)*vessfrow(i,l)+vessfrol(i,l))/stepsm
      dsldfrow(i,l)=((stepsm-1.)*dsldfrow(i,l)+dsldfrol(i,l))/stepsm
      redsfrow(i,l)=((stepsm-1.)*redsfrow(i,l)+redsfrol(i,l))/stepsm
      vedsfrow(i,l)=((stepsm-1.)*vedsfrow(i,l)+vedsfrol(i,l))/stepsm
      bcldfrow(i,l)=((stepsm-1.)*bcldfrow(i,l)+bcldfrol(i,l))/stepsm
      rebcfrow(i,l)=((stepsm-1.)*rebcfrow(i,l)+rebcfrol(i,l))/stepsm
      vebcfrow(i,l)=((stepsm-1.)*vebcfrow(i,l)+vebcfrol(i,l))/stepsm
      ocldfrow(i,l)=((stepsm-1.)*ocldfrow(i,l)+ocldfrol(i,l))/stepsm
      reocfrow(i,l)=((stepsm-1.)*reocfrow(i,l)+reocfrol(i,l))/stepsm
      veocfrow(i,l)=((stepsm-1.)*veocfrow(i,l)+veocfrol(i,l))/stepsm
      zcdnfrow(i,l)=((stepsm-1.)*zcdnfrow(i,l)+zcdnfrol(i,l))/stepsm
      bcicfrow(i,l)=((stepsm-1.)*bcicfrow(i,l)+bcicfrol(i,l))/stepsm
    end do
  end do ! loop 250
  !
  return
end
