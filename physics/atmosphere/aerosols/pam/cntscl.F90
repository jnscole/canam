subroutine cntscl(tscl,dvp,tkp,wetrb,dvx,tkx,esw,temp,dv,tk, &
                  ilga,leva,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     parameters relevant to droplet growth, i.e. droplet growth time
  !     scale, diffusivity, and thermal conductivity.
  !
  !     history:
  !     --------
  !     * aug 11/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use sdphys
  !
  implicit none
  integer :: is
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(ilga,leva) :: esw !<
  real, intent(in), dimension(ilga,leva) :: temp !<
  real, intent(in), dimension(ilga,leva) :: dv !<
  real, intent(in), dimension(ilga,leva) :: dvx !<
  real, intent(in), dimension(ilga,leva) :: tk !<
  real, intent(in), dimension(ilga,leva) :: tkx !<
  real, intent(in), dimension(ilga,leva,isec) :: wetrb !<
  real, intent(out), dimension(ilga,leva,isec) :: tscl !<
  real, intent(out), dimension(ilga,leva,isec) :: dvp !<
  real, intent(out), dimension(ilga,leva,isec) :: tkp !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  !
  !-----------------------------------------------------------------------
  !
  !     * allocate work arrays.
  !
  allocate(term1(ilga,leva))
  allocate(term2(ilga,leva))
  !
  !-----------------------------------------------------------------------
  do is = 1,isec
    !
    !       * modified diffusivity and thermal conductivity.
    !
    dvp(:,:,is) = dv(:,:)/(1. + dvx(:,:)/wetrb(:,:,is))
    tkp(:,:,is) = tk(:,:)/(1. + tkx(:,:)/wetrb(:,:,is))
    !
    !       * growth time scale parameter (sv-sp)/(r*dr/dt).
    !
    term1 = rhoh2o * rgasm * temp(:,:)/(esw(:,:) * dvp(:,:,is) * wh2o)
    term2 = (rl * rhoh2o/(tkp(:,:,is) * temp(:,:))) &
            * (rl * wa/(rgasm * temp(:,:)) - 1.)
    tscl(:,:,is) = term1 + term2
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(term1)
  deallocate(term2)
  !
end subroutine cntscl
