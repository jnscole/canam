subroutine amdiag(acas,acoa,acbc,acss,acmd,pemas,pimas,pifrc, &
                  rhoa,ilga,leva)
  !
  use sdparm
  !
  implicit none
  integer, intent(in) :: ilga
  integer :: is
  integer, intent(in) :: leva
  !
  real, intent(out), dimension(ilga,leva) :: acas !<
  real, intent(out), dimension(ilga,leva) :: acoa !<
  real, intent(out), dimension(ilga,leva) :: acbc !<
  real, intent(out), dimension(ilga,leva) :: acss !<
  real, intent(out), dimension(ilga,leva) :: acmd !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  !
  !     * ammonium sulphate.
  !
  acas = 0.
  if (kextso4 > 0) then
    do is = 1,isextso4
      acas = acas + pemas(:,:,iexso4(is))
    end do
  end if
  if (kintso4 > 0) then
    do is = 1,isintso4
      acas = acas + pimas(:,:,iinso4(is)) * pifrc(:,:,iinso4(is),kintso4)
    end do
  end if
  acas = acas * rhoa
  !
  !     * organic aerosol.
  !
  acoa = 0.
  if (kextoc > 0) then
    do is = 1,isextoc
      acoa = acoa + pemas(:,:,iexoc(is))
    end do
  end if
  if (kintoc > 0) then
    do is = 1,isintoc
      acoa = acoa + pimas(:,:,iinoc(is)) * pifrc(:,:,iinoc(is),kintoc)
    end do
  end if
  acoa = acoa * rhoa
  !
  !     * black carbon.
  !
  acbc = 0.
  if (kextbc > 0) then
    do is = 1,isextbc
      acbc = acbc + pemas(:,:,iexbc(is))
    end do
  end if
  if (kintbc > 0) then
    do is = 1,isintbc
      acbc = acbc + pimas(:,:,iinbc(is)) * pifrc(:,:,iinbc(is),kintbc)
    end do
  end if
  acbc = acbc * rhoa
  !
  !     * sea salt.
  !
  acss = 0.
  if (kextss > 0) then
    do is = 1,isextss
      acss = acss + pemas(:,:,iexss(is))
    end do
  end if
  if (kintss > 0) then
    do is = 1,isintss
      acss = acss + pimas(:,:,iinss(is)) * pifrc(:,:,iinss(is),kintss)
    end do
  end if
  acss = acss * rhoa
  !
  !     * mineral dust.
  !
  acmd = 0.
  if (kextmd > 0) then
    do is = 1,isextmd
      acmd = acmd + pemas(:,:,iexmd(is))
    end do
  end if
  if (kintmd > 0) then
    do is = 1,isintmd
      acmd = acmd + pimas(:,:,iinmd(is)) * pifrc(:,:,iinmd(is),kintmd)
    end do
  end if
  acmd = acmd * rhoa
  !
end subroutine amdiag
