subroutine rdconf(nurad)
  !
  use rdmod
  !
  implicit none
  integer :: irec
  integer, intent(in) :: nurad
  !
  do irec = 1,idim2
    read(nurad,'(6(1x,e14.7))') sextt2(irec,:)
    read(nurad,'(5(1x,e14.7))') somgt2(irec,:)
    read(nurad,'(5(1x,e14.7))')   sgt2(irec,:)
    read(nurad,'(9(1x,e14.7))') sabst2(irec,:)
  end do
  !
end subroutine rdconf
