subroutine cnigwm(fres,svn,sv,qrc,qrvn,rsn,tempn,cerc,cesc, &
                  cewmass,cewetrn,cewetrb,ciwmass,ciwetrn, &
                  ciwetrb,circ,cisc,cebk,cetscl,cerat,ceird, &
                  cen0,cepsi,cemom2,cemom3,cemom4,cemom5,cemom6, &
                  cemom7,cemom8,cibk,citscl,cirat,ciird,cin0, &
                  cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                  cimom7,cimom8,hlst,dhdt,qr,drdt,zh,pres,ak, &
                  svi,dt,ilga,leva,cemod1,cemod2,cemod3,cimod1, &
                  cimod2,cimod3)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     residual for calculation of droplet growth from changes
  !     in environmental conditions and aerosol concentrations. results
  !     of this subroutine are used to determine the supersaturation.
  !     for use with scaled version of the growth equation.
  !
  !     history:
  !     --------
  !     * feb 04/2015 - k.vonsalzen   cosmetic change for gcm18:
  !     *                             {ak,cebk,cibk} declared
  !     *                             with intent(inout) instead of intent(in).
  !     * jun 20/2008 - k.vonsalzen   account for numerical truncation
  !                                   in droplet growth, add kcor.
  !     * aug 11/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use scparm
  use fpdef
  !
  implicit none
  integer :: is
  integer :: isc
  integer :: ist
  integer :: kx
  integer :: kxm
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, intent(inout), dimension(ilga,leva,isextb) :: cebk !<
  real, intent(inout), dimension(ilga,leva,isintb) :: cibk !<
  real, intent(inout), dimension(ilga,leva,isextb) :: cewetrb !<
  real, intent(inout), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(inout), dimension(ilga,leva) :: ak !<
  real, intent(in), dimension(ilga,leva) :: drdt !<
  real, intent(in), dimension(ilga,leva) :: dhdt !<
  real, intent(in), dimension(ilga,leva) :: sv !<
  real, intent(in), dimension(ilga,leva) :: qr !<
  real, intent(in), dimension(ilga,leva) :: hlst !<
  real, intent(in), dimension(ilga,leva) :: zh !<
  real, intent(in), dimension(ilga,leva) :: pres !<
  real, intent(in), dimension(ilga,leva) :: dt !<
  real, intent(in), dimension(ilga,leva) :: svi !<
  real, intent(in), dimension(ilga,leva,isext) :: cen0 !<
  real, intent(in), dimension(ilga,leva,isext) :: cepsi !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom2 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom3 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom4 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom5 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom6 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom7 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom8 !<
  real, intent(in), dimension(ilga,leva,isint) :: cin0 !<
  real, intent(in), dimension(ilga,leva,isint) :: cipsi !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom2 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom3 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom4 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom5 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom6 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom7 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom8 !<
  real, intent(in), dimension(ilga,leva,isextb) :: cetscl !<
  real, intent(in), dimension(ilga,leva,isextb) :: cerat !<
  integer, intent(in), dimension(ilga,leva,isintb) :: ceird !<
  real, intent(in), dimension(ilga,leva,isintb) :: citscl !<
  real, intent(in), dimension(ilga,leva,isintb) :: cirat !<
  integer, intent(in), dimension(ilga,leva,isextb) :: ciird !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod1 !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod2 !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod3 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod1 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod2 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod3 !<
  real, intent(out), dimension(ilga,leva,isextb) :: cerc !<
  real, intent(out), dimension(ilga,leva,isextb) :: cesc !<
  real, intent(out), dimension(ilga,leva,isextb) :: cewetrn !<
  real, intent(out), dimension(ilga,leva,isintb) :: circ !<
  real, intent(out), dimension(ilga,leva,isintb) :: cisc !<
  real, intent(out), dimension(ilga,leva,isintb) :: ciwetrn !<
  real, intent(out), dimension(ilga,leva,isext) :: cewmass !<
  real, intent(out), dimension(ilga,leva,isint) :: ciwmass !<
  real, intent(out), dimension(ilga,leva) :: qrc !<
  real, intent(out), dimension(ilga,leva) :: fres !<
  real, intent(out), dimension(ilga,leva) :: rsn !<
  real, intent(out), dimension(ilga,leva) :: tempn !<
  real, intent(out), dimension(ilga,leva) :: qrvn !<
  real, intent(out), dimension(ilga,leva) :: svn !<
  real, parameter :: yssec = 0.999 !<
  real, parameter :: ygrwl = 1000. !<
  logical, parameter :: kcor = .true. !<
  !      logical, parameter :: kcor=.false.
  !
  !-----------------------------------------------------------------------
  !     * initialization of diagnostic results.
  !
  if (isextb > 0) then
    cemod1 = 0
    cemod2 = 0
    cemod3 = 1
  end if
  if (isintb > 0) then
    cimod1 = 0
    cimod2 = 0
    cimod3 = 1
  end if
  !
  !     * integrate growth equation.
  !
  if (isextb > 0) then
    call cnigge(cewetrn,cerc,cesc,cewetrb,cebk,cetscl,sv,ak, &
                svi,cerat,ceird,cedryrb,dt,ilga,leva,isextb, &
                cemod1,cemod2,cemod3)
    cewetrb = cewetrn
  end if
  if (isintb > 0) then
    call cnigge(ciwetrn,circ,cisc,ciwetrb,cibk,citscl,sv,ak, &
                svi,cirat,ciird,cidryrb,dt,ilga,leva,isintb, &
                cimod1,cimod2,cimod3)
    ciwetrb = ciwetrn
  end if
  !
  if (kcor) then
    !
    !       * limit droplet growth to physically reasonable range.
    !
    do is = 1,isextb
      cewetrb(:,:,is) = min(cewetrb(:,:,is),ygrwl * cedryrb(is))
    end do
    do is = 1,isintb
      ciwetrb(:,:,is) = min(ciwetrb(:,:,is),ygrwl * cidryrb(is))
    end do
    !
    !       * do not allow smaller particles to overtake larger particles.
    !       * prevent this by limiting the growth for the smaller particles,
    !       * if necessary. this may occur owing to numerical truncation
    !       * errors for the droplet growth calculations.
    !
    if (isextb > 0) then
      isc = isextb + 1
      do is = iswext,2, - 1
        kx = sextf%iswet(is)%ityp
        kxm = sextf%iswet(is - 1)%ityp
        do ist = 1,isesc
          isc = isc - 1
          where (cewetrb(:,:,isc - 1) >= cewetrb(:,:,isc) )
            cewetrb(:,:,isc - 1) = yssec * cewetrb(:,:,isc)
          end where
        end do
        if (kxm /= kx) then
          isc = isc - 1
        end if
      end do
      do ist = 1,isesc
        isc = isc - 1
        where (cewetrb(:,:,isc - 1) >= cewetrb(:,:,isc) )
          cewetrb(:,:,isc - 1) = yssec * cewetrb(:,:,isc)
        end where
      end do
    end if
    if (isintb > 0) then
      do is = isintb,2, - 1
        where (ciwetrb(:,:,is - 1) >= ciwetrb(:,:,is) )
          ciwetrb(:,:,is - 1) = yssec * ciwetrb(:,:,is)
        end where
      end do
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * calculate amount of condensed water.
  !
  call scwatm(qrc,cewmass,ciwmass,cen0,cepsi,cewetrb,cemom2, &
              cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
              cin0,cipsi,ciwetrb,cimom2,cimom3,cimom4,cimom5, &
              cimom6,cimom7,cimom8,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * implied supersaturation for given cloud water content.
  !
  call cnsbnd(svn,qrvn,rsn,tempn,qrc,hlst,dhdt,qr, &
              drdt,zh,pres,dt,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * residual supersaturation.
  !
  fres = svn - sv
  !
end subroutine cnigwm
