subroutine cnsbnd(svi,qrvn,rsn,tempn,qrc,hlst,dhdt,qr, &
                  drdt,zh,pres,dt,ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     calculates supersaturation for given cloud water content based
  !     on conservation of energy and water.
  !
  !     history:
  !     --------
  !     * jun 20/2008 - k.vonsalzen   add option to use liquid water
  !                                   static energy in addition to moist
  !                                   static energy.
  !     * aug 25/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use cnparm
  use sdphys
  use fpdef
  !
  implicit none
  integer :: il
  integer :: l
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, intent(in), dimension(ilga,leva) :: qr !<
  real, intent(in), dimension(ilga,leva) :: drdt !<
  real, intent(in), dimension(ilga,leva) :: hlst !<
  real, intent(in), dimension(ilga,leva) :: dhdt !<
  real, intent(in), dimension(ilga,leva) :: zh !<
  real, intent(in), dimension(ilga,leva) :: pres !<
  real, intent(in), dimension(ilga,leva) :: qrc !<
  real, intent(in), dimension(ilga,leva) :: dt !<
  real, intent(out), dimension(ilga,leva) :: rsn !<
  real, intent(out), dimension(ilga,leva) :: tempn !<
  real, intent(out), dimension(ilga,leva) :: qrvn !<
  real, intent(out), dimension(ilga,leva) :: svi !<
  real, allocatable, dimension(:,:) :: relh !<
  real, allocatable, dimension(:,:) :: tmp1 !<
  real, allocatable, dimension(:,:) :: tmp2 !<
  real, allocatable, dimension(:,:) :: tmp3 !<
  real, allocatable, dimension(:,:) :: esw !<
  real, allocatable, dimension(:,:) :: qrct !<
  real, parameter :: yepsm = 0.999 !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(relh(ilga,leva))
  allocate(tmp1(ilga,leva))
  allocate(tmp2(ilga,leva))
  allocate(tmp3(ilga,leva))
  allocate(esw (ilga,leva))
  allocate(qrct(ilga,leva))
  !
  !     * make sure cloud water mixing ratio does not exceed
  !     * total water mixing ratio.
  !
  qrct = qrc
  tmp1 = qr + drdt * dt
  where (qrct >= tmp1)
    qrct = yepsm * tmp1
  end where
  !
  !     * update water vapour and temperature. either moist static
  !     * energy or liquid/ice water static energy is used, depending
  !     * on the model input.
  !
  qrvn = qr + drdt * dt - qrct
  if (knorm) then
    tempn = (hlst + dhdt * dt - rl * qrvn - grav * zh)/cpres
  else
    tempn = (hlst + dhdt * dt + rl * qrct - grav * zh)/cpres
  end if
  !
  !     * update saturation mixing ratio.
  !
  if (ksatf) then
    if (kdbl) then
      tmp3 = rw1 + rw2/tempn
      tmp1 = exp(tmp3)
      tmp3 = rw3
      tmp2 = tempn ** tmp3
    else
      tmp3 = rw1 + rw2/tempn
      tmp1 = exp(tmp3)
      tmp3 = rw3
      tmp2 = tempn ** tmp3
    end if
    esw = 1.e+02 * tmp1 * tmp2
  else
    do l = 1,leva
      do il = 1,ilga
        esw(il,l) = 1.e+02 * exp(ppa - ppb/tempn(il,l))
      end do
    end do
  end if
  where (abs(pres - esw) > ysmall)
    rsn = eps1 * esw/(pres - esw)
  else where
    rsn = ylarge
  end where
  !
  !     * implied relative humidity and supersaturation for given
  !     * temperature and water vapour.
  !
  where (abs(rsn - ylarge) > ysmall)
    relh = qrvn/rsn * (1. + rsn/eps1)/(1. + qrvn/eps1)
  else where
    relh = 0.
  end where
  svi = relh - 1.
  !
  !     * deallocate work arrays.
  !
  deallocate(relh)
  deallocate(tmp1)
  deallocate(tmp2)
  deallocate(tmp3)
  deallocate(esw)
  deallocate(qrct)
  !
end subroutine cnsbnd
