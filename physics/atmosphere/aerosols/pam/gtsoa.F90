subroutine gtsoa (dgdt,pedndt,pedmdt,pidndt,pidmdt,pidfdt, &
                  peddn,pemas,pen0,penum,pephi0,pepsi,pewetrc, &
                  pifrc,piddn,pimas,pin0,pinum,piphi0,pipsi, &
                  piwetrc,t,rhoa,soapi,sgpr,dt,ilga,leva, &
                  dersn,dersm,dirsn,dirsmn,dirsms,decnd, &
                  dicnd,ierr)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     gas-to-particle converstion for secondary organic aerosol.
  !
  !     note: it is critical that the dry particle densities (peddn,
  !     piddn) and mass fractions for internally mixed aerosol (pifrc)
  !     are consistent with the aerosol mass (pemas, pimas) and number
  !     (penum, pinum) concentrations. make sure these are calculated
  !     before this subroutine.
  !
  !     history:
  !     --------
  !     * dec  6/2017 - k.vonsalzen   correction of growth term calculation
  !     * apr 17/2015 - k.vonsalzen   changes to support single precision
  !     * feb 19/2010 - k.vonsalzen   correct pidfdt
  !     * mar 16/2009 - k.vonsalzen   new, based on gtpcnv.
  !
  !     ************************ index of variables **********************
  !
  !     primary input/output variables (argument list):
  !
  ! o   dgdt     gas-phase soa precursor tendency (kg/kg/sec)
  ! o   decnd    soa precursor condensation rate (kg/kg/sec), ext. mixture
  ! o   dersm    mass residuum from numerical truncation in growth
  !              calculations (kg/kg), ext. mixture
  ! o   dersn    number residuum from numerical truncation in growth
  !              calculations (kg/kg), ext. mixture
  ! o   dicnd    soa precursor condensation rate (kg/kg/sec), int. mixture
  ! o   dirsmn   mass residuum for non-soa species from num.
  !              truncation in growth calculations (kg/kg), int. mixture
  ! o   dirsms   mass residuum for soa from num. truncation
  !              in growth calculations (kg/kg), int. mixture
  ! o   dirsn    number residuum from numerical truncation in growth
  !              calculations (kg/kg), int. mixture
  ! i   dt       time step (sec)
  ! o   ierr     warning code index
  ! i   ilga     number of grid points in horizontal direction
  ! i   leva     number of grid pointa in vertical direction
  ! i   peddn    density of dry aerosol particle (kg/m3), ext. mixture
  ! o   pedmdt   mass tendency (kg/kg/sec), ext. mixture
  ! o   pedndt   number tendency (1/kg/sec), ext. mixture
  ! i   pemas    aerosol mass concentration (kg/kg), ext. mixture
  ! i   pen0     1st pla size distribution parameter (amplitude), ext.
  !              mixture
  ! i   penum    aerosol number concentration (1/kg), ext. mixture
  ! i   pephi0   3rd pla size distribution parameter (mode size), ext.
  !              mixture
  ! i   pepsi    2nd pla size distribution parameter (width), ext. mixture
  ! i   pewetrc  total/wet paricle radius (m), ext. mixture
  ! i   piddn    density of dry aerosol particle (kg/m3), int. mixture
  ! o   pidfdt   aerosol species mass fraction tendency (1/sec), int.
  !              mixture
  ! o   pidmdt   mass tendency (kg/kg/sec), int. mixture
  ! o   pidndt   number tendency (1/kg/sec), int. mixture
  ! i   pifrc    aerosol species mass fraction
  ! i   pimas    aerosol mass concentration (kg/kg), int. mixture
  ! i   pin0     1st pla size distribution parameter (amplitude), int.
  !              mixture
  ! i   pinum    aerosol number concentration (1/kg), int. mixture
  ! i   piphi0   3rd pla size distribution parameter (mode size), int.
  !              mixture
  ! i   pipsi    2nd pla size distribution parameter (width), int. mixture
  ! i   piwetrc  total/wet paricle radius (m), int. mixture
  ! i   rhoa     air density (kg/m3)
  ! i   sgpr     gas-phase production rate of soa precursors (kg/kg/sec)
  ! i   soapi    initial soa precursor mixing ratio (kg/kg)
  ! i   t        air temperature (k)
  !
  !     secondary input/output variables (via modules and common blocks):
  !
  ! i   aextf%tp(:)%dryrc(:)
  !              dry particle radius in section centre (m), ext. mixture
  ! i   aextf%tp(:)%dryr(:)%vl
  !              lower dry particle radius for each section (m), ext.
  !              mixture
  ! i   aextf%tp(:)%dryr(:)%vr
  !              upper dry particle radius for each section (m), ext.
  !              mixture
  ! i   aintf%dryrc(:)
  !              dry particle radius in section centre (m), int. mixture
  ! i   aintf%dryr(:)%vl
  !              lower dry particle radius for each section (m), int.
  !              mixture
  ! i   aintf%dryr(:)%vr
  !              upper dry particle radius for each section (m), int.
  !              mixture
  ! i   iexoc    aerosol tracer index for organic carbon, ext. mixture
  ! i   iinoc    aerosol tracer index for organic carbon, int. mixture
  ! i   isaext   number of aerosol tracers, ext. mixture
  ! i   isaint   number of aerosol tracers, int. mixture
  ! i   isextoc  number of sections for oc, ext. mixture
  ! i   isintoc  number of sections for oc, int. mixture
  ! i   iocs     number of non-oc aerosol species, int. mixture
  ! i   kextoc   species index for oc, ext. mixture
  ! i   kint     number of internally mixed aerosol types, int. mixture
  ! i   kintoc   species index for oc, int. mixture
  ! i   pedphis  section width, ext. mixture
  ! i   pephiss  lower dry particle size (=log(r/r0)), ext. mixture
  ! i   pidphis  section width, int. mixture
  ! i   piphiss  lower dry particle size (=log(r/r0)), int. mixture
  ! i   ylarge   large number
  ! i   ysec     security parameter for adjustment of timestep
  ! i   ysmall   small number
  ! i   ytiny    smallest valid number
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use fpdef
  !
  implicit none
  real :: cfm
  real :: cfn
  real :: corfm
  real, intent(in) :: dt
  integer :: il
  integer :: inds
  integer :: is
  integer :: it
  integer :: l
  real :: tecorfm
  real :: term5
  real :: ticorfm
  real :: wrat
  !
  real, intent(out), dimension(ilga,leva) :: dgdt !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !<
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !<
  real, intent(out), dimension(ilga,leva) :: dersn !<
  real, intent(out), dimension(ilga,leva) :: dersm !<
  real, intent(out), dimension(ilga,leva) :: decnd !<
  real, intent(out), dimension(ilga,leva) :: dirsmn !<
  real, intent(out), dimension(ilga,leva) :: dirsn !<
  real, intent(out), dimension(ilga,leva) :: dirsms !<
  real, intent(out), dimension(ilga,leva) :: dicnd !<
  real, intent(in), dimension(ilga,leva) :: sgpr !<
  real, intent(in), dimension(ilga,leva) :: t !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva) :: soapi !<
  !
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !<
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !<
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  integer, intent(out) :: ierr !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, allocatable, dimension(:,:,:,:) :: timn !<
  real, allocatable, dimension(:,:,:,:) :: tifrcn !<
  real(r8), allocatable, dimension(:,:,:) :: tecndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tect1 !<
  real(r8), allocatable, dimension(:,:,:) :: tect2 !<
  real, allocatable, dimension(:,:,:) :: tewetrc !<
  real, allocatable, dimension(:,:,:) :: teddn !<
  real, allocatable, dimension(:,:,:) :: tecgr1 !<
  real, allocatable, dimension(:,:,:) :: tecgr2 !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: tedphit !<
  real, allocatable, dimension(:,:,:) :: tephist !<
  real, allocatable, dimension(:,:,:) :: tecorf !<
  real, allocatable, dimension(:,:,:) :: tedccnd !<
  real, allocatable, dimension(:,:,:) :: tent !<
  real, allocatable, dimension(:,:,:) :: tefrc !<
  real, allocatable, dimension(:,:,:) :: tedryrt !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3s !<
  real(r8), allocatable, dimension(:,:,:) :: ticndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tict1 !<
  real(r8), allocatable, dimension(:,:,:) :: tict2 !<
  real, allocatable, dimension(:,:,:) :: tiwetrc !<
  real, allocatable, dimension(:,:,:) :: tiddn !<
  real, allocatable, dimension(:,:,:) :: ticgr1 !<
  real, allocatable, dimension(:,:,:) :: ticgr2 !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tidphit !<
  real, allocatable, dimension(:,:,:) :: tiphist !<
  real, allocatable, dimension(:,:,:) :: ticorf !<
  real, allocatable, dimension(:,:,:) :: tidccnd !<
  real, allocatable, dimension(:,:,:) :: tint !<
  real, allocatable, dimension(:,:,:) :: tifrcs !<
  real, allocatable, dimension(:,:,:) :: tidryrt !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3s !<
  real(r8), allocatable, dimension(:,:,:) :: temt !<
  real(r8), allocatable, dimension(:,:,:) :: timst !<
  real(r8), allocatable, dimension(:,:,:,:) :: timnt !<
  real, allocatable, dimension(:,:,:) :: ten !<
  real, allocatable, dimension(:,:,:) :: tem !<
  real, allocatable, dimension(:,:,:) :: tin !<
  real, allocatable, dimension(:,:,:) :: tim !<
  real, allocatable, dimension(:,:,:) :: timt !<
  real, allocatable, dimension(:,:,:) :: tims !<
  real, allocatable, dimension(:,:,:) :: tirsmn !<
  real, allocatable, dimension(:,:,:) :: titm1 !<
  real, allocatable, dimension(:,:,:) :: titm2 !<
  real(r8), allocatable, dimension(:,:) :: dfsoap !<
  real(r8), allocatable, dimension(:,:) :: dbl1 !<
  real(r8), allocatable, dimension(:,:) :: dbl2 !<
  real(r8), allocatable, dimension(:,:) :: acnd !<
  real, allocatable, dimension(:,:) :: plfn !<
  real, allocatable, dimension(:,:) :: cndm !<
  real, allocatable, dimension(:,:) :: cndmx !<
  real, allocatable, dimension(:,:) :: termx !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  real, allocatable, dimension(:,:) :: term3 !<
  real, allocatable, dimension(:,:) :: term4 !<
  real, allocatable, dimension(:,:) :: frp !<
  real, allocatable, dimension(:,:) :: plam !<
  real, allocatable, dimension(:,:) :: pertc !<
  real, allocatable, dimension(:,:) :: corfa !<
  real, allocatable, dimension(:,:) :: corfar !<
  real, allocatable, dimension(:,:) :: dtc !<
  real, allocatable, dimension(:,:) :: expt !<
  real, allocatable, dimension(:,:) :: sadt !<
  real, allocatable, dimension(:,:) :: ctmp !<
  real, allocatable, dimension(:,:) :: tecorfa !<
  real, allocatable, dimension(:,:) :: ticorfa !<
  real, allocatable, dimension(:) :: tecorfs !<
  real, allocatable, dimension(:) :: tedryr !<
  real, allocatable, dimension(:) :: tedryrc !<
  real, allocatable, dimension(:) :: ticorfs !<
  real, allocatable, dimension(:) :: tidryr !<
  real, allocatable, dimension(:) :: tidryrc !<
  integer, allocatable, dimension(:,:,:) :: tei0l !<
  integer, allocatable, dimension(:,:,:) :: tei0r !<
  integer, allocatable, dimension(:,:,:) :: tii0l !<
  integer, allocatable, dimension(:,:,:) :: tii0r !<
  !
  !     * numerical constants.
  !
  real, parameter :: yeps = 0.1 ! threshold for convergence test
  integer, parameter :: iter = 3 ! number of iterations for
  ! newton-rhapson calculation
  integer, parameter :: imodc = 1 !<
  !
  !-----------------------------------------------------------------------
  !
  ierr = 0
  !
  !     * allocate memory for local fields.
  !
  if (isextoc > 0) then
    allocate(tewetrc(ilga,leva,isextoc))
    allocate(ten    (ilga,leva,isextoc))
    allocate(tent   (ilga,leva,isextoc))
    allocate(tem    (ilga,leva,isextoc))
    allocate(temt   (ilga,leva,isextoc))
    allocate(teddn  (ilga,leva,isextoc))
    allocate(ten0   (ilga,leva,isextoc))
    allocate(tephi0 (ilga,leva,isextoc))
    allocate(tepsi  (ilga,leva,isextoc))
    allocate(tect1  (ilga,leva,isextoc))
    allocate(tect2  (ilga,leva,isextoc))
    allocate(tecgr1 (ilga,leva,isextoc))
    allocate(tecgr2 (ilga,leva,isextoc))
    allocate(tecndgp(ilga,leva,isextoc))
    allocate(tedccnd(ilga,leva,isextoc))
    allocate(tedphit(ilga,leva,isextoc))
    allocate(tephist(ilga,leva,isextoc))
    allocate(tecorf (ilga,leva,isextoc))
    allocate(tei0l  (ilga,leva,isextoc))
    allocate(tei0r  (ilga,leva,isextoc))
    allocate(tedp0l (ilga,leva,isextoc))
    allocate(tedp1l (ilga,leva,isextoc))
    allocate(tedp2l (ilga,leva,isextoc))
    allocate(tedp3l (ilga,leva,isextoc))
    allocate(tedp4l (ilga,leva,isextoc))
    allocate(tedp5l (ilga,leva,isextoc))
    allocate(tedp6l (ilga,leva,isextoc))
    allocate(tedm1l (ilga,leva,isextoc))
    allocate(tedm2l (ilga,leva,isextoc))
    allocate(tedm3l (ilga,leva,isextoc))
    allocate(tedp0r (ilga,leva,isextoc))
    allocate(tedp1r (ilga,leva,isextoc))
    allocate(tedp2r (ilga,leva,isextoc))
    allocate(tedp3r (ilga,leva,isextoc))
    allocate(tedp4r (ilga,leva,isextoc))
    allocate(tedp5r (ilga,leva,isextoc))
    allocate(tedp6r (ilga,leva,isextoc))
    allocate(tedm1r (ilga,leva,isextoc))
    allocate(tedm2r (ilga,leva,isextoc))
    allocate(tedm3r (ilga,leva,isextoc))
    allocate(tedp0s (ilga,leva,isextoc))
    allocate(tedp1s (ilga,leva,isextoc))
    allocate(tedp2s (ilga,leva,isextoc))
    allocate(tedp3s (ilga,leva,isextoc))
    allocate(tedp4s (ilga,leva,isextoc))
    allocate(tedp5s (ilga,leva,isextoc))
    allocate(tedp6s (ilga,leva,isextoc))
    allocate(tedm1s (ilga,leva,isextoc))
    allocate(tedm2s (ilga,leva,isextoc))
    allocate(tedm3s (ilga,leva,isextoc))
    allocate(tefrc  (ilga,leva,isextoc))
    allocate(tedryrt(ilga,leva,isextoc + 1))
    allocate(tecorfs(isextoc))
    allocate(tedryr (isextoc + 1))
    allocate(tedryrc(isextoc))
  end if
  if (isintoc > 0) then
    allocate(timn   (ilga,leva,isintoc,iocs))
    allocate(timnt  (ilga,leva,isintoc,iocs))
    allocate(tifrcn (ilga,leva,isintoc,iocs))
    allocate(tims   (ilga,leva,isintoc))
    allocate(timst  (ilga,leva,isintoc))
    allocate(tiwetrc(ilga,leva,isintoc))
    allocate(tin    (ilga,leva,isintoc))
    allocate(tint   (ilga,leva,isintoc))
    allocate(tim    (ilga,leva,isintoc))
    allocate(timt   (ilga,leva,isintoc))
    allocate(tiddn  (ilga,leva,isintoc))
    allocate(tin0   (ilga,leva,isintoc))
    allocate(tiphi0 (ilga,leva,isintoc))
    allocate(tipsi  (ilga,leva,isintoc))
    allocate(tict1  (ilga,leva,isintoc))
    allocate(tict2  (ilga,leva,isintoc))
    allocate(ticgr1 (ilga,leva,isintoc))
    allocate(ticgr2 (ilga,leva,isintoc))
    allocate(ticndgp(ilga,leva,isintoc))
    allocate(tidccnd(ilga,leva,isintoc))
    allocate(tidphit(ilga,leva,isintoc))
    allocate(tiphist(ilga,leva,isintoc))
    allocate(ticorf (ilga,leva,isintoc))
    allocate(tii0l  (ilga,leva,isintoc))
    allocate(tii0r  (ilga,leva,isintoc))
    allocate(tidp0l (ilga,leva,isintoc))
    allocate(tidp1l (ilga,leva,isintoc))
    allocate(tidp2l (ilga,leva,isintoc))
    allocate(tidp3l (ilga,leva,isintoc))
    allocate(tidp4l (ilga,leva,isintoc))
    allocate(tidp5l (ilga,leva,isintoc))
    allocate(tidp6l (ilga,leva,isintoc))
    allocate(tidm1l (ilga,leva,isintoc))
    allocate(tidm2l (ilga,leva,isintoc))
    allocate(tidm3l (ilga,leva,isintoc))
    allocate(tidp0r (ilga,leva,isintoc))
    allocate(tidp1r (ilga,leva,isintoc))
    allocate(tidp2r (ilga,leva,isintoc))
    allocate(tidp3r (ilga,leva,isintoc))
    allocate(tidp4r (ilga,leva,isintoc))
    allocate(tidp5r (ilga,leva,isintoc))
    allocate(tidp6r (ilga,leva,isintoc))
    allocate(tidm1r (ilga,leva,isintoc))
    allocate(tidm2r (ilga,leva,isintoc))
    allocate(tidm3r (ilga,leva,isintoc))
    allocate(tidp0s (ilga,leva,isintoc))
    allocate(tidp1s (ilga,leva,isintoc))
    allocate(tidp2s (ilga,leva,isintoc))
    allocate(tidp3s (ilga,leva,isintoc))
    allocate(tidp4s (ilga,leva,isintoc))
    allocate(tidp5s (ilga,leva,isintoc))
    allocate(tidp6s (ilga,leva,isintoc))
    allocate(tidm1s (ilga,leva,isintoc))
    allocate(tidm2s (ilga,leva,isintoc))
    allocate(tidm3s (ilga,leva,isintoc))
    allocate(tifrcs (ilga,leva,isintoc))
    allocate(tidryrt(ilga,leva,isintoc + 1))
    allocate(tirsmn (ilga,leva,iocs))
    allocate(titm1  (ilga,leva,iocs))
    allocate(titm2  (ilga,leva,iocs))
    allocate(ticorfs(isintoc))
    allocate(tidryr (isintoc + 1))
    allocate(tidryrc(isintoc))
  end if
  allocate(tecorfa(ilga,leva))
  allocate(ticorfa(ilga,leva))
  allocate(ctmp   (ilga,leva))
  allocate(acnd   (ilga,leva))
  allocate(plfn   (ilga,leva))
  allocate(cndm   (ilga,leva))
  allocate(cndmx  (ilga,leva))
  allocate(termx  (ilga,leva))
  allocate(term1  (ilga,leva))
  allocate(term2  (ilga,leva))
  allocate(term3  (ilga,leva))
  allocate(term4  (ilga,leva))
  allocate(dfsoap (ilga,leva))
  allocate(frp    (ilga,leva))
  allocate(plam   (ilga,leva))
  allocate(pertc  (ilga,leva))
  allocate(corfa  (ilga,leva))
  allocate(corfar (ilga,leva))
  allocate(dtc    (ilga,leva))
  allocate(expt   (ilga,leva))
  allocate(sadt   (ilga,leva))
  allocate(dbl1   (ilga,leva))
  allocate(dbl2   (ilga,leva))
  !
  !     * initialization.
  !
  if (isaext > 0) then
    pedndt = 0.
    pedmdt = 0.
  end if
  if (isaint > 0) then
    pidndt = 0.
    pidmdt = 0.
    pidfdt = 0.
  end if
  !
  !     * temporary aerosol fields for oc aerosol for dry
  !     * and wet particle sizes for externally mixed aerosol type.
  !
  if (kextoc > 0) then
    do is = 1,isextoc
      tewetrc(:,:,is) = pewetrc(:,:,iexoc(is))
      ten    (:,:,is) = penum  (:,:,iexoc(is))
      tem    (:,:,is) = pemas  (:,:,iexoc(is))
      teddn  (:,:,is) = peddn  (:,:,iexoc(is))
      ten0   (:,:,is) = pen0   (:,:,iexoc(is))
      tephi0 (:,:,is) = pephi0 (:,:,iexoc(is))
      tepsi  (:,:,is) = pepsi  (:,:,iexoc(is))
      tedphit(:,:,is) = pedphis(iexoc(is))
      tephist(:,:,is) = pephiss(iexoc(is))
      tedryrc(is) = aextf%tp(kextoc)%dryrc(is)
      tedryr(is) = aextf%tp(kextoc)%dryr(is)%vl
      tedryrt(:,:,is) = tedryr(is)
    end do
    tedryr(isextoc + 1) = aextf%tp(kextoc)%dryr(isextoc)%vr
    tedryrt(:,:,isextoc + 1) = tedryr(isextoc + 1)
  end if
  !
  !     * temporary aerosol fields for oc aerosol for dry
  !     * and wet particle sizes for internally mixed aerosol type.
  !
  if (kintoc > 0) then
    do is = 1,isintoc
      tiwetrc(:,:,is) = piwetrc(:,:,iinoc(is))
      tin    (:,:,is) = pinum  (:,:,iinoc(is))
      tim    (:,:,is) = pimas  (:,:,iinoc(is))
      tims   (:,:,is) = pimas  (:,:,iinoc(is)) &
                        * pifrc  (:,:,iinoc(is),kintoc)
      tifrcs (:,:,is) = pifrc  (:,:,iinoc(is),kintoc)
      do it = 1,iocs
        timn(:,:,is,it) = pimas  (:,:,iinoc(is)) &
                          * pifrc  (:,:,iinoc(is),itoc(it))
        tifrcn(:,:,is,it) = pifrc(:,:,iinoc(is),itoc(it))
      end do
      tiddn  (:,:,is) = piddn  (:,:,iinoc(is))
      tin0   (:,:,is) = pin0   (:,:,iinoc(is))
      tiphi0 (:,:,is) = piphi0 (:,:,iinoc(is))
      tipsi  (:,:,is) = pipsi  (:,:,iinoc(is))
      tidphit(:,:,is) = pidphis(iinoc(is))
      tiphist(:,:,is) = piphiss(iinoc(is))
      tidryrc(is) = aintf%dryrc(iinoc(is))
      tidryr(is) = aintf%dryr(iinoc(is))%vl
      tidryrt(:,:,is) = tidryr(is)
    end do
    tidryr(isintoc + 1) = aintf%dryr(aintf%tp(kintoc)%isce)%vr
    tidryrt(:,:,isintoc + 1) = tidryr(isintoc + 1)
  end if
  !
  !-----------------------------------------------------------------------
  !     * diffusivity of soa precursors, mean free path, and rate
  !     * coefficients for condensation of soa precursors onto pre-
  !     * existing aerosol (cndgp, in 1/s).
  !
  term1 = sqrt(t)
  term1 = term1 * t
  dfsoap = 2.150e-09_r8 * term1
  dbl1 = 1.62864e-25/t
  dbl2 = sqrt(dbl1)
  frp = 3.372999311e+11_r8 * dfsoap * dbl2
  wrat = 1.
  acnd = 0.
  if (isextoc > 0) then
    call grwtrm(tecndgp,tecgr1,tecgr2,tect1,tect2,ten0,ten,tewetrc, &
                tedryrc,tedryr,teddn,tephi0,tepsi,tephist,tedphit, &
                frp,dfsoap,wrat,rhoa,leva,ilga,isextoc)
    acnd = acnd + sum(tecndgp,dim = 3)
  end if
  if (isintoc > 0) then
    call grwtrm(ticndgp,ticgr1,ticgr2,tict1,tict2,tin0,tin,tiwetrc, &
                tidryrc,tidryr,tiddn,tiphi0,tipsi,tiphist,tidphit, &
                frp,dfsoap,wrat,rhoa,leva,ilga,isintoc)
    acnd = acnd + sum(ticndgp,dim = 3)
  end if
  !
  !-----------------------------------------------------------------------
  !     * parameters for time-evolution of soa precursor concentrations
  !     * to equilibrium solution (ctmp).
  !
  ctmp = sgpr/max(acnd,ytiny)
  plam = acnd
  dbl1 = - plam * dt
  dbl2 = exp(dbl1)
  expt = dbl2
  !
  !     * time-integrated soa precursor concentration (sadt).
  !
  pertc = soapi - ctmp
  term2 = pertc * (expt - 1.)
  where (plam <= max(abs(term2)/ylarge,ytiny) )
    plfn = - pertc * dt
    sadt = soapi * dt + .5 * sgpr * dt ** 2
  else where
    plfn = term2/plam
    sadt = ctmp * dt - plfn
  end where
  sadt = max(sadt,0.)
  !
  !     * growth factor from time-integration of r*dr/dt. units: m**2.
  !     * ysec is the threshold for the adjustment to time step.
  !
  tecorfm = 1./ysec
  tecorfa = 1./ysec
  if (isextoc > 0) then
    do is = 1,isextoc
      tecgr1(:,:,is) = tecgr1(:,:,is) * sadt(:,:)
      tecgr2(:,:,is) = tecgr2(:,:,is) * sadt(:,:)
      term1 = max(tecgr1(:,:,is) + tecgr2(:,:,is) * tedryr(is),ysmall)
      term2 = max(tecgr1(:,:,is) + tecgr2(:,:,is) * tedryr(is + 1),ysmall)
      tecorf(:,:,is) = ysec * min(tedryr(is) ** 2/term1(:,:), &
                       tedryr(is + 1) ** 2/term2(:,:))
    end do
    tecorf = min(1./ysec,tecorf)
    tecorfm = minval(tecorf)
    do l = 1,leva
      do il = 1,ilga
        tecorfs(:) = tecorf(il,l,:)
        tecorfa(il,l) = minval(tecorfs)
      end do
    end do
  end if
  ticorfm = 1./ysec
  ticorfa = 1./ysec
  if (isintoc > 0) then
    do is = 1,isintoc
      ticgr1(:,:,is) = ticgr1(:,:,is) * sadt(:,:)
      ticgr2(:,:,is) = ticgr2(:,:,is) * sadt(:,:)
      term1 = max(ticgr1(:,:,is) + ticgr2(:,:,is) * tidryr(is),ysmall)
      term2 = max(ticgr1(:,:,is) + ticgr2(:,:,is) * tidryr(is + 1),ysmall)
      ticorf(:,:,is) = ysec * min(tidryr(is) ** 2/term1(:,:), &
                       tidryr(is + 1) ** 2/term2(:,:))
    end do
    ticorf = min(1./ysec,ticorf)
    ticorfm = minval(ticorf)
    do l = 1,leva
      do il = 1,ilga
        ticorfs(:) = ticorf(il,l,:)
        ticorfa(il,l) = minval(ticorfs)
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * if necessary, adjust time step to limit aerosol growth
  !     * if growth rate exceeds critical thrshold for treatment
  !     * of condensation in subroutine pgrwth.
  !
  dtc = dt
  corfm = min(tecorfm,ticorfm)
  corfar = 1.
  if (corfm < 1. ) then
    corfa = min(tecorfa,ticorfa)
    corfar = min(corfa,1.)
    sadt = sadt * corfar
    if (isextoc > 0) then
      do is = 1,isextoc
        tecgr1(:,:,is) = tecgr1(:,:,is) * corfar(:,:)
        tecgr2(:,:,is) = tecgr2(:,:,is) * corfar(:,:)
      end do
    end if
    if (isintoc > 0) then
      do is = 1,isintoc
        ticgr1(:,:,is) = ticgr1(:,:,is) * corfar(:,:)
        ticgr2(:,:,is) = ticgr2(:,:,is) * corfar(:,:)
      end do
    end if
    !
    !       * iterate to obtain reduced time step.
    !
    termx = ctmp + pertc * expt
    where (termx < ytiny .and. corfa < 1. ) dtc = 0.
    term2 = 0.
    do inds = 1,iter
      term1 = pertc * (expt - 1.)
      where (corfa < 1. .and. plam > max(abs(term1)/ylarge,ytiny) &
          .and. termx > max(abs(term2)/ylarge,ytiny) )
        plfn = term1/plam
        term2 = ctmp * dtc - plfn - sadt
        dtc = min(max(dtc - term2/termx,0.),dt)
      end where
      expt = exp( - plam * dtc)
      termx = ctmp + pertc * expt
    end do
    term1 = abs(term2)
    where (sadt > max(term1/ylarge,ytiny) )
      termx = term1/sadt
    else where
      termx = 0.
    end where
    if (maxval(termx) > yeps) ierr = - 2
    term1 = pertc * (expt - 1.)
    where (abs(plam) > max(abs(term1)/ylarge,ytiny) )
      plfn = term1/plam
    else where
      plfn = - pertc * dtc
    end where
  end if
  if (minval(dtc) < dt) ierr = - 3
  !
  !-----------------------------------------------------------------------
  !     * change in soa precursor concentration from condensation.
  !
  if (isextoc > 0) then
    do is = 1,isextoc
      tedccnd(:,:,is) = - tecndgp(:,:,is) * sadt(:,:)/rhoa(:,:)
    end do
  end if
  if (isintoc > 0) then
    do is = 1,isintoc
      tidccnd(:,:,is) = - ticndgp(:,:,is) * sadt(:,:)/rhoa(:,:)
    end do
  end if
  !
  !     * diagnose condensation rates.
  !
  decnd = 0.
  dicnd = 0.
  cndm = 0.
  if (isextoc > 0) then
    termx = sum(tedccnd,dim = 3)
    cndm = cndm + termx
    decnd = decnd - termx/dt
  end if
  if (isintoc > 0) then
    termx = sum(tidccnd,dim = 3)
    cndm = cndm + termx
    dicnd = dicnd - termx/dt
  end if
  !
  !     * adjust condensation rates to make sure that truncation errors
  !     * in calculations of condensation rates do not lead to any
  !     * negative soa precursor concentrations.
  !
  cndmx = - soapi - sgpr * dt
  termx = 1.
  where (cndm < cndmx .and. cndm < min(cndmx/ylarge, - ytiny) )
    termx = cndmx/cndm
    cndm = cndmx
    decnd = decnd * termx
    dicnd = dicnd * termx
  end where
  if (isextoc > 0) then
    do is = 1,isextoc
      tedccnd(:,:,is) = tedccnd(:,:,is) * termx(:,:)
    end do
  end if
  if (isintoc > 0) then
    do is = 1,isintoc
      tidccnd(:,:,is) = tidccnd(:,:,is) * termx(:,:)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * particle growth due to condensation/evaporation. the following
  !     * calculations provide the updated size distributions for
  !     * particle number,oc aerosol mass, and mass of other aerosol
  !     * species after the condensation step.
  !
  dersn = 0.
  dersm = 0.
  if (isextoc > 0) then
    call pgrpar (tedp0l,tedp1l,tedp2l,tedp3l,tedp4l,tedp5l,tedp6l, &
                 tedm1l,tedm2l,tedm3l,tedp0r,tedp1r,tedp2r,tedp3r, &
                 tedp4r,tedp5r,tedp6r,tedm1r,tedm2r,tedm3r,tedp0s, &
                 tedp1s,tedp2s,tedp3s,tedp4s,tedp5s,tedp6s,tedm1s, &
                 tedm2s,tedm3s,tei0l,tei0r,tecgr1,tecgr2,ten0, &
                 tedryrt,tephist,tedphit,tephi0,tepsi,ilga,leva, &
                 isextoc,ierr,imodc)
    call pgrwth0(tent,dersn,tei0l,tei0r,tedp0l,tedp0r,tedp0s, &
                 ten0,ilga,leva,isextoc)
    tefrc(:,:,:) = 1.
    call pgrwths(temt,dersm,tefrc,tei0l,tei0r, &
                 tedp0l,tedp1l,tedp2l,tedp3l,tedm1l,tedm2l,tedm3l, &
                 tedp0r,tedp1r,tedp2r,tedp3r,tedm1r,tedm2r,tedm3r, &
                 tedp0s,tedp1s,tedp2s,tedp3s,tedm1s,tedm2s,tedm3s, &
                 tecgr1,tecgr2,ten0,teddn,ilga,leva,isextoc)
  end if
  dirsn = 0.
  dirsms = 0.
  dirsmn = 0.
  if (isintoc > 0) then
    call pgrpar (tidp0l,tidp1l,tidp2l,tidp3l,tidp4l,tidp5l,tidp6l, &
                 tidm1l,tidm2l,tidm3l,tidp0r,tidp1r,tidp2r,tidp3r, &
                 tidp4r,tidp5r,tidp6r,tidm1r,tidm2r,tidm3r,tidp0s, &
                 tidp1s,tidp2s,tidp3s,tidp4s,tidp5s,tidp6s,tidm1s, &
                 tidm2s,tidm3s,tii0l,tii0r,ticgr1,ticgr2,tin0, &
                 tidryrt,tiphist,tidphit,tiphi0,tipsi,ilga,leva, &
                 isintoc,ierr,imodc)
    call pgrwth0(tint,dirsn,tii0l,tii0r,tidp0l,tidp0r,tidp0s, &
                 tin0,ilga,leva,isintoc)
    call pgrwths(timst,dirsms,tifrcs,tii0l,tii0r, &
                 tidp0l,tidp1l,tidp2l,tidp3l,tidm1l,tidm2l,tidm3l, &
                 tidp0r,tidp1r,tidp2r,tidp3r,tidm1r,tidm2r,tidm3r, &
                 tidp0s,tidp1s,tidp2s,tidp3s,tidm1s,tidm2s,tidm3s, &
                 ticgr1,ticgr2,tin0,tiddn,ilga,leva,isintoc)
    if (iocs > 0) then
      tirsmn = 0.
      call pgrwthn(timnt,tirsmn,tifrcn,tii0l,tii0r,tidp3l,tidp3r, &
                   tidp3s,tin0,tiddn,ilga,leva,iocs,isintoc)
      dirsmn = sum(tirsmn,dim = 3)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * number and mass fixers. this corrects for any truncation
  !     * errors that may have occurred previously. the approach is
  !     * to scale the total number and mass concentrations so that the
  !     * final total mass will be consistent with the corresponding
  !     * physico-chemical production and loss rates.
  !
  term1 = 0.
  term2 = 0.
  term3 = 0.
  term4 = 0.
  if (isextoc > 0) then
    tent = max(tent,0.)
    temt = max(temt,0._r8)
    term1 = term1 + sum(temt,dim = 3) + dersm
    term2 = term2 + sum(tent,dim = 3) + dersn
    term3 = term3 + sum(tem ,dim = 3)
    term4 = term4 + sum(ten ,dim = 3)
  end if
  if (isintoc > 0) then
    tint = max(tint,0.)
    timst = max(timst,0._r8)
    term1 = term1 + sum(timst,dim = 3) + dirsms
    term2 = term2 + sum(tint ,dim = 3) + dirsn
    term3 = term3 + sum(tims ,dim = 3)
    term4 = term4 + sum(tin  ,dim = 3)
    if (iocs > 0) then
      titm1 = sum(timnt,dim = 3) + tirsmn
      titm2 = sum(timn ,dim = 3)
    end if
  end if
  !
  do l = 1,leva
    do il = 1,ilga
      !
      !       * correct aerosol number concentration.
      !
      if (term2(il,l) > max(term4(il,l)/ylarge,ytiny) ) then
        cfn = term4(il,l)/term2(il,l)
        if (isextoc > 0) tent(il,l,:) = tent(il,l,:) * cfn
        if (isintoc > 0) tint(il,l,:) = tint(il,l,:) * cfn
      end if
      !
      !       * correct soa concentration.
      !
      term5 = term3(il,l) - cndm(il,l)
      if (term1(il,l) > max(term5/ylarge,ytiny) ) then
        cfm = term5/term1(il,l)
        if (isextoc > 0) temt (il,l,:) = temt (il,l,:) * cfm
        if (isintoc > 0) timst(il,l,:) = timst(il,l,:) * cfm
      end if
    end do
  end do
  !
  !     * correct non-soa mass concentration.
  !
  if (isintoc > 0) then
    do it = 1,iocs
      do l = 1,leva
        do il = 1,ilga
          if (titm1(il,l,it) > max(titm2(il,l,it)/ylarge,ytiny) ) then
            cfm = titm2(il,l,it)/titm1(il,l,it)
            timnt(il,l,:,it) = timnt(il,l,:,it) * cfm
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * total aerosol mass for internally mixed aerosol species.
  !
  if (kintoc > 0) then
    timt = timst
    do it = 1,iocs
      timt(:,:,:) = timt(:,:,:) + timnt(:,:,:,it)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * soa precursor tendency.
  !
  dgdt = (sgpr * dt + cndm)/dt
  !
  !-----------------------------------------------------------------------
  !     * tracer tendencies.
  !
  if (kextoc > 0) then
    do is = 1,isextoc
      pedndt(:,:,iexoc(is)) = (tent(:,:,is) - ten(:,:,is))/dt
      pedmdt(:,:,iexoc(is)) = (temt(:,:,is) - tem(:,:,is))/dt
    end do
  end if
  if (kintoc > 0) then
    do is = 1,isintoc
      pidndt(:,:,iinoc(is)) = (tint(:,:,is) - tin(:,:,is))/dt
      pidmdt(:,:,iinoc(is)) = (timt(:,:,is) - tim(:,:,is))/dt
      where (timt(:,:,is) > ytiny)
        pidfdt(:,:,iinoc(is),kintoc) = &
                                       (timst(:,:,is)/timt(:,:,is) - tifrcs(:,:,is))/dt
      else where
        pidfdt(:,:,iinoc(is),kintoc) = 0.
      end where
      do it = 1,iocs
        where (timt(:,:,is) > ytiny)
          pidfdt(:,:,iinoc(is),itoc(it)) = &
                                           (timnt(:,:,is,it)/timt(:,:,is) - tifrcn(:,:,is,it))/dt
        else where
          pidfdt(:,:,iinoc(is),itoc(it)) = 0.
        end where
      end do
    end do
  end if
  !
  !     * reset tendencies in case no condensation occurs to account
  !     * for numerical truncation errors.
  !
  if (isextoc > 0) then
    do l = 1,leva
      do il = 1,ilga
        if (cndm(il,l) > - ytiny) then
          pedndt(il,l,:) = 0.
          pedmdt(il,l,:) = 0.
        end if
      end do
    end do
  end if
  if (isintoc > 0) then
    do l = 1,leva
      do il = 1,ilga
        if (cndm(il,l) > - ytiny) then
          pidndt(il,l,:) = 0.
          pidmdt(il,l,:) = 0.
          pidfdt(il,l,:,:) = 0.
        end if
      end do
    end do
  end if
  !
  !     * deallocation.
  !
  if (isextoc > 0) then
    deallocate(tewetrc)
    deallocate(ten)
    deallocate(tent)
    deallocate(tem)
    deallocate(temt)
    deallocate(teddn)
    deallocate(ten0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tect1)
    deallocate(tect2)
    deallocate(tecgr1)
    deallocate(tecgr2)
    deallocate(tecndgp)
    deallocate(tedccnd)
    deallocate(tedphit)
    deallocate(tephist)
    deallocate(tecorf)
    deallocate(tei0l)
    deallocate(tei0r)
    deallocate(tedp0l)
    deallocate(tedp1l)
    deallocate(tedp2l)
    deallocate(tedp3l)
    deallocate(tedp4l)
    deallocate(tedp5l)
    deallocate(tedp6l)
    deallocate(tedm1l)
    deallocate(tedm2l)
    deallocate(tedm3l)
    deallocate(tedp0r)
    deallocate(tedp1r)
    deallocate(tedp2r)
    deallocate(tedp3r)
    deallocate(tedp4r)
    deallocate(tedp5r)
    deallocate(tedp6r)
    deallocate(tedm1r)
    deallocate(tedm2r)
    deallocate(tedm3r)
    deallocate(tedp0s)
    deallocate(tedp1s)
    deallocate(tedp2s)
    deallocate(tedp3s)
    deallocate(tedp4s)
    deallocate(tedp5s)
    deallocate(tedp6s)
    deallocate(tedm1s)
    deallocate(tedm2s)
    deallocate(tedm3s)
    deallocate(tefrc)
    deallocate(tedryrt)
    deallocate(tecorfs)
    deallocate(tedryr)
    deallocate(tedryrc)
  end if
  if (isintoc > 0) then
    deallocate(timn)
    deallocate(timnt)
    deallocate(tifrcn)
    deallocate(tims)
    deallocate(timst)
    deallocate(tiwetrc)
    deallocate(tin)
    deallocate(tint)
    deallocate(tim)
    deallocate(timt)
    deallocate(tiddn)
    deallocate(tin0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tict1)
    deallocate(tict2)
    deallocate(ticgr1)
    deallocate(ticgr2)
    deallocate(ticndgp)
    deallocate(tidccnd)
    deallocate(tidphit)
    deallocate(tiphist)
    deallocate(ticorf)
    deallocate(tii0l)
    deallocate(tii0r)
    deallocate(tidp0l)
    deallocate(tidp1l)
    deallocate(tidp2l)
    deallocate(tidp3l)
    deallocate(tidp4l)
    deallocate(tidp5l)
    deallocate(tidp6l)
    deallocate(tidm1l)
    deallocate(tidm2l)
    deallocate(tidm3l)
    deallocate(tidp0r)
    deallocate(tidp1r)
    deallocate(tidp2r)
    deallocate(tidp3r)
    deallocate(tidp4r)
    deallocate(tidp5r)
    deallocate(tidp6r)
    deallocate(tidm1r)
    deallocate(tidm2r)
    deallocate(tidm3r)
    deallocate(tidp0s)
    deallocate(tidp1s)
    deallocate(tidp2s)
    deallocate(tidp3s)
    deallocate(tidp4s)
    deallocate(tidp5s)
    deallocate(tidp6s)
    deallocate(tidm1s)
    deallocate(tidm2s)
    deallocate(tidm3s)
    deallocate(tifrcs)
    deallocate(tidryrt)
    deallocate(tirsmn)
    deallocate(titm1)
    deallocate(titm2)
    deallocate(ticorfs)
    deallocate(tidryr)
    deallocate(tidryrc)
  end if
  deallocate(tecorfa)
  deallocate(ticorfa)
  deallocate(ctmp)
  deallocate(acnd)
  deallocate(plfn)
  deallocate(cndm)
  deallocate(cndmx)
  deallocate(termx)
  deallocate(term1)
  deallocate(term2)
  deallocate(term3)
  deallocate(term4)
  deallocate(dfsoap)
  deallocate(frp)
  deallocate(plam)
  deallocate(pertc)
  deallocate(corfa)
  deallocate(corfar)
  deallocate(dtc)
  deallocate(expt)
  deallocate(sadt)
  deallocate(dbl1)
  deallocate(dbl2)
  !
end subroutine gtsoa
