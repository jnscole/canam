subroutine nm2pla (pn0,phi0,psi,resn,resm,bnum,bmass, &
                   drydn,phiss,dphis,phis0,dphi0, &
                   ilga,leva,isec,kpt)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     transformation of number and mass to pla-parameters. subroutine
  !     cornm should be called before this subroutine.
  !
  !     history:
  !     --------
  !     * jan 07/2008 - k.vonsalzen   improve numerical stability.
  !     * apr 13/2006 - k.vonsalzen   completely revised for use with
  !     *                             internally and externally mixed
  !     *                             types of aerosols.
  !     * dec 11/2005 - k.vonsalzen   new.
  !
  !     ************************ index of variables **********************
  !
  !     primary input/output variables (argument list):
  !
  ! i   bmass    aerosol mass concentration (kg/kg)
  ! i   bnum     aerosol number concentration (1/kg)
  ! i   dphi0    section width
  ! i   dphis    section width
  ! i   drydn    density of dry aerosol particle (kg/m3)
  ! i   ilga     number of grid points in horizontal direction
  ! i   isec     number of separate aerosol tracers
  ! i   kpt      flag to distinguish between externally
  !              resp. internally mixed types of aerosol
  ! i   leva     number of grid pointa in vertical direction
  ! o   phi0     3rd pla size distribution parameter (mode size)
  ! i   phis0    lower dry particle size (=log(r/r0))
  ! i   phiss    lower dry particle size (=log(r/r0))
  ! o   pn0      1st pla size distribution parameter (amplitude)
  ! o   psi      2nd pla size distribution parameter (width)
  ! i   ptrans(:)%flg(:)
  !              flag to indicate valid data in pla data table
  ! i   ptrans(:)%idrb
  !              number of points in pla data table
  ! i   ptrans(:)%dphib(:)%vl
  !              tabulated derivative of delta-phi at lower section
  !              boundary
  ! i   ptrans(:)%dphib(:)%vr
  !              tabulated derivative of delta-phi at upper section
  !              boundary
  ! i   ptrans(:)%phib(:)%vl
  !              tabulated results for delta-phi at lower section
  !              boundary
  ! i   ptrans(:)%phib(:)%vr
  !              tabulated results for delta-phi at upper section
  !              boundary
  ! i   ptrans(:)%psib(:)%vl
  !              tabulated results for psi at lower section boundary
  ! o   resm     mass residuum from numerical truncation (kg/kg)
  ! o   resn     number residuum from numerical truncation (kg/kg)
  !
  !     secondary input/output variables (via modules and common blocks):
  !
  ! i   ilarge   integer :: equivalent to ylarge
  ! i   r0       particle radius reference radius (1.e-06 m)
  ! i   ycnst    4*pi/3
  ! i   ylarge   large number
  ! i   yna      default for undefined value
  ! i   ytiny    smallest valid number
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use sdcode
  use fpdef
  !
  implicit none
  real :: acnst
  real :: aterm2
  real :: dratb
  real :: dratx
  real :: frcm
  real :: frcn
  integer :: idef
  integer :: idrb
  integer :: il
  integer :: iok
  integer :: ir
  integer :: is
  integer :: it
  integer :: l
  real :: philx
  real :: phirx
  real :: r0p
  real :: rat0
  real :: rmom
  real :: sigp
  real :: weigf
  !
  logical, intent(in) :: kpt !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(isec) :: phiss !<
  real, intent(in), dimension(isec) :: dphis !<
  real, intent(in), dimension(ilga,leva,isec) :: bnum !<
  real, intent(in), dimension(ilga,leva,isec) :: bmass !<
  real, intent(in), dimension(ilga,leva,isec) :: drydn !<
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !<
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !<
  real, intent(out), dimension(ilga,leva) :: resn !<
  real, intent(out), dimension(ilga,leva) :: resm !<
  real, intent(out), dimension(ilga,leva,isec) :: pn0 !<
  real, intent(out), dimension(ilga,leva,isec) :: phi0 !<
  real, intent(out), dimension(ilga,leva,isec) :: psi !<
  real, allocatable, dimension(:,:,:) :: pn0n !<
  real, allocatable, dimension(:,:,:) :: pn0m !<
  real, allocatable, dimension(:,:,:) :: sdfn !<
  real, allocatable, dimension(:,:,:) :: sdfm !<
  real, allocatable, dimension(:,:,:) :: rat0s !<
  real, allocatable, dimension(:,:,:) :: arg !<
  real, allocatable, dimension(:,:,:) :: phihat !<
  real, allocatable, dimension(:,:,:) :: atmp !<
  real, allocatable, dimension(:,:,:) :: amass !<
  real, allocatable, dimension(:,:) :: totn !<
  real, allocatable, dimension(:,:) :: totm !<
  type(ptrant), allocatable, dimension(:) :: ptrans
  !
  real, parameter :: onethird = 1./3. !<
  real, parameter :: yscal = 1.e+09 !<
  real, parameter :: rweigh = 1. ! weighting factor
  integer, parameter :: itmax = 5 ! number of iterations for
  ! increased accuracy
  integer, parameter :: iconsm = 2 ! determines if numerical
  ! truncation will primarily
  ! affect mass or number results
  logical, parameter :: kiter = .false. ! iterations for increased
  ! accuracy
  real(r8) :: rmom3 !<
  real(r8) :: trm1 !<
  real(r8) :: trm2 !<
  real(r8) :: sqrtpsi !<
  real(r8) :: dphi !<
  real(r8) :: arg1 !<
  real(r8) :: arg2 !<
  real(r8) :: arg3 !<
  real(r8) :: arg4 !<
  real(r8) :: aerf1 !<
  real(r8) :: aerf2 !<
  real(r8) :: aerf3 !<
  real(r8) :: aerf4 !<
  real(r8) :: aterm1 !<
  real(r8) :: aderf1 !<
  real(r8) :: aderf2 !<
  real(r8) :: aderf3 !<
  real(r8) :: aderf4 !<
  real(r8) :: adfun !<
  real(r8) :: afun !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(pn0n  (ilga,leva,isec))
  allocate(pn0m  (ilga,leva,isec))
  allocate(sdfn  (ilga,leva,isec))
  allocate(sdfm  (ilga,leva,isec))
  allocate(rat0s (ilga,leva,isec))
  allocate(arg   (ilga,leva,isec))
  allocate(phihat(ilga,leva,isec))
  allocate(atmp  (ilga,leva,isec))
  allocate(amass (ilga,leva,isec))
  allocate(ptrans(isatr))
  do is = 1,isatr
    allocate(ptrans(is)%flg  (idrbm))
    allocate(ptrans(is)%psib (idrbm))
    allocate(ptrans(is)%phib (idrbm))
    allocate(ptrans(is)%dphib(idrbm))
  end do
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  pn0 = 0.
  pn0m = 0.
  pn0n = 0.
  psi = yna
  phi0 = yna
  r0p = r0 ** 3
  acnst = ycnst * r0p * yscal
  !
  !     * copy parameters specific to internally resp. externally
  !     * mixed types of aerosol from permanent to temporary memory.
  !
  if (kpt .eqv. kpext) then
    do is = 1,isatr
      ptrans(is)%idrb       = pext(is)%idrb
      ptrans(is)%flg  (:)   = pext(is)%flg  (:)
      ptrans(is)%psib (:)%vl = pext(is)%psib (:)%vl
      ptrans(is)%psib (:)%vr = pext(is)%psib (:)%vr
      ptrans(is)%phib (:)%vl = pext(is)%phib (:)%vl
      ptrans(is)%phib (:)%vr = pext(is)%phib (:)%vr
      ptrans(is)%dphib(:)%vl = pext(is)%dphib(:)%vl
      ptrans(is)%dphib(:)%vr = pext(is)%dphib(:)%vr
    end do
  else
    do is = 1,isatr
      ptrans(is)%idrb       = pint(is)%idrb
      ptrans(is)%flg  (:)   = pint(is)%flg  (:)
      ptrans(is)%psib (:)%vl = pint(is)%psib (:)%vl
      ptrans(is)%psib (:)%vr = pint(is)%psib (:)%vr
      ptrans(is)%phib (:)%vl = pint(is)%phib (:)%vl
      ptrans(is)%phib (:)%vr = pint(is)%phib (:)%vr
      ptrans(is)%dphib(:)%vl = pint(is)%dphib(:)%vl
      ptrans(is)%dphib(:)%vr = pint(is)%dphib(:)%vr
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * values of psi and phi0.
  !
  arg = 1.
  atmp = acnst * drydn * bnum
  amass = bmass * yscal
  where (bnum > ytiny .and. bmass > ytiny &
      .and. atmp > max(amass/ylarge,ytiny) )
    arg = amass/atmp
  end where
  phihat = onethird * log(arg)
  do is = 1,isec
    idrb = ptrans(is)%idrb
    dratb = 1./real(idrb)
    do l = 1,leva
      do il = 1,ilga
        if (bnum(il,l,is) > ytiny .and. bmass(il,l,is) > ytiny &
            ) then
          !
          !           * size ratio from mass and number in each section.
          !
          rat0 = (phihat(il,l,is) - phiss(is))/dphis(is)
          rat0s(il,l,is) = max(0.,min(1.,rat0))
          ir = min(nint(rat0 * idrb + 0.5),idrb)
          !
          !           * check if size ratio is valid. abort if this is not the case
          !           * subroutine cornm will prevent this from happening if
          !           * called before this subroutine.
          !
          if (int(rat0) /= 0 .or. rat0 < 0. ) call xit ('nm2pla', - 1)
          if (ptrans(is)%flg(ir) ) then
            !
            !             * use tabulated results to calculate phi-parameter from
            !             * linear interpolation and to set psi-parameter.
            !
            psi(il,l,is) = ptrans(is)%psib(ir)%vl
            dratx = rat0 - real(ir - 1) * dratb
            weigf = 1. - dratx/dratb
            philx = ptrans(is)%phib(ir)%vl &
                    + dratx * ptrans(is)%dphib(ir)%vl
            phirx = ptrans(is)%phib(ir)%vr &
                    - (dratb - dratx) * ptrans(is)%dphib(ir)%vr
            phi0(il,l,is) = phiss(is) + weigf * philx + (1. - weigf) * phirx
          else
            !
            !             * size distribution is a delta-function
            !
            psi(il,l,is) = ylarge
            phi0(il,l,is) = phihat(il,l,is)
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(arg)
  deallocate(phihat)
  deallocate(ptrans)
  !
  !-----------------------------------------------------------------------
  !     * iterations to increase accuracy.
  !
  if (kiter) then
    rmom = 3.
    trm1 = .5 * rmom
    trm2 = 0.25 * rmom ** 2
    do is = 1,isec
      do l = 1,leva
        do il = 1,ilga
          if (psi(il,l,is) < ylarges &
              .and. abs(psi(il,l,is) - yna) > ytiny) then
            sqrtpsi = sqrt(abs(psi(il,l,is)))
            dphi = phi0(il,l,is) - phiss(is)
            iok = 1
            do it = 1,itmax
              arg3 = sqrtpsi * (dphis(is) - dphi)
              arg4 = - sqrtpsi * dphi
              if (psi(il,l,is) > ytiny) then
                arg1 = arg3 - trm1/sqrtpsi
                arg2 = arg4 - trm1/sqrtpsi
                aerf1 = &! proc-depend
                        erf(arg1)
                aerf2 = &! proc-depend
                        erf(arg2)
                aerf3 = &! proc-depend
                        erf(arg3)
                aerf4 = &! proc-depend
                        erf(arg4)
                sigp = 1.
              else
                arg1 = arg3 + trm1/sqrtpsi
                arg2 = arg4 + trm1/sqrtpsi
                aerf1 = erfi(arg1)
                aerf2 = erfi(arg2)
                aerf3 = erfi(arg3)
                aerf4 = erfi(arg4)
                sigp = - 1.
              end if
              !
              !             * critical function.
              !
              arg1 = 3. * (rat0s(il,l,is) * dphis(is) - dphi) &
                     - 9./(4. * psi(il,l,is))
              aterm1 = &! proc-depend
                       exp(arg1)
              if (abs(aerf3 - aerf4) > ytiny) then
                aterm2 = (aerf1 - aerf2)/(aerf3 - aerf4)
                afun = aterm1 - aterm2
                !
                !               * derivative.
                !
                arg1 = - sigp * arg1 ** 2
                arg2 = - sigp * arg2 ** 2
                arg3 = - sigp * arg3 ** 2
                arg4 = - sigp * arg4 ** 2
                aderf1 = - 2. * &! proc-depend
                         exp(arg1) * sqrtpsi/sqrtpi
                aderf2 = - 2. * &! proc-depend
                         exp(arg2) * sqrtpsi/sqrtpi
                aderf3 = - 2. * &! proc-depend
                         exp(arg3) * sqrtpsi/sqrtpi
                aderf4 = - 2. * &! proc-depend
                         exp(arg4) * sqrtpsi/sqrtpi
                adfun = - 3. * aterm1 - ((aderf1 - aderf2) * (aerf3 - aerf4) &
                        - (aderf3 - aderf4) * (aerf1 - aerf2)) &
                        /(aerf3 - aerf4) ** 2
                !
                !               * update dphi
                !
                dphi = dphi - afun/adfun
              else
                iok = idef
              end if
            end do
            if (iok /= idef) phi0(il,l,is) = phiss(is) + dphi
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work array.
  !
  deallocate(rat0s)
  !
  !-----------------------------------------------------------------------
  !     * calculate remaining third parameter of gaussian distribution.
  !     * the calculation of the phi-parameter from tabulated results
  !     * will lead to inaccuracies for this parameter.
  !
  sdfn = sdint0(phi0,psi,phis0,dphi0,ilga,leva,isec)
  rmom = 3.
  sdfm = drydn * ycnst * sdint(phi0,psi,rmom,phis0,dphi0,ilga,leva,isec)
  where (sdfn > max(bnum /ylarge,ytiny) &
      .and. sdfm > max(bmass/ylarge,ytiny) )
    pn0n = bnum /sdfn
    pn0m = bmass/sdfm
  end where
  if (iconsm == 1) then
    !
    !       * truncation errors are assumed to only affect the mass
    !       * size distribution.
    !
    pn0 = pn0n
    resn = 0.
    resm = sum(pn0 * sdfm - bmass,dim = 3)
  else if (iconsm == 2) then
    !
    !       * truncation errors are assumed to only affect the number
    !       * size distribution.
    !
    pn0 = pn0m
    resn = sum(pn0 * sdfn - bnum ,dim = 3)
    resm = 0.
  else if (iconsm == 3) then
    !
    !       * reduce the impact of inaccuracies on total mass and number
    !       * by using a weighted average of the parameters that would
    !       * result for mass and number only.
    !
    allocate(totn(ilga,leva))
    allocate(totm(ilga,leva))
    totm = sum(bmass,dim = 3)
    totn = sum(bnum ,dim = 3)
    do is = 1,isec
      do l = 1,leva
        do il = 1,ilga
          if (bnum(il,l,is) > ytiny .and. bmass(il,l,is) > ytiny &
              ) then
            frcm = rweigh * bmass(il,l,is)/totm(il,l)
            frcn = bnum(il,l,is)/totn(il,l)
            weigf = frcn/(frcn + frcm)
            pn0(il,l,is) = weigf * pn0n(il,l,is) + (1. - weigf) * pn0m(il,l,is)
          end if
        end do
      end do
    end do
    resn = sum(pn0 * sdfn - bnum ,dim = 3)
    resm = sum(pn0 * sdfm - bmass,dim = 3)
    deallocate(totn)
    deallocate(totm)
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(pn0n)
  deallocate(pn0m)
  deallocate(sdfn)
  deallocate(sdfm)
  deallocate(atmp)
  deallocate(amass)
  !
end subroutine nm2pla
