!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine gwdsat3(um,vm,tsg,tsgb,sgj,sgbj,pressg,radj, &
                   rgas,rgocp,ilev,il1,il2,ilg,laun_z,eplaunch, &
                   nadim,ncdim,utend,vtend,twodti)
  !
  !     * apr 29, 2012 - j.scinocca. new version for gcm16:
  !     *                            - deposit all outgoing gwd in top
  !     *                              layer.
  !     * jun 27, 2006 - m.lazare.   previous version gwdsat for gcm15f/g/h/i:
  !     *                            - radj passed in and used as sin(radj)
  !     *                              instead of passing in sinj (since
  !     *                              sinj no longer in physics). note
  !     *                              that it is real :: and not real(8).
  !     *                            - work arrays are now local to the
  !     *                              routine, ie not passed in with
  !     *                              "WRK" pointers.
  !     *                            - replace constants by variables in
  !     *                              intrinsic calls such as "MAX" to
  !     *                              enable compiling in 32-bits with real(8).
  !
  !     non-orographic gravity-wave drag parameterization scinocca (jas 2003).
  !     exact hydrostatic, non-rotating, version of warner and mcintyre (1996)
  !     this version includes a critical-level correction that prevents the momemtum
  !     deposition in each azimuth from driving the flow to speeds faster than
  !     the phase speed of the waves that meet thair critical levels.
  !
  !     ptwo - 2*p, where -p is the exponent of the intrinsic frequency in the
  !            expression for launch energy density
  !     ipreal - flag to indicate if p is integer :: or real -> used for optimization

  implicit none
  real :: a
  real :: afct
  real :: afdp
  real :: anorm
  real :: atmp
  real :: az
  real :: az_fct
  real :: b
  real :: bs
  real :: bv3
  real :: bvmmin
  real :: cimax
  real :: cimin
  real :: cin
  real :: cin3
  real :: cinc
  real :: cng
  real :: da
  real :: dep
  real :: dft
  real :: dtds
  real :: dtx
  real, intent(in) :: eplaunch
  real :: eps
  real :: epsq
  real :: f
  real :: f0
  real :: f1
  real :: fct1
  real :: fct2
  real :: fct3
  real :: gam
  real :: grav
  real :: grsq
  integer :: i
  integer :: ia
  integer :: ic
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ilevm
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: imcons
  integer :: ipreal
  integer :: l
  integer, intent(in) :: laun_z
  integer, intent(in) :: nadim
  integer, intent(in) :: ncdim
  real :: p5
  real :: pexp
  real :: pi
  real :: ptwo
  real :: ratio
  real :: rd
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: rgocp  !< Ideal gas constant divided by heat capacity for dry air \f$[unitless]\f$
  real :: rog
  real :: rot
  real :: s1
  real, intent(in) :: twodti
  real :: twodtinv
  real :: tx
  real :: u
  real :: ulm
  real :: ulon
  real :: xmax
  real :: xmin
  real :: xrng
  real :: zero

  parameter(ptwo = 3.e0,ipreal = 1)
  !
  !     i/o arrays.
  !
  real, intent(in), dimension(ilg,ilev) :: um !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: vm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tsg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tsgb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(inout), dimension(ilg,ilev) :: utend !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: vtend !< Variable description\f$[units]\f$
  !
  !     * arrays passed through call to physics are native real.
  !
  real, intent(in), dimension(ilg) :: radj   !< Latitude \f$[radians]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * local internal work arrays.
  !
  real, dimension(ilg,ilev) :: ub
  real, dimension(ilg,ilev) :: vb
  real, dimension(ilg,ilev) :: bvm
  real, dimension(ilg,ilev) :: bvb
  real, dimension(ilg,ilev) :: pm
  real, dimension(ilg,ilev) :: pb
  real, dimension(ilg,ilev) :: rhom
  real, dimension(ilg,ilev) :: rhob

  real, dimension(ncdim) :: x
  real, dimension(ncdim) :: ci
  real, dimension(ncdim) :: dci
  real, dimension(ilg,nadim,ilev) :: ui
  real, dimension(ilg,nadim) :: ul
  real, dimension(ilg) :: nl
  real, dimension(2,nadim) :: unit
  real, dimension(ilg,ilev) :: fct
  real, dimension(ilg) :: fnorm
  real, dimension(ilg,nadim) :: ci_min

  real, dimension(ilg,ncdim,nadim) :: ep
  real, dimension(ilg,ncdim,nadim) :: act
  real, dimension(ilg,ilev,nadim) :: pu
  real, dimension(ilg,ilev,2) :: epf
  real, dimension(ilg,ilev,nadim) :: crt
  real, dimension(ilg,ilev,nadim) :: dfl
  real, dimension(ilg,ncdim,nadim) :: acc

  real, dimension(ilg,ilev) :: zm
  real, dimension(ilg,ilev) :: zb

  real   :: ms_l
  real   :: ms
  data grav /9.80616/
  data zero /0./
  data bvmmin /1.e-20/
  data p5 /0.5/
  !=====================================================================
  ilevm = ilev - 1
  grsq = grav ** 2
  pi = 2 * asin(1.e0)
  rot = 7.292e-5
  twodtinv = twodti
  !     input spectra info
  !     ms_l,ms - specification of m-star in launch spectra
  ms_l = 1000.e0
  ms = 2 * pi/ms_l
  pexp = ptwo/2.e0


  !     coordinate transform
  !     cimin,cimax - min,max intrinsic launch-level phase speed (c-u_o) (m/s)
  cimin = 0.25
  cimax = 100.
  !     x=1/ci - transformation variable
  xmax = 1./cimin
  xmin = 1./cimax
  !     parameters for coordinate transform
  gam = 0.25
  xrng = xmax - xmin
  dtx = xrng/float(ncdim - 1)
  a = xrng/(exp(xrng/gam) - 1.)
  b = xmin - a

  !     set initial min ci in each column and azimuth (used for critical levels)
  do ia = 1,nadim
    do il = il1,il2
      ci_min(il,ia) = cimin
    end do
  end do


  !     *     momentum
  !     * mid layer  interface
  !     *
  !     *//////////////////////
  !     *
  !     *
  !     *......sgj(1)..........
  !     *
  !     *-------------- sgbj(1)
  !     *
  !     *......sgj(2)..........
  !     *
  !     *-------------- sgbj(2)
  !     *
  !     *......sgj(3)..........
  !     *
  !     *-------------- sgbj(3)
  !     *
  !     *......sgj(4)..........
  !     *
  !     *-------------- sgbj(4)
  !     *
  !     *......sgj(5)..........
  !     *
  !     *
  !     *////////////// sgbj(5)=1.


  !     calculate pressue on mid and base levels
  do il = il1,il2
    pb(il,1) = sgbj(il,1) * pressg(il)
    pm(il,1)  = sgj(il,1) * pressg(il)
  end do

  do l = 2,ilev
    do il = il1,il2
      pb(il,l)   = sgbj(il,l)   * pressg(il)
      pm(il,l)    = sgj(il,l)   * pressg(il)
    end do
  end do

  !     calculate height on mid and base levels
  rog = rgas/grav
  do il = il1,il2
    zb(il,ilev) = 0
  end do

  do  l = ilevm,1, - 1
    do il = il1,il2
      zb(il,l) = zb(il,l + 1) + &
                 rog * tsg(il,l + 1) * log(pb(il,l + 1)/pb(il,l))
    end do
  end do

  do il = il1,il2
    zm(il,ilev) = rog * tsgb(il,ilev) * log(pressg(il)/pm(il,ilev))
  end do

  do  l = ilevm,1, - 1
    do il = il1,il2
      zm(il,l) = zm(il,l + 1) + &
                 rog * tsgb(il,l) * log(pm(il,l + 1)/pm(il,l))
    end do
  end do

  !     calculate bv frequency on mid level
  do l = 2,ilev
    do i = il1,il2
      dtds = - sgj(i,l)/tsg(i,l) &
             * (tsgb(i,l) - tsgb(i,l - 1))/(sgbj(i,l) - sgbj(i,l - 1)) &
             + rgocp
      bvm(i,l) = max(grsq * (dtds/(rgas * tsg(i,l))),bvmmin)
      bvm(i,l) = sqrt(bvm(i,l))
    end do
  end do

  do i = il1,il2
    bvm(i,1) = bvm(i,2)
  end do

  !     get base-level winds and bvf

  do l = 1,ilev - 1
    do i = il1,il2
      fct1 = log(sgj (i,l)/sgbj(i,l))
      fct2 = log(sgbj(i,l)/sgj(i,l + 1))
      fct3 = 1./log(sgj (i,l)/sgj(i,l + 1))
      ub(i,l) =    (um(i,l + 1) * fct1 + um(i,l) * fct2) * fct3
      vb(i,l) =    (vm(i,l + 1) * fct1 + vm(i,l) * fct2) * fct3
      bvb(i,l) = (bvm(i,l + 1) * fct1 + bvm(i,l) * fct2) * fct3
    end do
  end do

  do i = il1,il2
    ub(i,ilev) = ub(i,ilev - 1)
    vb(i,ilev) = vb(i,ilev - 1)
    bvb(i,ilev) = bvb(i,ilev - 1)
  end do

  !     remove diurnal cycle in bvf to be consistent with oro scheme
  do l = 2,ilev
    do i = il1,il2
      ratio = 5. * log(sgj(i,l)/sgj(i,l - 1))
      bvm(i,l) = (bvm(i,l - 1) + ratio * bvm(i,l)) &
                 /(1. + ratio)
    end do
  end do

  do l = 2,ilev
    do i = il1,il2
      ratio = 5. * log(sgbj(i,l)/sgbj(i,l - 1))
      bvb(i,l) = (bvb(i,l - 1) + ratio * bvb(i,l)) &
                 /(1. + ratio)
    end do
  end do

  !     mid and base level density
  do l = 1,ilev
    do il = il1,il2
      rhom(il,l) = pm(il,l)/(rgas * tsg(il,l))
      rhob(il,l) = pb(il,l)/(rgas * tsgb(il,l))
    end do
  end do


  !     set up azimuth directions and some trig factors
  da = 2 * pi/nadim
  az_fct = 1.
  !     get normalization factor to ensure that the same amount of e-p
  !     flux is directed (n,s,e,w) no mater how many azimuths are selected.
  !     note, however, the code below assumes a symmetric distribution of
  !     of azimuthal directions (ie 4,8,16,32,...)
  anorm = 0.
  do ia = 1,nadim
    az = (ia - 1) * da
    unit(1,ia) = cos(az)
    unit(2,ia) = sin(az)
    anorm = anorm + abs(cos(az))
  end do
  az_fct = 2. * az_fct/anorm


  !     define coordinate transformation
  !     note that this is expresed in terms of the intrinsic phase speed
  !     at launch ci=c-u_o so that the transformation is identical at every
  !     launch site.
  do i = 1,ncdim
    tx = float(i - 1) * dtx + xmin
    x(i) = a * exp((tx - xmin)/gam) + b
    ci(i) = 1./x(i)
    dci(i) = ci(i) ** 2 * (a/gam) * exp((tx - xmin)/gam) * dtx
  end do

  !     if constant resolution desired just uncomment the loop below
  ! ccc      dciinc=(cimax-cimin)/(ncdim-1)
  ! ccc      do i=1,ncdim
  ! ccc         ci(i)=float(ncdim-i)*dciinc+cimin
  ! ccc         x(i)=1./ci(i)
  ! ccc         dci(i)=dciinc
  ! ccc      end do

  !     define intrinsic velocity u(z)-u(z_launch), and coefficients
  do ia = 1,nadim
    do il = il1,il2
      ul(il,ia) = unit(1,ia) * ub(il,laun_z) + unit(2,ia) * vb(il,laun_z)
    end do
  end do
  do il = il1,il2
    nl(il) = bvb(il,laun_z)
  end do


  do l = 1,laun_z
    do ia = 1,nadim
      do il = il1,il2
        u = unit(1,ia) * ub(il,l) + unit(2,ia) * vb(il,l)
        ui(il,ia,l) = u - ul(il,ia)
      end do
    end do
  end do

  !     set proportionality constants
  do l = 1,laun_z
    do il = il1,il2
      f = 2.e0 * rot * abs(sin(radj(il)))
      if (pexp /= 1.) then
        rd = 1. - pexp
        bs = rd/(bvb(il,l) ** rd - f ** rd)
      else
        bs = 1./log(bvb(il,l)/f)
      end if
      fct(il,l) = bs * rhob(il,l)/bvb(il,l)
    end do
  end do

  !     initialize integrated e-p flux to zero
  do ia = 1,nadim
    do l = 1,ilev
      do il = il1,il2
        pu(il,l,ia) = 0.e0
        crt(il,l,ia) = 0.e0
        dfl(il,l,ia) = 0.e0
      end do
    end do
  end do

  !     initialize tendencies to zero
  do l = 1,ilev
    do il = il1,il2
      utend(il,l) = 0.e0
      vtend(il,l) = 0.e0
    end do
  end do

  !     zero final ep flux vector and forcing vector
  do l = 1,ilev
    do il = il1,il2
      epf(il,l,1) = 0.d0
      epf(il,l,2) = 0.d0
    end do
  end do



  !     set launch ep flux density
  !     do this for only one azimuth since it is identical to all azimuths
  !     and it will be renormalized
  !     s=1 case
  ! ccc      do ic=1,ncdim
  ! ccc         cin=ci(ic)
  ! ccc         cin4=(ms*cin)**4
  ! ccc         do il=il1,il2
  ! ccc            bv4=nl(il)**4
  ! ccc            denom=(bv4+cin4)
  ! ccc            ep(il,ic,1)=fct(il,laun_z)*bv4*cin/(bv4+cin4)
  ! ccc            act(il,ic,1)=1.e0
  ! ccc         end do
  ! ccc      end do
  !     s=-1 case
  ! ccc      do ic=1,ncdim
  ! ccc         cin=ci(ic)
  ! ccc         cin2=(ms*cin)**2
  ! ccc         do il=il1,il2
  ! ccc            bv2=nl(il)**2
  ! ccc            ep(il,ic,1)=fct(il,laun_z)*bv2*cin/(bv2+cin2)
  ! ccc            act(il,ic,1)=1.e0
  ! ccc         end do
  ! ccc      end do
  !     s=0 case
  do ic = 1,ncdim
    cin = ci(ic)
    cin3 = (ms * cin) ** 3
    do il = il1,il2
      bv3 = nl(il) ** 3
      ep(il,ic,1) = fct(il,laun_z) * bv3 * cin/(bv3 + cin3)
      act(il,ic,1) = 1.e0
      acc(il,ic,1) = 1.e0
    end do
  end do


  !     normalize launch ep flux
  !     first integrate
  do ic = 1,ncdim
    cinc = dci(ic)
    do il = il1,il2
      pu(il,laun_z,1) = pu(il,laun_z,1) + ep(il,ic,1) * cinc
    end do
  end do
  do il = il1,il2
    fnorm(il) = eplaunch/pu(il,laun_z,1)
  end do
  do ia = 1,nadim
    do il = il1,il2
      pu(il,laun_z,ia) = eplaunch
    end do
  end do

  !     adjust proportionality constant fct used to normalize saturation
  !     bound
  do l = 1,laun_z
    do il = il1,il2
      fct(il,l) = fnorm(il) * fct(il,l)
    end do
  end do

  !     renormalize each spectral element in one azimuth
  do ic = 1,ncdim
    do il = il1,il2
      ep(il,ic,1) = fnorm(il) * ep(il,ic,1)
    end do
  end do

  !     copy results into all other azimuths
  !     act - has a value of 1 or 0 used to ientify active spectral
  !           elements (ie those that have not been removed by critical
  !           levels
  do ia = 2,nadim
    do ic = 1,ncdim
      do il = il1,il2
        ep(il,ic,ia) = ep(il,ic,1)
        act(il,ic,ia) = 1.e0
        acc(il,ic,ia) = 1.e0
      end do
    end do
  end do

  !     begin main loop over levels

  do l = laun_z - 1,1, - 1

    do ia = 1,nadim

      !     first do critical levels
      do il = il1,il2
        ci_min(il,ia) = max(ci_min(il,ia),ui(il,ia,l))
      end do


      !     set act to zero if critical level encountered
      do ic = 1,ncdim
        cin = ci(ic)
        do il = il1,il2
          atmp = 0.5e0 + sign(p5,cin - ci_min(il,ia))
          acc(il,ic,ia) = act(il,ic,ia) - atmp
          act(il,ic,ia) = atmp
        end do
      end do

      !     integrate to get critical-level contribution to mom deposition on this level
      do ic = 1,ncdim
        cinc = dci(ic)
        do il = il1,il2
          dfl(il,l,ia) = dfl(il,l,ia) + &
                         acc(il,ic,ia) * ep(il,ic,ia) * cinc
        end do
      end do

      !     get weighted average of phase speed in layer
      do il = il1,il2
        if (dfl(il,l,ia) > 0.) then
          do ic = 1,ncdim
            crt(il,l,ia) = crt(il,l,ia) + ci(ic) * &
                           acc(il,ic,ia) * ep(il,ic,ia) * dci(ic)
          end do
          crt(il,l,ia) = crt(il,l,ia)/dfl(il,l,ia)
        else
          crt(il,l,ia) = crt(il,l + 1,ia)
        end if
      end do


      !     do saturation
      !     slow for ipreal
      if (ipreal /= 1) then
        rd = 2. - pexp
        do ic = 1,ncdim
          cin = ci(ic)
          do il = il1,il2
            f0 = abs(cin - ui(il,ia,l))
            eps = fct(il,l) * f0 * (f0/cin) ** rd
            dep = act(il,ic,ia) * (ep(il,ic,ia) - eps)
            if (dep > 0.e0) ep(il,ic,ia) = eps
          end do
        end do
      else if (ptwo == 3.e0) then
        do ic = 1,ncdim
          cin = ci(ic)
          do il = il1,il2
            f0 = cin - ui(il,ia,l)
            f1 = fct(il,l) * f0
            epsq = f1 * f1 * f0/cin
            dep = act(il,ic,ia) * (ep(il,ic,ia) ** 2 - epsq)
            if (dep > 0.e0) ep(il,ic,ia) = sqrt(epsq)
          end do
        end do
      else if (ptwo == 2.e0) then
        do ic = 1,ncdim
          cin = ci(ic)
          do il = il1,il2
            eps = fct(il,l) * (cin - ui(il,ia,l)) ** 2/cin
            dep = act(il,ic,ia) * (ep(il,ic,ia) - eps)
            if (dep > 0.e0) ep(il,ic,ia) = eps
          end do
        end do
      end if



      !     integrate spectrum
      do ic = 1,ncdim
        cinc = dci(ic)
        do il = il1,il2
          pu(il,l,ia) = pu(il,l,ia) + &
                        act(il,ic,ia) * ep(il,ic,ia) * cinc
        end do
      end do

    end do
  end do

  !     make correction for critical-level momentum deposition
  do ia = 1,nadim
    do il = il1,il2
      cng = 0.e0
      ulon = ul(il,ia)
      do l = 2,laun_z
        ulm = unit(1,ia) * um(il,l) + unit(2,ia) * vm(il,l) - ulon
        dfl(il,l - 1,ia) = dfl(il,l - 1,ia) + cng
        dft = min(dfl(il,l - 1,ia),(zb(il,l - 1) - zb(il,l)) * rhom(il,l) * &
              (crt(il,l - 1,ia) - ulm) * twodtinv)
        dft = max(dft,zero)
        cng = (dfl(il,l - 1,ia) - dft)
      end do
    end do
  end do

  !     sum contribution for total zonal and meridional flux
  do ia = 1,nadim
    do l = laun_z,1, - 1
      do il = il1,il2
        epf(il,l,1) = epf(il,l,1) + (pu(il,l,ia)) &
                      * az_fct * unit(1,ia)
        epf(il,l,2) = epf(il,l,2) + (pu(il,l,ia)) &
                      * az_fct * unit(2,ia)
      end do
    end do
  end do

  !     perform vertical difference of flux to get forcing tendency
  do l = 2,laun_z
    do il = il1,il2
      s1 = zb(il,l - 1) - zb(il,l)
      afct = - 1.e0/(rhom(il,l) * s1)
      utend(il,l) = afct * (epf(il,l - 1,1) - epf(il,l,1))
      vtend(il,l) = afct * (epf(il,l - 1,2) - epf(il,l,2))
    end do
  end do

  imcons = 1
  if (imcons == 1) then
    !     deposit all momentum in top layer
    l = 1
    do il = il1,il2
      s1 = zb(il,1) - zb(il,2)
      afct = - 1.e0/(rhom(il,l) * s1)
      afdp = rhom(il,l)
      utend(il,l) = - afct * (epf(il,l,1))
      vtend(il,l) = - afct * (epf(il,l,2))
    end do
  else
    !     let mom escape out the top of model
    do il = il1,il2
      utend(il,1) = 0.e0
      vtend(il,1) = 0.e0
    end do
  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
