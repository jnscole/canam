!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oxistr3(il1,     il2,    ilev,    ilg,   ntrac, &
                   neqp,    iso2,    iso4,   ihpo,    issa, &
                   issc,   itrac, &
                   roarow,  nh3row,  nh4row, hno3row,   zzo3, &
                   ogtso2,  agtso4,   agto3, ogtco2,  agtho2, &
                   ogthno3,  ogtnh3,   achpa, antso2,  antho2, &
                   antso4,    xrow,  aoh2o2, aresid,   zmlwc, &
                   zclf,   kcalc,      th,     qv,     shj, &
                   pressg,     thg,    rgas,   eps1,   dqldt, &
                   ztmst,  zxtp1c,    zso4,   af1v,    af2v, &
                   amhv,  itrphs,   ilwcp,  ichem, &
                   slo3row, slhprow,  phlrow, saverad, &
                   lev, isvchem,    grav,    dshj, &
                   wrk1,    wrk2,    wrk3, zhenry)
  !-------------------------------------------------------------------------
  !     * apr 25/10 -  k. vonsalzen/     new version for gcm15i:
  !     *              m. lazare.        - diagnostic array clphrow removed
  !     *                                  and calculation of diagnostic
  !     *                                  array phlrow revised (used to
  !     *                                  be based on clphrow), under
  !     *                                  control of "isvchem".
  !     * nov 23/06 -  m. lazare.        code revision (including promoted
  !     *                                scalars) to run accurately in 32-bit.
  !     *                                this also includes a revised call
  !     *                                to a new oequip2.
  !     * jun 20/06 -  m. lazare.        previous version oxistr2 for gcm15f/g/h:
  !     *                                - use variable instead of constant
  !     *                                  in intrinsics such as "max",
  !     *                                  so that can compile in 32-bit mode
  !     *                                  with real(8).
  !     * dec 20/02 -  k. vonsalzen.     previous final version oxistr for
  !     *                                gcm15b/c/d/e:
  !     *                                - remove "itrind" common block
  !     *                                  and pass indices instead.
  !     *                                - bugfix to diagnostic calculations
  !     *                                  involving wrk2.
  !     *                                - include sea-salt in molarity.
  !     * jun 18/02 -  k. vonsalzen.     bugfixes to diagnostic calculations
  !     *                                of slo3row,shhprow involving
  !     *                                added work arrays: wrk1,wrk2,wrk3.
  !     * mar 20/02 -  m.lazare.         buxfixes to: a. pass lev and use
  !     *                                properly xrow(l+1) instead of xrow(l).
  !     *                                b. save amh,af1,af2
  !     *                                into new work vectors to be
  !     *                                passed to loop 355 for diagnostic
  !     *                                calculations.
  !     * oct 17/01 -  k. von salzen.    revised for gcm15
  !     * apr 29/98 -  k. von salzen.    new subroutine
  !
  !     * calculates in-cloud chemical processes for sulphur species
  !     * in stratiform clouds (i.e. mixing processes are not considered).
  !
  !-----------------------------------------------------------------------
  implicit none

  real :: adeltax
  real :: adeltx
  real :: afact
  real :: afras
  real :: agtco2
  real :: agthno3
  real :: agtnh3
  real :: agtso2
  real :: alwcvmr
  real :: amssani
  real :: asrso2
  real :: asum
  real :: atempo
  real :: atimst
  real, intent(in) :: eps1
  real, intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer, intent(in) :: ihpo  !< Tracer index for hydrogen peroxide \f$[unitless]\f$
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: ilgg
  integer :: indti
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: iso4
  integer, intent(in) :: issa  !< Tracer index for sea salt (accumulation mode) \f$[unitless]\f$
  integer, intent(in) :: issc  !< Tracer index for sea salt (coarse mode) \f$[unitless]\f$
  integer, intent(in) :: isvchem  !< Switch to save extra chemistry diagnostics (0 = don't save, 1 = save) \f$[unitless]\f$
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: jno
  integer :: jyes
  integer :: l
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: neqp
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real :: qmr
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real :: rgasa
  real, intent(in) :: saverad  !< Reciprocal of timestep interval to save radiative transfer output \f$[unitless]\f$
  real :: ycom3l
  real :: ymass
  real :: ymolga
  real :: yrhow
  real :: zero
  real, intent(in) :: ztmst
  !
  integer :: ysub
  !
  parameter (ysub   = 2)
  parameter (ycom3l = 1.e+03)
  parameter (ymolga = 28.84)
  parameter (yrhow  = 1.e+03)
  parameter (ymass  = 32.06e-03)
  !
  real, intent(in), dimension(ilg,ilev) :: zclf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,lev,ntrac) :: xrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zmlwc !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: aresid !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: ogtso2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: agtso4 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: agto3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: ogtco2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zzo3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: agtho2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: ogthno3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: ogtnh3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: antso2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: antho2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: antso4 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: hno3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: nh3row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: nh4row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: roarow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: aoh2o2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qv !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dqldt !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: zxtp1c !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: zso4 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(inout), dimension(ilg,ilev) :: thg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: wrk1 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: wrk2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: wrk3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: zhenry !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: af1v !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: af2v !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: amhv !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,neqp) :: achpa !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(ilg) :: ilwcp   !< Tracer index for cloud liquid water \f$[unitless]\f$
  integer, intent(inout), dimension(ilg) :: ichem !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itrphs !< Variable description\f$[units]\f$
  logical, intent(inout), dimension(ilg,ilev) :: kcalc !< Variable description\f$[units]\f$
  logical :: kchem !< Variable description\f$[units]\f$
  !
  !     * i/o fields.
  !
  real, intent(inout), dimension(ilg) :: slo3row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: slhprow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: phlrow !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !
  !     * scalar promotion.
  !
  real*8 :: amso3
  real*8 :: amnh4
  real*8 :: amno3
  real*8 :: amso4
  real*8 :: assmr
  real*8 :: aconvf
  real*8 :: amssso4
  real*8 :: amsscl
  real*8 :: amssant
  real*8 :: amsscat
  real*8 :: amcl
  real*8 :: amhcl
  real*8 :: agthcl
  real*8 :: adelta
  real*8 :: afoh
  real*8 :: amh
  real*8 :: atval
  real*8 :: afnh4
  real*8 :: afno3
  real*8 :: afcl
  real*8 :: afhso
  real*8 :: afco3
  real*8 :: atvo3
  real*8 :: af1
  real*8 :: afrah
  real*8 :: af2
  real*8 :: apara
  real*8 :: aparb
  real*8 :: aparc
  real*8 :: apard
  real*8 :: atvalx
  real*8 :: amhmax
  real*8 :: amhmin
  !
  data zero,amhmin,amhmax /0., 1.e-10, 1.e-01/
  !-----------------------------------------------------------------------
  !
  !     * initializations.
  !
  atimst = ztmst/real(ysub)
  do il = il1,il2
    ilwcp(il) = 0
  end do ! loop 3
  do l = 1,ilev
    do il = il1,il2
      kcalc(il,l)  = .false.
      aresid(il,l) = 0.
      wrk1(il,l)   = 0.
      wrk2(il,l)   = 0.
      wrk3(il,l)   = 0.
    end do
  end do ! loop 5
  if (itrac /= 0) then
    kchem =   itrphs(iso2) > 0 .and. itrphs(iso4) > 0
  end if
  !
  !     * test if sufficient liquid water is available.
  !
  do l = 1,ilev
    do il = il1,il2
      if (zmlwc(il,l) > 0.1e-06 &
          .and. zclf(il,l) > 0.                  ) then
        kcalc(il,l) = kchem
        ilwcp(il) = 1
        qmr = qv(il,l)/(1. - qv(il,l))
        rgasa = rgas * (1. + qmr/eps1)/(1. + qmr)
        roarow(il,l) = shj(il,l) * pressg(il)/(rgasa * th(il,l))
      end if
    end do
  end do ! loop 10
  !
  !     * determine grid points at which the situation permits
  !     * chemical calculations.
  !
  jyes = 0
  jno = il2 - il1 + 2
  do il = il1,il2
    if (ilwcp(il) == 1) then
      jyes        = jyes + 1
      ichem(jyes) = il
    else
      jno         = jno - 1
      ichem(jno) = il
    end if
  end do ! loop 20
  ilgg = jyes
  !
  !     * henry's law constants.
  !
  do l = 1,ilev
    do il = 1,ilgg
      thg(il,l) = th(ichem(il),l)
    end do
  end do ! loop 30
  !
  call oequip2(ilg,ilev,neqp,1,1,ilgg,achpa,thg)
  !
  !     * unit conversions.
  !
  if (kchem) then
    do l = 1,ilev
      do il = 1,ilgg
        if (kcalc(ichem(il),l) ) then
          !
          !              sulphur dioxide. conversion kg-s/kg -> mol/l.
          ogtso2(il,l) = zxtp1c(ichem(il),l) * roarow(ichem(il),l) &
                         /(ymass * ycom3l)
          !
          !              sulphate. conversion kg-s/kg -> mol/l.
          agtso4(il,l) = zso4(ichem(il),l) * roarow(ichem(il),l) &
                         /(ymass * ycom3l)
          !
          !              ozone in mol/l.
          agto3(il,l) = zzo3(ichem(il),l) * 1.e+06 &
                        /(6.022045e+23 * ycom3l)
          !
          !              hydrogen peroxide. conversion kg-s/kg -> mol/l.
          agtho2(il,l) = xrow(ichem(il),l + 1,ihpo) * roarow(ichem(il),l) &
                         /(ymass * ycom3l)
          aoh2o2(il,l) = agtho2(il,l)
          !
          !              nitric acid. conversion vmr -> mol/l.
          ogthno3(il,l) = hno3row(ichem(il),l) * roarow(ichem(il),l) &
                          /ymolga
          !
          !              ammonia. conversion vmr -> mol/l.
          ogtnh3(il,l) = (nh3row(ichem(il),l) + nh4row(ichem(il),l)) &
                         * roarow(ichem(il),l)/ymolga
          !
          !              carbon dioxide. conversion vmr -> mol/l.
          ogtco2(il,l) = 370.e-06 * roarow(ichem(il),l)/ymolga
        end if
      end do
    end do ! loop 100
  end if
  !
  !     * s(iv) oxidation. chemical sources/sinks are considered
  !     * for sulphur dioxide, hydrogen peroxide, ozone. they
  !     * are only applied if all of these three tracers plus
  !     * sulphate are available. the corresponding sulphate
  !     * production rate is given by af1*[o3]*[so2]+af2*[h2o2]*[so2]
  !     * (in kg-sulphur/kg-air/sec), where the concentrations
  !     * refer to the total (gas phase + dissolved) species (in
  !     * kg-sulphur/kg-air). the internal time step in these
  !     * calculations is given by: time_step(model)/ysub.
  !
  do indti = 1, ysub
    !
    !      *** initialization.
    !
    do l = 1,ilev
      do il = 1,ilgg
        if (kcalc(ichem(il),l) ) then
          antso2(il,l) = ogtso2(il,l)
          antho2(il,l) = agtho2(il,l)
          antso4(il,l) = agtso4(il,l)
        end if
      end do
    end do ! loop 310
    !
    !      *** chemical fields.
    !
    do l = 1,ilev
      do il = 1,ilgg
        if (kcalc(ichem(il),l) ) then
          !
          !             liquid water volume mixing ratio.
          !
          !             atempo  = max(zmlwc(il,l)-dqldt(il,l),zero)
          atempo  = max(zmlwc(ichem(il),l),zero)
          alwcvmr = atempo * roarow(ichem(il),l)/yrhow
          !
          !             total (aq. + gas phase) concentrations.
          !
          agtso2  = ogtso2(il,l)
          agtco2  = ogtco2(il,l)
          agthno3 = ogthno3(il,l)
          agtnh3  = ogtnh3(il,l)
          !
          !             molarities.
          !
          amso3 = 0.
          amnh4 = agtnh3 /alwcvmr
          amno3 = agthno3/alwcvmr
          amso4 = agtso4(il,l)/alwcvmr
          !
          !             sea salt species.
          !
          assmr = xrow(ichem(il),l + 1,issa) + xrow(ichem(il),l + 1,issc)
          aconvf = roarow(ichem(il),l)/(alwcvmr * ycom3l)
          !
          !             calculate molarities of sea salt species assuming a typical
          !             composition of sea water. no anions other than sulphate and
          !             chloride are considered. all cations, except h+, are lumped
          !             together. the ph of the sea water is assumed to be 8.2.
          !             h+ is accounted for in the ion balance and in the calculation
          !             of hydrogen chlorine in the gas phase.
          !
          amssso4 = 0.077 * aconvf/96.058e-03 * assmr
          amsscl = 0.552 * aconvf/35.453e-03 * assmr
          amssani = 0.
          amsscat = (amsscl + 2. * amssso4 + amssani) * (1. - 1.048e-08)
          amso4 = amso4 + amssso4
          amcl = amsscl
          amhcl = amsscl * 6.31e-09/achpa(il,l,10)
          agthcl = (amsscl + amhcl) * alwcvmr
          !              agthcl=amsscl*alwcvmr
          !
          !             initial guess h+ molarity from initial ion
          !             molarities.
          !
          adelta = amno3 + 2. * (amso4 + amso3) + amcl + amssani - amnh4 - amsscat
          afoh = 1.e-14
          amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * (afoh &
                + achpa(il,l,2) * agtso2 + achpa(il,l,5) * agtco2)) )
          amh = max(min(amh,amhmax),amhmin)
          !
          !             equilibrium parameters for soluble species.
          !
          atval = achpa(il,l,1) + achpa(il,l,2)/amh &
                  + achpa(il,l,3)/amh ** 2
          afras = 1./( 1. + alwcvmr * atval)
          afnh4 = 1./( 1. + agtnh3/(1./achpa(il,l,9) &
                  + alwcvmr * amh) )
          afno3 = agthno3/( 1./achpa(il,l,8) + alwcvmr/amh)
          !
          !             first iteration h+ molarity calculation.
          !
          afcl = agthcl/( 1./achpa(il,l,10) + alwcvmr/amh)
          afhso = agtso2 * afras * achpa(il,l,2)
          afco3 = agtco2 * achpa(il,l,5)
          amso3 = ( agtso2 * afras * achpa(il,l,3) )/amh ** 2
          adelta = afnh4 * ( 2. * (amso4 + amso3) + amssani - amsscat)
          amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * afnh4 * (afoh &
                + afco3 + afhso + afno3 + afcl)) )
          amh = max(min(amh,amhmax),amhmin)
          atval = achpa(il,l,1) + achpa(il,l,2)/amh &
                  + achpa(il,l,3)/amh ** 2
          afras = 1./( 1. + alwcvmr * atval)
          afnh4 = 1./( 1. + agtnh3/(1./achpa(il,l,9) &
                  + alwcvmr * amh) )
          afno3 = agthno3/( 1./achpa(il,l,8) + alwcvmr/amh)
          !
          !             second iteration h+ molarity calculation.
          !
          afcl = agthcl/( 1./achpa(il,l,10) + alwcvmr/amh)
          afhso = agtso2 * afras * achpa(il,l,2)
          afco3 = agtco2 * achpa(il,l,5)
          amso3 = ( agtso2 * afras * achpa(il,l,3) )/amh ** 2
          adelta = afnh4 * ( 2. * (amso4 + amso3) + amssani - amsscat)
          amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * afnh4 * (afoh &
                + afco3 + afhso + afno3 + afcl)) )
          amh = max(min(amh,amhmax),amhmin)
          atval = achpa(il,l,1) + achpa(il,l,2)/amh &
                  + achpa(il,l,3)/amh ** 2
          afras = 1./( 1. + alwcvmr * atval)
          !
          !             ozone oxidation parameter (in 1/sec).
          !
          atvo3 = 1./( 1. + alwcvmr * achpa(il,l,7) )
          af1 = ( achpa(il,l,11) + achpa(il,l,12)/amh) * afras &
                * atval * atvo3 * achpa(il,l,7) * alwcvmr
          af1 = af1 * agto3(il,l)
          !
          !             hydrogen peroxide oxidation parameter
          !             (in litre-air/mol/sec).
          !
          afrah = 1./( 1. + alwcvmr * achpa(il,l,6) )
          af2 = ( achpa(il,l,13)/(0.1 + amh) ) * afras &
                * achpa(il,l,1) * afrah * achpa(il,l,6) * alwcvmr
          !
          !             semi-implicit integration.
          apard = agtho2(il,l)
          apara = ( 1. + atimst * af1) * atimst * af2
          aparc = - agtso2
          aparb = 1. + atimst * ( af1 + af2 * (apard + aparc) )
          atvalx = - aparb/( 2. * apara)
          antso2(il,l) = atvalx + sqrt( atvalx ** 2 - aparc/apara)
          antso2(il,l) = max(antso2(il,l),zero)
          antso2(il,l) = min(antso2(il,l),agtso2)
          antho2(il,l) = apard/(1. + atimst * af2 * antso2(il,l))
          antho2(il,l) = max(antho2(il,l),zero)
          antho2(il,l) = min(antho2(il,l),agtho2(il,l))
          adelta = agtso2 - antso2(il,l)
          antso4(il,l) = agtso4(il,l) + adelta
          antso4(il,l) = max(antso4(il,l),zero)
          wrk1(il,l)   = wrk1(il,l) + af1/real(ysub)
          wrk2(il,l)   = wrk2(il,l) + af2 * agtho2(il,l)/real(ysub)
          amhv(il)     = amh
          !
          !           *** scavenging ratios, i.e. fraction of tracer that
          !           *** is dissolved. only for so2 so far.
          !
          asrso2 = max( 1. - afras, zero)
          zhenry(il,l) = zhenry(il,l) + asrso2/real(ysub)
        end if
      end do ! loop 350
      !
      !         * diagnostic fields, saved optionally.
      !
      if (isvchem > 0) then
        do il = 1,ilgg
          if (kcalc(ichem(il),l) ) then
            if (amhv(il) > 1.e-20) then
              phlrow(ichem(il),l) = - log10(amhv(il))
            else
              phlrow(ichem(il),l) = 0.
            end if
          end if
        end do ! loop 355
      end if
    end do ! loop 360
    !
    !        * update fields.
    !
    do l = 1,ilev
      do il = 1,ilgg
        if (kcalc(ichem(il),l) ) then
          adelta = ogtso2(il,l) - antso2(il,l)
          aresid(ichem(il),l) = aresid(ichem(il),l) + adelta
          !
          !---           final concentrations after atimst
          ogtso2(il,l) = antso2(il,l)
          agtho2(il,l) = antho2(il,l)
          agtso4(il,l) = antso4(il,l)
        end if
      end do
    end do ! loop 380
  end do ! loop 400
  !
  !     * final results.
  !
  do l = 1,ilev
    do il = il1,il2
      if (kcalc(il,l) ) then
        !
        !          *** sulphate fraction produced by h2o2.
        !
        adeltax = aresid(il,l) * ycom3l * ymass/roarow(il,l)
        adeltax = max(zero,adeltax)
        adeltax = min(zxtp1c(il,l),adeltax)
        wrk3(il,l) = adeltax
        !
        !         *** sulphur dioxide in kg/kg.
        !
        zxtp1c(il,l) = zxtp1c(il,l) - adeltax
        !
        !         *** sulphate in kg/kg.
        !
        zso4(il,l) = zso4(il,l) + adeltax
      end if
    end do
  end do ! loop 500
  !
  !     * diagnostics of production rates.
  !
  if (isvchem > 0) then
    do l = 1,ilev
      do il = 1,ilgg
        if (kcalc(ichem(il),l) ) then
          afact = wrk3(ichem(il),l) * pressg(ichem(il)) * zclf(ichem(il),l) &
                  * dshj(ichem(il),l)/grav/ztmst
          asum = wrk1(il,l) + wrk2(il,l)
          if (asum /= 0. ) then
            slo3row(ichem(il)) = slo3row(ichem(il)) + wrk1(il,l) * afact &
                                 * saverad/asum
            slhprow(ichem(il)) = slhprow(ichem(il)) + wrk2(il,l) * afact &
                                 * saverad/asum
          end if
        end if
      end do
    end do ! loop 520
  end if
  !
  do l = 1,ilev
    do il = 1,ilgg
      if (kcalc(ichem(il),l) ) then
        adeltx = zclf(ichem(il),l) * (agtho2(il,l) - aoh2o2(il,l)) &
                 * ycom3l * ymass/roarow(ichem(il),l)
        !
        !         *** hydrogen peroxide.
        !
        adeltax = zclf(ichem(il),l) * (agtho2(il,l) - aoh2o2(il,l)) &
                  * ycom3l * ymass/roarow(ichem(il),l)
        xrow(ichem(il),l + 1,ihpo) = xrow(ichem(il),l + 1,ihpo) + adeltax
      end if
    end do
  end do ! loop 550

  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
