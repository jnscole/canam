!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xtemiss10 &
    (xemis,xrow,ilg,il1,il2,ilev,lev,ntrac,ztmst,kount, &
    jl,lnz0row,fcanrow,icanp1,th, &
    gtrowbs,ustarbs,gtroww,pressg,dshj,dmsorow,edmsrow, &
    shj,sicnrow,edsorow,edslrow,zspdso,zspdsbs, &
    flndrow,fwatrow,fnrol, &
    spotrow,st01row, &
    st02row,st03row,st04row,st06row, &
    st13row,st14row,st15row,st16row, &
    st17row,suz0row,pdsfrow,isvdust, &
    fallrow,fa10row,fa2row,fa1row, &
    duwdrow,dustrow,duthrow,usmkrow, &
    clayrow,sandrow,orgmrow,bsfrac,smfrac, &
    esdrow,ignd,ipam,sicn_crt, &
    issa,issc,idua,iduc,idms,iso2,saverad,isvchem)
  !
  !     * this routine calculates the oceanic surface dms emission
  !     * (from specified concentrations), the surface dust emissions
  !     * (accumulation and coarse modes) and the volcanic dry
  !     * dry deposition flux.
  !
  !     * jun 11/2018 - k.vonsalzen.  - increase cuscale from 1.0 to
  !     *                               1.6 for cmip6.
  !     * aug 09/2017 - m.lazare.     - remove unused "maskrow".
  !     * aug 07/2017 - m.lazare.     - new git version.
  !     * sep 13/2014 - k.vonsalzen/  new version for gcm18+:
  !     *               m.lazare.     - ustarbs passed in and used to
  !     *                               define ustar in dustemi3, instead
  !     *                               of previous calculation.
  !     *                             - unused snorow and cdm removed.
  !     *                             - zspdsbs (surface bare soil averaged
  !     *                               windspeed) is passed in and used
  !     *                               (including passing in to dustemi3)
  !     *                               rather than the grid-average surface
  !     *                               wind components and gustiness separately.
  !     *                             - zspdso (surface-level wind including
  !     *                               gustiness, over oceans) is passed in and
  !     *                               used instead of the grid-average lowest-
  !     *                               level wind "zspd".
  !     *                             - ictemmod removed since no longer
  !     *                               needed to distinguish between
  !     *                               surface flux for class vs ctem.
  !     *                             - flndrow and fwatrow are passed in
  !     *                               instead of {gcrow} and used
  !     *                               to determine separate
  !     *                               emissions over land/noland,
  !     *                               as follows:
  !     *                             - afac is scaled by
  !     *                               fwatrow(il) in loop 10 and
  !     *                               if condition on gc,mask modified to
  !     *                               use flndrow instead.
  !     *                             - zdmscon is scaled by fwatrow(il)
  !     *                               in loop 20 and if condition on gc,mask
  !     *                               modified to use flndrow instead.
  !     *                             - edmsrow is scaled by flndrow(il) in
  !     *                               loop 30 for dms emissions from land
  !     *                               sources and if condition modified.
  !     *                             - separate gtrow for land and water
  !     *                               (gtrowbs and gtroww, respectively)
  !     *                               are passed in rather than one
  !     *                               gtrow. gtrowbs is passed on to
  !     *                               the new dustemi3 while gtroww
  !     *                               is used in the seasalt emissions.
  !     *                             - a new version of the dust
  !     *                               emissions is called (dustemi3)
  !     *                               with {gcrow,maskrow} replaced
  !     *                               by flndrow.
  !     *                             - sicn_crt is used to determine
  !     *                               open water for seasalt emissions.
  !     * nov 15/2013 - k.vonsalzen/  cuscale value now dependant on
  !     *               m.lazare.     ctem switch "ictemmod" (higher
  !     *                             tuned value for ctem).
  !     * jun 18/2013 - k.vonsalzen/  previous version xtemiss9 for gcm17+:
  !     *               m.lazare.     - passes in "ipam" and skips around
  !     *                               seasalt and dust emissions (**not**
  !     *                               dms emissions) for pla.
  !     *                             - uses "duparm1" with "cuscale"
  !     *                               removed (defined in this routine now).
  !     *                             - calls new "duinit1", passin in
  !     *                               "cuscale".
  !     *                             - calls new "dustemi2", passing in
  !     *                               "cuscale".
  !     *                             - more accurate schmidt number calculation.
  !     * apr 24/2012 - y.peng/       previous version xtemiss8 for gcm16:
  !     *               k.vonsalzen.  - new dust scheme.
  !     *                               this includes new "duparm" module
  !     *                               and new subroutines "duinit" and
  !     *                               dustemi, which are called.
  !     * apr 26/2010 - k.vonsalzen/  previous version xtemiss7 for gcm15i:
  !     *               m.lazare.     - dafx,dafc replaced by esd (both modes).
  !     *                             - bsfrac used for dust production,
  !     *                               instead of equivilent (1.-vgfrac).
  !     *                             - emissions now stored into general
  !     *                               "emis" array, instead of updating
  !     *                               xrow at this point.
  !     *                             - a different upliftc is used for
  !     *                               ctem (based on tuning), with "ictemmod"
  !     *                               passed in accordingly.
  !     *                             - virtual temperature effect is added
  !     *                               to calculate moist air density,
  !     *                               rather than just using dry air density
  !     *                               for "zrho0".
  !     * feb 16/2008 - k.vonsalzen.  previous version xtemiss6 for gcm15h:
  !     *                             - add dms dust source over land.
  !     * jan 17/2008 - k.vonsalzen/  previous version xtemiss5 for gcm15g:
  !     *               m.lazare/     - add gustiness effect for all species
  !     *               x.ma.           consistently through passing in and
  !     *                               using "zspd" which has gustiness
  !     *                               incorporated into it in physics driver.
  !     *                             - remove calculation of removed tsem.
  !     *                             - pass in "adelt" from physics as
  !     *                               ztmst and use directly, rather than
  !     *                               passing in delt and forming
  !     *                               ztmst=2.*delt inside.
  !     *                             - remove updating of so2 from old
  !     *                               volcanoes formulation and remove
  !     *                               passing in of now removed evol,hvol.
  !     *                             - remove calculation of unused "tsem"
  !     *                               and tdem->edso.
  !     * jun 19/2006 - m.lazare.     previous version xtemiss4 for gcm15f:
  !     *                             - use variable instead of constant
  !     *                               in intrinsics such as "max",
  !     *                               so that can compile in 32-bit mode
  !     *                               with real(8).
  !     *                             - "keeptim" common block removed (wasn't
  !     *                               used).
  !     *                             - work arrays now local.
  !     * dec 12/2005 - c.reader/     previous version xtemiss3 for gcm15d/e:
  !     *               m.lazare.     - drag partition formulation for spedt.
  !     *                             - gustiness parameterization added for dust.
  !     *                             - suppression of dust over frozen ground.
  !     *                             - limit zsst to min(zsst,36.) so that
  !     *                               schmidt number is positive and thus
  !     *                               dms flux formulation is not ill-behaved.
  !     * dec 07/2004 - c.reader/     previous version xtemiss2 for gcm15c:
  !     *               m.lazare.     - volcanic emissions nowconsistent
  !     *                               with specified "hvol" field being
  !     *                               above ground and not sea-level
  !     *                               (and volcanic eruptions permitted
  !     *                               in lowest layer).
  !     *                             - many changes to dust emissions
  !     *                               (new upliftc, pass in cdm and use
  !     *                               to calculate ustar,wlt1 and spedt
  !     *                               now calculated rather than
  !     *                               specified data constants,wlt2 and
  !     *                               snowt no longer used and thus
  !     *                               removed as well, plfx0=0. for
  !     *                               ustar<=spedt).
  !     * oct 24/2002 - k.vonsalzen/- previous version xtemiss for gcm15b
  !     *               m.lazare/     (modified from lohman original).
  !     *               c.reader.
  !     * mike lazare,ulrike lohman  cccma,dalhousie   08/2000
  !     * johann feichter          uni/hamburg         08/91
  !     * modified  u. schlese    dkrz-hamburg        jan-95
  !
  use duparm1
  implicit none
  real :: afac
  real :: ai
  real :: alpha
  real :: amwr
  real :: asq
  real :: assa
  real :: assc
  real :: atune
  real :: aw
  real :: bi
  real :: bw
  real :: cpres
  real :: cpresv
  real :: cuscale
  real :: dztr
  real :: edmslnd
  real :: fact
  real :: gamma
  real :: grav
  integer :: icanp1
  integer :: idms
  integer :: idua
  integer :: iduc
  integer :: ignd
  integer :: il
  integer :: il1
  integer :: il2
  integer :: ilev
  integer :: ilg
  integer :: ipam
  integer :: iso2
  integer :: issa
  integer :: issc
  integer :: isvchem
  integer :: isvdust
  integer :: kount
  integer :: lev
  integer :: ntrac
  real :: one
  real :: opem4
  real :: rayon
  real :: rgas
  real :: rgasv
  real :: rgoasq
  real :: rgocp
  real :: saverad
  real :: sicn_crt
  real :: slp
  real :: sstmax
  real :: sstmin
  real :: t1s
  real :: t2s
  real :: tw
  real :: wlt1max
  real :: wlt1min
  real :: ww
  real :: zdmscon
  real :: zdmsemiss
  real :: zka
  real :: zkw
  real :: zrho0
  real :: zschmidt
  real :: zspdgust
  real :: zsst
  real :: ztmst
  real :: zvdms
  !
  !     * i/o fields.
  !
  real, dimension(ilg) :: gtrowbs !< Variable description\f$[units]\f$
  real, dimension(ilg) :: ustarbs !< Variable description\f$[units]\f$
  real, dimension(ilg) :: gtroww !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev) :: dshj !< Variable description\f$[units]\f$
  real, dimension(ilg) :: pressg !< Variable description\f$[units]\f$
  real, dimension(ilg) :: dmsorow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: edmsrow !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  real, dimension(ilg,lev,ntrac) :: xrow !< Variable description\f$[units]\f$
  real, dimension(ilg,icanp1) :: lnz0row !< Variable description\f$[units]\f$
  real, dimension(ilg,icanp1) :: fcanrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: zspdso !< Variable description\f$[units]\f$
  real, dimension(ilg) :: zspdsbs !< Variable description\f$[units]\f$
  real, dimension(ilg) :: flndrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: fwatrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: sicnrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: edsorow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: edslrow !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev) :: shj !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, dimension(ilg,ignd) :: clayrow !< Variable description\f$[units]\f$
  real, dimension(ilg,ignd) :: sandrow !< Variable description\f$[units]\f$
  real, dimension(ilg,ignd) :: orgmrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: bsfrac !< Variable description\f$[units]\f$
  real, dimension(ilg) :: smfrac !< Variable description\f$[units]\f$
  real, dimension(ilg) :: esdrow !< Variable description\f$[units]\f$
  real :: ssfrac1 !< Variable description\f$[units]\f$
  real :: ssfrac2 !< Variable description\f$[units]\f$
  real :: wlt1 !< Variable description\f$[units]\f$
  real :: pflx1 !< Variable description\f$[units]\f$
  real :: pflx2 !< Variable description\f$[units]\f$
  real :: spedt !< Variable description\f$[units]\f$
  real :: upliftc !< Variable description\f$[units]\f$
  real, dimension(ilg) :: fallrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: fa10row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: fa2row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: fa1row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: duwdrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: dustrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: duthrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: usmkrow !< Variable description\f$[units]\f$
  !
  real, dimension(ilg) :: sfcurow
  real, dimension(ilg) :: sfcvrow
  real, dimension(ilg) :: gustrol
  real, dimension(ilg) :: fnrol !< Variable description\f$[units]\f$
  real, dimension(ilg) :: suz0row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: pdsfrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: spotrow !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st01row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st02row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st03row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st04row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st06row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st13row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st14row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st15row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st16row !< Variable description\f$[units]\f$
  real, dimension(ilg) :: st17row !< Variable description\f$[units]\f$

  integer, dimension(ilg) :: jl !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work fields.
  !
  real, dimension(nclass) :: uth
  real, dimension(nats,nclass) :: srel
  real, dimension(nats,nclass) :: srelv
  real, dimension(nats,nclass) :: su_srelv
  real  ,  dimension(7)  :: apaa
  real  ,  dimension(7)  :: apac
  real  ,  dimension(7)  :: apba
  real  ,  dimension(7)  :: apbc
  !
  common /htcp/   t1s,t2s,ai,bi,aw,bw,slp
  common /params/ ww,tw,rayon,asq,grav,rgas,rgocp,rgoasq,cpres
  common /params/ rgasv,cpresv
  !
  data apaa / 0.   , - 5.49,  - 30.18,   10.69,   17.55,   7.56, &
             7.56 /
  data apac / 0.   , - 2.63, - 104.15, - 239.02, - 139.53, - 44.08, &
            - 44.08 /
  data apba / .490, 1.405,  4.148,   .433,  - .095,   .571,   .571 /
  data apbc / .295,  .732, 12.012, 24.274, 16.621, 10.257, 10.257 /
  !
  !     * variable arguments for min/max intrinsics.
  !
  data one /1./
  data sstmax /36./
  data sstmin / - 1.8/
  data opem4 /0.0001/
  data wlt1min,wlt1max /0.1, 0.15/
  ! --------------------------------------------------------------
  !     * tuning parameter for bulk dust
  !     * (scale factor for wind stress threshold).
  !
  cuscale = 1.6
  !
  !     * 1.   surface emission.
  !            ------- --------
  !
  !     * dry sea salt concentration in first model layer (in kg/kg).
  !     * dust source flux calculation.
  !
  atune = 1.3
  if (ipam == 0) then
    do il = il1,il2
      if (fwatrow(il) > 0. .and. sicnrow(il) <= sicn_crt) then
        zrho0 = shj(il,ilev) * pressg(il)/(rgas * th(il,ilev))
        dztr = (dshj(il,ilev) * pressg(il))/grav
        afac = fwatrow(il) * (1. - sicnrow(il))
        zspdgust = zspdso(il)
        if (zspdgust <= 6. ) then
          assa = (apaa(1) + apba(1) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(1) + apbc(1) * zspdgust)/zrho0 * 1.e-09
        else if (zspdgust <= 9. ) then
          assa = (apaa(2) + apba(2) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(2) + apbc(2) * zspdgust)/zrho0 * 1.e-09
        else if (zspdgust <= 11. ) then
          assa = (apaa(3) + apba(3) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(3) + apbc(3) * zspdgust)/zrho0 * 1.e-09
        else if (zspdgust <= 13. ) then
          assa = (apaa(4) + apba(4) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(4) + apbc(4) * zspdgust)/zrho0 * 1.e-09
        else if (zspdgust <= 15. ) then
          assa = (apaa(5) + apba(5) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(5) + apbc(5) * zspdgust)/zrho0 * 1.e-09
        else if (zspdgust <= 18. ) then
          assa = (apaa(6) + apba(6) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(6) + apbc(6) * zspdgust)/zrho0 * 1.e-09
        else
          assa = (apaa(7) + apba(7) * zspdgust)/zrho0 * 1.e-09
          assc = (apac(7) + apbc(7) * zspdgust)/zrho0 * 1.e-09
        end if
        xemis(il,ilev,issa) = xemis(il,ilev,issa) + &
                              afac * (atune * assa - xrow(il,lev,issa))
        xemis(il,ilev,issc) = xemis(il,ilev,issc) + &
                              afac * (atune * assc - xrow(il,lev,issc))
      end if
    end do ! loop 10
    !
    call duinit1(uth,srel,srelv,su_srelv,cuscale)
    !
    call dustemi3 &
     (flndrow,gtrowbs,pressg,dshj,smfrac,fnrol,bsfrac, &
     zspdsbs,ustarbs,suz0row,pdsfrow, &
     spotrow,st01row,st02row,st03row,st04row,st06row, &
     st13row,st14row,st15row,st16row,st17row, &
     esdrow,xemis,idua,iduc,saverad,isvdust, &
     uth,srel,srelv,su_srelv,fallrow,fa10row,fa2row,fa1row, &
     duwdrow,dustrow,duthrow,usmkrow, &
     ilg,il1,il2,ilev,lev,ntrac,ztmst,cuscale)
  end if
  !
  !     * calculate dms emissions following liss+merlivat
  !     * dms seawater conc. from kettle et al.
  !     * limit zsst so formulation for schmidt number remains
  !     * positive and reasonably so.
  !
  do il = il1,il2
    if (fwatrow(il) > 0. .and. sicnrow(il) <= sicn_crt) then
      zdmscon = dmsorow(il) * fwatrow(il) * (1. - sicnrow(il))
      zsst = max( sstmin, min(gtroww(il) - 273.15,sstmax) )
      !
      !       * schmidt number according to saltzman et al. (1993).
      !
      zschmidt = 2674.0 - 147.12 * zsst + 3.726 * zsst ** 2 - 0.038 * zsst ** 3
      !
      !       * gas transfer velocity at schmidt number of 600
      !       * according to nigtingale et al. (2000).
      !
      zkw = 0.222 * zspdso(il) ** 2 + 0.333 * zspdso(il)
      !
      !       * gas transfer velocity at current schmidt number, airside
      !       * transfer velocity, and final transfer velocity (mcgillis
      !       * et al., 2000).
      !
      zkw = zkw * sqrt(600./zschmidt)
      amwr = 18.0153/62.13
      zka = 659. * zspdso(il) * sqrt(amwr)
      alpha = exp(3525./(zsst + 273.15) - 9.464)
      gamma = 1./(1. + zka/(alpha * zkw))
      zvdms = zkw * (1. - gamma)
      !
      !       * dms flux in kg/m2/sec, converted from nanomol/ltr*cm/hour.
      !
      zdmsemiss = zdmscon * zvdms * 32.064e-11/3600.
      !
      !       * nanomol/ltr*cm/hour --> kg/m**2/sec
      !
      xemis(il,ilev,idms) = xemis(il,ilev,idms) + zdmsemiss * &
                            ztmst * grav/(dshj(il,ilev) * pressg(il))
      if (isvchem /= 0) then
        edsorow(il) = edsorow(il) + zdmsemiss * saverad
      end if
    end if
  end do ! loop 20
  !
  !     * dms emissions from land sources.
  !
  do il = il1,il2
    if (flndrow(il) > 0.) then
      fact = ztmst * grav/(dshj(il,ilev) * pressg(il))
      edmslnd = edmsrow(il) * flndrow(il)
      xemis(il,ilev,idms) = xemis(il,ilev,idms) + edmslnd * fact
      if (isvchem /= 0) then
        edslrow(il) = edslrow(il) + edmslnd * saverad
      end if
    end if
  end do ! loop 30
  !
  !     * end of surface emissions
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
