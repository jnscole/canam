!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine inttemi2(sairrow,sairrol,ssfcrow,ssfcrol,sbiorow, &
                    sbiorol,sshirow,sshirol,sstkrow,sstkrol, &
                    sfirrow,sfirrol,oairrow,oairrol,osfcrow, &
                    osfcrol,obiorow,obiorol,oshirow,oshirol, &
                    ostkrow,ostkrol,ofirrow,ofirrol,bairrow, &
                    bairrol,bsfcrow,bsfcrol,bbiorow,bbiorol, &
                    bshirow,bshirol,bstkrow,bstkrol,bfirrow, &
                    bfirrol,ilg,il1,il2,delt,gmt,iday,mday)
  !
  !     * jun 18/2013 - k.vonsalzen. new version for gcm17+:
  !     *                            - add {sbio,sshi,obio,oshi,bbio,bshi}.
  !     * apr 20/2010 - k.vonsalzen. previous version inttemi for gcm15i+.
  !
  !     * interpolate transient emission data.
  !
  implicit none
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmsec
  real, intent(in) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: sec0
  real :: secsm
  !
  real, intent(inout), dimension(ilg) :: sairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sairrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ssfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ssfcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: sbiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sbiorol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: sshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sshirol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: sstkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sstkrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: sfirrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sfirrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: oairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: oairrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: osfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: osfcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: obiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: obiorol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: oshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: oshirol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ostkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ostkrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ofirrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ofirrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bairrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bsfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bsfcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bbiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bbiorol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bshirol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bstkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bstkrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bfirrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bfirrol !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !---------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  sec0 = real(iday) * 86400. + gmt
  !
  fmsec = real(mday) * 86400.
  if (fmsec < sec0) fmsec = fmsec + 365. * 86400.
  !
  secsm = fmsec - sec0
  !
  !     * general interpolation.
  !
  do i = il1,il2
    sairrow(i) = ((secsm - delt) * sairrow(i) + delt * sairrol(i))/secsm
    ssfcrow(i) = ((secsm - delt) * ssfcrow(i) + delt * ssfcrol(i))/secsm
    sbiorow(i) = ((secsm - delt) * sbiorow(i) + delt * sbiorol(i))/secsm
    sshirow(i) = ((secsm - delt) * sshirow(i) + delt * sshirol(i))/secsm
    sstkrow(i) = ((secsm - delt) * sstkrow(i) + delt * sstkrol(i))/secsm
    sfirrow(i) = ((secsm - delt) * sfirrow(i) + delt * sfirrol(i))/secsm
    oairrow(i) = ((secsm - delt) * oairrow(i) + delt * oairrol(i))/secsm
    osfcrow(i) = ((secsm - delt) * osfcrow(i) + delt * osfcrol(i))/secsm
    obiorow(i) = ((secsm - delt) * obiorow(i) + delt * obiorol(i))/secsm
    oshirow(i) = ((secsm - delt) * oshirow(i) + delt * oshirol(i))/secsm
    ostkrow(i) = ((secsm - delt) * ostkrow(i) + delt * ostkrol(i))/secsm
    ofirrow(i) = ((secsm - delt) * ofirrow(i) + delt * ofirrol(i))/secsm
    bairrow(i) = ((secsm - delt) * bairrow(i) + delt * bairrol(i))/secsm
    bsfcrow(i) = ((secsm - delt) * bsfcrow(i) + delt * bsfcrol(i))/secsm
    bbiorow(i) = ((secsm - delt) * bbiorow(i) + delt * bbiorol(i))/secsm
    bshirow(i) = ((secsm - delt) * bshirow(i) + delt * bshirol(i))/secsm
    bstkrow(i) = ((secsm - delt) * bstkrow(i) + delt * bstkrol(i))/secsm
    bfirrow(i) = ((secsm - delt) * bfirrow(i) + delt * bfirrol(i))/secsm
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
