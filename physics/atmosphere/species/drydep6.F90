!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine drydep6(xrow, &
                   flndrow,gtrowl,gtrowi,pressg,dshj,throw, &
                   shj,sicnrow,snorow,zsoi,qrow,hmfnrow, &
                   zfor, depb, dd4, dd6, ddd, ddb, ddo,dds, &
                   saverad,isvchem, &
                   ibco,ibcy,ioco,iocy,issa,issc, &
                   idua,iduc,iso2,iso4,ipam,ztmst, &
                   ilg,il1,il2,ilev,lev,ntrac)
  !
  !    this routine calculates the  dry deposition flux.
  !
  !     * may 12/2014 - m.lazare.   new version for gcm18+:
  !     *                           - fractional land (flndrow) replaces
  !     *                             discrete ground cover (gcrow).
  !     *                           - changed call sequences.
  !     *                           - gt over land (gtrowl) and seaice
  !     *                             (gtrowi) are passed in, instead of
  !     *                             general gtrow, and used in their
  !     *                             respective sections.
  !     * jun 19/2013 - k.vonsalzen/previous version drydep5 for gcm17:
  !     *               m.namazi/   - pass in "ipam" switch to not do
  !     *               m.lazare.     calculations/diagnostics on most
  !     *                             fields (except iso2) if using pla.
  !     *                           - also pass in and calculate bc deposition
  !     *                             (depb) if using bulk.
  !     * apr 25/2010 - m.lazare.   previous version drydep4 for gcm15i,gcm16:
  !     *                           - dda,ddc removed and
  !     *                             ddd,ddb,ddo,dds added for
  !     *                             diagnostic fields under
  !     *                             control of "isvchem" (dust,
  !     *                             black carbon, organic
  !     *                             carbon and sea-salt, respectively).
  !     * dec 18/2007 - m.lazare.   previous version for gcm15g/h:
  !     *                           pass in "adelt" from physics as
  !     *                           ztmst and use directly, rather than
  !     *                           passing in delt and forming
  !     *                           ztmst=2.*delt inside.
  !     * jun 20/06 -  m. lazare.   previous version drydep2 for gcm15f:
  !     *                           - use variable instead of constant
  !     *                             in intrinsics such as "max",
  !     *                             so that can compile in 32-bit mode
  !     *                             with real(8).
  !     * dec 20/02 -  k. vonsalzen.final previous version drydep for
  !     *                           gcm15b/c/d/e:
  !     *                           - remove "itrind" common block
  !     *                             and pass indices instead.
  !     *                           - remove xaddrow calculations.
  !     *                           - add dust fields.
  !     *                           - optimize loops.
  !     * sep 21/2001 k.von salzen. correct calculation of xaddrow.
  !     *                           now optionally save  dd4, dd6.
  !     * m.lazare - nov 16/2000. pass in sicnrow instead of sicrow
  !     *                         to better define "alfa", and
  !     *                         correct "if" condition on gcrow
  !     *                         so that first branch is water/ice
  !     *                         instead of just water.
  !
  !    johann feichter          uni/hamburg         08/91
  !    modified  u. schlese    dkrz-hamburg        jan-95
  !
  !    purpose
  !   ---------
  !    the lower boundary condition for calculating the
  !    turbulent exchange in the boundary layer is
  !    determined by the emission and the dry deposition flux.
  !
  implicit none
  real :: ai
  real :: aw
  real :: bi
  real :: bw
  real :: fac
  real :: fac1
  real :: fac1l
  real :: fac1nl
  real :: fac2
  real :: fac2l
  real :: fac2nl
  real :: fac3
  real :: fac3nl
  real :: fac4
  real :: fac4nl
  real :: fac5
  real :: fac6
  integer, intent(in) :: ibco  !< Tracer index for black carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: ibcy  !< Tracer index for black carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: idua  !< Tracer index for dust (accumulation mode) \f$[unitless]\f$
  integer, intent(in) :: iduc  !< Tracer index for dust (coarse mode) \f$[unitless]\f$
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioco  !< Tracer index for organic carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: iocy  !< Tracer index for organic carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: ipam  !< Switch to use PAM or bulk aerosol scheme (0=bulk. 1=PAM) \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: iso4
  integer, intent(in) :: issa  !< Tracer index for sea salt (accumulation mode) \f$[unitless]\f$
  integer, intent(in) :: issc  !< Tracer index for sea salt (coarse mode) \f$[unitless]\f$
  integer, intent(in) :: isvchem  !< Switch to save extra chemistry diagnostics (0 = don't save, 1 = save) \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: saverad  !< Reciprocal of timestep interval to save radiative transfer output \f$[unitless]\f$
  real :: slp
  real :: t1s
  real :: t2s
  real :: zmaxvdry
  real :: zrho0
  real, intent(in) :: ztmst
  real :: zvd2ice
  real :: zvd2nof
  real :: zvd4ice
  real :: zvd4nof
  real :: zvddust
  real :: zvdphob
  real :: zvdrd1
  real :: zvdrd1l
  real :: zvdrd1nl
  real :: zvdrd2
  real :: zvdrd2l
  real :: zvdrd2nl
  real :: zzdp
  real :: zzfcrow

  real, intent(in), dimension(ilg) :: gtrowl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gtrowi !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: throw   !< Physics internal field for temperature, including moon layer at atmospheric model top \f$[K]\f$
  real, intent(in), dimension(ilg) :: sicnrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: snorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qrow   !< Physics internal field for specific humidity, including moon layer at atmospheric model top \f$[kg kg^{-1}]\f$
  real, intent(inout), dimension(ilg,lev,ntrac) :: xrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: flndrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: hmfnrow !< Variable description\f$[units]\f$
  real   :: zvwc2 !< Variable description\f$[units]\f$
  real   :: zvw02 !< Variable description\f$[units]\f$
  real   :: zvwc4 !< Variable description\f$[units]\f$
  real   :: zvw04 !< Variable description\f$[units]\f$
  real   :: zsncri !< Variable description\f$[units]\f$
  real   :: alfa !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zsoi !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zfor !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dd4 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dd6 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ddd !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ddb !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ddo !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dds !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: depb !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real   :: ww
  real   :: tw
  real   :: rayon
  real   :: asq
  real   :: grav
  real   :: rgas
  real   :: rgocp
  real   :: rgoasq
  real   :: cpres
  real   :: rgasv
  real   :: cpresv

  common /params/ ww,tw,rayon,asq,grav,rgas,rgocp,rgoasq,cpres
  common /params/ rgasv,cpresv
  common /htcp/   t1s,t2s,ai,bi,aw,bw,slp
  !=====================================================================
  !     m water equivalent  critical snow height (from *surf*)
  zsncri = 0.025
  !
  !     coefficients for zvdrd = function of soil moisture
  !
  zvwc2 = (0.8e-2 - 0.2e-2)/(1. - 0.9)
  zvw02 = zvwc2 - 0.8e-2
  zvwc4 = (0.2e-2 - 0.025e-2)/(1. - 0.9)
  zvw04 = zvwc4 - 0.2e-2
  zvdphob = 2.5e-4
  zvddust = 1.e-2
  !
  !
  !*      2.    dry deposition.
  !             --- ----------
  do il = il1,il2
    zrho0 = shj(il,ilev) * pressg(il)/(rgas * throw(il,ilev) &
            * (1. + (rgasv/rgas - 1.) * qrow(il,ilev)))
    !
    zzdp = zrho0 * ztmst * grav/(dshj(il,ilev) * pressg(il))

    zmaxvdry = 1./zzdp
    zvdrd1nl = 0.
    zvdrd2nl = 0.
    zvdrd1l = 0.
    zvdrd2l = 0.
    !
    !      * dry deposition of so2, so4.
    !
    !      - sea(water or seaice) -
    if (flndrow(il) < 1.) then
      !        - melting/not melting seaice-
      if (gtrowi(il) >= (t1s - 0.1)) then
        zvd2ice = 0.8e-2
        zvd4ice = 0.2e-2
      else
        zvd2ice = 0.1e-2
        zvd4ice = 0.025e-2
      end if
      alfa = sicnrow(il)
      zvdrd1nl = (1. - alfa) * 1.0e-2 + alfa * zvd2ice
      zvdrd2nl = (1. - alfa) * 0.2e-2 + alfa * zvd4ice
      zvdrd1nl = min(max(zvdrd1nl,0.),zmaxvdry)
      zvdrd2nl = min(max(zvdrd2nl,0.),zmaxvdry)
    end if
    !
    !      - land -
    if (flndrow(il) > 0.) then
      !        - non-forest areas -
      !        - snow/no snow -
      if (snorow(il) > zsncri) then
        !          - melting/not melting snow -
        if (hmfnrow(il) > 0.) then
          zvd2nof = 0.8e-2
          zvd4nof = 0.2e-2
        else
          zvd2nof = 0.1e-2
          zvd4nof = 0.025e-2
        end if
      else
        !          - frozen/not frozen soil -
        if (gtrowl(il) <= t1s) then
          zvd2nof = 0.2e-2
          zvd4nof = 0.025e-2
        else
          !          - wet/dry -
          if (zsoi(il) >= 0.99) then
            zvd2nof = 0.8e-2
            zvd4nof = 0.2e-2
          else if (zsoi(il) < 0.9) then
            zvd2nof = 0.2e-2
            zvd4nof = 0.025e-2
          else
            zvd2nof = zvwc2 * zsoi(il) - zvw02
            zvd4nof = zvwc4 * zsoi(il) - zvw04
          end if
        end if
      end if
      zzfcrow = max(zfor(il),0.)
      zvdrd1l = zzfcrow * 0.8e-2 &
                + (1. - zzfcrow) * zvd2nof
      zvdrd2l = zzfcrow * 0.2e-2 &
                + (1. - zzfcrow) * zvd4nof
      zvdrd1l = min(max(zvdrd1l,0.),zmaxvdry)
      zvdrd2l = min(max(zvdrd2l,0.),zmaxvdry)
    end if
    zvdrd1 = flndrow(il) * zvdrd1l + (1. - flndrow(il)) * zvdrd1nl
    zvdrd2 = flndrow(il) * zvdrd2l + (1. - flndrow(il)) * zvdrd2nl
    !
    fac    = zzdp * sicnrow(il)
    fac1nl = 1. - zvdrd1nl * zzdp
    fac2nl = 1. - zvdrd2nl * zzdp
    fac3nl = 1. - zvdrd2nl * fac
    fac4nl = 1. - zvddust * fac
    fac1l  = 1. - zvdrd1l * zzdp
    fac2l  = 1. - zvdrd2l * zzdp
    fac1   = flndrow(il) * fac1l + (1. - flndrow(il)) * fac1nl
    fac2   = flndrow(il) * fac2l + (1. - flndrow(il)) * fac2nl
    fac3   = 1. - zvdphob * zzdp
    fac4   = 1. - zvddust * zzdp
    fac5   = flndrow(il) * fac2l + (1. - flndrow(il)) * fac3nl
    fac6   = flndrow(il) * fac4  + (1. - flndrow(il)) * fac4nl
    !
    !      * sulphate is done regardless of "ipam".
    !
    xrow(il,lev,iso2) = xrow(il,lev,iso2) * fac1
    if (isvchem /= 0) then
      dd4(il) = dd4(il) + xrow(il,lev,iso2) * zvdrd1 * zrho0 * saverad
    end if
    !
    if (ipam == 0) then
      depb(il) = depb(il) + (xrow(il,lev,ibco) * zvdphob &
                 + xrow(il,lev,ibcy) * zvdrd2) * zrho0
      xrow(il,lev,iso4) = xrow(il,lev,iso4) * fac2
      xrow(il,lev,ibcy) = xrow(il,lev,ibcy) * fac2
      xrow(il,lev,iocy) = xrow(il,lev,iocy) * fac2
      xrow(il,lev,ibco) = xrow(il,lev,ibco) * fac3
      xrow(il,lev,ioco) = xrow(il,lev,ioco) * fac3
      xrow(il,lev,idua) = xrow(il,lev,idua) * fac2
      xrow(il,lev,iduc) = xrow(il,lev,iduc) * fac4
      xrow(il,lev,issa) = xrow(il,lev,issa) * fac5
      xrow(il,lev,issc) = xrow(il,lev,issc) * fac6
      if (isvchem /= 0) then
        dd6(il) = dd6(il) + xrow(il,lev,iso4) * zvdrd2 * zrho0 * saverad
        ddd(il) = ddd(il) + xrow(il,lev,idua) * zvdrd2 * zrho0 * saverad
        ddd(il) = ddd(il) + xrow(il,lev,iduc) * zvddust * zrho0 * saverad
        ddb(il) = ddb(il) + xrow(il,lev,ibco) * zvdphob * zrho0 * saverad
        ddb(il) = ddb(il) + xrow(il,lev,ibcy) * zvdrd2 * zrho0 * saverad
        ddo(il) = ddo(il) + xrow(il,lev,ioco) * zvdphob * zrho0 * saverad
        ddo(il) = ddo(il) + xrow(il,lev,iocy) * zvdrd2 * zrho0 * saverad
        dds(il) = dds(il) + xrow(il,lev,issa) * zvdrd2 * zrho0 * saverad
        dds(il) = dds(il) + xrow(il,lev,issc) * zvddust * zrho0 * saverad
      end if
    end if
  end do ! loop 205
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
