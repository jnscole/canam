!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xttemi2(xemis,x2demisa,x2demiss,x2demisk,x2demisf, &
                   fbbcrow,fairrow,pressg,dshj,zf,zfs,ztmst,levwf, &
                   levair,il1,il2,lev,ilg,ilev,ntrac)
  !
  !     * apr 28/2012 - k.vonsalzen/ new version for gcm16:
  !     *               m.lazare.    - initialize llow and lhgh to
  !     *                              ilev instead of zero, to
  !     *                              avoid possible invalid
  !     *                              memory references.
  !     *                            - bugfix for incorrect memory
  !     *                              reference: llow(l)->llow(il).
  !     *                            - eliminate resetting of llow to
  !     *                              ilev if rlh is not positive.
  !     * apr 26/2010 - k.vonsalzen: previous version xttemi for gcm15h.
  !
  implicit none
  real :: a
  real :: asq
  real :: cpres
  real :: cpresv
  real :: dul
  real :: dz
  real :: emitmp
  real :: fact
  real :: grav
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: l
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: levair      !< Number of vertical layers for aircraft emissions input data \f$[unitless]\f$
  integer, intent(in) :: levwf      !< Number of vertical layers for wildfire emissions input data \f$[unitless]\f$
  integer :: n
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real :: rgas
  real :: rgasv
  real :: rgoasq
  real :: rgocp
  real :: rlh
  real :: ruh
  real :: tw
  real :: wgt
  real :: ww
  real :: zmax
  real :: zmin
  real, intent(in) :: ztmst
  !
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev)  :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev)  :: zf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev + 1) :: zfs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levwf) :: fbbcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levair) :: fairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ntrac) :: x2demisa !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ntrac) :: x2demiss !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ntrac) :: x2demisk !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ntrac) :: x2demisf !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer, dimension(ilg) :: llow
  integer, dimension(ilg) :: lhgh
  !      integer, dimension(2000) ::  llow,lhgh
  real, dimension(6) :: zwf
  data zwf /100., 500., 1000., 2000., 3000., 6000./
  real, dimension(25) :: zair
  data zair / 610., 1220., 1830., 2440., 3050., 3660., 4270., &
            4880., 5490., 6100., 6710., 7320., 7930., 8540., &
            9150., 9760.,10370.,10980.,11590.,12200.,12810., &
           13420.,14030.,14640.,15250. /
  !
  common /params/ ww,     tw,    a,     asq,  grav, rgas,  rgocp, &
                 rgoasq, cpres, rgasv, cpresv
  !
  !-------------------------------------------------------------------
  !     * check dimensions.
  !
  if (levwf  /= 6) call xit("xttemi2", - 1)
  if (levair /= 25) call xit("xttemi2", - 2)
  !
  !-------------------------------------------------------------------
  !     * surface emissions. insert 2d emissions into first model layer
  !     * above ground.
  !
  do n = 1,ntrac
    do il = il1,il2
      fact = ztmst * grav/(dshj(il,ilev) * pressg(il))
      xemis(il,ilev,n) = xemis(il,ilev,n) + fact * x2demiss(il,n)
    end do
  end do
  !
  !-------------------------------------------------------------------
  !     * determine layer corresponding to stack emissions (100-300m).
  !
  zmin = 100.
  zmax = 300.
  dz = zmax - zmin
  llow(il1:il2) = ilev
  lhgh(il1:il2) = ilev
  do l = ilev,1, - 1
    do il = il1,il2
      if (zf(il,l) <= zmin) llow(il) = l
      if (zf(il,l) <= zmax) lhgh(il) = l
    end do
  end do
  !
  !     * stack emissions.
  !
  do l = 1,ilev
    do il = il1,il2
      fact = ztmst * grav/(dshj(il,l) * pressg(il))
      if (l == llow(il) .and. l == lhgh(il) ) then
        wgt = 1.
      else if (l == lhgh(il) ) then
        wgt = (zmax - zf(il,l))/dz
      else if (l > lhgh(il) .and. l < llow(il) .and. l > 1) then
        wgt = (zf(il,l - 1) - zf(il,l))/dz
      else if (l == llow(il) .and. l > 1) then
        wgt = (zf(il,l - 1) - zmin)/dz
      else
        wgt = 0.
      end if
      do n = 1,ntrac
        emitmp = x2demisk(il,n)
        xemis(il,l,n) = xemis(il,l,n) + wgt * emitmp * fact
      end do
    end do
  end do
  !
  !-------------------------------------------------------------------
  !     * wild fire emissions.
  !
  do k = 1,levwf
    ruh = zwf(k)
    if (k == 1) then
      rlh = 0.
    else
      rlh = zwf(k - 1)
    end if
    dul = ruh - rlh
    !
    !       * determine layer corresponding to emissions.
    !
    llow(il1:il2) = ilev
    lhgh(il1:il2) = ilev
    !
    do l = ilev,1, - 1
      do il = il1,il2
        if (zf(il,l) <= rlh)   llow(il) = l
        if (zf(il,l) <= ruh)   lhgh(il) = l
      end do
    end do
    do l = 1,ilev
      do il = il1,il2
        fact = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == llow(il) .and. l == lhgh(il) ) then
          wgt = 1.
        else if (l == lhgh(il) ) then
          wgt = ( ruh - zf(il,l) )/dul
        else if (l > lhgh(il) .and. l < llow(il) .and. l > 1) then
          wgt = (zf(il,l - 1) - zf(il,l))/dul
        else if (l == llow(il) .and. l > 1) then
          wgt = ( zf(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        do n = 1,ntrac
          emitmp = fbbcrow(il,k) * x2demisf(il,n)
          xemis(il,l,n) = xemis(il,l,n) + wgt * emitmp * fact
        end do
      end do
    end do
  end do
  !
  !-------------------------------------------------------------------
  !     * aircraft emissions.
  !
  do k = 1,levair
    ruh = zair(k)
    if (k == 1) then
      rlh = 0.
    else
      rlh = zair(k - 1)
    end if
    dul = ruh - rlh
    !
    !       * determine layer corresponding to emissions.
    !
    llow(il1:il2) = ilev
    lhgh(il1:il2) = ilev
    !
    do l = ilev + 1,1, - 1
      do il = il1,il2
        if (zfs(il,l) <= rlh)   llow(il) = l
        if (zfs(il,l) <= ruh)   lhgh(il) = l
      end do
    end do
    do l = 1,ilev
      do il = il1,il2
        fact = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == llow(il) .and. l == lhgh(il) ) then
          wgt = 1.
        else if (l == lhgh(il) ) then
          wgt = ( ruh - zfs(il,l) )/dul
        else if (l > lhgh(il) .and. l < llow(il) .and. l > 1) then
          wgt = (zfs(il,l - 1) - zfs(il,l))/dul
        else if (l == llow(il) .and. l > 1) then
          wgt = ( zfs(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        do n = 1,ntrac
          emitmp = fairrow(il,k) * x2demisa(il,n)
          xemis(il,l,n) = xemis(il,l,n) + wgt * emitmp * fact
        end do
      end do
    end do
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
