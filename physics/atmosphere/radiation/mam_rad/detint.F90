!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine detint(x,xn,n,z,z1,z2,k)
  !***********************************************************************
  !*                                                                     *
  !*              subroutine detint                                      *
  !*                                                                     *
  !***********************************************************************
  !
  ! to calculate the arrays of second order interpolation coefficients
  !      from xn(n) grid to x level.  v. fomichev, july 17, 1992.
  !
  ! called by precl
  ! calls nothing

  implicit none
  real :: a
  real :: a1
  real :: a2
  integer :: i
  integer, intent(inout) :: k
  integer :: l1
  integer, intent(in) :: n
  real, intent(in) :: x
  real, intent(inout) :: z
  real, intent(inout) :: z1
  real, intent(inout) :: z2

  ! input:   x, xn(n)

  real, intent(in), dimension(n) :: xn !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  k=1
  l1=n-1
  i=int((k+l1)/2.)
  do while (i/=k)
    if (x-xn(i) < 0) then
      l1=i
    else
      k=i
    end if
    i=int((k+l1)/2.)
  end do

  ! output parameter  k   has been found:    xn(k) <= x < xn(k+1)

  a=(x-xn(k+1))/(xn(k)-xn(k+2))
  a1=((x-xn(k))/(xn(k)-xn(k+1)))*(1.+a)
  a2=((x-xn(k))/(xn(k+1)-xn(k+2)))*a

  ! output: z1, z2, z3 - interpolation coefficients (weight) for
  !                     for xn(k),xn(k+1),xn(k+2) levels, correspondly.

  z=1.+a1
  z1=-a1-a2
  z2=a2
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
