!> \file
!>\brief Calculate optical property for internal mixing of sulfate (NH4)2SO4,
!! black carbon and organic carbon
!!
!! @author Jiangnan Li
!
subroutine aeromx3op (exta, exoma, exomga, fa, absa, taua055, &
                      absa055, taua086, rhin, so4load, bcoload, &
                      bcyload, ocoload, ocyload, ream3, veam3, &
                      reso4, veso4, rebcy, vebcy, reocy, veocy, &
                      rebco, vebco, reoco, veoco, &
                      il1, il2, ilg, lay)
  !
  !     july. 2017, j. li & k.von salzen
  !----------------------------------------------------------------------c
  !     internal mixing of sulfate (nh4)2so4, bc and oc and also considerc
  !     parial internal mixing effect, apply to bulk and pla             c
  !     new bc refractive index (following bond) is used                 c
  !                                                                      c
  !     exta:    extinction coefficient                                  c
  !     exoma:   extinction coefficient times single scattering albedo   c
  !     exomga:  exoma times asymmetry factor                            c
  !     fa:      square of asymmetry factor                              c
  !     taua055: optical depth at 0.55/0.86 um, 4: total of three as     c
  !        /086  mixing + hydrophobic, 1, 2, 3: pure sulfate, bc, oc     c
  !     absa:    absorption coefficient                                  c
  !              with dp factor                                          c
  !     rhin:    input relative humidity                                 c
  !     so4load, bcoload, bcyload, ocoload, ocyload,                     c
  !              y/o: hydrophylic/hydrophobic                            c
  !     re:      effective radius, like reso4 is the effective r for so4 c
  !     ve:      effective variance                                      c
  !     ream3/veam3:   effective radius for the mixed three              c
  !     rh:      relative humidity                                       c
  !                                                                      c
  !     sext:    aeromx3s(1:17,irec), 1 the growth factor, 2-5 ext for 4 c
  !              solar bands, 6-7 for 0.55 and 0.86 um, 8-11 single scattc
  !              albedo for 4 bands, 12 for 0.55 um, 13-16 g for 4 bands c
  !              17 g for 0.55 um                                        c
  !     labs:    first 9 are results for 9 lw bands                      c
  !----------------------------------------------------------------------c
  !
  implicit none
  integer, external :: mvidx
  integer :: i
  integer :: j
  integer :: k
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: nf1
  integer :: nf2
  integer :: if1
  integer :: if2
  integer :: iff1
  integer :: iff2
  integer :: ih
  integer :: ihh
  integer :: ir
  integer :: irec
  integer :: irr
  integer :: iv
  integer :: ivv
  integer :: nbl
  integer :: nbs
  integer :: nh
  integer :: nr
  integer :: nv
  real, dimension(9,2592) :: aeromx3l
  real, dimension(17,2592) :: aeromx3s
  real :: bco
  real :: bcym
  real :: bcyn
  real :: ext1
  real :: ext2
  real :: ext3
  real :: ext4
  real :: ext5
  real :: ext6
  real :: extom1
  real :: extom2
  real :: extom3
  real :: extom4
  real :: extom5
  real :: extom6
  real :: extomg1
  real :: extomg2
  real :: extomg3
  real :: extomg4
  real :: extomg5
  real :: extomg6
  real :: fact
  real :: fr1
  real :: fr2
  real :: fracbcy
  real :: fracocy
  real :: fracso4
  real :: oco
  real :: ocym
  real :: ocyn
  real :: re
  real :: rh
  real :: sbo
  real :: so4m
  real :: so4n
  real :: tot
  real :: ve
  real :: wtt
  real :: xb
  real :: xm
  real :: xo
  real :: xs
  real :: ytiny
  parameter (nbs = 4, nbl = 9, nh = 6, nr = 4, nv = 3, nf1 = 6, &
                   nf2 = 6)
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient of mixed aerosol \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo of mixed aerosol \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymmetry factor of mixed aerosol \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymmetry factor of mixed aerosol \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa !< Absorption depth at 0.55um for mixed aerosol \f$[1]\f$
  real, dimension(nbs,6) :: sexta !< Variable description\f$[units]\f$
  real, dimension(nbs,6) :: somga !< Variable description\f$[units]\f$
  real, dimension(nbs,6) :: sga !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: rhin !< Relative humidity \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: so4load !< SO4 loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: bcoload !< Hydrophobic black carbon loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: bcyload !< Hydrophylic black carbon loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ocoload !< Hydrophobic organic carbon loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ocyload !< Hydrophylic organic carbon loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ream3 !< Effective radius for the mixed aerosol\f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: veam3 !< Effective variance for the mixed aerosol \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: reso4 !< Effective radius for SO4 \f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: veso4 !< Effective variance for SO4 \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: rebcy !< Effective radius for hydrophylic black carbon \f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: vebcy !< Effective variance for hydrophylic black carbon \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: reocy !< Effective radius for hydrophylic organic carbon \f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: veocy !< Effective variance for hydrophylic organic carbon \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: rebco !< Effective radius for hydrophobic black carbon \f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: vebco !< Effective variance for hydrophobic black carbon \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: reoco !< Effective radius for hydrophobic organic carbon \f$[um]\f$
  real, intent(in), dimension(ilg,lay) :: veoco !< Effective variance for hydrophobic organic carbon \f$[1]\f$
  real, intent(inout), dimension(ilg,lay,4) :: taua055 !< Optical depth at 0.55um for mixed aerosol
                                                       !! 1-sulfate, 2- black carbon, 3- organic carbon, 4- all \f$[1]\f$
  real, intent(inout), dimension(ilg,lay,4) :: absa055 !< Absorption depth at 0.55um for mixed aerosol \f$[1]\f$
  real, intent(inout), dimension(ilg,lay,4) :: taua086 !< Optical depth at 0.86um for mixed aerosol
                                                       !! 1-sulfate, 2- black carbon, 3- organic carbon, 4- all \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  real, dimension(nh) :: rhnode
  real, dimension(nr) :: renode
  real, dimension(nv) :: venode
  real, dimension(nf1) :: fr1node
  real, dimension(nf2) :: fr2node
  real, dimension(6) :: sext
  real, dimension(5) :: somg
  real, dimension(5) :: sg
  real, dimension(9) :: labs
  real, dimension(6) :: sexta055
  real, dimension(6) :: sabsa055
  real, dimension(6) :: sexta086
  real, dimension(nbl,6) :: labsa
  real, dimension(17) :: ss
  real, dimension(2) :: th
  real, dimension(2) :: tr
  real, dimension(2) :: tv
  real, dimension(2) :: tf1
  real, dimension(2) :: tf2
  real :: rea1

  !
  data rhnode  /0.1, 0.6, 0.8, 0.9, 0.94, 0.98/
  data renode  /0.06, 0.14, 0.22, 0.3/
  data venode  /0.6, 1.5, 2.4/
  data fr1node /1.0, 0.8, 0.6, 0.4, 0.2, 0.0/
  data fr2node /0.0, 0.05, 0.1, 0.3, 0.7, 1.0/
  !
  common /amix3opt/aeromx3s, aeromx3l
  !
  !----------------------------------------------------------------------c
  !     factor 10000, because the unit of specific extinction for aerosolc
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv the same dp  c
  !     (air density * layer thickness) is used for both gas and aerosol.c
  !     aload is dry loading in unit g (aerosol) / g(air).               c
  !----------------------------------------------------------------------c
  !
  ytiny = 10. * tiny(rea1)
  !
  !     fracso4, fracbcy, fracocy determine how many percent (nh)4so4,
  !     bcy, ocy join the mixing, as so4m, bcym, ocym, and how many are
  !     internal mixing, as so4n, bcyn, ocyn
  !
  fracso4  = 1.
  fracbcy  = 1.
  fracocy  = 1.
  do k = 1, lay
    do i = il1, il2
      rh               =  max(min (rhin(i,k), 0.98), 0.1)
      so4m             =  10000. * so4load(i,k) * fracso4
      so4n             =  10000. * so4load(i,k) * (1.0 - fracso4)
      bco              =  10000. * bcoload(i,k)
      bcym             =  10000. * bcyload(i,k) * fracbcy
      bcyn             =  10000. * bcyload(i,k) * (1.0 - fracbcy)
      oco              =  10000. * ocoload(i,k)
      ocym             =  10000. * ocyload(i,k) * fracocy
      ocyn             =  10000. * ocyload(i,k) * (1.0 - fracocy)
      !
      sexta(:,:)       =  0.0
      somga(:,:)       =  0.0
      sga(:,:)         =  0.0
      sexta055(:)      =  0.0
      sabsa055(:)      =  0.0
      sexta086(:)      =  0.0
      labsa(:,:)       =  0.0
      !
      sbo              =  so4m + bcym + ocym
      tot              =  sbo + so4n + bcyn  + ocyn + bco  + oco
      if (tot > 5.e-09) then
        !
        re               =  max(min (ream3(i,k), 0.3), 0.06)
        ve               =  max(min (veam3(i,k), 2.4), 0.6)
        !
        ih               =  mvidx(rhnode, nh, rh)
        ir               =  mvidx(renode, nr, re)
        iv               =  mvidx(venode, nv, ve)
        th(2)            = (rh - rhnode(ih)) / &
                           (rhnode(ih + 1) - rhnode(ih))
        th(1)            =  1.0 - th(2)
        tr(2)            = (re - renode(ir)) / &
                           (renode(ir + 1) - renode(ir))
        tr(1)            =  1.0 - tr(2)
        tv(2)            = (ve - venode(iv)) / &
                           (venode(iv + 1) - venode(iv))
        tv(1)            =  1.0 - tv(2)
        !
        if (sbo > ytiny) then
          fr1            =  max(min (so4m / sbo, 1.0), 0.0)
          fr2            =  max(min (bcym / sbo, 1.0), 0.0)
        else
          fr1            =  0.0
          fr2            =  0.0
        end if
        if1              =  mvidx(fr1node, nf1, fr1)
        if2              =  mvidx(fr2node, nf2, fr2)
        !
        tf1(2)           = (fr1 - fr1node(if1)) / &
                           (fr1node(if1 + 1) - fr1node(if1))
        tf1(1)           =  1.0 - tf1(2)
        tf2(2)           = (fr2 - fr2node(if2)) / &
                           (fr2node(if2 + 1) - fr2node(if2))
        tf2(1)           =  1.0 - tf2(2)
        !
        do ihh       =  ih, ih + 1
          do irr       =  ir, ir + 1
            do ivv       =  iv, iv + 1
              do iff1      =  if1, if1 + 1
                do iff2      =  if2, if2 + 1
                  !
                  irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                   (irr - 1) * nv * nf1 * nf2 + &
                                   (ivv - 1) * nf1 * nf2 + &
                                   (iff1 - 1) * nf2 + iff2
                  !
                  ss(1:17)       =  aeromx3s(1:17,irec)
                  sext(1:6)      =  ss(2:7)
                  somg(1:5)      =  ss(8:12)
                  sg(1:5)        =  ss(13:17)
                  labs(1:9)      =  aeromx3l(1:9,irec)
                  !
                  wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                   tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                   tf2(iff2 - if2 + 1)
                  do j  = 1, nbs
                    sexta(j,1)   =  sexta(j,1) + sext(j) * ss(1) * wtt
                    somga(j,1)   =  somga(j,1) + somg(j) * wtt
                    sga(j,1)     =  sga(j,1) + sg(j) * wtt
                  end do
                  fact         =  sext(5) * ss(1) * wtt
                  sexta055(1)  =  sexta055(1) + fact
                  sabsa055(1)  =  sabsa055(1) + fact * (1.0 - somg(5))
                  sexta086(1)  =  sexta086(1) + sext(6) * ss(1) * wtt
                  labsa(1:9,1) =  labsa(1:9,1) + labs(1:9) * ss(1) * wtt
                end do
              end do
            end do
          end do
        end do ! loop 101
        !
        ! pure solfate, the external mixing part, hydroscopic
        !
        if (so4m > 1.e-10 .or. so4n > 1.e-10) then
          re               =  max(min (reso4(i,k), 0.3), 0.06)
          ve               =  max(min (veso4(i,k), 2.4), 0.6)
          !
          ir               =  mvidx(renode, nr, re)
          iv               =  mvidx(venode, nv, ve)
          tr(2)            = (re - renode(ir)) / &
                             (renode(ir + 1) - renode(ir))
          tr(1)            =  1.0 - tr(2)
          tv(2)            = (ve - venode(iv)) / &
                             (venode(iv + 1) - venode(iv))
          tv(1)            =  1.0 - tv(2)
          !
          tf1(1)           =  1.0
          tf1(2)           =  0.0
          tf2(1)           =  1.0
          tf2(2)           =  0.0
          if1              =  1
          if2              =  1
          !
          do ihh       =  ih, ih + 1
            do irr       =  ir, ir + 1
              do ivv       =  iv, iv + 1
                do iff1      =  if1, if1 + 1
                  do iff2      =  if2, if2 + 1
                    !
                    irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                     (irr - 1) * nv * nf1 * nf2 + &
                                     (ivv - 1) * nf1 * nf2 + &
                                     (iff1 - 1) * nf2 + iff2
                    !
                    ss(1:17)       =  aeromx3s(1:17,irec)
                    sext(1:6)      =  ss(2:7)
                    somg(1:5)      =  ss(8:12)
                    sg(1:5)        =  ss(13:17)
                    labs(1:9)      =  aeromx3l(1:9,irec)
                    !
                    wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                     tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                     tf2(iff2 - if2 + 1)
                    do j  = 1, nbs
                      sexta(j,2)   =  sexta(j,2) + sext(j) * ss(1) * wtt
                      somga(j,2)   =  somga(j,2) + somg(j) * wtt
                      sga(j,2)     =  sga(j,2) + sg(j) * wtt
                    end do
                    fact         =  sext(5) * ss(1) * wtt
                    sexta055(2)  =  sexta055(2) + fact
                    sabsa055(2)  =  sabsa055(2) + fact * (1.0 - somg(5))
                    sexta086(2)  =  sexta086(2) + sext(6) * ss(1) * wtt
                    labsa(1:9,2) =  labsa(1:9,2) + labs(1:9) * ss(1) * wtt
                  end do
                end do
              end do
            end do
          end do ! loop 102
        end if
        !
        ! pure bcy external mixing, hydroscpic
        !
        if (bcym > 1.e-10 .or. bcyn > 1.e-10) then
          re               =  max(min (rebcy(i,k), 0.3), 0.06)
          ve               =  max(min (vebcy(i,k), 2.4), 0.6)
          ir               =  mvidx(renode, nr, re)
          iv               =  mvidx(venode, nv, ve)
          tr(2)            = (re - renode(ir)) / &
                             (renode(ir + 1) - renode(ir))
          tr(1)            =  1.0 - tr(2)
          tv(2)            = (ve - venode(iv)) / &
                             (venode(iv + 1) - venode(iv))
          tv(1)            =  1.0 - tv(2)
          tf1(1)           =  0.0
          tf1(2)           =  1.0
          tf2(1)           =  0.0
          tf2(2)           =  1.0
          if1              =  5
          if2              =  5
          !
          do ihh       =  ih, ih + 1
            do irr       =  ir, ir + 1
              do ivv       =  iv, iv + 1
                do iff1      =  if1, if1 + 1
                  do iff2      =  if2, if2 + 1
                    !
                    irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                     (irr - 1) * nv * nf1 * nf2 + &
                                     (ivv - 1) * nf1 * nf2 + &
                                     (iff1 - 1) * nf2 + iff2
                    !
                    ss(1:17)       =  aeromx3s(1:17,irec)
                    sext(1:6)      =  ss(2:7)
                    somg(1:5)      =  ss(8:12)
                    sg(1:5)        =  ss(13:17)
                    labs(1:9)      =  aeromx3l(1:9,irec)
                    !
                    wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                     tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                     tf2(iff2 - if2 + 1)
                    do j  = 1, nbs
                      sexta(j,3)   =  sexta(j,3) + sext(j) * ss(1) * wtt
                      somga(j,3)   =  somga(j,3) + somg(j) * wtt
                      sga(j,3)     =  sga(j,3) + sg(j) * wtt
                    end do
                    fact         =  sext(5) * ss(1) * wtt
                    sexta055(3)  =  sexta055(3) + fact
                    sabsa055(3)  =  sabsa055(3) + fact * (1.0 - somg(5))
                    sexta086(3)  =  sexta086(3) + sext(6) * ss(1) * wtt
                    labsa(1:9,3) =  labsa(1:9,3) + labs(1:9) * ss(1) * wtt
                  end do
                end do
              end do
            end do
          end do ! loop 103
        end if
        !
        ! pure bco external mixing, non-hydroscpic
        !
        if (bco > 1.e-10) then
          re               =  max(min (rebco(i,k), 0.3), 0.06)
          ve               =  max(min (vebco(i,k), 2.4), 0.6)
          ir               =  mvidx(renode, nr, re)
          iv               =  mvidx(venode, nv, ve)
          tr(2)            = (re - renode(ir)) / &
                             (renode(ir + 1) - renode(ir))
          tr(1)            =  1.0 - tr(2)
          tv(2)            = (ve - venode(iv)) / &
                             (venode(iv + 1) - venode(iv))
          tv(1)            =  1.0 - tv(2)
          th(1)            =  1.0
          th(2)            =  0.0
          ih               =  1
          tf1(1)           =  0.0
          tf1(2)           =  1.0
          tf2(1)           =  0.0
          tf2(2)           =  1.0
          if1              =  5
          if2              =  5
          !
          do ihh       =  ih, ih + 1
            do irr       =  ir, ir + 1
              do ivv       =  iv, iv + 1
                do iff1      =  if1, if1 + 1
                  do iff2      =  if2, if2 + 1
                    !
                    irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                     (irr - 1) * nv * nf1 * nf2 + &
                                     (ivv - 1) * nf1 * nf2 + &
                                     (iff1 - 1) * nf2 + iff2
                    !
                    ss(1:17)       =  aeromx3s(1:17,irec)
                    sext(1:6)      =  ss(2:7)
                    somg(1:5)      =  ss(8:12)
                    sg(1:5)        =  ss(13:17)
                    labs(1:9)      =  aeromx3l(1:9,irec)
                    !
                    wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                     tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                     tf2(iff2 - if2 + 1)
                    do j  = 1, nbs
                      sexta(j,4)   =  sexta(j,4) + sext(j) * ss(1) * wtt
                      somga(j,4)   =  somga(j,4) + somg(j) * wtt
                      sga(j,4)     =  sga(j,4) + sg(j) * wtt
                    end do
                    fact         =  sext(5) * ss(1) * wtt
                    sexta055(4)  =  sexta055(4) + fact
                    sabsa055(4)  =  sabsa055(4) + fact * (1.0 - somg(5))
                    sexta086(4)  =  sexta086(4) + sext(6) * ss(1) * wtt
                    labsa(1:9,4) =  labsa(1:9,4) + labs(1:9) * ss(1) * wtt
                  end do
                end do
              end do
            end do
          end do ! loop 104
        end if
        !
        ! pure ocy external mixing, hydroscpic
        !
        if (ocym > 1.e-10 .or. ocyn > 1.e-10) then
          re               =  max(min (reocy(i,k), 0.3), 0.06)
          ve               =  max(min (veocy(i,k), 2.4), 0.6)
          ih               =  mvidx(rhnode, nh, rh)
          ir               =  mvidx(renode, nr, re)
          iv               =  mvidx(venode, nv, ve)
          th(2)            = (rh - rhnode(ih)) / &
                             (rhnode(ih + 1) - rhnode(ih))
          th(1)            =  1.0 - th(2)
          tr(2)            = (re - renode(ir)) / &
                             (renode(ir + 1) - renode(ir))
          tr(1)            =  1.0 - tr(2)
          tv(2)            = (ve - venode(iv)) / &
                             (venode(iv + 1) - venode(iv))
          tv(1)            =  1.0 - tv(2)
          tf1(1)           =  0.0
          tf1(2)           =  1.0
          tf2(1)           =  1.0
          tf2(2)           =  0.0
          if1              =  5
          if2              =  1
          !
          do ihh       =  ih, ih + 1
            do irr       =  ir, ir + 1
              do ivv       =  iv, iv + 1
                do iff1      =  if1, if1 + 1
                  do iff2      =  if2, if2 + 1
                    !
                    irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                     (irr - 1) * nv * nf1 * nf2 + &
                                     (ivv - 1) * nf1 * nf2 + &
                                     (iff1 - 1) * nf2 + iff2
                    !
                    ss(1:17)       =  aeromx3s(1:17,irec)
                    sext(1:6)      =  ss(2:7)
                    somg(1:5)      =  ss(8:12)
                    sg(1:5)        =  ss(13:17)
                    labs(1:9)      =  aeromx3l(1:9,irec)
                    !
                    wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                     tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                     tf2(iff2 - if2 + 1)
                    !
                    do j  = 1, nbs
                      sexta(j,5)   =  sexta(j,5) + sext(j) * ss(1) * wtt
                      somga(j,5)   =  somga(j,5) + somg(j) * wtt
                      sga(j,5)     =  sga(j,5) + sg(j) * wtt
                    end do
                    fact         =  sext(5) * ss(1) * wtt
                    sexta055(5)  =  sexta055(5) + fact
                    sabsa055(5)  =  sabsa055(5) + fact * (1.0 - somg(5))
                    sexta086(5)  =  sexta086(5) + sext(6) * ss(1) * wtt
                    labsa(1:9,5) =  labsa(1:9,5) + labs(1:9) * ss(1) * wtt
                  end do
                end do
              end do
            end do
          end do ! loop 105
        end if
        !
        ! pure oco external mixing, non-hydroscpic
        !
        if (oco > 1.e-10) then
          re               =  max(min (reocy(i,k), 0.3), 0.06)
          ve               =  max(min (veocy(i,k), 2.4), 0.6)
          ir               =  mvidx(renode, nr, re)
          iv               =  mvidx(venode, nv, ve)
          tr(2)            = (re - renode(ir)) / &
                             (renode(ir + 1) - renode(ir))
          tr(1)            =  1.0 - tr(2)
          tv(2)            = (ve - venode(iv)) / &
                             (venode(iv + 1) - venode(iv))
          tv(1)            =  1.0 - tv(2)
          th(1)            =  1.0
          th(2)            =  0.0
          ih               =  1
          tf1(1)           =  0.0
          tf1(2)           =  1.0
          tf2(1)           =  1.0
          tf2(2)           =  0.0
          if1              =  5
          if2              =  1
          !
          do ihh       =  ih, ih + 1
            do irr       =  ir, ir + 1
              do ivv       =  iv, iv + 1
                do iff1      =  if1, if1 + 1
                  do iff2      =  if2, if2 + 1
                    !
                    irec           = (ihh - 1) * nr * nv * nf1 * nf2 + &
                                     (irr - 1) * nv * nf1 * nf2 + &
                                     (ivv - 1) * nf1 * nf2 + &
                                     (iff1 - 1) * nf2 + iff2
                    !
                    ss(1:17)       =  aeromx3s(1:17,irec)
                    sext(1:6)      =  ss(2:7)
                    somg(1:5)      =  ss(8:12)
                    sg(1:5)        =  ss(13:17)
                    labs(1:9)      =  aeromx3l(1:9,irec)
                    !
                    wtt            =  th(ihh - ih + 1) * tr(irr - ir + 1) * &
                                     tv(ivv - iv + 1) * tf1(iff1 - if1 + 1) * &
                                     tf2(iff2 - if2 + 1)
                    !
                    do j  = 1, nbs
                      sexta(j,6)   =  sexta(j,6) + sext(j) * ss(1) * wtt
                      somga(j,6)   =  somga(j,6) + somg(j) * wtt
                      sga(j,6)     =  sga(j,6) + sg(j) * wtt
                    end do
                    fact         =  sext(5) * ss(1) * wtt
                    sexta055(6)  =  sexta055(6) + fact
                    sabsa055(6)  =  sabsa055(6) + fact * (1.0 - somg(5))
                    sexta086(6)  =  sexta086(6) + sext(6) * ss(1) * wtt
                    labsa(1:9,6) =  labsa(1:9,6) + labs(1:9) * ss(1) * wtt
                  end do
                end do
              end do
            end do
          end do ! loop 106
        end if
        !
        !----------------------------------------------------------------------c
        !     the results of exta, exoma, exomga, fa, absa are accumulated     c
        !     with dust & ssalt calculated in other subroutines                c
        !----------------------------------------------------------------------c
        !
        do j  = 1, nbs
          ext1           =  sexta(j,1) * sbo
          ext2           =  sexta(j,2) * so4n
          ext3           =  sexta(j,3) * bcyn
          ext4           =  sexta(j,4) * bco
          ext5           =  sexta(j,5) * ocyn
          ext6           =  sexta(j,6) * oco
          exta(i,k,j)    =  exta(i,k,j) + ext1 + ext2 + ext3 + &
                           ext4 + ext5 + ext6
          extom1         =  ext1 * somga(j,1)
          extom2         =  ext2 * somga(j,2)
          extom3         =  ext3 * somga(j,3)
          extom4         =  ext4 * somga(j,4)
          extom5         =  ext5 * somga(j,5)
          extom6         =  ext6 * somga(j,6)
          exoma(i,k,j)   =  exoma(i,k,j) + extom1 + extom2 + extom3 + &
                           extom4 + extom5 + extom6
          extomg1        =  extom1 * sga(j,1)
          extomg2        =  extom2 * sga(j,2)
          extomg3        =  extom3 * sga(j,3)
          extomg4        =  extom4 * sga(j,4)
          extomg5        =  extom5 * sga(j,5)
          extomg6        =  extom6 * sga(j,6)
          exomga(i,k,j)  =  exomga(i,k,j) + extomg1 + extomg2 + &
                           extomg3 + extomg4 + extomg5 + extomg6
          !
          fa(i,k,j)      =  fa(i,k,j) + extomg1 * sga(j,1) + &
                           extomg2 * sga(j,2) + &
                           extomg3 * sga(j,3) + &
                           extomg4 * sga(j,4) + &
                           extomg5 * sga(j,5) + &
                           extomg6 * sga(j,6)
        end do
        !
        !----------------------------------------------------------------------c
        !     4: the total mixed three; 1: sulfate; 2: bc; 3: oc               c
        !----------------------------------------------------------------------c
        !
        xm             =  sexta055(1) * sbo
        xs             =  sexta055(2) * so4m
        xb             =  sexta055(3) * bcym
        xo             =  sexta055(5) * ocym
        taua055(i,k,1) =  sexta055(2) * so4n + (xm - xb - xo)
        taua055(i,k,2) =  sexta055(3) * bcyn + sexta055(4) * bco + &
                         (xm - xs - xo)
        taua055(i,k,3) =  sexta055(5) * ocyn + sexta055(6) * oco + &
                         (xm - xs - xb)
        taua055(i,k,4) =  taua055(i,k,1) + taua055(i,k,2) + &
                         taua055(i,k,3)
        !
        xm             =  sabsa055(1) * sbo
        xs             =  sabsa055(2) * so4m
        xb             =  sabsa055(3) * bcym
        xo             =  sabsa055(5) * ocym
        absa055(i,k,1) =  sabsa055(2) * so4n + (xm - xb - xo)
        absa055(i,k,2) =  sabsa055(3) * bcyn + sabsa055(4) * bco + &
                         (xm - xs - xo)
        absa055(i,k,3) =  sabsa055(5) * ocyn + sabsa055(6) * oco + &
                         (xm - xs - xb)
        absa055(i,k,4) =  absa055(i,k,1) + absa055(i,k,2) + &
                         absa055(i,k,3)
        !
        xm             =  sexta086(1) * sbo
        xs             =  sexta086(2) * so4m
        xb             =  sexta086(3) * bcym
        xo             =  sexta086(5) * ocym
        taua086(i,k,1) =  sexta086(2) * so4n + (xm - xb - xo)
        taua086(i,k,2) =  sexta086(3) * bcyn + sexta086(4) * bco + &
                         (xm - xs - xo)
        taua086(i,k,3) =  sexta086(5) * ocyn + sexta086(6) * oco + &
                         (xm - xs - xb)
        taua086(i,k,4) =  taua086(i,k,1) + taua086(i,k,2) + &
                         taua086(i,k,3)
        !
        do j  = 1, nbl
          absa(i,k,j)    =  absa(i,k,j) + labsa(j,1) * sbo + &
                           labsa(j,2) * so4n + labsa(j,3) * bcyn + &
                           labsa(j,4) * bco  + labsa(j,5) * ocyn + &
                           labsa(j,6) * oco
        end do
        !
      else
        taua055(i,k,:) =  0.0
        absa055(i,k,:) =  0.0
        taua086(i,k,:) =  0.0
      end if
    end do
  end do ! loop 200
  return
end
!> \file
!> Calculation of optical property for 3 internal mixing of sulfate
! (NH4)2SO4, black carbon and organic carbon. The interoplation is based on 7 
! dimensional spheriod of relative humidity, effective radius and variance.  The
! partial internal mixing effect is applied to bulk and pla new black carbon 
! refractive index (following Bond et al).