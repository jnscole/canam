!> \file
!>\brief Set the atmospheric gas mixing ratios
!!
!! @author Jiangnan Li
!
subroutine set_mmr(co2_ppm, ch4_ppm, n2o_ppm, f11_ppm, f12_ppm, &
                   f113_ppm, f114_ppm)
  !
  !=======================================================================
  ! set mass mixing ratios in common trace from input concentrations.
  !   l. solheim ...jan, 2008
  !
  ! this has been extracted from subroutine radcons to provide a modular
  ! approach that will allow a consistent way to change these mixing
  ! ratios to thread specific values inside an openmp parallel region.
  !=======================================================================
  !
  implicit none
  !
  real, intent(in) :: co2_ppm !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: ch4_ppm !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: n2o_ppm !< N2O mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: f11_ppm !< CFC11 mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: f12_ppm !< CFC12 mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: f113_ppm !< CFC113 mixing ratio \f$[gram/gram]\f$
  real, intent(in) :: f114_ppm !< CFC114 mixing ratio \f$[gram/gram]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * common block for absorber mixing ratios.
  !
  real :: rmco2
  real :: rmch4
  real :: rmn2o
  real :: rmo2
  real :: rmf11
  real :: rmf12
  real :: rmf113
  real :: rmf114
  common /trace / rmco2, rmch4, rmn2o, rmo2, rmf11, rmf12, rmf113, &
                 rmf114
  !$omp threadprivate (/trace/)
  !=======================================================================
  !
  !----------------------------------------------------------------------c
  !     input trace gas concentrations in unit ppmv,                     c
  !     parts per million by volume, transform to mass mixing ratio.     c
  !     the same as water vapor and ozone.                               c
  !     1.5188126 = 44.    / 28.97                                       c
  !     0.5522955 = 16.    / 28.97                                       c
  !     1.5188126 = 44.    / 28.97                                       c
  !     o2 input as a constant, unit mixing rato by mass                 c
  !     4.7418019 = 137.37 / 28.97                                       c
  !     4.1736279 = 120.91 / 28.97                                       c
  !     6.4704867 = 187.45 / 28.97                                       c
  !     5.6920953 = 164.90 / 28.97                                       c
  !     28.97 molecular weight of air, e-06 per million                  c
  !----------------------------------------------------------------------c
  !
  rmco2  =  co2_ppm  * 1.5188126
  rmch4  =  ch4_ppm  * 0.5522955
  rmn2o  =  n2o_ppm  * 1.5188126
  rmo2   =  0.2315
  rmf11  =  f11_ppm  * 4.7418019
  rmf12  =  f12_ppm  * 4.1736279
  rmf113 =  f113_ppm * 6.4704867
  rmf114 =  f114_ppm * 5.6920953

  return
end
!> \file
!> Set the mixing ratios for well mixed atmospheric gases.