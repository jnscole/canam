!> \file
!>\brief Compute Rayleigh scatter optical thickness for wavelength interval 1
!!
!! @author Jiangnan Li
!
subroutine raylev2 (taur, ig, dp, rmu3, il1, il2, ilg, lay)
  !
  !     * dec 05,2007 - j.li.  new version for gcm15g:
  !     *                      - revised data for ri0,ri2.
  !     * apr 25,2003 - j.li.  previous version raylev up through gcm15f.
  !----------------------------------------------------------------------c
  !     rayleigh scattering for each sub-band in bands1, visible region  c
  !     taur is the optical depth rayleigh scattering for a given layer  c
  !     for uvc (35700 - 50000 cm^-1), since the optical depth of o3 and c
  !     o2 are very large, rayleigh scattering effect is neglected, it   c
  !     is shown even for 10% o3 amount of the standard atmo, the        c
  !     rayleigh scattering for uvc still can be neglected.              c
  !     for par and uva, since their spectral ranges are very wide, smallc
  !     errors could occur for large zenith angle, slightly adjustment   c
  !     is needed, this does mean the rayleigh optical depth is related  c
  !     solar zenith angle for multiple scattering process in swtran.    c
  !                                                                      c
  !     taur: rayleigh optical depth                                     c
  !     dp:   air mass path for a model layer (exlained in raddriv).     c
  !     rmu3:  a factor of solar zenith angle, given in raddriv          c
  !----------------------------------------------------------------------c
  implicit none
  integer :: i
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  real, dimension(6) :: ri0
  real, dimension(3) :: ri2
  real, intent(inout), dimension(ilg,lay) :: taur !< Raylegh scattering optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg) :: rmu3 !< Factor related to solar zenth angle \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  data ri0 / .67758e-04, .19425e-03, .52145e-03, .88082e-03, &
            .10541e-02, .12835e-02 /
  data ri2 / .28000e-05, .21300e-04, .36000e-04 /
  !=======================================================================
  if (ig <= 3) then
    do k = 1, lay
      do i = il1, il2
        taur(i,k) = (ri0(ig) - ri2(ig) * rmu3(i)) * dp(i,k)
      end do
    end do ! loop 100
  else
    do k = 1, lay
      do i = il1, il2
        taur(i,k) =  ri0(ig) * dp(i,k)
      end do
    end do ! loop 200
  end if
  !
  return
end
!> \file
!> Rayleigh scattering for each sub-band in bands1, visible region
!! taur is the optical depth rayleigh scattering for a given layer
!! for uvc (35700 - 50000 cm^-1), since the optical depth of o3 and 
!! o2 are very large, rayleigh scattering effect is neglected, it 
!! is shown even for 10% o3 amount of the standard atmo, the  
!! Rayleigh scattering for uvc still can be neglected
!! for par and uva, since their spectral ranges are very wide, small
!! errors could occur for large zenith angle, slightly adjustment for  
!! solar zenith is needed, as the rayleigh optical depth is related  
!! solar zenith angle for multiple scattering process in swtran.