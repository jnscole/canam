!> \file
!>\brief Compute Rayleigh scatter optical thickness for wavelength intervals 2, 3 and 4
!!
!! @author Jiangnan Li
!
subroutine raylei (taur, ib, dp, il1, il2, ilg, lay)
  !
  !     * apr 25,2003 - j.li.
  !----------------------------------------------------------------------c
  !     rayleigh scattering for bands2-bands4, near infrared region      c
  !                                                                      c
  !     taur: rayleigh optical depth                                     c
  !     dp:   air mass path for a model layer (exlained in raddriv).     c
  !----------------------------------------------------------------------c
  implicit none
  integer :: i
  integer, intent(in) :: ib
  integer :: ibm1
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  !
  real, dimension(3) :: ri
  real, intent(inout), dimension(ilg,lay) :: taur !< Raylegh scattering optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  data ri / .16305e-04, .17997e-05, .13586e-06 /
  !=======================================================================
  ibm1 = ib - 1
  do k = 1, lay
    do i = il1, il2
      taur(i,k) =  ri(ibm1) * dp(i,k)
    end do
  end do ! loop 100
  !
  return
end
!> \file
!> Compute the Rayleigh optical properties for solar wavelength intervals 2, 3 and 4,
!! i.e., the intervals in the near infrared.