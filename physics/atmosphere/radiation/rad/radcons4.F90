!> \file
!>\brief Physical constants used in radiative transfer
!!
!! @author Jiangnan Li
!
subroutine radcons4
  !
  !     * apr 30/2012 - m.lazare. new version for gcm16:
  !     *                         solar_c reduced from 1365 to 1361 in
  !     *                         line with newer sattellite obs.
  !     * feb 13/2009 - m.lazare. previous version radcons3 for gcm15h/i:
  !     *                         remove cfc-13 and cfc-14 effect since
  !     *                         uncertain.
  !     * apr 18/2007 - l.solheim. previous version radcons2 for gcm15g:
  !     *                          - mmr of trace gases now defined
  !     *                            in separate routine set_mmr (also
  !     *                            called else where in code). thus
  !     *                            "trace" common block explicit
  !     *                            in this routine removed (now in
  !     *                            set_mmr).
  !     * apr  3/2003 - m.lazare/ previous version radcons for gcm15f
  !     *               j.li.     and earlier.
  !
  !     *                        defines constants in common blocks
  !     *                        at start of model, used in li radiation.
  !
  !     * original common block containin ppm constants.
  !
  use phys_parm_defs, only : pp_solar_const

  implicit none
  real :: ch4_ppm
  real :: co2_ppm
  real :: f113_ppm
  real :: f114_ppm
  real :: f11_ppm
  real :: f12_ppm
  real :: solar_c
  !
  real   :: n2o_ppm
  common /radcon/ solar_c, co2_ppm, ch4_ppm, n2o_ppm, f11_ppm, &
                 f12_ppm, f113_ppm, f114_ppm
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !============================================================================
  !     * define ppm values (actually, these are concentrations
  !     * expressed as a ppm value multiplied by 1.e-6).
  !
  !     * note that for time-varying constants, one can use
  !     * these as values at kount=0, then modify in time
  !     * at start of section 2, based on values of kount,delt.
  !     * in this case, the mixing ratio derivations below would
  !     * ** also ** have to be relocated to section 2 !
  !
  solar_c  = pp_solar_const
  co2_ppm  = 348.0   * 1.e-6
  ch4_ppm  = 1.650   * 1.e-6
  n2o_ppm  = 0.306   * 1.e-6
  f11_ppm  = 0.18e-3 * 1.e-6
  f12_ppm  = 0.28e-3 * 1.e-6
  !     f113_ppm = 0.05e-3 * 1.e-6
  !     f114_ppm = 0.03e-3 * 1.e-6
  f113_ppm = 0.
  f114_ppm = 0.
  !
  !     * set corresponding mass mixing ratios in common/trace/.
  !
  call set_mmr(co2_ppm, ch4_ppm, n2o_ppm, f11_ppm,f12_ppm, &
               f113_ppm, f114_ppm)
  !-------------------------------------------------------------
  return
end
!> \file
!> Radiative constants used in radiatived transfer.