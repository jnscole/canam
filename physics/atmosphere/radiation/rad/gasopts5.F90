!> \file
!>\brief Compute the gas optical thickness for solar wavelengths
!!
!! @author Jiangnan Li
!
subroutine gasopts5 (taug, gw, dp, ib, ig, o3, q, co2, ch4, o2, &
                     inpt, mcont, dip, dt, rmu3, lev1, gh, &
                     il1, il2, ilg, lay)
  !
  !     * may 01,2012 - j.li.     new version for gcm16:
  !     *                         - include water vapour continuum
  !     *                           (bands 2-4).
  !     * feb 09,2009 - j.li.     previous version gasopts4 for gcm15h/i:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls tline{1,2,3}z instead of
  !     *                           tline{1,2,3}y.
  !     * apr 18,2008 - m.lazare/ previous version gasopts3 for gcm15g:
  !     *               l.solheim/- cosmetic change to add threadprivate
  !     *               j.li.       for common block "trace", in support
  !     *                         - calls tline{1,2,3}y instead of
  !     *                           tline{1,2,3}x.
  !     *                         - using temperture dependent o3 absorption
  !     *                           data, adding ch4 in solar range, using
  !     *                           kuruz solar function.
  !     * may 05,2006 - m.lazare. previous version gasopts2 for gcm15e/f:
  !     *                         - pass integer :: variables "init" and
  !     *                           "mit" instead of actual integer
  !     *                           values, to "tline_" routines.
  !     * apr 25,2003 - j.li.     previous version gasopts for gcm15d.
  !----------------------------------------------------------------------c
  !     calculation of the optical depths due to nongray gaseous         c
  !     absorption for the solar, in each layer for a given band ib and  c
  !     cumulative probability gw.                                       c
  !     relative solar energy in each solar band are                     c
  !     band 1:   622.8483                                               c
  !     band 1gh:   7.5917                                               c
  !     band 2:   430.0919                                               c
  !     band 2gh:   8.9036                                               c
  !     band 3:   238.6979                                               c
  !     band 3gh:   7.4453                                               c
  !     band 4:    33.4129                                               c
  !     band 4gh:   7.0384                                               c
  !                                                                      c
  !     total relative solar energy in from 0.2 - 4 um is                c
  !     1356.0300 w / m2, plus 11.9096 w / m2 in 4 - 10 um.            c
  !     total  1367.9396 w / m2                                         c
  !                                                                      c
  !     this subroutine only calculates taug below 1 mb                  c
  !                                                                      c
  !     taug:  gaseous optical depth                                     c
  !     dp:    air mass path for a model layer (exlained in raddriv).    c
  !     o3:    o3 mass mixing ratio                                      c
  !     q:     water vapor mass mixing ratio                             c
  !     co2,   ch4, o2 also are mass mixing ratios                       c
  !     dip:   interpretation factor for pressure between two            c
  !            neighboring standard input data pressure levels           c
  !     dt:    layer temperature - 250 k                                 c
  !     inpt:  number of the level for the standard input data pressures c
  !     rmu3:  a factor of solar zenith angle, given in raddriv          c
  !     mcont: the highest level for water vapor continuum calculation   c
  !----------------------------------------------------------------------c
  implicit none
  real :: cs1o21
  real, dimension(3,6) :: cs1o3
  real, dimension(5,5,4) :: cs2cf
  real, dimension(5,5,4) :: cs2cs
  real, dimension(5,18,4) :: cs2h2o
  real, dimension(5,18,2) :: cs2o2
  real, dimension(4) :: cs2o3
  real, dimension(5,5,6) :: cs3cf
  real, dimension(5,18,2) :: cs3ch4
  real, dimension(5,18,6) :: cs3co2
  real, dimension(5,5,6) :: cs3cs
  real, dimension(5,18,6) :: cs3h2o
  real, dimension(5,5,4) :: cs4cf
  real, dimension(5,18,2) :: cs4ch4
  real, dimension(5,18,4) :: cs4co2
  real, dimension(5,5,4) :: cs4cs
  real, dimension(5,18,4) :: cs4h2o
  real :: dto3
  real, intent(inout) :: gw
  real, dimension(6) :: gw1
  real, dimension(4) :: gw2
  real, dimension(6) :: gw3
  real, dimension(4) :: gw4
  integer :: i
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: init
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: lc
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer :: m
  integer, intent(in) :: mcont!< Highest level for water vapor continuum calculation \f$[1]\f$
  integer :: mtl
  real :: x
  !
  real, parameter :: r_zero = 0.0
  !
  real, intent(inout), dimension(ilg,lay) :: taug !< Gaseous optical thickness \f$[1]\f$
  !
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: q !< H2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ch4 !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o2 !< O2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: dip !< Interpretation between two neighboring standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  real, intent(in), dimension(ilg) :: rmu3 !< Adjust factor of solar zenith angle for O2 absorption coefficient \f$[cm^2/gram]\f$
  real, dimension(ilg,lay) :: s !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg,lay) :: inpt !< Level number of the standard input pressures \f$[1]\f$
  logical, intent(in) :: gh !< If is true, use large gaseous optical depth group for calculations \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  common /bands1/ gw1, cs1o3, cs1o21
  common /bands2/ gw2, cs2h2o, cs2o2, cs2o3, cs2cs, cs2cf
  common /bands3/ gw3, cs3h2o, cs3co2, cs3ch4, cs3cs, cs3cf
  common /bands4/ gw4, cs4h2o, cs4co2, cs4ch4, cs4cs, cs4cf

  !
  !     * number of vertical levels in absorber pressure-based coefficient
  !     * array ("m" references non-saturated bands active below 1 mb only).
  !
  data mtl /18/
  !=======================================================================
  if (ib == 1) then
    !
    !----------------------------------------------------------------------c
    !     band (14500 - 50000 cm-1), nongray gaseous absorption of o3,    c
    !     h2o and o2.                                                      c
    !     relative solar energy 630.4401 wm-2.                            c
    !     ig9 (50000-43000)  uvc                           1.21100 (w m-2)c
    !     ig8 (43000-37500)  uvc                           3.17570         c
    !     ig7 (37500-35700)  uvc                           3.20501         c
    !     uvc all included in gh part                                      c
    !                                                                      c
    !     ig6 (35700-34200)  uvb                           4.73084         c
    !     ig5 (34200-32185)  uvb                          10.14919         c
    !     ig4 (32185-31250)  uvb  j value: 32185 cm-1     6.70594         c
    !                                                                      c
    !     ig3 (31250-25000)  uva                          83.43346         c
    !                                                                      c
    !     ig2 (25000-19000)  par                         236.97212         c
    !     ig1 (19000-14500)  par                         280.85678         c
    !     par: photosynthetic active radiation                             c
    !     note the spectral structure is slightly diff from li & barker    c
    !     (2005 jas)                                                       c
    !                                                                      c
    !     the effect of h2o and o2 is added with simple method             c
    !----------------------------------------------------------------------c
    !
    if (ig == 1) then
      do k = lev1, lay
        do i = il1, il2
          if (inpt(1,k) < 950) then
            m =  inpt(i,k)
          else
            m =  inpt(i,k) - 1000
          end if
          !
          if (m < 7) then
            x       = (cs1o21 - 0.881e-05 * rmu3(i)) * o2(i,k)
          else
            x       = (0.108e-04 - 0.881e-05 * rmu3(i)) * o2(i,k)
          end if
          !
          if (m < 15) then
            x       =  x + (0.199e-02 - 0.952e-03 * rmu3(i)) * q(i,k)
          else
            x       =  x + (0.208e-02 - 0.952e-03 * rmu3(i)) * q(i,k)
          end if
          !
          dto3      =  dt(i,k) + 23.13
          taug(i,k) = ((cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                      dto3 * cs1o3(3,ig))) * o3(i,k) + x) * dp(i,k)
        end do
      end do ! loop 110
    else
      do k = lev1, lay
        do i = il1, il2
          dto3      =  dt(i,k) + 23.13
          taug(i,k) = (cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                      dto3 * cs1o3(3,ig))) * o3(i,k) * dp(i,k)
        end do
      end do ! loop 120
    end if
    !
    gw =  gw1(ig)
    !
  else if (ib == 2) then
    !
    !----------------------------------------------------------------------c
    !     band (8400 - 14500 cm-1), nongray gaseous absorption of h2o,    c
    !     o2 and o3                                                        c
    !     relative solar energy 430.0919 w m-2                            c
    !----------------------------------------------------------------------c
    !
    if (ig <= 2) then
      call tline2z (taug, cs2h2o(1,1,ig), cs2o2(1,1,ig), q, o2, &
                    dp, dip, dt, inpt, lev1, gh, mtl, &
                    il1, il2, ilg, lay)
    else
      init = 2
      call tline1z (taug, cs2h2o(1,1,ig), q, dp, dip, dt, inpt, &
                    lev1, gh, mtl, init, il1, il2, ilg, lay)
    end if
    !
    !----------------------------------------------------------------------c
    !     simply add o3 effect                                             c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        taug(i,k)   =  taug(i,k) + cs2o3(ig) * o3(i,k) * dp(i,k)
      end do
    end do ! loop 200
    !
    !----------------------------------------------------------------------c
    !     water vapour continuum                                           c
    !----------------------------------------------------------------------c
    !
    lc =  5
    call tcontl1 (taug, cs2cs(1,1,ig), cs2cf(1,1,ig), q, dp, dip, &
                  dt, lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    gw =  gw2(ig)
    !
  else if (ib == 3) then
    !
    !----------------------------------------------------------------------c
    !     band (4200 - 8400 cm-1), nongray gaseous absorption of h2o, co2 c
    !     and ch4                                                          c
    !     relative solar energy  238.6979 w m-2                           c
    !----------------------------------------------------------------------c
    !
    if (ig <= 2) then
      call tline3z (taug, cs3h2o(1,1,ig), cs3co2(1,1,ig), &
                    cs3ch4(1,1,ig), q, co2, ch4, dp, dip, dt, inpt, &
                    lev1, gh, mtl, il1, il2, ilg, lay)
    else
      call tline2z (taug, cs3h2o(1,1,ig), cs3co2(1,1,ig), q, co2, &
                    dp, dip, dt, inpt, lev1, gh, mtl, &
                    il1, il2, ilg, lay)
    end if
    !
    !----------------------------------------------------------------------c
    !     water vapour continuum                                           c
    !----------------------------------------------------------------------c
    !
    lc =  5
    call tcontl1 (taug, cs3cs(1,1,ig), cs3cf(1,1,ig), q, dp, dip, &
                  dt, lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    gw =  gw3(ig)
    !
  else if (ib == 4) then
    !
    !----------------------------------------------------------------------c
    !     band (2500 - 4200 cm-1), nongray gaseous absorption of h2o      c
    !     and co2                                                          c
    !     relative solar energy 33.4129 w m-2                             c
    !----------------------------------------------------------------------c
    !
    if (ig <= 2) then
      call tline3z (taug, cs4h2o(1,1,ig), cs4co2(1,1,ig), &
                    cs4ch4(1,1,ig), q, co2, ch4, dp, dip, dt, inpt, &
                    lev1, gh, mtl, il1, il2, ilg, lay)
    else
      call tline2z (taug, cs4h2o(1,1,ig), cs4co2(1,1,ig), q, co2, &
                    dp, dip, dt, inpt, lev1, gh, mtl, &
                    il1, il2, ilg, lay)
    end if
    !
    !----------------------------------------------------------------------c
    !     water vapour continuum                                           c
    !----------------------------------------------------------------------c
    !
    lc =  5
    call tcontl1 (taug, cs4cs(1,1,ig), cs4cf(1,1,ig), q, dp, dip, &
                  dt, lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    gw =  gw4(ig)
    !
  end if
  !
  ! check to verify that taug is greater than 0.0

  do k = lev1, lay
    do i = il1, il2
      if (taug(i,k) < r_zero) taug(i,k) = r_zero
    end do ! i
  end do ! i

  return
end
!> \file
!> Calculation of the optical depths due to nongray gaseous below 1 hPa,
!! in each layer for a given band ib and cumulative probability gw.
!!\n
!! Note that relative solar energy in each solar band are
!! - band 1:   622.8483 W/m2
!! - band 1gh:   7.5917 W/m2
!! - band 2:   430.0919 W/m2
!! - band 2gh:   8.9036 W/m2
!! - band 3:   238.6979 W/m2
!! - band 3gh:   7.4453 W/m2
!! - band 4:    33.4129 W/m2
!! - band 4gh:   7.0384 W/m2
!!\n
!! This results in a total relative solar energy from 0.2 - 4 \f$\mu\f$m of
!! 1356.0300 W/m2 and there is 11.9096 W/m2 for wavelengths greater than 4 \f$\mu\f$m,
!! with a total of 1367.9396 W/m2.
