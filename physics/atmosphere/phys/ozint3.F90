!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ozint3(ozrow,ozrol,ilg,il1,il2,levoz,delt, &
                  gmt,iday,lday)
  !
  !     * may 06/05 - m.lazare. new version for gcm15e onward:
  !     *                       - interpolates in terms of seconds
  !     *                         instead of timesteps, to avoid
  !     *                         roundoff error which came from
  !     *                         dividing by timestep.
  !     *                         this arose following investigation
  !     *                         by david plummer.
  !     * apr 28/03 - m.lazare. previous version ozint2 prior to gcm15e.
  !
  !     * ozrow  = ozone field    for previous timestep.
  !     * ozrol  = ozone field    at next physics time.
  !
  !     * ilg    = size of longitude loop (chained).
  !     * il1    = start of longitude loop.
  !     * il2    = end   of longitude loop.
  !     * levoz  = number of ozone levels.
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * lday   = date of target ozone.
  !
  !     * multi-level ppmv for ozone.
  !
  implicit none
  real, intent(inout) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: flsec
  real, intent(inout) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(inout) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: lday  !< Julian calendar day of next first-day-of-month \f$[day]\f$
  integer, intent(inout) :: levoz  !< Number of vertical layers for ozone input data (other than chemistry) \f$[unitless]\f$
  real :: sec0
  real :: secsl

  real, intent(inout), dimension(ilg,levoz) :: ozrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levoz) :: ozrol !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  sec0 = real(iday) * 86400. + gmt
  !
  flsec = real(lday) * 86400.
  if (flsec < sec0) flsec = flsec + 365. * 86400.
  !
  secsl = flsec - sec0
  !
  !     * general interpolation.
  !
  do l = 1,levoz
    do i = il1,il2
      ozrow(i,l) = ((secsl - delt) * ozrow(i,l) + delt * ozrol(i,l))/secsl
    end do
  end do ! loop 200
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
