!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tgcal2 (tsg,tsgb, tsh, sg,sgb,sh, &
                   ilev,lev, ilg,is,if)
  !
  !     * jul 15/88 - m.lazare - change (nk,i) arrays to proper (i,nk).
  !     * jan 29/88 - r.laprise.
  !     * set temperature arrays for physics on momentum levels
  !     * for hybrid version of model.
  !     * sgb are momentum layer interfaces,
  !     * sg  are momentum mid layer positions.
  !     * sh  are thermodynamics mid layer positions.
  !     * lev = ilev+1.
  !     *
  !     *     momentum            thermodynamics
  !     * mid layer  interface  mid layer   tsh(th)
  !     *
  !     */////////////////////////////////////////////////////////////////
  !     *                         moon layer tsh(1)
  !     *                      ---------------------
  !     *......sg(1)..........
  !     *                      ...sh(1)......tsh(2)
  !     *-------------- sgb(1)
  !     *                      ---------------------
  !     *......sg(2)..........
  !     *                      ...sh(2)......tsh(3)
  !     *-------------- sgb(2)
  !     *                      ---------------------
  !     *......sg(3)..........
  !     *                      ...sh(3)......tsh(4)
  !     *-------------- sgb(3)
  !     *                      ---------------------
  !     *......sg(4)..........
  !     *                      ...sh(4)......tsh(5)
  !     *-------------- sgb(4)
  !     *                      ---------------------
  !     *......sg(5)..........
  !     *                      ...sh(5)......tsh(6)
  !     *
  !     *////////////// sgb(5)=1. ////////////////////////////////////////

  implicit none
  integer :: i
  integer, intent(inout) :: if
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: is
  integer :: l
  integer, intent(inout) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer :: nc4to8

  real, intent(inout), dimension(ilg,ilev) :: tsg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: tsgb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg, lev) :: tsh !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sgb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sh !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !     * momentum mid layer temperatures.
  !
  do i = is,if
    tsg(i,1) = tsh(i,2)
  end do ! loop 400
  !
  do l = 2,ilev
    do i = is,if
      tsg(i,l)   = (tsh(i,l) * log(sh(i  ,l)/sg(i  ,l)) &
                   + tsh(i,l + 1) * log(sg(i  ,l)/sh(i,l - 1))) &
                   /log(sh(i  ,l)/sh(i,l - 1))
    end do
  end do ! loop 450
  !
  !     * momentum layer interface temperatures.
  !
  do l = 1,ilev - 1
    do i = is,if
      tsgb(i,l) = (tsh(i,l + 1) * log(sh (i,l + 1)/sgb(i  ,l)) &
                  + tsh(i,l + 2) * log(sgb(i  ,l)/sh (i  ,l))) &
                  /log(sh (i,l + 1)/sh (i  ,l))
    end do
  end do ! loop 500
  !
  !     * for compatibility with former staggered model...
  !
  do  i = is,if
    tsgb(i,ilev) = tsh(i,ilev + 1)
    !
  end do ! loop 550
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
