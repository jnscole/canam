!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
function rd3(x,y,z)
  !
  !     * jun 15/2013 - m.lazare.   new version for gcm17+:
  !     *                           - define "TINY" and "BIG" in
  !     *                             double-precision format, to
  !     *                             avoid inaccuracy who showed up
  !     *                             (only) in rcm.
  !     * sep 11/2006 - f.majaess.  previous version rd2, hard-coding
  !     *                           real(8).
  !     * mar 24/1999 - j scinocca. original version rd in "COMM".
  !
  !     * numerical recipes routine used to evaluate incomplete
  !     * elliptic integrals. called by gwlooku2 through functions
  !     * ef kf
  !     * "PAUSE" statement is replaced with with a call to the
  !     * "XIT" subroutine.
  !     (c) copr. 1986-92 numerical recipes software #=! e=#,)]ubcj
  !
  implicit none
  real*8 :: rd3
  real*8 :: errtol
  real*8 :: tiny
  real*8 :: big
  real*8 :: c1
  real*8 :: c2
  real*8 :: c3
  real*8 :: c4
  real*8 :: c5
  real*8 :: c6
  real*8, intent(in) :: x !< Variable description\f$[units]\f$
  real*8, intent(in) :: y !< Variable description\f$[units]\f$
  real*8, intent(in) :: z !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  parameter (errtol = 0.05e0,tiny = 1.d-50,big = 4.5d42, &
                      c1 = 3.e0/14.e0,c2 = 1.e0/6.e0,c3 = 9.e0/22.e0,c4 = 3.e0/26.e0, &
                      c5 = 0.25e0 * c3,c6 = 1.5e0 * c4)

  real*8 :: alamb
  real*8 :: ave
  real*8 :: delx
  real*8 :: dely
  real*8 :: delz
  real*8 :: ea
  real*8 :: eb
  real*8 :: ec
  real*8 :: ed
  real*8 :: ee
  real*8 :: fac
  real*8 :: sqrtx
  real*8 :: sqrty
  real*8 :: sqrtz
  real*8 :: sum
  real*8 :: xt
  real*8 :: yt
  real*8 :: zt
  !======================================================================
  if (min(x,y) < 0.e0 .or. min(x + y,z) < tiny &
      .or. max(x,y,z) > big) then
    write(6,'(A)') ' INVALID ARGUMENTS IN RD'
    call                                       xit ('RD3', - 1)
  end if

  xt = x
  yt = y
  zt = z
  sum = 0.e0
  fac = 1.e0

1 continue
  sqrtx = sqrt(xt)
  sqrty = sqrt(yt)
  sqrtz = sqrt(zt)
  alamb = sqrtx * (sqrty + sqrtz) + sqrty * sqrtz
  sum = sum + fac/(sqrtz * (zt + alamb))
  fac = 0.25e0 * fac
  xt = 0.25e0 * (xt + alamb)
  yt = 0.25e0 * (yt + alamb)
  zt = 0.25e0 * (zt + alamb)
  ave = 0.2e0 * (xt + yt + 3.e0 * zt)
  delx = (ave - xt)/ave
  dely = (ave - yt)/ave
  delz = (ave - zt)/ave
  if (max(abs(delx),abs(dely),abs(delz)) > errtol)goto 1

  ea = delx * dely
  eb = delz * delz
  ec = ea - eb
  ed = ea - 6.e0 * eb
  ee = ed + ec + ec
  rd3 = 3.e0 * sum + fac * (1.e0 + ed * ( - c1 + c5 * ed - c6 * delz * ee) &
        + delz * (c2 * ee + &
        delz * ( - c3 * ec + delz * c4 * ea))) &
        /(ave * sqrt(ave))

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
