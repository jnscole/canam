!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oxp2e(ohphs,h2o2phs,o3phs,no3phs,hno3phs,nh3phs,nh4phs, &
                       ohrow,h2o2row,o3row,no3row,hno3row,nh3row,nh4row, &
                       pressg,shj,levox,ilev,il1,il2,ilg,ioxtyp)
  !
  !     * this routine interpolates the input oxidants fields to model
  !     * thermodynamic levels.
  !
  implicit none
  !
  !     * output arrays...
  !
  real, intent(out) , dimension(ilg,ilev) :: ohphs      !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: h2o2phs    !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: o3phs      !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: no3phs     !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: hno3phs    !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: nh3phs     !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,ilev) :: nh4phs     !< Variable description\f$[units]\f$
  !
  !     * input
  !
  integer, intent(in) :: levox      !< Number of vertical layers for oxidant input data \f$[unitless]\f$
  integer, intent(in) :: ilev     !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: il1      !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2      !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg      !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioxtyp !< Variable description\f$[units]\f$
  !
  real, intent(in) , dimension(ilg,levox) :: ohrow      !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: h2o2row    !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: o3row      !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: no3row     !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: hno3row    !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: nh3row     !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,levox) :: nh4row     !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: pressg             !< Surface pressure \f$[Pa]\f$
  real, intent(in) , dimension(ilg,ilev) :: shj           !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !     * local work space
  !     * ya and ym are dimensioned with number of oxidants, so parameter statement
  !     * nox must always reflect this.
  !     * nox(1-7) is ordered for {oh,h2o2,o3,no3,hno3,nh3,nh4}

  integer, parameter :: nox=7 !<
  integer :: i !<
  integer :: k !<
  integer :: l !<
  integer :: lstart !<
  real, dimension(ilg,nox) :: ya !<
  real, dimension(ilg,nox) :: ym !<
  real, dimension(ilg,nox) :: slope !<
  real, dimension(ilg,nox) :: tercep !<
  real, dimension(ilg) :: xa !<
  real, dimension(ilg) :: xm !<
  real, dimension(ilg) :: ploc !<
  real, dimension(ilg) :: ris !<
  real, dimension(200) :: pref !<
  !
  !     * pressure-level data for ioxtyp=1.
  !
  real, parameter, dimension(19) :: pref1 = (/ 100.,    500.,     1000.,   2000.,   3000.,  &
                                               5000.,   7000.,    10000.,  15000.,  20000., &
                                               25000.,  30000.,   40000.,  50000.,  60000., &
                                               70000.,  85000.,   92500.,  100000. /) !<
  !----------------------------------------------------------------------
  !
  !     * define working pressure level reference vertical grid, based on
  !     * choice of ioxtyp.
  !
  if (ioxtyp==1) then
    if (levox/=19) then
      print *, '0WRONG VALUE FOR LEVOX !: IOXTYP,LEVOX = ',IOXTYP,LEVOX
      call xit('OXVINT',-1)
    end if
    !
    do k=1,19
      pref(k)=pref1(k)
    end do
  else
    call xit('OXVINT',-2)
  end if
  !
  !     * locate indices for interpolation at full pressure levels.
  !
  lstart = 1
  do l = 1, ilev
    do i = il1, il2
      ris(i) = 0.
    end do ! loop 120

    do k = lstart, levox
      do i = il1, il2
        ploc(i)=shj(i,l)*pressg(i)
        if (pref(1)>=ploc(i).and.ris(i)==0.) then
          ris(i)=1.
          xa (i)=pref(2)
          xm (i)=pref(1)
          ya (i,1)=ohrow(i,1)
          ym (i,1)=ohrow(i,1)
          ya (i,2)=h2o2row(i,1)
          ym (i,2)=h2o2row(i,1)
          ya (i,3)=o3row(i,1)
          ym (i,3)=o3row(i,1)
          ya (i,4)=no3row(i,1)
          ym (i,4)=no3row(i,1)
          ya (i,5)=hno3row(i,1)
          ym (i,5)=hno3row(i,1)
          ya (i,6)=nh3row(i,1)
          ym (i,6)=nh3row(i,1)
          ya (i,7)=nh4row(i,1)
          ym (i,7)=nh4row(i,1)
        end if
        if (pref(k)>=ploc(i).and.ris(i)==0. ) then
          ris(i)=real(k)
          xa (i)=pref(k)
          xm (i)=pref(k-1)
          ya (i,1)=ohrow(i,k)
          ym (i,1)=ohrow(i,k-1)
          ya (i,2)=h2o2row(i,k)
          ym (i,2)=h2o2row(i,k-1)
          ya (i,3)=o3row(i,k)
          ym (i,3)=o3row(i,k-1)
          ya (i,4)=no3row(i,k)
          ym (i,4)=no3row(i,k-1)
          ya (i,5)=hno3row(i,k)
          ym (i,5)=hno3row(i,k-1)
          ya (i,6)=nh3row(i,k)
          ym (i,6)=nh3row(i,k-1)
          ya (i,7)=nh4row(i,k)
          ym (i,7)=nh4row(i,k-1)
        end if
        if (pref(levox)<=ploc(i).and.ris(i)==0. ) then
          ris(i)=real(levox)
          xa (i)=pref(levox)
          xm (i)=pref(levox-1)
          ya (i,1)=ohrow(i,levox)
          ym (i,1)=ohrow(i,levox)
          ya (i,2)=h2o2row(i,levox)
          ym (i,2)=h2o2row(i,levox)
          ya (i,3)=o3row(i,levox)
          ym (i,3)=o3row(i,levox)
          ya (i,4)=no3row(i,levox)
          ym (i,4)=no3row(i,levox)
          ya (i,5)=hno3row(i,levox)
          ym (i,5)=hno3row(i,levox)
          ya (i,6)=nh3row(i,levox)
          ym (i,6)=nh3row(i,levox)
          ya (i,7)=nh4row(i,levox)
          ym (i,7)=nh4row(i,levox)
        end if
      end do ! loop 130
    end do ! loop 140
    !
    !         * linear interpolation (volume mixing ratio)
    !
    lstart=levox
    do i=il1,il2
      lstart = min(lstart, nint(ris(i)))
      slope  (i,1) = (ya(i,1) - ym(i,1)) / (xa(i) - xm(i))
      slope  (i,2) = (ya(i,2) - ym(i,2)) / (xa(i) - xm(i))
      slope  (i,3) = (ya(i,3) - ym(i,3)) / (xa(i) - xm(i))
      slope  (i,4) = (ya(i,4) - ym(i,4)) / (xa(i) - xm(i))
      slope  (i,5) = (ya(i,5) - ym(i,5)) / (xa(i) - xm(i))
      slope  (i,6) = (ya(i,6) - ym(i,6)) / (xa(i) - xm(i))
      slope  (i,7) = (ya(i,7) - ym(i,7)) / (xa(i) - xm(i))
      !
      tercep (i,1) = ym(i,1) - slope(i,1) * xm(i)
      tercep (i,2) = ym(i,2) - slope(i,2) * xm(i)
      tercep (i,3) = ym(i,3) - slope(i,3) * xm(i)
      tercep (i,4) = ym(i,4) - slope(i,4) * xm(i)
      tercep (i,5) = ym(i,5) - slope(i,5) * xm(i)
      tercep (i,6) = ym(i,6) - slope(i,6) * xm(i)
      tercep (i,7) = ym(i,7) - slope(i,7) * xm(i)
      !
      ohphs  (i,l) = slope(i,1) * ploc(i) + tercep(i,1)
      h2o2phs(i,l) = slope(i,2) * ploc(i) + tercep(i,2)
      o3phs  (i,l) = slope(i,3) * ploc(i) + tercep(i,3)
      no3phs (i,l) = slope(i,4) * ploc(i) + tercep(i,4)
      hno3phs(i,l) = slope(i,5) * ploc(i) + tercep(i,5)
      nh3phs (i,l) = slope(i,6) * ploc(i) + tercep(i,6)
      nh4phs (i,l) = slope(i,7) * ploc(i) + tercep(i,7)
    end do ! loop 160
  end do ! loop 180
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
