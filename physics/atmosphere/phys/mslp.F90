!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine mslp(pmsl,tl,pressg,phis,shjl,ilg,il1,il2)
  !
  !     * july 27, 2005 - m.lazare.
  !
  !     * computes m.s.l. pressure (in mbs).
  !     * the code is pulled from the diagnostic calculation to maintain
  !     * consistency (program gsmslph called by deck mslpr_eta).
  !
  !     * we assume a uniform lapse rate of (dt/dz)=-gam, from reference
  !     * level (lowest layer) to the surface, and then from the surface to sea level.
  !     * the hydrostatic equation and constant-lapse rate equation are
  !     * used first to determine "SURFACE TEMPERATURE".
  !
  implicit none
  real :: asq
  real :: cpres
  real :: cpresv
  real :: fact
  real :: gam
  real :: gamrgog
  real :: grav
  integer :: il
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  real :: rayon
  real :: rgas
  real :: rgasv
  real :: rgoasq
  real :: rgocp
  real :: tbar
  real :: ts
  real :: tw
  real :: ww
  !
  real, intent(inout), dimension(ilg) :: pmsl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg) :: phis !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: shjl   !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  common /params/ ww,tw,rayon,asq,grav,rgas,rgocp,rgoasq,cpres
  common /params/ rgasv,cpresv
  !
  data gam/0.0065/
  !==========================================================================
  gamrgog = gam * rgas/grav
  fact    = 0.5 * gam /grav
  do il = il1,il2
    ts       = tl(il) / shjl(il) ** gamrgog
    tbar     = ts + fact * phis(il)
    pmsl(il) = 0.01 * pressg(il) * exp(phis(il)/(rgas * tbar))
  end do ! loop 200
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
