!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine bcconc(zsnow,gcrow,bcsno,depb,spcp,rofn,rhosno, &
                  delt,zsnmin,zsnmax,ilg,il1,il2,gcmin,gcmax)

  !     * feb 10/2015 - j.cole.   new version for gcm18:
  !                               - truncate bc concentrations to
  !                                 avoid excessively large values
  !                                 that may occur under special
  !                                 circumstances given the simplicity
  !                                 of the current bc/snow parameterization.
  !                                 this will ensure that subsequent
  !                                 albedo calculations will be
  !                                 numerically robust.
  !     * nov 15/2013 - m.lazare. previous version for gcm17.
  !     *                         - no SNOW (wasn't being used).
  !     *                         - two if branches combined into one
  !     *                           for zsnow>zsnmin.
  !     *                         - k decreased from 0.5 to 0.1 (tuning).
  !     *
  !     * aug 06/2013 - m. namazi &  k. v. salzen.
  !     *              bc concentration in snow due to deposition
  !     *              of bc and scavenging through melting.

  implicit none

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !< Variable description\f$[units]\f$
  integer :: j !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bcsno !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: depb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: spcp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rhosno !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zsnow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rofn !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real, intent(inout)  :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real  :: coeff1
  real  :: coeff2
  real, intent(inout)  :: zsnmin
  real, intent(inout)  :: zsnmax
  real, intent(inout)  :: gcmin
  real, intent(inout)  :: gcmax
  real  :: zsn
  !
  real, parameter :: k = 1.e-1
  real, parameter :: eps1 = 1.e-5
  real, parameter :: delm = 1.
  !--------------------------------------------------------------------
  do i = il1,il2
    if (gcrow(i) > gcmin .and. gcrow(i) < gcmax) then
      if (zsnow(i) > zsnmin) then
        zsn = min(zsnow(i),zsnmax)
        coeff1 = zsn + delt * spcp(i)/rhosno(i)
        bcsno(i) = bcsno(i) * zsn + delt * depb(i)
        bcsno(i) = bcsno(i)/coeff1
        coeff2 = rhosno(i) * zsn - (1 - k) * delt * delm * rofn(i)
        bcsno(i) = rhosno(i) * zsn * bcsno(i)/coeff2
        !
        !             * truncate bc concentrations to avoid excessively large values
        !             * that may occur under special circumstances given the simplicity
        !             * of the current bc/snow parameterization.
        !             * this will ensure that subsequent albedo calculations will be
        !             * numerically robust.
        !
        bcsno(i) = min(bcsno(i),1.)
      else
        bcsno(i) = 0.
      end if
    end if
  end do ! loop 100

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
