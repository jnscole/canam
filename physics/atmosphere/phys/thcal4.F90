!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine thcal4 (tsh,tshb, tg,gt, sh,sht,shxkj, &
                   ilev,lev, ilg,is,if)
  !
  !     * jan 05/2003 - m.lazare. unused "IRAD" removed from call.
  !     * mar 22/2001 - m.lazare. new version for gcmiv onward.
  !     *                         like thcal3 except tshb(1)=tsh(1)=tsh(2),
  !     *                         i.e. no extrapolation. consequently,
  !     *                         "SIG1" is no longer needed and all
  !     *                         references to the "LWCONS" common block
  !     *                         is removed.
  !     * may 20/97 - m.lazare.   previous version thcal3 for gcmiii.
  !     * sep 23/88 - m.lazare.   previous model version thcal2 for gcmii.
  !
  !     * set temperature arrays for physics on thermodynamics levels
  !     * for hybrid version of model.
  !     * shb are thermodynamics layer interfaces,
  !     * sh  are thermodynamics mid layer positions.
  !     * lev = ilev+1.
  !     *
  !     *        thermodynamics
  !     *     interface   mid layer  tg(model) tsh(th)     tshb(tf)
  !     *
  !     *        sigma=0. ////////////////////////////////////////////////
  !     *                  moon layer          tsh(1)
  !     * sht(1)=thermo.top ------------------------------ tshb(1)
  !     *                  sh(1)     tg(1)     tsh(2)
  !     * sht(2)=shb(1) ---------------------------------- tshb(2)
  !     *                  sh(2)     tg(2)     tsh(3)
  !     * sht(3)=shb(2) ---------------------------------- tshb(3)
  !     *                  sh(3)     tg(3)     tsh(4)
  !     * sht(4)=shb(3) ---------------------------------- tshb(4)
  !     *                  sh(4)     tg(4)     tsh(5)
  !     * sht(5)=shb(4) ---------------------------------- tshb(5)
  !     *                  sh(5)     tg(5)     tsh(6)
  !     * sht(6)=shb(5)=1. /////////////////////////////// tshb(6)

  implicit none
  integer :: i
  integer, intent(inout) :: if
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: is
  integer :: l
  integer, intent(inout) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$

  real, intent(in), dimension(ilg,ilev) :: tg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg, lev) :: tsh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg, lev) :: tshb !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sh !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg, lev) :: sht !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shxkj !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !     * mid layer temperatures, with extra one at top.
  !
  do l = 1,ilev
    do i = is,if
      tsh(i,l + 1) = tg(i,l)
    end do
  end do ! loop 100
  !
  !     * layer interface temperatures, with extra one at top.
  !     * temperature discontinuity at the ground based on adiabatic
  !     * extrapolation to surface under unstable conditions only, i.e.
  !     * for these cases most of the superadiabatic lapse rate is
  !     * contained very close to the surface.
  !
  do i = is,if
    tshb(i,lev) = min( tsh(i,lev)/shxkj(i,ilev), gt(i) )
  end do ! loop 200
  !
  do l = 1,ilev - 1
    do i = is,if
      tshb(i,l + 1) = (tsh(i,l + 1) * log(sh (i,l + 1)/sht(i,l + 1)) &
                      + tsh(i,l + 2) * log(sht(i,l + 1)/sh (i  ,l))) &
                      /log(sh (i,l + 1)/sh (i  ,l))
    end do
  end do ! loop 300
  !
  !     * top layer base and moon layer temperature.
  !
  do i = is,if
    tshb(i,1) = tsh(i,2)
    tsh(i,1) = tsh(i,2)
  end do ! loop 400
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
