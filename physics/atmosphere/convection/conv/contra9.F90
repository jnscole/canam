!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine contra9 (x,xu,xd,dxdt,dudt,dvdt,xga,sumdx,cldcv,wrk, &
                    ideep,duo,euo,edo,mu,md,mdo,ql,q,t,p,pfg,dz, &
                    dp,pressg,mb,eps0,jb,jt,mx,jd,itrphs,itrcch, &
                    xsrc,xsnk,phdrow,dsr,ntrac, &
                    ntraca,iu,iv,inh3,ihno3, &
                    io3,ico2,iso2,iso4,ihpo,issa,issc,idua,iduc, &
                    c0fac,eps1,rgas,grav,rog,ilg,ilev,il1g,il2g, &
                    msg,dt,neqp,isvchem,ipam,lstag,pstag,pfstag)
  !-----------------------------------------------------------------------
  !     * feb 01/2018 - m.lazare.    bugfixes to handle cases where euo~duo.
  !     * feb 10/2015 - k.vonsalzen. new routine for gcm18:
  !     *                            - scavenging now allowed for so2.
  !     * jun 20/2013 - k.vonsalzen/ previous version contra8 for gcm17:
  !     *               m.lazare.    - add support for pla.
  !     *                            - cosmetic cleanup of loop numbers.
  !     *                            - remove "ISULF", including call.
  !     * apr 30/2012 - m.lazare.    previous version contra7 for gcm16:
  !     *                            - ccu,ccd decreased from 0.55 to zero.
  !     *                            - calls new xtend5.
  !     * apr 23/2010 - m.lazare/    previous version contra6 for gcm15i:
  !     *               k.vonsalzen. - calls new xtend4.
  !     *                            - explicit source/sink terms.
  !     *                            - revised diagnostic fields.
  !     * jul 15/2009 - m.lazare/    bugfix to tracer dimension for
  !     *               k.vonsalzen. input array "X" (ntrac instead of
  !     *                            ntraca). cosmetic only for non-scm
  !     *                            because we pass by address not value.
  !     * feb 16/2009 - m.lazare/    previous version contra5 for gcm15h:
  !     *               k.vonsalzen. accuracy restriction on calculation
  !     *                            of facv to handle 0/0 unusual case.
  !     * dec 18/2007 - m.lazare/    previous version contra4 for gcm15g:
  !     *               k.vonsalzen. add calculations for diagnostic field
  !     *                            wdd4row.
  !     * nov 23/2006 - m.lazare/    previous version contra3 for gcm15f:
  !     *               k.vonsalzen. - various changes to avoid pathalogical
  !     *                              cases for small values in 32-bit
  !     *                              mode, particularily in definition of
  !     *                              "ATMP".
  !     * jul 30/2006 - m.lazare.    - c0fac now passed in from convection
  !     *                              driver, to ensure consistent useage
  !     *                              in cldprp and contra.
  !     * jun 15/2006 - m.lazare.    - "ZERO","ONE","OPEM1","OPEM10"
  !     *                              data constants added and used in
  !     *                              calls to intrinsics.
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     *                            - calls revised xtend3.
  !     * dec 13/2005 - k. von salzen. previous version contra2 for gcm15d/e:
  !     *                              - adds code for momentum, including
  !     *                                calling revised xtend2 and passing
  !     *                                in edo from new cldprp9.
  !     * may 14/2004 - m.lazare/      previous version contra for gcm15b/c.
  !     *               k. von salzen.
  !     * sep 18/2000 - k. von salzen. original version based on narcm code.
  !-----------------------------------------------------------------------
  implicit none
  real :: aco2
  real :: aconvf
  real :: acort
  real :: adelta
  real :: aexp
  real :: af1
  real :: af2
  real :: afakt
  real :: afcl
  real :: afco3
  real :: afhso
  real :: afnh4
  real :: afno3
  real :: afoh
  real :: afrah
  real :: afras
  real :: afs
  real :: agtco2
  real :: agthcl
  real :: agthno3
  real :: agtnh3
  real :: agtso2
  real :: agtso4
  real :: ahno3
  real :: ai
  real :: amcl
  real :: amh
  real :: amhcl
  real :: amnh4
  real :: amno3
  real :: amr2m
  real :: amso3
  real :: amso4
  real :: amssani
  real :: amsscat
  real :: amsscl
  real :: amssso4
  real :: amuo
  real :: anh3
  real :: ap1
  real :: ap2
  real :: asrco2
  real :: asrhno
  real :: asrhpo
  real :: asrnh3
  real :: asro3
  real :: asrphob
  real :: asrso2
  real :: asrso4
  real :: assmr
  real :: atmp
  real :: atval
  real :: atvo3
  real :: aw
  real :: beta
  real :: bi
  real :: bw
  real :: c0
  real, intent(in) :: c0fac
  real :: ccd
  real :: ccu
  real, intent(in) :: dt
  real, intent(in) :: eps1
  real :: entwgt1, entwgt2, velwgt1, velwgt2
  real :: fac
  real :: fact
  real, intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  real :: hpot
  integer, intent(in) :: ico2  !< Tracer index for carbon dioxide \f$[unitless]\f$
  integer, intent(in) :: idua  !< Tracer index for dust (accumulation mode) \f$[unitless]\f$
  integer, intent(in) :: iduc  !< Tracer index for dust (coarse mode) \f$[unitless]\f$
  integer, intent(in) :: ihno3
  integer, intent(in) :: ihpo  !< Tracer index for hydrogen peroxide \f$[unitless]\f$
  integer :: il
  integer, intent(in) :: il1g  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2g  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: inh3
  integer, intent(in) :: io3
  integer, intent(in) :: ipam  !< Switch to use PAM or bulk aerosol scheme (0=bulk. 1=PAM) \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: iso4
  integer, intent(in) :: issa  !< Tracer index for sea salt (accumulation mode) \f$[unitless]\f$
  integer, intent(in) :: issc  !< Tracer index for sea salt (coarse mode) \f$[unitless]\f$
  integer, intent(in) :: isvchem  !< Switch to save extra chemistry diagnostics (0 = don't save, 1 = save) \f$[unitless]\f$
  integer, intent(in) :: iu
  integer, intent(in) :: iv
  integer :: j
  integer :: l
  integer, intent(in) :: msg
  integer :: n
  integer, intent(in) :: neqp
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(in) :: ntraca    !< Total number of tracers in convection (sulfur cycle, convective momentum drag plus prognostic tracers) \f$[unitless]\f$
  real :: one
  real :: opem1
  real :: opem10
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: rog
  real :: rufac
  real :: scat
  real :: slp
  real :: so2t
  real :: so4a
  real :: so4t
  real :: t2s
  real :: tfreez
  real :: ycom3l
  real :: yeps
  real :: yrhow
  real :: ysmall
  real :: ysmass
  real :: zero
  real :: zrhoh
  real :: zrhol
  !
  !     * output arrays.
  !

  real, intent(inout)  , dimension(ilg,ilev,ntrac)  :: xu !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg,ilev,ntrac)  :: xd !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg,ilev,ntrac)  :: dxdt !< Variable description\f$[units]\f$

  real, intent(inout)  , dimension(ilg,ntrac)       :: sumdx !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg,ntrac)       :: xsrc !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg,ntrac)       :: xsnk !< Variable description\f$[units]\f$

  real, intent(in)  , dimension(ilg,ilev)        :: dudt !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: dvdt !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg,ilev)        :: phdrow !< Variable description\f$[units]\f$

  real, intent(in)  , dimension(ilg)             :: wrk !< Variable description\f$[units]\f$

  !
  !     * input tracer fields.
  !
  real, intent(in)  , dimension(ilg,ilev,ntrac) :: x !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev,ntraca) :: xga !< Variable description\f$[units]\f$
  !
  !     * other input fields.
  !

  real, intent(in)  , dimension(ilg,ilev)        :: duo !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: euo !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: dsr !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: edo !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: pfg !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: mdo !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: ql !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: q !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: p !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: dp !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: cldcv !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: md !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: t !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)        :: mu !< Variable description\f$[units]\f$
  real, intent(inout) , dimension(ilg,ilev)        :: pstag !< Variable description\f$[units]\f$
  real, intent(inout) , dimension(ilg,ilev)        :: pfstag !< Variable description\f$[units]\f$

  real, intent(in)  , dimension(ilg)             :: mb !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg)             :: eps0 !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg)             :: pressg   !< Surface pressure \f$[Pa]\f$

  integer, intent(in),dimension(ilg)             :: jb !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg)             :: jt !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg)             :: mx !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg)             :: jd !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg)             :: ideep !< Variable description\f$[units]\f$

  integer, intent(in),dimension(ntrac)           :: itrphs !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ntrac)           :: itrcch   !< Switches to indicate application of convective chemistry to tracer \f$[unitless]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================



  !
  !     * internal work arrays.
  !
  real  , dimension(ilg,ilev,ntrac)  :: sc
  real  , dimension(ilg,ilev,ntrac)  :: ru
  real  , dimension(ilg,ilev,ntrac)  :: xf
  real  , dimension(ilg,ilev,ntrac)  :: wrkx
  real  , dimension(ilg,ilev,ntraca) :: xua
  real  , dimension(ilg,ilev,ntraca) :: xda
  real  , dimension(ilg,ilev,ntraca) :: rua
  real  , dimension(ilg,ilev,ntraca) :: xfa
  real  , dimension(ilg,ilev,ntraca) :: wrkxa
  real  , dimension(ilg,ilev,neqp)   :: achpa
  real  , dimension(ilg,ilev)        :: alwcvmr
  real  , dimension(ilg,ilev)        :: wrks
  real  , dimension(ilg)             :: amhval
  real  , dimension(ilg)             :: facv
  real  , dimension(ilg)             :: atmpv
  real  , dimension(ilg)             :: c0v
  real  , dimension(ilg)             :: mbc
  real  , dimension(ilg)             :: ambcor
  !
  logical :: kchem
  logical :: krit1
  logical :: krit2
  logical, intent(inout) :: lstag  !< Switch to indicate staggered grid in physics (.true. = staggered, .false. = unstaggered) \f$[unitless]\f$
  !
  !     * parameters.
  !
  parameter (ycom3l = 1.e+03)
  parameter (yeps   = 1.e-20)
  parameter (yrhow  = 1.e+03)
  parameter (ysmall = 1.e-33)
  parameter (ysmass = 32.06e-03)
  parameter (beta   = .5)
  parameter (ccu = 0.)
  parameter (ccd = 0.)
  !
  !     * common declarations.
  !
  common /htcp  / tfreez,t2s,ai,bi,aw,bw,slp
  !
  data zero    / 0.     /
  data one     / 1.     /
  data opem1   / 1.e-1  /
  data opem10  / 1.e-10 /
  !-----------------------------------------------------------------------
  kchem = .true.
  asrso4 = 0.9
  asrphob = 0.
  !
  !     * invoke cfl to limit cloud base mass flux. this ensures
  !     * stability of the explicit calculations of tracer tendencies
  !     * in subroutine xtend3.
  !
  do il = il1g,il2g
    mbc(il) = mb(il)
    ambcor(il) = 0.
  end do ! loop 10
  do l = msg + 2,ilev
    do il = il1g,il2g
      if ((mu(il,l) + md(il,l)) /= 0.) then
        mbc(il) = min( mbc(il) , mb(il) * min(dp(il,l)/dsr(il,l), &
                  dp(il,l - 1)/dsr(il,l - 1)) &
                  /(dt * abs(mu(il,l) + md(il,l))) )
      end if
    end do
  end do ! loop 15
  do il = il1g,il2g
    if (mb(il) > 0. ) then
      ambcor(il) = mbc(il)/mb(il)
    end if
  end do ! loop 20
  !
  !     * initialization of work fields.
  !     * initialize updraft and downdraft properties.
  !
  do n = 1,ntrac
    do l = msg + 1,ilev
      do il = il1g,il2g
        ru(il,l,n) = 0.
        sc(il,l,n) = 0.
        xu(il,l,n) = x(il,l,n)
        xd(il,l,n) = x(il,l,n)
      end do
    end do
  end do ! loop 25
  !
  ! initialize work array for use in staggering
  !
  do n=1,ntraca
    do l=msg+1,ilev
      do il=il1g,il2g
        wrkxa(il,l,n) = 0.0
      enddo
    enddo
  enddo
  !
  !     * set cloud base updraft properties.
  !
  do n = 1,ntrac
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        xu(il,jb(il),n) = x(il,mx(il),n)
      end if
    end do
  end do ! loop 30
  !
  !     * calculate updraft properties for passive tracers.
  !
  do n = 1,ntrac
    if (itrphs(n) > 0 .and. itrcch(n) == 0) then
      do j = ilev - 1,msg + 1, - 1
        do il = il1g,il2g
          if (eps0(il) > 0. .and. (j > jt(il) .and. j < jb(il)) .and. &
              mu(il,j + 1) /= 0. ) then
            if (euo(il,j) <= 10. * abs(euo(il,j) - duo(il,j))) then
              aexp = euo(il,j)/(duo(il,j) - euo(il,j))
              atmp = (abs(mu(il,j)/mu(il,j + 1))) ** aexp
            else
              atmp = exp( - mb(il) * euo(il,j) * dp(il,j)/mu(il,j + 1))
            end if
            xu(il,j,n) = x(il,j,n) + (xu(il,j + 1,n) - x(il,j,n)) * atmp
          end if
        end do
      end do
    end if
  end do
  !
  if (ipam == 0) then
    do l = msg + 1,ilev
      do il = il1g,il2g
        alwcvmr(il,l) = 0.
      end do
    end do
  end if
  !
  do n = 1,ntraca
    do l = msg + 1,ilev
      do il = il1g,il2g
        rua(il,l,n) = 0.
        xua(il,l,n) = xga(il,l,n)
        xda(il,l,n) = xga(il,l,n)
      end do
    end do
  end do ! loop 40
  do n = 1,ntraca
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        xua(il,jb(il),n) = xga(il,mx(il),n)
      end if
    end do
  end do ! loop 160
  do n = iu,iv
    do j = ilev - 1,msg + 1, - 1
      do il = il1g,il2g
        if (eps0(il) > 0. .and. (j > jt(il) .and. j < jb(il)) .and. &
            mu(il,j + 1) /= 0. ) then
          !
          !          * unperturbed wind in updraft.
          !
          if (euo(il,j) <= 10. * abs(euo(il,j) - duo(il,j))) then
            aexp = euo(il,j)/(duo(il,j) - euo(il,j))
            atmp = (abs(mu(il,j)/mu(il,j + 1))) ** aexp
          else
            if(lstag) then
              entwgt1=euo(il,j+1)*(pstag(il,j+1)-pfstag(il,j+1))
              entwgt2=euo(il,j)*(pfstag(il,j+1)-pstag(il,j))
              atmp=exp(-mb(il)*(entwgt1/mu(il,j+1)+entwgt2/mu(il,j)))
            else
              atmp=exp(-mb(il)*euo(il,j)*dp(il,j)/mu(il,j+1))
            endif
          end if
          if(lstag) then
            velwgt1=(pstag(il,j+1)-pfstag(il,j+1))/(pstag(il,j+1)-pstag(il,j))
            velwgt2=(pfstag(il,j+1)-pstag(il,j))/(pstag(il,j+1)-pstag(il,j))
            wrkxa(il,j,n)=xga(il,j+1,n)*velwgt1 +xga(il,j,n)*velwgt2
            xua(il,j,n)=wrkxa(il,j,n)*(1.-atmp)+ xua(il,j+1,n)*atmp
          else
           xua(il,j,n)=xga(il,j,n)+(xua(il,j+1,n)-xga(il,j,n))*atmp
          endif
        endif
      end do
    end do
  end do ! loop 161
  do n = iu,iv
    do j = ilev - 1,msg + 1, - 1
      do il = il1g,il2g
        if (eps0(il) > 0. .and. (j > jt(il) .and. j < jb(il)) .and. mu(il,j + 1) /= 0. ) then
          if (euo(il,j) <= 10. * abs(euo(il,j) - duo(il,j))) then
            aexp = euo(il,j)/(duo(il,j) - euo(il,j))
            atmp = (abs(mu(il,j)/mu(il,j + 1))) ** aexp
          else
            if(lstag) then
              entwgt1=euo(il,j+1)*(pstag(il,j+1)-pfstag(il,j+1))
              entwgt2=euo(il,j)*(pfstag(il,j+1)-pstag(il,j))
              atmp=exp(-mb(il)*(entwgt1/mu(il,j+1)+entwgt2/mu(il,j)))
            else
              atmp=exp(-mb(il)*euo(il,j)*dp(il,j)/mu(il,j+1))
            endif
          endif
          !
          !         * wind in updraft from pressure gradient force perturbation.
          !
          !*
          !* STAGGERED VERSION
          !*
          if(lstag) then
           velwgt1=(pstag(il,j+1)-pfstag(il,j+1))/(pstag(il,j+1)-pstag(il,j))
           velwgt2=(pfstag(il,j+1)-pstag(il,j))/(pstag(il,j+1)-pstag(il,j))
           wrkxa(il,j,n)=xga(il,j+1,n)*velwgt1 +xga(il,j,n)*velwgt2
           xua(il,j,n)=ccu*wrkxa(il,j,n)+(1.-ccu)*xua(il,j,n)
          else
            xua(il,j,n) = ccu * xga(il,j,n) + (1. - ccu) * xua(il,j,n)
          endif
        end if
      end do
    end do
  end do ! loop 162
  !
  if (kchem .and. ipam == 0) then
    !
    !       *** COMPUTE HENRY'S LAW CONSTANTS.
    !
    call oequip2(ilg,ilev,neqp,msg + 1,il1g,il2g,achpa,t)
    !
    !       *** liquid water volume mixing ratios.
    !
    do l = ilev,msg + 2, - 1
      do il = il1g,il2g
        zrhol = 100. * p(il,l)/(rgas * t(il,l))
        zrhoh = 100. * p(il,l - 1)/(rgas * t(il,l - 1))
        if (eps0(il) > 0. .and. (l > jt(il) &
            .and. l < jb(il)) .and. mu(il,l) > 0. &
            .and. (ql(il,l) > 1.e-09) &
            .and. (cldcv(il,l) > 1.e-06) ) then
          alwcvmr(il,l) = ql(il,l) * .5 * (zrhol + zrhoh)/yrhow
        end if
      end do
    end do ! loop 170
  end if
  !
  !     * updraft mixing ratios of chemical species.
  !
  do l = ilev - 1,msg + 2, - 1
    do il = il1g,il2g
      amhval(il) = 0.
      atmpv(il) = 0.
      facv(il) = 0.
      c0v(il) = 0.
      if (l > jt(il) .and. l < jb(il) .and. eps0(il) > 0. &
          .and. mu(il,l) > 0. ) then
        if (euo(il,l) <= 10. * abs(euo(il,l) - duo(il,l))) then
          aexp = euo(il,l)/(duo(il,l) - euo(il,l))
          atmp = (abs(mu(il,l)/mu(il,l + 1))) ** aexp
        else
          atmp = exp( - mb(il) * euo(il,l) * dp(il,l)/mu(il,l + 1))
        end if
        afakt = 0.
        atmpv(il) = atmp
        if (euo(il,l) /= 0. ) then
          facv(il) = (1. - atmp)/euo(il,l)
        else
          facv(il) = 0.
        end if
        c0v(il) = 0.
        if (ipam == 0) then
          so2t = max(x(il,l,iso2) + (xu(il,l + 1,iso2) - x(il,l,iso2)) &
                 * atmp,zero)
          so4t = max(x(il,l,iso4) + (xu(il,l + 1,iso4) - x(il,l,iso4)) &
                 * atmp,zero)
          hpot = max(x(il,l,ihpo) + (xu(il,l + 1,ihpo) - x(il,l,ihpo)) &
                 * atmp,zero)
          afakt = cldcv(il,l) * dp(il,l)/(dsr(il,l) * max(mbc(il),ysmall) &
                  * dz(il,l))
          af1 = 0.
          af2 = 0.
          if (alwcvmr(il,l) > yeps .and. kchem) then
            !
            !              *** reaction rates af1 and af2 (in litre/mol/sec) and
            !              *** scavenging rates. cloud water ph and concentrations
            !              *** are obtained from linear inerpolation of the
            !              *** results at the level below and at the actual
            !              *** level under the assumption that in-cloud oxidation
            !              *** can be omitted in the calculation of the values at
            !              *** the actual level in a first approximation.
            !
            !              total (gas + aq. phase) concentrations.
            !              conversion kg-s/kg -> mol/l.
            !
            zrhoh = 100. * p(il,l)/(rgas * t(il,l))
            zrhol = 100. * p(il,l + 1)/(rgas * t(il,l + 1))
            amr2m = (beta * zrhol + (1. - beta) * zrhoh) &
                    /(ysmass * ycom3l)
            agtso2 = amr2m * ( beta * xu (il,l + 1,iso2) + (1. - beta) * so2t)
            agtso4 = amr2m * ( beta * xu (il,l + 1,iso4) + (1. - beta) * so4t)
            ahno3 = xga(il,l,ihno3) + (xua(il,l + 1,ihno3) - xga(il,l,ihno3) &
                    ) * atmp
            agthno3 = amr2m * ( beta * xua(il,l + 1,ihno3) + (1. - beta) * ahno3)
            anh3 = xga(il,l,inh3) + (xua(il,l + 1,inh3) - xga(il,l,inh3) &
                   ) * atmp
            agtnh3 = amr2m * ( beta * xua(il,l + 1,inh3) + (1. - beta) * anh3)
            aco2 = xga(il,l,ico2) + (xua(il,l + 1,ico2) - xga(il,l,ico2) &
                   ) * atmp
            agtco2 = amr2m * ( beta * xua(il,l + 1,ico2) + (1. - beta) * aco2)
            !
            !              sea salt species.
            !
            assmr = xu(il,l + 1,issa) + xu(il,l + 1,issc)
            aconvf = (beta * zrhol + (1. - beta) * zrhoh) &
                     /(alwcvmr(il,l) * ycom3l)
            !
            !            *** initial guess for ph.
            !
            !              initial molarities in cloud water.
            !
            amso3 = 0.                         ! so3(2-)
            amnh4 = agtnh3 /alwcvmr(il,l)      ! nh4(+)
            amno3 = agthno3/alwcvmr(il,l)      ! no3(-)
            amso4 = agtso4 /alwcvmr(il,l)      ! so4(2-)
            !
            !              calculate molarities of sea salt species assuming a typical
            !              composition of sea water. no anions other than sulphate and
            !              chloride are considered. all cations, except h+, are lumped
            !              together. the ph of the sea water is assumed to be 8.2.
            !              h+ is accounted for in the ion balance and in the calculation
            !              of hydrogen chlorine in the gas phase.
            !
            amssso4 = 0.077 * aconvf/96.058e-03 * assmr
            amsscl = 0.552 * aconvf/35.453e-03 * assmr
            amssani = 0.
            amsscat = (amsscl + 2. * amssso4 + amssani) * (1. - 1.048e-08)
            amso4 = amso4 + amssso4
            amcl = amsscl
            amhcl = amsscl * 6.31e-09/achpa(il,l,10)
            agthcl = (amsscl + amhcl) * alwcvmr(il,l)
            !              agthcl=amsscl*alwcvmr(il,l)
            !
            !              initial guess h+ molarity from initial ion molarities.
            !
            adelta = amno3 + 2. * (amso4 + amso3) + amcl + amssani - amnh4 &
                     - amsscat
            afoh = 1.e-14
            amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * (afoh &
                  + achpa(il,l,2) * agtso2 + achpa(il,l,5) * agtco2)) )
            amh = max(min(amh,opem1),opem10)
            !
            !              equilibrium parameters for soluble species.
            !
            atval = achpa(il,l,1) + achpa(il,l,2)/amh + achpa(il,l,3) &
                    /amh ** 2
            afras = 1./( 1. + alwcvmr(il,l) * atval)
            afnh4 = 1./( 1. + agtnh3/(1./achpa(il,l,9) &
                    + alwcvmr(il,l) * amh) )
            afno3 = agthno3/( 1./achpa(il,l,8) + alwcvmr(il,l)/amh)
            !
            !              first iteration h+ molarity calculation.
            !
            afcl = agthcl/( 1./achpa(il,l,10) + alwcvmr(il,l)/amh)
            afhso = agtso2 * afras * achpa(il,l,2)
            afco3 = agtco2 * achpa(il,l,5)
            amso3 = ( agtso2 * afras * achpa(il,l,3) )/amh ** 2
            adelta = afnh4 * ( 2. * (amso4 + amso3) + amssani - amsscat)
            amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * afnh4 * (afoh &
                  + afco3 + afhso + afno3 + afcl)) )
            amh = max(min(amh,opem1),opem10)
            atval = achpa(il,l,1) + achpa(il,l,2)/amh + achpa(il,l,3) &
                    /amh ** 2
            afras = 1./( 1. + alwcvmr(il,l) * atval)
            afnh4 = 1./( 1. + agtnh3/(1./achpa(il,l,9) &
                    + alwcvmr(il,l) * amh) )
            afno3 = agthno3/( 1./achpa(il,l,8) + alwcvmr(il,l)/amh)
            !
            !              second iteration h+ molarity calculation.
            !
            afcl = agthcl/( 1./achpa(il,l,10) + alwcvmr(il,l)/amh)
            afhso = agtso2 * afras * achpa(il,l,2)
            afco3 = agtco2 * achpa(il,l,5)
            amso3 = ( agtso2 * afras * achpa(il,l,3) )/amh ** 2
            adelta = afnh4 * ( 2. * (amso4 + amso3) + amssani - amsscat)
            amh = 0.5 * ( adelta + sqrt(adelta ** 2 + 4. * afnh4 * (afoh &
                  + afco3 + afhso + afno3 + afcl)) )
            amh = max(min(amh,opem1),opem10)
            atval = achpa(il,l,1) + achpa(il,l,2)/amh + achpa(il,l,3) &
                    /amh ** 2
            afras = 1./( 1. + alwcvmr(il,l) * atval)
            amhval(il) = amh
            !
            !              ozone oxidation parameter (in litre-air/mol/sec).
            !
            atvo3 = 1./( 1. + alwcvmr(il,l) * achpa(il,l,7) )
            af1 = ( achpa(il,l,11) + achpa(il,l,12)/amh) * afras &
                  * atval * atvo3 * achpa(il,l,7) * alwcvmr(il,l)
            !
            !              hydrogen peroxide oxidation parameter
            !              (in litre-air/mol/sec).
            !
            afrah = 1./( 1. + alwcvmr(il,l) * achpa(il,l,6) )
            af2 = ( achpa(il,l,13)/(0.1 + amh) ) * afras &
                  * achpa(il,l,1) * afrah * achpa(il,l,6) * alwcvmr(il,l)
            !
            !            *** conversion from litre/mol/sec to
            !            *** kg-air/kg-sulphur/sec.
            !
            af1 = af1 * amr2m
            af2 = af2 * amr2m
            !
            !            *** conversion to hpa/(sec*m*cloud_base_flx)*kg-air/kg-s,
            !            *** horizontally averaged in grid cell.
            !
            af1 = af1 * afakt
            af2 = af2 * afakt
            !
            !            *** scavenging ratios, i.e. fraction of tracer that
            !            *** is dissolved.
            !
            asrso2 = max( 1. - afras, zero)
            asrhpo = max( 1. - afrah, zero)
            asro3 = max( 1. - atvo3, zero)
            asrnh3 = max(alwcvmr(il,l) * amh/(1./achpa(il,l,9) &
                     + alwcvmr(il,l) * amh),zero)
            asrhno = max(alwcvmr(il,l)/(amh/achpa(il,l,8) &
                     + alwcvmr(il,l)),zero)
            asrco2 = max(alwcvmr(il,l) * achpa(il,l,5)/amh,zero)
            !
            !            *** calculate tracer scavenging proportional to conversion
            !            *** of cloud water into precipitation.
            !
            fac = max(t(il,jb(il)) - .5 * (t(il,l + 1) + t(il,l)),zero) &
                  /max(t(il,jb(il)) - tfreez, one)
            c0 = 0.
            if (mbc(il) > 0. ) &
                c0 = c0fac * min(fac ** 2, one) * mu(il,l)/mbc(il)
            c0v(il) = c0
            !
            !            *** scavenging rates.
            !
            ru (il,l,iso4) = asrso4 * c0
            ru (il,l,iso2) = asrso2 * c0
            ru (il,l,ihpo) = asrhpo * c0
            rua(il,l,inh3) = asrnh3 * c0
            rua(il,l,ihno3) = asrhno * c0
            rua(il,l,ico2) = asrco2 * c0
            rua(il,l,io3)  = asro3  * c0
          end if
          !
          !          *** updraft tracer concentrations for additional species.
          !
          xua(il,l,inh3) = (xga(il,l,inh3) + (xua(il,l + 1,inh3) - &
                           xga(il,l,inh3)) * atmp)/ &
                           (1. + rua(il,l,inh3) * facv(il))
          xua(il,l,ihno3) = (xga(il,l,ihno3) + (xua(il,l + 1,ihno3) - &
                            xga(il,l,ihno3)) * atmp)/ &
                            (1. + rua(il,l,ihno3) * facv(il))
          xua(il,l,ico2) = (xga(il,l,ico2) + (xua(il,l + 1,ico2) - &
                           xga(il,l,ico2)) * atmp)/ &
                           (1. + rua(il,l,ico2) * facv(il))
          xua(il,l,io3)  = (xga(il,l,io3) + (xua(il,l + 1,io3) - &
                           xga(il,l,io3)) * atmp)/ &
                           (1. + rua(il,l,io3) * facv(il))
          !
          !          *** updraft tracer concentrations for reactive species.
          !          *** no chemical sources/sinks are assumed for the
          !          *** pathological :: case that entrainment equals detrainment.
          !
          if ( ((af1 * xua(il,l,io3) + af2 * hpot) * so2t) > 0. &
              .and. (.5 * (t(il,l) + t(il,l + 1))) > 273. &
              .and. facv(il) /= 0. ) then
            ap1 = - (1. + facv(il) * (ru(il,l,iso2) + af1 * xua(il,l,io3) &
                  + af2 * (hpot - so2t)))/(2. * af2 * facv(il) * ( &
                  1. + facv(il) * (ru(il,l,iso2) + af1 * xua(il,l,io3))))
            ap2 = so2t/(af2 * facv(il) * ( &
                  1. + facv(il) * (ru(il,l,iso2) + af1 * xua(il,l,io3))))
            xu(il,l,iso2) = ap1 + sqrt(ap1 ** 2 + ap2)
            xu(il,l,ihpo) = hpot/(1. + (ru(il,l,ihpo) &
                            + af2 * xu(il,l,iso2)) * facv(il))
            so4a = xu(il,l,iso2) * (af1 * xua(il,l,io3) &
                   + af2 * xu(il,l,ihpo))/euo(il,l)
            xu(il,l,iso4) = (x(il,l,iso4) + so4a + (xu(il,l + 1,iso4) &
                            - x(il,l,iso4) - so4a) * atmp) &
                            /(1. + ru(il,l,iso4) * facv(il))
            fact = xu(il,l,iso2) * mbc(il) * dz(il,l) * dsr(il,l)/dp(il,l)
            sc(il,l,ihpo) = - af2 * xu(il,l,ihpo) * fact
            sc(il,l,iso2) = - (af1 * xua(il,l,io3) + af2 * xu(il,l,ihpo)) &
                            * fact
            sc(il,l,iso4) = - sc(il,l,iso2)
          else
            xu(il,l,iso2) = so2t/(1. + ru(il,l,iso2) * facv(il))
            xu(il,l,ihpo) = hpot/(1. + ru(il,l,ihpo) * facv(il))
            xu(il,l,iso4) = so4t/(1. + ru(il,l,iso4) * facv(il))
          end if
          !
          xu(il,l,iso2) = max(xu(il,l,iso2),zero)
          xu(il,l,iso4) = max(xu(il,l,iso4),zero)
          xu(il,l,ihpo) = max(xu(il,l,ihpo),zero)
        else                                               ! ipam
          fac = max(t(il,jb(il)) - .5 * (t(il,l + 1) + t(il,l)),zero) &
                /max(t(il,jb(il)) - tfreez, one)
          c0 = 0.
          if (mbc(il) > 0. ) then
            c0 = c0fac * min(fac ** 2, one) * mu(il,l)/mbc(il)
          end if
          c0v(il) = c0
        end if                                              ! ipam
      end if
    end do ! loop 240
    !
    !        *** updraft tracer concentrations for tracers that
    !        *** do not undergo chemical reactions but are affected
    !         *** by scavenging.
    !
    do n = 1,ntrac
      if (ipam == 0) then
        krit1 = itrcch(n) /= 0 .and. &
                (n /= iso2 .and. n /= iso4 .and. n /= ihpo)
        krit2 = itrcch(n) == - 1
      else
        krit1 = itrcch(n) /= 0
        krit2 = itrcch(n) == - 1 .or. n == ihpo
      end if
      if (krit1) then
        if (krit2) then
          rufac = asrphob
        else
          rufac = asrso4
        end if
        !
        do il = il1g,il2g
          if (l > jt(il) .and. l < jb(il) .and. eps0(il) > 0. &
              .and. mu(il,l) > 0. ) then
            ru(il,l,n) = rufac * c0v(il)
            xu(il,l,n) = (x(il,l,n) + (xu(il,l + 1,n) - x(il,l,n)) * atmpv(il))/ &
                         (1. + ru(il,l,n) * facv(il))
            xu(il,l,n) = max(xu(il,l,n),zero)
          end if
        end do ! loop 245
      end if
    end do ! loop 247
    !
    !      *** diagnostic calculations for ph.
    !
    if (isvchem /= 0 .and. ipam == 0) then
      do il = il1g,il2g
        if (amhval(il) > 1.e-20) then
          phdrow(ideep(il),l) = - log10(amhval(il))
        else
          phdrow(ideep(il),l) = 0.
        end if
      end do ! loop 250
    end if
  end do ! loop 260
  !
  !     *** convert scavenging rates to kg/kg/sec.
  !
  do n = 1,ntrac
    if (itrcch(n) /= 0) then
      do l = msg + 1,ilev
        do il = il1g,il2g
          ru(il,l,n) = ru(il,l,n) * mbc(il) * dz(il,l) * xu(il,l,n) * dsr(il,l) &
                       /dp(il,l)
        end do
      end do ! loop 280
    end if
  end do ! loop 285
  !
  !     *** flux of tracers into the downdraft due to evaporation
  !     *** of rain (units kg/m**2/sec).
  !
  do n = 1,ntrac
    if (itrcch(n) /= 0) then
      do l = ilev - 1,msg + 2, - 1
        do il = il1g,il2g
          if (l > jt(il) .and. l < jb(il) .and. eps0(il) > 0. &
              .and. mu(il,l) > 0.) then
            sumdx(il,n) = sumdx(il,n) + wrk(il) * ru(il,l,n) * 100. * dp(il,l) &
                          /(dsr(il,l) * grav)
          end if
        end do
      end do ! loop 300
    end if
  end do ! loop 320
  !
  !     *** tracer tendency in the subcloud-layer due to production
  !     *** in the downdraft (units kg/kg/sec).
  !
  do n = 1,ntrac
    if (itrcch(n) /= 0) then
      do il = il1g,il2g
        l = mx(il)
        sumdx(il,n) = sumdx(il,n) * grav * dsr(il,l)/(100. * dp(il,l))
      end do ! loop 350
    end if
  end do ! loop 360
  !
  !     * tracer downdraft properties.
  !
  do n = 1,ntrac
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        xd(il,jd(il),n) = x(il,jd(il) - 1,n)
      end if
    end do
  end do ! loop 910
  !
  do n = 1,ntrac
    if (itrphs(n) > 0) then
      do j = msg + 2,ilev
        do il = il1g,il2g
          if ((j > jd(il) .and. j <= jb(il)) .and. eps0(il) > 0. &
              .and. jd(il) < jb(il)) then
            xd(il,j,n) = x(il,j - 1,n) + (xd(il,j - 1,n) - x(il,j - 1,n)) &
                         * (mdo(il,j - 1)/mdo(il,j))
          end if
        end do
      end do ! loop 920
    end if
  end do ! loop 922
  do n = iu,iv
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        xda(il,jd(il),n) = xga(il,jd(il) - 1,n)
      end if
    end do
  end do ! loop 930
  !
  do n = iu,iv
    do j = msg + 2,ilev
      do il = il1g,il2g
        if ((j > jd(il) .and. j <= jb(il)) .and. eps0(il) > 0. &
            .and. jd(il) < jb(il)) then
          atmp = mdo(il,j)/mdo(il,j - 1)
          !
          !         * unperturbed wind in downdraft.
          !
          if(lstag) then
            velwgt1=(pstag(il,j)-pfstag(il,j))/(pstag(il,j)-pstag(il,j-1))
            velwgt2=(pfstag(il,j)-pstag(il,j-1))/(pstag(il,j)-pstag(il,j-1))
            wrkxa(il,j-1,n)=velwgt1*xga(il,j,n)+velwgt2*xga(il,j-1,n)
            xda(il,j,n)=wrkxa(il,j-1,n)*(1.-1./atmp)+xda(il,j-1,n)/atmp
          else
            xda(il,j,n)=xga(il,j-1,n)+(xda(il,j-1,n)-xga(il,j-1,n))/atmp
          endif
        end if
      end do
    end do
  end do ! loop 940
  do n = iu,iv
    do j = msg + 2,ilev
      do il = il1g,il2g
        if ((j > jd(il) .and. j <= jb(il)) .and. eps0(il) > 0. &
            .and. jd(il) < jb(il)) then
          atmp = mdo(il,j)/mdo(il,j - 1)
          !
          !         * wind in downdraft from pressure gradient force perturbation.
          !
          if(lstag) then
            velwgt1=(pstag(il,j)-pfstag(il,j))/(pstag(il,j)-pstag(il,j-1))
            velwgt2=(pfstag(il,j)-pstag(il,j-1))/(pstag(il,j)-pstag(il,j-1))
            wrkxa(il,j-1,n)=velwgt1*xga(il,j,n)+velwgt2*xga(il,j-1,n)
            xda(il,j,n)=wrkxa(il,j-1,n)*(1.-1./atmp)+xda(il,j-1,n)/atmp
          else
            xda(il,j,n) = ccd * xga(il,j - 1,n) + (1. - ccd) * xda(il,j,n)
          endif
        end if
      end do
    end do
  end do ! loop 941
  !
  !     * update tracer tendencies.
  !
  call xtend5(dudt,dvdt,xga,xfa,xua,xda,dxdt,x,xf,xu,xd, &
              sc,ru,mu,md,dsr,wrkx,wrkxa,sumdx,wrks,dp,pfg,p, &
              pressg,mbc,ambcor,jb,jt,mx,itrphs,dt,grav,rog, &
              ilg,ilev,il1g,il2g,msg,iu,iv,ntrac,ntraca, &
              lstag,pstag,pfstag)
  !
  !     * ensure positiveness by scaling of production and loss rates.
  !
  afs = 0.999999999
  do n = 1,ntrac
    do il = il1g,il2g
      sumdx(il,n) = 1.
    end do
  end do ! loop 520
  do n = 1,ntrac
    if (itrphs(n) > 0) then
      do l = msg + 1,ilev
        do il = il1g,il2g
          if (dxdt(il,l,n) < 0. ) then
            acort = - (afs * (x(il,l,n) + dxdt(il,l,n) * dt)) &
                    /(dxdt(il,l,n) * dt)
            sumdx(il,n) = max(min(sumdx(il,n),acort),zero)
          end if
        end do
      end do ! loop 526
    end if
  end do ! loop 527
  do n = 1,ntrac
    if (itrphs(n) > 0) then
      do l = msg + 1,ilev
        do il = il1g,il2g
          dxdt(il,l,n) = dxdt(il,l,n) * sumdx(il,n)
          ru  (il,l,n) = ru  (il,l,n) * sumdx(il,n)
          sc  (il,l,n) = sc  (il,l,n) * sumdx(il,n)
        end do
      end do ! loop 620
    end if
  end do ! loop 600
  !
  if (kchem) then
    !
    !      *** chemical production rates (in kg-s/m**2/sec).
    !
    xsrc(il1g:il2g,:) = 0.
    do n = 1,ntrac
      do l = msg + 1,ilev
        do il = il1g,il2g
          fact = 100. * dp(il,l)/(dsr(il,l) * grav)
          xsrc(il,n) = xsrc(il,n) + sc(il,l,n) * fact
        end do
      end do
    end do ! loop 370
    !
    !      *** wet deposition (in kg-s/m**2/sec).
    !
    xsnk(il1g:il2g,:) = 0.
    do n = 1,ntrac
      do l = ilev - 1,msg + 2, - 1
        do il = il1g,il2g
          if (l > jt(il) .and. l < jb(il) .and. eps0(il) > 0. &
              .and. mu(il,l) > 0. ) then
            fact = (1. - wrk(il)) * 100. * dp(il,l)/(dsr(il,l) * grav)
            xsnk(il,n) = xsnk(il,n) + ru(il,l,n) * fact
          end if
        end do
      end do
    end do ! loop 310
  end if
  !
  return
end

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
