!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine layer_rh_diag(rh,t,q,pressg,fmask,ilg,lay,il1,il2)
  !
  !     * mar 31, 2010 - j. cole
  !
  !     * calculates relative humidity based on input
  !     * temperature, specific humidity and surface pressure.
  !     * the formulae used here are consistent with that used else where
  !     * in the gcm physics.
  !     * this subroutine is effectively identical to screenrh except
  !     * it compute the relative humidity for each model layer
  !
  implicit none
  !
  !     * output field:
  !
  real, intent(inout),   dimension(ilg,lay)  :: rh !< Variable description\f$[units]\f$
  !
  !     * input fields.
  !
  real, intent(in),   dimension(ilg)      :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in),   dimension(ilg,lay)  :: t !< Variable description\f$[units]\f$
  real, intent(in),   dimension(ilg,lay)  :: q !< Variable description\f$[units]\f$
  real, intent(in),   dimension(ilg,lay)  :: fmask !< Variable description\f$[units]\f$

  real :: facte
  real :: epslim
  real :: fracw
  real :: etmp
  real :: estref
  real :: esat
  real :: qsw
  real :: a
  real :: b
  real :: eps1
  real :: eps2
  real :: t1s
  real :: t2s
  real :: ai
  real :: bi
  real :: aw
  real :: bw
  real :: slp
  real :: rw1
  real :: rw2
  real :: rw3
  real :: ri1
  real :: ri2
  real :: ri3
  real :: esw
  real :: esi
  real :: esteff
  real :: ttt
  real :: uuu
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  integer :: il
  integer :: l
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * common blocks for thermodynamic constants.
  !
  common /eps /  a,b,eps1,eps2
  common /htcp/  t1s,t2s,ai,bi,aw,bw,slp
  !
  !     * parameters used in new saturation vapour pressure formulation.
  !
  common /estwi/ rw1,rw2,rw3,ri1,ri2,ri3
  !
  !     * computes the saturation vapour pressure over water or ice.
  !
  esw(ttt)        = exp(rw1 + rw2/ttt) * ttt ** rw3
  esi(ttt)        = exp(ri1 + ri2/ttt) * ttt ** ri3
  esteff(ttt,uuu) = uuu * esw(ttt) + (1. - uuu) * esi(ttt)
  !========================================================================
  epslim = 0.001
  facte = 1./eps1 - 1.
  do l = 1, lay
    do il = il1,il2
      if (fmask(il,l) > 0.) then
        !
        !       * compute the fractional probability of water phase
        !       * existing as a function of temperature (from rockel,
        !       * raschke and weyres, beitr. phys. atmosph., 1991.)
        !
        fracw = merge( 1., &
                0.0059 + 0.9941 * exp( - 0.003102 * (t1s - t(il,l)) ** 2), &
                t(il,l) >= t1s)
        !
        etmp = esteff(t(il,l),fracw)
        estref = 0.01 * pressg(il) * (1. - epslim)/(1. - epslim * eps2)
        if (etmp < estref) then
          esat = etmp
        else
          esat = estref
        end if
        !
        qsw = eps1 * esat/(0.01 * pressg(il) - eps2 * esat)
        rh(il,l) = min(max((q(il,l) * (1. + qsw * facte)) &
                   /(qsw * (1. + q(il,l) * facte)),0.),1.)
      end if
    end do ! il
  end do ! l
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
