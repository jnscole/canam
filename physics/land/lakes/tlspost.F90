!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tlspost(gsnow,tsnow,wsnow,rhosno,qmelts,gzero,tsnbot, &
                   htcs,hmfn,qfn,evaps,rpcn,trpcn,spcn,tspcn, &
                   gconsts,gcoeffs,t0,zsnow,tcsnow,hcpsno,qtrans, &
                   rpcp,trpcp,spcp,tspcp,tzeros,rhosni, &
                   fls,delskin,ilg,il1,il2,jl)
  !
  !     * jan 30/18 - m.lazare.   last element in call, "N", removed
  !     *                         because not in call statement from
  !     *                         routine classl, and is not used.
  !     * sep 01/15 - d.verseghy. lake snow temperature and heat flux
  !     *                         calculations (based on class subroutine
  !     *                         tspost); net surface water flux terms
  !     *                         (based on class subroutine wprep).
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !< Variable description\f$[units]\f$
  integer :: i !< Variable description\f$[units]\f$
  integer :: j !< Variable description\f$[units]\f$
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg) :: gzero !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tsnbot !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rpcn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: trpcn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: spcn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tspcn !< Variable description\f$[units]\f$
  !
  !     * input/output arrays.
  !
  real, intent(inout), dimension(ilg) :: gsnow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tsnow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: wsnow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhosno !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qmelts !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: htcs !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hmfn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qfn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: evaps !< Variable description\f$[units]\f$
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg) :: t0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zsnow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tcsnow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hcpsno !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: qtrans !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gconsts !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gcoeffs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rpcp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: trpcp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: spcp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tspcp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tzeros !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rhosni !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: fls !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  real, intent(in) :: delskin
  !
  !     * temporary variables.
  !
  real, dimension(ilg) :: radd
  real, dimension(ilg) :: sadd
  !
  real :: hadd
  real :: hconv
  real :: wfrez
  !
  !     * common block parameters.
  !
  real :: delt
  real :: tfrez
  real :: tcw
  real :: tcice
  real :: tcsand
  real :: tcclay
  real :: tcom
  real :: tcdrys
  real :: rhosol
  real :: rhoom
  real :: hcpw
  real :: hcpice
  real :: hcpsol
  real :: hcpom
  real :: hcpsnd
  real :: hcpcly
  real :: sphw
  real :: sphice
  real :: sphveg
  real :: sphair
  real :: rhow
  real :: rhoice
  real :: tcglac
  real :: clhmlt
  real :: clhvap
  !
  common /class1/ delt,tfrez
  common /class3/ tcw,tcice,tcsand,tcclay,tcom,tcdrys, &
                 rhosol,rhoom
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !-----------------------------------------------------------------------
  !
  do i = il1,il2
    if (fls(i) > 0.) then
      tsnbot(i) = (zsnow(i) * tsnow(i) + delskin * t0(i))/ &
                  (zsnow(i) + delskin)
      gzero(i) = - 2.0 * tcsnow(i) * (tsnbot(i) - tsnow(i))/zsnow(i)
      !     1                 +tcice*(t0(i)-tsnbot(i))/delskin)
      if (qmelts(i) < 0.) then
        gsnow(i) = gsnow(i) + qmelts(i)
        qmelts(i) = 0.
      end if
      tsnow(i) = tsnow(i) + (gsnow(i) - gzero(i)) * delt/ &
                 (hcpsno(i) * zsnow(i)) - tfrez
      if (tsnow(i) > 0.) then
        qmelts(i) = qmelts(i) + tsnow(i) * hcpsno(i) * zsnow(i)/delt
        gsnow(i) = gsnow(i) - tsnow(i) * hcpsno(i) * zsnow(i)/delt
        tsnow(i) = 0.
      end if
      !              gzero(i)=gzero(i)+qmelts(i)
      !              qmelts(i)=0.0
    end if
  end do ! loop 100
  !
  do i = il1,il2
    if (fls(i) > 0. .and. tsnow(i) < 0. .and. wsnow(i) > 0.) &
        then
      htcs(i) = htcs(i) - fls(i) * hcpsno(i) * (tsnow(i) + tfrez) * zsnow(i)/ &
                delt
      hadd = - tsnow(i) * hcpsno(i) * zsnow(i)
      hconv = clhmlt * wsnow(i)
      if (hadd <= hconv) then
        wfrez = hadd/clhmlt
        hadd = 0.0
        wsnow(i) = max(0.0,wsnow(i) - wfrez)
        tsnow(i) = 0.0
        rhosno(i) = rhosno(i) + wfrez/zsnow(i)
        hcpsno(i) = hcpice * rhosno(i)/rhoice + hcpw * wsnow(i)/ &
                    (rhow * zsnow(i))
      else
        hadd = hadd - hconv
        wfrez = wsnow(i)
        wsnow(i) = 0.0
        rhosno(i) = rhosno(i) + wfrez/zsnow(i)
        hcpsno(i) = hcpice * rhosno(i)/rhoice
        tsnow(i) = - hadd/(hcpsno(i) * zsnow(i))
      end if
      hmfn(i) = hmfn(i) - fls(i) * clhmlt * wfrez/delt
      htcs(i) = htcs(i) - fls(i) * clhmlt * wfrez/delt
      htcs(i) = htcs(i) + fls(i) * hcpsno(i) * (tsnow(i) + tfrez) * zsnow(i)/ &
                delt
    end if
  end do ! loop 200
  !
  do i = il1,il2
    qfn(i) = fls(i) * evaps(i) * rhow
    if (spcp(i) > 0. .or. evaps(i) < 0.) then
      sadd(i) = spcp(i) - evaps(i) * rhow/rhosni(i)
      if (abs(sadd(i)) < 1.0e-12) sadd(i) = 0.0
      if (sadd(i) > 0.0) then
        spcn (i) = sadd(i)
        if (spcp(i) > 0.0) then
          tspcn(i) = tspcp(i)
        else
          tspcn(i) = min((tzeros(i) - tfrez),0.0)
        end if
        evaps(i) = 0.0
      else
        evaps(i) = - sadd(i) * rhosni(i)/rhow
        spcn (i) = 0.0
        tspcn(i) = 0.0
      end if
    else
      spcn (i) = 0.0
      tspcn(i) = 0.0
    end if

    if (rpcp(i) > 0.) then
      radd(i) = rpcp(i) - evaps(i)
      if (abs(radd(i)) < 1.0e-12) radd(i) = 0.0
      if (radd(i) > 0.) then
        rpcn (i) = radd(i)
        trpcn(i) = trpcp(i)
        evaps(i) = 0.0
      else
        evaps(i) = - radd(i)
        rpcn (i) = 0.0
        trpcn(i) = 0.0
      end if
    else
      rpcn (i) = 0.0
      trpcn(i) = 0.0
    end if
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
