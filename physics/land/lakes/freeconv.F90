!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine freeconv(lkiceh,t0,tlak,rhoiw,nlak,nlakmax,ilg,il1,il2)
  !=======================================================================
  !     * dec 10/16 - m.lazare. integer :: timekeeping variables removed.
  implicit none
  !
  ! ----* lake model variables *----------------------------------------
  !
  integer, intent(in) :: nlakmax !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg) :: nlak !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: t0 !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: lkiceh !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg,nlakmax) :: tlak !< Variable description\f$[units]\f$
  !
  ! ----* input *-------------------------------------------
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  real, intent(in) :: rhoiw !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! ----* class common blocks *------------------------------------------
  !
  real :: delt
  real :: tfrez
  real :: tkecn
  real :: tkecf
  real :: tkece
  real :: tkecs
  real :: tkecl
  real :: hdpthmin
  real :: tkemin
  real :: delmax
  real :: delmin
  real :: emsw
  real :: delzlk
  real :: delskin
  real :: dhmax
  real :: dumax
  common /class1/ delt,tfrez
  common /lakecon/ tkecn,tkecf,tkece,tkecs,hdpthmin, &
                  tkemin,delmax,delmin,emsw,delzlk,delskin, &
                  dhmax,tkecl,dumax
  !
  ! ----* local variables *---------------------------------------------
  !
  integer :: i
  integer :: j
  integer :: k
  integer :: nmix
  real :: ztop
  real :: zbot
  real :: ttest
  real :: rho1
  real :: rho2
  real :: tc1
  real :: tc2
  real :: tbar
  real :: xxx
  real :: icebot
  !=======================================================================
  !
  do i = il1,il2
    if (lkiceh(i) <= 0.0) then
      tc1 = t0(i) - tfrez
      tc2 = tlak(i,1) - tfrez
      call eqnst(xxx,rho1,tc1,0.05)
      call eqnst(xxx,rho2,tc2,0.5)
      if (rho1 > rho2) then
        tbar = ((delskin * rho1 * t0(i)) + (delzlk * rho2 * tlak(i,1)))/ &
               ((delskin * rho1) + (delzlk * rho2))
        t0(i) = tbar
        tlak(i,1) = tbar
      end if
    end if
    icebot = rhoiw * lkiceh(i)

    nmix = 1
    do j = 1,nlak(i) - 1
      ztop = delskin + (j - 1) * delzlk
      zbot = ztop + delzlk
      if (icebot <= ztop) then
        tc1 = tlak(i,j) - tfrez
        tc2 = tlak(i,j + 1) - tfrez
        ! mdm       ttest=(tc1-3.9816)*(tc2-3.9816)
        ttest = (tc1 - 3.98275) * (tc2 - 3.98275)
        call eqnst(xxx,rho1,tc1,zbot)
        call eqnst(xxx,rho2,tc2,zbot + delzlk)
        !--------- mix layers if rho1>rho2 or temperatures span
        !--------- t_maxdensity=3.9816 c.
        if ((rho1 > rho2) .or. (ttest < 0.0)) then
          tbar = ((nmix * rho1 * tlak(i,j)) + (rho2 * tlak(i,j + 1)))/ &
                 (nmix * rho1 + rho2)
          do k = j - nmix + 1,j + 1
            tlak(i,k) = tbar
          end do ! loop 430
          nmix = nmix + 1
          ! mdm        write(6,6666) "static instability removed under ice:"
        else
          nmix = 1
        end if
      end if
    end do ! loop 420
  end do ! loop 100
6666 format(a37,4i5,f5.1)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
