subroutine timeck(year,day,sec,itest)

  !     * august 19, 2002  - w.g. lee
  !
  !     * timeck checks to see if the input year, day and
  !     * seconds are integers. if not, it aborts.
  !
  !     * if itest = 1, then the subroutine aborts
  !     * if day is outside of:   1,2,3...   365
  !     * or sec is outside of:   0,1,2... 86399
  !
  !     * if itest = 0, then the year/day/sec
  !     * values will be adjusted so that day and
  !     * sec will fit the range:
  !     *
  !     * day is in range:  1,2,3...   365
  !     * sec is in range:  0,1,2... 86399

  !     -------------------------------------------------------------
  implicit none

  integer, intent(in)  :: itest !<
  real, intent(inout)     :: year !<
  real, intent(inout)     :: day !<
  real, intent(inout)     :: sec !<
  real     :: test !<

  !     -------------------------------------------------------------

  !     test and abort if non-integers are found:

  test = sec - int(sec)
  if (test/=0)   call                        xit('TIMECK',-1)
  test = day  - int(day)
  if (test/=0)   call                        xit('TIMECK',-2)
  test = year - int(year)
  if (test/=0)   call                        xit('TIMECK',-3)

  !
  if (itest==1) then

    if (sec<0)      call                   xit('TIMECK',-11)
    if (sec>86399)  call                   xit('TIMECK',-12)
    if (day<1)      call                   xit('TIMECK',-13)
    if (day>365)    call                   xit('TIMECK',-14)

  else if (itest==0) then

    do while (sec<0)
      sec = sec + 86400
      day = day - 1
    end do

    do while (sec>86399)
      sec = sec - 86400
      day = day + 1
    end do

    do while (day<1)
      day  = day  + 365
      year = year - 1
    end do

    do while (day>365)
      day  = day  - 365
      year = year + 1
    end do

  else

    call                                      xit('TIMECK',-22)

  end if

  return
end
