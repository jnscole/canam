subroutine hetresg (litrmass, soilcmas,      icc,       ig, &
                          ilg,      il1,      il2,     tbar, &
                          thliq,     sand,      clay,   zbotw, &
                          frac,    isnow,      isand, &
  !    -------------- inputs above this line, outputs below -------------
                          litres,   socres)
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !           heterotrophic respiration subtoutine for bare fraction
  !
  !     11  apr. 2003 - this subroutine calculates heterotrophic respiration
  !     v. arora        over the bare subarea of a grid cell (i.e. ground only
  !                     and snow over ground subareas).
  !
  !     change history:
  !
  !     j. melton and v.arora - changed tanhq10 parameters, they were switched
  !               25 sep 2012
  !     j. melton 31 aug 2012 - remove isnow, it is not used.
  !     j. melton 23 aug 2012 - bring in isand, converting sand to
  !                             int was missing some gridcells assigned
  !                             to bedrock in classb

  !     ------
  !     inputs
  !
  !     litrmass  - litter mass for the 8 pfts + bare in kg c/m2
  !     soilcmas  - soil carbon mass for the 8 pfts + bare in kg c/m2
  !     icc       - no. of vegetation types (currently 8)
  !     ig        - no. of soil layers (currently 3)
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     tbar      - soil temperature, k
  !     thliq     - liquid soil moisture content in 3 soil layers
  !     sand      - percentage sand
  !     clay      - percentage clay
  !     zbotw     - bottom of soil layers
  !     frac      - fraction of ground (fg) or snow over ground (fgs)
  !     isnow     - integer :: telling if bare fraction is fg (0) or fgs (1)
  !
  !     outputs
  !
  !     litres    - litter respiration over the given unvegetated sub-area
  !                 in umol co2/m2.s
  !     socres    - soil c respiration over the given unvegetated sub-area
  !                 in umol co2/m2.s
  !
  implicit none
  !
  !     isnow is changed to isnow(ilg) in classt of class version higher
  !     than 3.4 for coupling with ctem
  !
  !      integer :: ilg, icc,ig,il1,il2,i,j,k,isnow(ilg),isand(ilg,ig)
  integer, intent(in) :: ilg ! jm test  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer, intent(in) :: isnow !<
  integer, intent(in) :: isand(ilg,ig) !<

  real, intent(in)                  :: litrmass(ilg,icc+1) !<
  real, intent(in)                  :: soilcmas(ilg,icc+1) !<
  real, intent(in)                  :: tbar(ilg,ig) !<
  real, intent(in)                  :: thliq(ilg,ig) !<
  real, intent(in)                  :: sand(ilg,ig) !<
  real, intent(in)                  :: zbotw(ilg,ig) !<
  real, intent(inout)                  :: litres(ilg) !<
  real, intent(inout)                  :: socres(ilg) !<
  real, intent(in)                  :: clay(ilg,ig) !<
  real, intent(in)                  :: frac(ilg) !<

  real      :: bsratelt(1) !<
  real      :: bsratesc(1) !<
  real      :: zero !<
  real      :: litrq10 !<
  real      :: soilcq10 !<
  real      :: litrtemp(ilg) !<
  real      :: solctemp(ilg) !<
  real      :: q10func !<
  real      :: psisat(ilg,ig) !<
  real      :: grksat(ilg,ig) !<
  real      :: b(ilg,ig) !<
  real      :: thpor(ilg,ig) !<
  real      :: beta !<
  real      :: fracarb(ilg,ig) !<
  real      :: a(1) !<
  real      :: zcarbon !<
  real      :: tempq10l(ilg) !<
  real      :: socmoscl(ilg) !<
  real      :: scmotrm(ilg,ig) !<
  real      :: ltrmoscl(ilg) !<
  real      :: psi(ilg,ig) !<
  real      :: tempq10s(ilg) !<
  real      :: fcoeff !<
  real      :: tanhq10(4) !<


  !     ------------------------------------------------------------------
  !                     constants used in the model

  !     litter respiration rate at 15 c in kg c/kg c.year
  data bsratelt/0.5605/
  !
  !     soil c respiration rates at 15 c in kg c/kg c.year
  data bsratesc/0.02258/
  !
  !     parameters of the hyperbolic tan q10 formulation
  !
  data tanhq10/1.44, 0.56, 0.075, 46.0/
  !                     a     b      c     d
  !     q10 = a + b * tanh[ c (d-temperature) ]
  !     when a = 2, b = 0, we get the constant q10 of 2. if b is non
  !     zero then q10 becomes temperature dependent
  !
  !     zero
  data zero/1e-20/
  !
  !     a, parameter describing exponential soil carbon profile. used for
  !     estimating temperature of the carbon pool
  data a/4.0/
  !
  !     ---------------------------------------------------------------
  !
  !     initialize required arrays to zero
  !
  do k = 1, ig
    do i = il1, il2
      fracarb(i,k)=0.0  ! fraction of carbon in each soil layer
    end do
  end do ! loop 100
  !
  do i = il1, il2
    litrtemp(i)=0.0     ! litter temperature
    solctemp(i)=0.0     ! soil carbon pool temperature
    socmoscl(i)=0.0     ! soil moisture scalar for soil carbon decomposition
    ltrmoscl(i)=0.0     ! soil moisture scalar for litter decomposition
    litres(i)=0.0       ! litter resp. rate
    tempq10l(i)=0.0
    socres(i)=0.0       ! soil c resp. rate
    tempq10s(i)=0.0
  end do ! loop 110
  !
  do j = 1, ig
    do i = il1, il2
      psisat(i,j) = 0.0       ! saturation matric potential
      grksat(i,j) = 0.0       ! saturation hyd. conductivity
      thpor(i,j) = 0.0        ! porosity
      b(i,j) = 0.0            ! parameter b of clapp and hornberger
      !         isand(i,j)=nint(sand(i,j)) ! now passed in. jm. aug 232012
    end do ! loop 130
  end do ! loop 120
  !
  !     initialization ends
  !
  !     ------------------------------------------------------------------
  !
  !     estimate temperature of the litter and soil carbon pools.
  !
  !     over the bare fraction there is no live root. so we make the
  !     simplest assumption that litter temperature is same as temperature
  !     of the top soil layer.
  !
  do i = il1, il2
    litrtemp(i)=tbar(i,1)
  end do ! loop 210
  !
  !     we estimate the temperature of the soil c pool assuming that soil
  !     carbon over the bare fraction is distributed exponentially. note
  !     that bare fraction may contain dead roots from different pfts all of
  !     which may be distributed differently. for simplicity we do not
  !     TRACK EACH PFT's DEAD ROOT BIOMASS AND ASSUME THAT DISTRIBUTION OF
  !     soil carbon over the bare fraction can be described by a single
  !     parameter.
  !
  do i = il1, il2
    !
    zcarbon=3.0/a(1)                 ! 95% depth
    if (zcarbon<=zbotw(i,1)) then
      fracarb(i,1)=1.0             ! fraction of carbon in
      fracarb(i,2)=0.0             ! soil layers
      fracarb(i,3)=0.0
    else
      fcoeff=exp(-a(1)*zcarbon)
      fracarb(i,1)= &
                          1.0-(exp(-a(1)*zbotw(i,1))-fcoeff)/(1.0-fcoeff)
      if (zcarbon<=zbotw(i,2)) then
        fracarb(i,2)=1.0-fracarb(i,1)
        fracarb(i,3)=0.0
      else
        fracarb(i,3)= &
                              (exp(-a(1)*zbotw(i,2))-fcoeff)/(1.0-fcoeff)
        fracarb(i,2)=1.0-fracarb(i,1)-fracarb(i,3)
      end if
    end if
    !
    solctemp(i)=tbar(i,1)*fracarb(i,1) + &
      tbar(i,2)*fracarb(i,2) + &
      tbar(i,3)*fracarb(i,3)
    solctemp(i)=solctemp(i) / &
      (fracarb(i,1)+fracarb(i,2)+fracarb(i,3))
    !
    !
    !       MAKE SURE WE DON'T USE TEMPERATURES OF 2nd AND 3rd SOIL LAYERS
    !       if they are specified bedrock via sand -3 flag
    !
    if (isand(i,3)==-3) then ! third layer bed rock
      solctemp(i)=tbar(i,1)*fracarb(i,1) + &
        tbar(i,2)*fracarb(i,2)
      solctemp(i)=solctemp(i) / &
        (fracarb(i,1)+fracarb(i,2))
    end if
    if (isand(i,2)==-3) then ! second layer bed rock
      solctemp(i)=tbar(i,1)
    end if
    !
  end do ! loop 240
  !
  !     find moisture scalar for soil c decomposition
  !
  !     this is modelled as function of logarithm of matric potential.
  !     we find values for all soil layers, and then find an average value
  !     based on fraction of carbon present in each layer.
  !
  do j = 1, ig
    do i = il1, il2
      !
      if (isand(i,j)==-3.or.isand(i,j)==-4) then
        scmotrm (i,j)=0.2
        psi (i,j) = 10000.0 ! set to large number so that
        !                               ! ltrmoscl becomes 0.2
      else ! i.e., sand/=-3 or -4
        psisat(i,j)= (10.0**(-0.0131*sand(i,j)+1.88))/100.0
        b(i,j)     = 0.159*clay(i,j)+2.91
        thpor(i,j) = (-0.126*sand(i,j)+48.9)/100.0
        psi(i,j)   = psisat(i,j)*(thliq(i,j)/thpor(i,j))**(-b(i,j))
        !
        if (psi(i,j)>10000.0) then
          scmotrm(i,j)=0.2
        else if (psi(i,j)<=10000.0 .and.  psi(i,j)>6.0) then
          scmotrm(i,j)=1.0 - 0.8* &
    ( (log10(psi(i,j)) - log10(6.0))/(log10(10000.0)-log10(6.0)) )
        else if (psi(i,j)<=6.0 .and.  psi(i,j)>=4.0) then
          scmotrm(i,j)=1.0
        else if (psi(i,j)<4.0.and.psi(i,j)>psisat(i,j) ) then
          scmotrm(i,j)=1.0 - &
           0.5*( (log10(4.0) - log10(psi(i,j))) / &
          (log10(4.0)-log10(psisat(i,j))) )
        else if (psi(i,j)<=psisat(i,j) ) then
          scmotrm(i,j)=0.5
        end if
      end if ! if sand==-3 or -4
      !
      scmotrm(i,j)=max(0.0,min(scmotrm(i,j),1.0))
    end do ! loop 270
  end do ! loop 260
  !
  do i = il1, il2
    socmoscl(i) = scmotrm(i,1)*fracarb(i,1) + &
                      scmotrm(i,2)*fracarb(i,2) + &
                      scmotrm(i,3)*fracarb(i,3)
    socmoscl(i) = socmoscl(i) / &
                      (fracarb(i,1)+fracarb(i,2)+fracarb(i,3))
    !
    !       MAKE SURE WE DON'T USE SCMOTRM OF 2nd AND 3rd SOIL LAYERS
    !       if they are specified bedrock via sand -3 flag
    !
    if (isand(i,3)==-3) then ! third layer bed rock
      socmoscl(i) = scmotrm(i,1)*fracarb(i,1) + &
                        scmotrm(i,2)*fracarb(i,2)
      socmoscl(i) = socmoscl(i) / &
                        (fracarb(i,1)+fracarb(i,2))
    end if
    if (isand(i,2)==-3) then ! second layer bed rock
      socmoscl(i) = scmotrm(i,1)
    end if
    !
    socmoscl(i)=max(0.2,min(socmoscl(i),1.0))
  end do ! loop 290
  !
  !     find moisture scalar for litter decomposition
  !
  !     the difference between moisture scalar for litter and soil c
  !     is that the litter decomposition is not constrained by high
  !     soil moisture (assuming that litter is always exposed to air).
  !     in addition, we use moisture content of the top soil layer
  !     as a surrogate for litter moisture content. so we use only
  !     psi(i,1) calculated in loops 260 and 270 above.
  !
  do i = il1, il2
    if (psi(i,1)>10000.0) then
      ltrmoscl(i)=0.2
    else if (psi(i,1)<=10000.0 .and.  psi(i,1)>6.0) then
      ltrmoscl(i)=1.0 - 0.8* &
     ( (log10(psi(i,1)) - log10(6.0))/(log10(10000.0)-log10(6.0)) )
    else if (psi(i,1)<=6.0) then
      ltrmoscl(i)=1.0
    end if
    ltrmoscl(i)=max(0.2,min(ltrmoscl(i),1.0))
  end do ! loop 300
  !
  !     use temperature of the litter and soil c pools, and their soil
  !     moisture scalars to find respiration rates from these pools
  !
  do i = il1, il2
    if (frac(i)>zero) then
      !
      !       first find the q10 response function to scale base respiration
      !       rate from 15 c to current temperature, we do litter first
      !
      tempq10l(i)=litrtemp(i)-273.16
      litrq10 = tanhq10(1) + tanhq10(2)* &
                  (tanh(tanhq10(3)*(tanhq10(4)-tempq10l(i))  ) )
      !
      q10func = litrq10**(0.1*(litrtemp(i)-273.16-15.0))
      litres(i)= ltrmoscl(i) * litrmass(i,icc+1)* &
                   bsratelt(1)*2.64*q10func ! 2.64 converts bsratelt from kg c/kg c.year
      !                                  ! to u-mol co2/kg c.s
      !
      !       respiration from soil c pool
      !
      tempq10s(i)=solctemp(i)-273.16
      soilcq10= tanhq10(1) + tanhq10(2)* &
                  (tanh(tanhq10(3)*(tanhq10(4)-tempq10s(i))  ) )
      !
      q10func = soilcq10**(0.1*(solctemp(i)-273.16-15.0))
      socres(i)= socmoscl(i)* soilcmas(i,icc+1)* &
                   bsratesc(1)*2.64*q10func ! 2.64 converts bsratesc from kg c/kg c.year
      !                                  ! to u-mol co2/kg c.s
      !
    end if
  end do ! loop 330
  !
  return
end

