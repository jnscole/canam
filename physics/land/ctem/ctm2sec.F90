subroutine ctm2sec(cyear,cday,csec,sec)

  !     * sept 27, 2002  - w.g. lee
  !
  !     * ctm2etm converts compact time (year,day,sec) to
  !     * total number of seconds.
  !
  !     * if input values are non-integer, subroutine aborts.
  !
  !     ------------------------------------------------------------------

  implicit none

  integer  :: m !<

  real, intent(in)     :: cyear !<
  real, intent(in)     :: cday !<
  real, intent(in)     :: csec !<
  real, intent(inout)     :: sec !<

  call timeck(cyear,cday,csec,1)

  sec = csec + (cday-1.0)*86400 + (cyear-1.0)*365.0*86400.0

  return
end
