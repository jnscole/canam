subroutine mortalty (stemmass, rootmass,    ailcg, gleafmas, &
                           bleafmas,      icc,      ilg,      il1, &
                           il2,     iday,   do_age_mort, sort, &
                           fcancmx, &
  !    + ------------------ inputs above this line ----------------------
                           lystmmas, lyrotmas, tymaxlai, grwtheff, &
  !    + -------------- inputs updated above this line ------------------
                           stemltrm, rootltrm, glealtrm, geremort, &
                           intrmort)
  !    + ------------------outputs above this line ----------------------
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !                             mortality subroutine
  !
  !     24  sep 2012  - add in checks to prevent calculation of non-present
  !     j. melton       pfts
  !
  !     07  may 2003  - this subroutine calculates the litter generated
  !     v. arora        from leaves, stem, and root components after
  !                     vegetation dies due to reduced growth efficiency
  !                     or due to aging (the intrinsic mortality)

  !     inputs
  !
  !     stemmass  - stem mass for each of the 9 ctem pfts, kg c/m2
  !     rootmass  - root mass for each of the 9 ctem pfts, kg c/m2
  !     ailcg     - green or live lai
  !     gleafmas  - green leaf mass for each of the 9 ctem pfts, kg c/m2
  !     bleafmas  - brown leaf mass for each of the 9 ctem pfts, kg c/m2
  !     lystmmas  - stem mass at the end of last year
  !     lyrotmas  - root mass at the end of last year
  !     TYMAXLAI  - THIS YEAR's MAXIMUM LAI
  !     grwtheff  - growth efficiency. change in biomass per year per
  !                 unit max. lai (g c/m2)/(m2/m2)
  !     icc       - no. of ctem plant function types, currently 8
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     iday      - day of the year
  !     do_age_mort - switch to control calc of age mortality (false=not done)
  !     sort      - index for correspondence between ctem 9 pfts and size
  !                 12 of parameters vectors
  !
  !     outputs
  !
  !     stemltrm  - stem litter generated due to mortality (kg c/m2)
  !     rootltrm  - root litter generated due to mortality (kg c/m2)
  !     glealtrm  - green leaf litter generated due to mortality (kg c/m2)
  !     geremort  - growth efficiency related mortality (1/day)
  !     intrmort  - intrinsic mortality (1/day)
  !
  implicit none
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer :: kk !<
  integer :: n !<
  !
  parameter (kk=12)  ! product of class pfts and l2max
  !
  integer, intent(in), dimension(icc) :: sort !<
  !
  logical, intent(in)      :: do_age_mort !<
  !
  real, intent(in)  :: stemmass(ilg,icc) !<
  real, intent(in)  :: rootmass(ilg,icc) !<
  real, intent(in)  :: gleafmas(ilg,icc) !<
  real, intent(in)  :: ailcg(ilg,icc) !<
  real, intent(inout)  :: grwtheff(ilg,icc) !<
  real, intent(inout)  :: lystmmas(ilg,icc) !<
  real, intent(inout)  :: lyrotmas(ilg,icc) !<
  real, intent(inout)  :: tymaxlai(ilg,icc) !<
  real, intent(in)  :: bleafmas(ilg,icc) !<
  !
  real, intent(inout)  :: stemltrm(ilg,icc) !<
  real, intent(inout)  :: rootltrm(ilg,icc) !<
  real, intent(inout)  :: glealtrm(ilg,icc) !<
  real, intent(inout)  :: geremort(ilg,icc) !<
  real, intent(inout)  :: intrmort(ilg,icc) !<
  real, intent(in)  :: fcancmx(ilg,icc) !<
  !
  real       :: mxmortge(kk) !<
  real       :: kmort1 !<
  real       :: zero !<
  real       :: maxage(kk) !<
  !
  !     ------------------------------------------------------------------
  !                     parameter used in the model
  !
  !     also note the structure of parameter vectors which clearly shows
  !     the class pfts (along rows) and ctem sub-pfts (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     kmort1, parameter used in growth efficiency mortality formulation
  data  kmort1/0.3/
  !
  !     maximum plant age. used to calculate intrinsic mortality rate.
  !     maximum age for crops is set to zero since they will be harvested
  !     anyway. grasses are treated the same way since the turnover time
  !     for grass leaves is ~1 year and for roots is ~2 year.
  data maxage/250.0, 250.0,   0.0, &
             250.0, 250.0, 250.0, &
               0.0,   0.0,   0.0, &
               0.0,   0.0,   0.0/
  !
  !     maximum mortality when growth efficiency is zero (1/year)
  data  mxmortge/0.01, 0.01, 0.00, &
                0.01, 0.01, 0.01, &
                0.00, 0.00, 0.00, &
                0.00, 0.00, 0.00/
  !
  !     zero
  data zero/1e-20/
  !
  !     ---------------------------------------------------------------
  !
  if (icc/=9)                            call xit('MORTALTY',-1)
  !
  !     initialize required arrays to zero
  !
  do j = 1,icc
    do i = il1, il2
      stemltrm(i,j)=0.0     ! stem litter due to mortality
      rootltrm(i,j)=0.0     ! root litter due to mortality
      glealtrm(i,j)=0.0     ! green leaf litter due to mortality
      geremort(i,j)=0.0     ! growth efficiency related mortality rate
      intrmort(i,j)=0.0     ! intrinsic mortality rate
    end do ! loop 150
  end do ! loop 140
  !
  !     initialization ends
  !
  !     ------------------------------------------------------------------
  !
  !     at the end of every year, i.e. when iday equals 365, we calculate
  !     growth related mortality. rather than using this number to kill
  !     plants at the end of every year, this mortality rate is applied
  !     gradually over the next year.
  !
  do j = 1, icc
    n = sort(j)
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if (iday==1) then
          tymaxlai(i,j) =0.0
        end if
        !
        if (ailcg(i,j)>tymaxlai(i,j)) then
          tymaxlai(i,j)=ailcg(i,j)
        end if
        !
        if (iday==365) then
          if (tymaxlai(i,j)>zero) then
            grwtheff(i,j)= ( (stemmass(i,j)+rootmass(i,j))- &
                             (lystmmas(i,j)+lyrotmas(i,j)) )/tymaxlai(i,j)
          else
            grwtheff(i,j)= 0.0
          end if
          grwtheff(i,j)=max(0.0,grwtheff(i,j))*1000.0
          lystmmas(i,j)=stemmass(i,j)
          lyrotmas(i,j)=rootmass(i,j)
        end if
        !
        !         CALCULATE GROWTH RELATED MORTALITY USING LAST YEAR'S GROWTH
        !         efficiency or the new growth efficiency if day is 365 and
        !         growth efficiency estimate has been updated above.
        !
        geremort(i,j)=mxmortge(n)/(1.0+kmort1*grwtheff(i,j))
        !
        !         convert (1/year) rate into (1/day) rate
        geremort(i,j)=geremort(i,j)/365.0
      end if
    end do ! loop 210
  end do ! loop 200
  !
  !     calculate intrinsic mortality rate due to aging which implicity
  !     includes effects of frost, hail, wind throw etc. it is assumed
  !     that only 1% of the plants exceed maximum age (which is a pft-
  !     dependent parameter). to achieve this some fraction of the plants
  !     need to be killed every year.
  !
  do j = 1, icc
    n = sort(j)
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if (maxage(n)>zero) then
          intrmort(i,j)=1.0-exp(-4.605/maxage(n))
        else
          intrmort(i,j)=0.0
        end if
        !         convert (1/year) rate into (1/day) rate
        intrmort(i,j)=intrmort(i,j)/365.0
      end if
    end do ! loop 260
  end do ! loop 250
  !
  do j = 1,icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        if (.not. do_age_mort) then
          geremort(i,j)=0.0
          intrmort(i,j)=0.0
        end if
      end if
    end do ! loop 280
  end do ! loop 270
  !
  !     now that we have both growth related and intrinsic mortality rates,
  !     lets combine these rates for every pft and estimate litter generated
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        stemltrm(i,j)=stemmass(i,j)* &
     (1.0-exp(-1.0*(geremort(i,j)+intrmort(i,j))) )
        rootltrm(i,j)=rootmass(i,j)* &
     (1.0-exp(-1.0*(geremort(i,j)+intrmort(i,j))) )
        glealtrm(i,j)=gleafmas(i,j)* &
     (1.0-exp(-1.0*(geremort(i,j)+intrmort(i,j))) )
      end if
    end do ! loop 310
  end do ! loop 300
  !
  return
end


