subroutine cgrow(growth,tbar,ta,fc,fcs,ilg,ig,il1,il2,jl)

  !     * mar 09/07 - d.verseghy. change senescence threshold from
  !     *                         0.10 to 0.90.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * dec 03/03 - v.arora. change threshold of leaf-out from 0 c to
  !     *                      2 c.
  !     * sep 25/97 - m.lazare. class - version 2.7.
  !     *                       insert "IF" condition to perform these
  !     *                       calculations only if canopy is present.
  !     * apr 24/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * apr 11/89 - d.verseghy. increment/decrement growth index for
  !     *                         vegetation types 1 and 2 (needleleaf
  !     *                         and broadleaf trees).
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  !
  !     * output array.
  !
  real, intent(inout), dimension(ilg) :: growth !<
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,ig) :: tbar !<
  !
  real, intent(in) :: ta(ilg) !<
  real, intent(in) :: fc(ilg) !<
  real, intent(in) :: fcs(ilg) !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  !
  common /class1/ delt,tfrez
  !-----------------------------------------------------------------------
  do i=il1,il2
    if ((fc(i)+fcs(i))>0.0) then
      if (growth(i)<0.0) then
        growth(i)=min(0.0,(growth(i)+delt/5.184e6))
      else
        if (ta(i)>(tfrez+2.0).and.tbar(i,1)>(tfrez+2.0)) &
            then
          growth(i)=min(1.0,(growth(i)+delt/5.184e6))
        else
          if (growth(i)>0.90) then
            growth(i)=-growth(i)
          else
            growth(i)=0.0
          end if
        end if
      end if
    end if
  end do ! loop 100
  !
  return
end
