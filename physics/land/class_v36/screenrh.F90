subroutine screenrh(srh,st,sq,pressg,fmask,ilg,il1,il2)
  !
  !     * dec 16, 2014 - v.fortin.  remove merge in calculation of fracw.
  !     * apr 30, 2009 - m.lazare.
  !
  !     * calculates screen relative humidity based on input screen
  !     * temperature, screen specific humidity and surface pressure.
  !     * the formulae used here are consistent with that used else where
  !     * in the gcm physics.
  !
  implicit none
  !
  !     * output field:
  !
  real, intent(inout),   dimension(ilg)              :: srh !<
  !
  !     * input fields.
  !
  real, intent(in),   dimension(ilg)              :: st !<
  real, intent(in),   dimension(ilg)              :: sq !<
  real, intent(in),   dimension(ilg)              :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in),   dimension(ilg)              :: fmask !<
  !
  real :: facte !<
  real :: epslim !<
  real :: fracw !<
  real :: etmp !<
  real :: estref !<
  real :: esat !<
  real :: qsw !<
  real :: a !<
  real :: b !<
  real :: eps1 !<
  real :: eps2 !<
  real :: t1s !<
  real :: t2s !<
  real :: ai !<
  real :: bi !<
  real :: aw !<
  real :: bw !<
  real :: slp !<
  real :: rw1 !<
  real :: rw2 !<
  real :: rw3 !<
  real :: ri1 !<
  real :: ri2 !<
  real :: ri3 !<
  real :: esw !<
  real :: esi !<
  real :: esteff !<
  real :: ttt !<
  real :: uuu !<
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: il !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  !
  !     * common blocks for thermodynamic constants.
  !
  common /eps /  a,b,eps1,eps2
  common /htcp/  t1s,t2s,ai,bi,aw,bw,slp
  !
  !     * parameters used in new saturation vapour pressure formulation.
  !
  common /estwi/ rw1,rw2,rw3,ri1,ri2,ri3
  !
  !     * computes the saturation vapour pressure over water or ice.
  !
  esw(ttt)        = exp(rw1+rw2/ttt)*ttt**rw3
  esi(ttt)        = exp(ri1+ri2/ttt)*ttt**ri3
  esteff(ttt,uuu) = uuu*esw(ttt) + (1.-uuu)*esi(ttt)
  !========================================================================
  epslim=0.001
  facte=1./eps1-1.
  do il=il1,il2
    if (fmask(il)>0.) then
      !
      !       * compute the fractional probability of water phase
      !       * existing as a function of temperature (from rockel,
      !       * raschke and weyres, beitr. phys. atmosph., 1991.)
      !
      if (st(il)>=t1s) then
        fracw=1.0
      else
        fracw=0.0059+0.9941*exp(-0.003102*(t1s-st(il))**2)
      end if
      !
      etmp=esteff(st(il),fracw)
      estref=0.01*pressg(il)*(1.-epslim)/(1.-epslim*eps2)
      if (etmp<estref) then
        esat=etmp
      else
        esat=estref
      end if
      !
      qsw=eps1*esat/(0.01*pressg(il)-eps2*esat)
      srh(il)=min(max((sq(il)*(1.+qsw*facte)) &
          /(qsw*(1.+sq(il)*facte)),0.),1.)
    end if
  end do
  !
  return
end
