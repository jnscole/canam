subroutine wfill(wmove,tmove,lzf,ninf,zf,trmdr,r,tr, &
                       psif,grkinf,thlinf,thliqx,tbarwx, &
                       delzx,zbotx,dzf,timpnd,wadj,wadd, &
                       ifill,ifind,ig,igp1,igp2,ilg,il1,il2,jl,n)

  !     * jan 06/09 - d.verseghy. correct lzf and zf assignments in loop
  !     *                         100; additional dzf check in loop 400.
  !     * mar 22/06 - d.verseghy. move ifill test outside all if blocks.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 29/04 - d.verseghy. protect sensitive calculations
  !     *                         against roundoff errors.
  !     * jun 21/02 - d.verseghy. update subroutine call.
  !     * may 17/99 - d.verseghy. put limit on condition based on "GRKINF"
  !     *                         so that "LZF" is always initialized.
  !     * nov 30/94 - m.lazare.   bracket terms in "WADJ" calculation in
  !     *                         loop 200 to avoid optimization leading
  !     *                         to re-ordering of calculation and
  !     *                         rare very large iteration limits.
  !     * aug 16/93 - d.verseghy/m.lazare. add missing outer loop on "J"
  !     *                                  in 200 loop.
  !     * apr 24/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. unsaturated flow of water into soil.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ig !<
  integer, intent(in) :: igp1 !<
  integer, intent(in) :: igp2 !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  integer :: j !<
  integer, intent(in) :: n !<
  !
  !     * output fields.
  !
  real, intent(inout) :: wmove (ilg,igp2) !<
  real, intent(inout) :: tmove (ilg,igp2) !<
  !
  integer, intent(inout)                  :: lzf   (ilg) !<
  integer, intent(inout)                  :: ninf  (ilg) !<
  !
  real, intent(inout) :: zf    (ilg) !<
  real, intent(inout) :: trmdr (ilg) !<
  !
  !     * input fields.
  !
  real, intent(in) :: r     (ilg) !<
  real, intent(in) :: tr    (ilg) !<
  real, intent(in) :: psif  (ilg,igp1) !<
  real, intent(in) :: grkinf(ilg,igp1) !<
  real, intent(in) :: thlinf(ilg,igp1) !<
  real, intent(in) :: thliqx(ilg,igp1) !<
  real, intent(in) :: tbarwx(ilg,igp1) !<
  real, intent(in) :: delzx (ilg,igp1) !<
  real, intent(in) :: zbotx (ilg,igp1) !<

  !
  integer, intent(in), dimension(ilg) :: ifill !<
  !
  !     * internal work fields.
  !
  real, intent(inout) :: dzf   (ilg)  !<
  real, intent(inout) :: timpnd (ilg) !<
  real, intent(inout) :: wadj  (ilg) !<
  real, intent(inout) :: wadd  (ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: ifind !<
  !
  !     * temporary variable.
  !
  real :: thladd !<
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  do i=il1,il2
    ifind(i)=0
    wadj(i)=0.
  end do ! loop 50
  !
  !     * test successive soil layers to find depth of wetting front
  !     * at the time ponding begins, i.e. at the time the decreasing
  !     * infiltration rate equals the rainfall rate.
  !
  do j=1,igp1
    do i=il1,il2
      if (ifill(i)>0 .and. ifind(i)==0) then
        if (grkinf(i,j)>1.0e-12 .and. &
            grkinf(i,j)<(r(i)-1.0e-8)) then
          zf(i)=psif (i,j)/(r(i)/grkinf(i,j)-1)
          if (zf(i)<(zbotx(i,j)-delzx(i,j))) then
            zf(i)=max(zbotx(i,j)-delzx(i,j),0.0)
            lzf(i)=j
            ifind(i)=1
          else if (zf(i)<zbotx(i,j)) then
            lzf(i)=j
            ifind(i)=1
          end if
        else if (grkinf(i,j)>1.0e-12) then
          zf(i)=zbotx(i,j)
          lzf(i)=min(j+1,igp1)
        else if (grkinf(i,j)<=1.0e-12) then
          if (j==1) then
            zf(i)=0.0
            lzf(i)=1
          else
            zf(i)=zbotx(i,j-1)
            lzf(i)=j-1
          end if
          ifind(i)=1
        end if
      end if
    end do
  end do ! loop 100
  !
  !     * find the volume of water needed to correct for the difference
  !     * (if any) between the liquid moisture contents of the layers
  !     * overlying the wetting front and that of the layer containing
  !     * the wetting front.
  !
  do j=1,igp1
    do i=il1,il2
      if (ifill(i)>0 .and. lzf(i)>1 .and. j<lzf(i)) then
        wadj(i)=wadj(i)+delzx(i,j)*( (thlinf(i,j)-thliqx(i,j)) - &
                 (thlinf(i,lzf(i))-thliqx(i,lzf(i))) )
      end if
    end do
  end do ! loop 200
  !
  !     * calculate the time to ponding, given the depth reached by the
  !     * wetting front at that time.

  do i=il1,il2
    if (ifill(i)>0) then
      timpnd(i)=(zf(i)*(thlinf(i,lzf(i))-thliqx(i,lzf(i)))+ &
                  wadj(i))/r(i)
      timpnd(i)=max(timpnd(i),0.0)
      if (zf(i)>10.0) timpnd(i)=1.0e+8
      !
      !     * in the case where the time to ponding exceeds or equals the
      !     * time remaining in the current model step, recalculate the
      !     * actual depth attained by the wetting front over the current
      !     * model step; assign values in the water movement matrix.
      !
      if (timpnd(i)>=trmdr(i)) then
        tmove(i,1)=tr(i)
        wmove(i,1)=r(i)*trmdr(i)
        wadd(i)=wmove(i,1)
      end if
    end if
  end do ! loop 250
  !
  do j=1,igp1
    do i=il1,il2
      if (ifill(i)>0) then
        if (timpnd(i)>=trmdr(i) .and. wadd(i)>0.) then
          thladd=max(thlinf(i,j)-thliqx(i,j),0.0)
          if (thladd>0.) then
            dzf(i)=wadd(i)/thladd
          else
            dzf(i)=1.0e+8
          end if
          if (dzf(i)>(delzx(i,j)+1.0e-5)) then
            wadd(i)=wadd(i)-thladd*delzx(i,j)
          else
            dzf(i)=min(dzf(i),delzx(i,j))
            lzf(i)=j
            if (j==1) then
              zf(i)=dzf(i)
            else
              zf(i)=zbotx(i,j-1)+dzf(i)
            end if
            wadd(i)=0.
          end if
        end if
      end if
    end do
  end do ! loop 300
  !
  do j=1,igp1
    do i=il1,il2
      if (ifill(i)>0) then
        if (timpnd(i)>=trmdr(i) .and. j<=lzf(i)) then
          tmove(i,j+1)=tbarwx(i,j)
          if (j==lzf(i) .and. dzf(i)<delzx(i,j)) then
            wmove(i,j+1)=thliqx(i,j)*dzf(i)
          else
            wmove(i,j+1)=thliqx(i,j)*delzx(i,j)
          end if
        end if
      end if
    end do
  end do ! loop 400
  !
  !     * in the case where the time to ponding is less than the time
  !     * remaining in the current model step, accept the depth of the
  !     * wetting front from loop 100; assign values in the water
  !     * movement matrix.
  !
  do i=il1,il2
    if (ifill(i)>0) then
      if (timpnd(i)<trmdr(i)) then
        tmove(i,1)=tr(i)
        wmove(i,1)=r(i)*timpnd(i)
        if (lzf(i)==1) then
          dzf(i)=zf(i)
        else
          dzf(i)=zf(i)-zbotx(i,lzf(i)-1)
        end if
      end if
    end if
  end do ! loop 450
  !
  do j=1,igp1
    do i=il1,il2
      if (ifill(i)>0) then
        if (timpnd(i)<trmdr(i) .and. j<=lzf(i)) then
          tmove(i,j+1)=tbarwx(i,j)
          if (j==lzf(i)) then
            wmove(i,j+1)=thliqx(i,j)*dzf(i)
          else
            wmove(i,j+1)=thliqx(i,j)*delzx(i,j)
          end if
        end if
      end if
    end do
  end do ! loop 500
  !
  !     * calculate time remaining in current model step after
  !     * unsaturated flow.
  !
  do i=il1,il2
    if (ifill(i)>0) then
      if (timpnd(i)>=trmdr(i)) then
        trmdr(i)=0.
      else
        trmdr(i)=trmdr(i)-timpnd(i)
      end if
      ninf(i)=lzf(i)+1
    end if
  end do ! loop 600
  !
  return
end
