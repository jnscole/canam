subroutine sninfl(r,tr,zsnow,tsnow,rhosno,hcpsno,wsnow, &
                        htcs,hmfn,pcpg,rofn,fi,ilg,il1,il2,jl)
  !
  !     * dec 23/09 - d.verseghy. reset wsnow to zero when snow
  !     *                         pack disappears.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 26/02 - d.verseghy. shortened class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         pass in new "CLASS4" common block.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * dec 16/94 - d.verseghy. class - version 2.3.
  !     *                         new diagnostic field "ROFN" added.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. rain infiltration into snowpack.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: r     (ilg) !<
  real, intent(inout) :: tr    (ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: wsnow (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: hmfn  (ilg) !<
  real, intent(inout) :: pcpg  (ilg) !<
  real, intent(inout) :: rofn  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg) :: fi !<
  !
  !     * temporary variables.
  !
  real :: rain !<
  real :: hrcool !<
  real :: hrfrez !<
  real :: hsnwrm !<
  real :: hsnmlt !<
  real :: zmelt !<
  real :: zfrez !<
  real :: wsncap !<
  real :: wavail !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class1/ delt,tfrez
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !
  wsncap=0.04
  !      wsncap=0.0
  !-----------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0. .and. r(i)>0. .and. zsnow(i)>0.) &
        then
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
      rain=r(i)*delt
      hrcool=tr(i)*hcpw*rain
      hrfrez=clhmlt*rhow*rain
      hsnwrm=(0.0-tsnow(i))*hcpsno(i)*zsnow(i)
      hsnmlt=clhmlt*rhosno(i)*zsnow(i)
      if (hrcool>=(hsnwrm+hsnmlt)) then
        hrcool=hrcool-(hsnwrm+hsnmlt)
        zmelt=zsnow(i)*rhosno(i)/rhow
        hmfn(i)=hmfn(i)+fi(i)*clhmlt*zmelt*rhow/delt
        htcs(i)=htcs(i)+fi(i)*clhmlt*zmelt*rhow/delt
        tr(i)=hrcool/(hcpw*(zmelt+rain+wsnow(i)/rhow))
        r(i)=r(i)+(zmelt+wsnow(i)/rhow)/delt
        zsnow(i)=0.0
        tsnow(i)=0.0
        rhosno(i)=0.0
        hcpsno(i)=0.0
        wsnow(i)=0.0
      else if (hrcool>=hsnwrm .and. hrcool<(hsnwrm+hsnmlt)) &
 then
        hsnmlt=hrcool-hsnwrm
        zmelt=hsnmlt/(clhmlt*rhosno(i))
        hmfn(i)=hmfn(i)+fi(i)*clhmlt*zmelt*rhosno(i)/delt
        htcs(i)=htcs(i)+fi(i)*clhmlt*zmelt*rhosno(i)/delt
        zsnow(i)=zsnow(i)-zmelt
        wavail=zmelt*rhosno(i)+wsnow(i)
        if (wavail>(wsncap*zsnow(i)*rhosno(i))) then
          wsnow(i)=wsncap*zsnow(i)*rhosno(i)
          zmelt=(wavail-wsnow(i))/rhow
        else
          wsnow(i)=wavail
          zmelt=0.0
        end if
        tsnow(i)=0.0
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                 (rhow*zsnow(i))
        tr(i)=0.0
        r(i)=r(i)+zmelt/delt
      else if (hsnwrm>=(hrcool+hrfrez)) then
        hsnwrm=(hrcool+hrfrez)-hsnwrm
        hmfn(i)=hmfn(i)-fi(i)*hrfrez/delt
        htcs(i)=htcs(i)-fi(i)*hrfrez/delt
        rhosno(i)=(rhosno(i)*zsnow(i)+rhow*rain)/zsnow(i)
        if (rhosno(i)>rhoice) then
          zsnow(i)=rhosno(i)*zsnow(i)/rhoice
          rhosno(i)=rhoice
        end if
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                 (rhow*zsnow(i))
        tsnow(i)=hsnwrm/(hcpsno(i)*zsnow(i))
        tr(i)=0.0
        r(i)=0.0
      else if (hsnwrm>=hrcool .and. hsnwrm<(hrcool+hrfrez)) &
 then
        hrfrez=hsnwrm-hrcool
        zfrez=hrfrez/(clhmlt*rhow)
        hmfn(i)=hmfn(i)-fi(i)*clhmlt*zfrez*rhow/delt
        htcs(i)=htcs(i)-fi(i)*clhmlt*zfrez*rhow/delt
        rhosno(i)=(rhosno(i)*zsnow(i)+rhow*zfrez)/zsnow(i)
        if (rhosno(i)>rhoice) then
          zsnow(i)=rhosno(i)*zsnow(i)/rhoice
          rhosno(i)=rhoice
        end if
        wavail=(rain-zfrez)*rhow+wsnow(i)
        if (wavail>(wsncap*zsnow(i)*rhosno(i))) then
          wsnow(i)=wsncap*zsnow(i)*rhosno(i)
          wavail=wavail-wsnow(i)
        else
          wsnow(i)=wavail
          wavail=0.0
        end if
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                 (rhow*zsnow(i))
        r(i)=wavail/(rhow*delt)
        tr(i)=0.0
        tsnow(i)=0.0
      end if
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
      pcpg(i)=pcpg(i)+fi(i)*r(i)*rhow
      rofn(i)=rofn(i)+fi(i)*r(i)*rhow
    end if
  end do ! loop 100
  !
  return
end
