subroutine gatprep (imos,jmos,farea, &
                          nm,ntstart,ntend,im,nl,ilg,il1,il2)
  !
  !     * jan 25/14 - m.lazare.  new version for gcm18+:
  !     *             e.chan.    - generalized version to be called
  !     *                          for each of {land,lake,water}
  !     *                          based on input number of tiles (ntile)
  !     *                          and the fractional area of each tile
  !     *                          farea.
  !     *                        - order of loops are reversed.
  !     *                        - midrot and gcrow are therefore no longer
  !     *                          needed and are removed.
  !     * dec 28/11 - d.verseghy. previous version gatprep.
  !     *                         change ilgm back to ilg and
  !     *                         ilg to nl for consistency with
  !     *                         both stand-alone and gcm
  !     *                         conventions.
  !     * oct 22/11 - m.lazare. remove ocean/ice code (now done
  !     *                       in coiss).
  !     * oct 21/11 - m.lazare. cosmetic: ilg->ilgm and nlat->ilg,
  !     *                       to be consistent with model
  !     *                       convention. also gcgrd->gcrow.
  !     * jun 12/06 - e.chan.  dimension iwat and iice by ilg.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * aug 09/02 - d.verseghy/m.lazare. determine indices for
  !     *                        gather-scatter operations on
  !     *                        current latitude loop.
  !
  !     * this routine calculates the chained tile length indices
  !     * for each set of tiles, called separately for: land
  !     * (with arguments {ilmos,jlmos}), lakes (with arguments
  !     * ((ilkmos,jlkmos}) and water/ice (with arguments
  !     * {iwmos,jwmos}).
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(inout) :: nm !<
  integer, intent(in) :: ntstart !<
  integer, intent(in) :: ntend !<
  integer, intent(in) :: im !<
  integer, intent(in) :: nl !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  !
  !     * output fields.
  !
  integer, intent(inout)  :: imos  (ilg) !<
  integer, intent(inout)  :: jmos  (ilg) !<
  !
  !     * input fields.
  !
  real, intent(in), dimension(nl,im) :: farea !<
  !---------------------------------------------------------------------
  nm=0
  do j=ntstart,ntend
    do i=il1,il2
      if (farea(i,j)>0.0) then
        nm=nm+1
        imos(nm)=i
        jmos(nm)=j
      end if
    end do
  end do ! loop 100
  !
  return
end
