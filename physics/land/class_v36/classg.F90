subroutine classg (tbargat,thlqgat,thicgat,tpndgat,zpndgat, &
                         tbasgat,albsgat,tsnogat,rhosgat,snogat, &
                         tcangat,rcangat,scangat,grogat, cmaigat, &
                         fcangat,lnz0gat,alvcgat,alicgat,pamxgat, &
                         pamngat,cmasgat,rootgat,rsmngat,qa50gat, &
                         vpdagat,vpdbgat,psgagat,psgbgat,paidgat, &
                         hgtdgat,acvdgat,acidgat,tsfsgat,wsnogat, &
                         thpgat, thrgat, thmgat, bigat,  psisgat, &
                         grksgat,thragat,hcpsgat,tcsgat, igdrgat, &
                         thfcgat,thlwgat,psiwgat,dlzwgat,zbtwgat, &
                         vmodgat,zsnlgat,zplggat,zplsgat,tacgat, &
                         qacgat, drngat, xslpgat,grkfgat,wfsfgat, &
                         wfcigat,algwvgat,algwngat,algdvgat, &
                         algdngat,asvdgat,asidgat,agvdgat, &
                         agidgat,isndgat,radjgat,zbldgat,z0orgat, &
                         zrfmgat,zrfhgat,zdmgat, zdhgat, fsvgat, &
                         fsigat, fsdbgat,fsfbgat,fssbgat,cszgat, &
                         fsggat, flggat, fdlgat, ulgat,  vlgat, &
                         tagat,  qagat,  presgat,pregat, padrgat, &
                         vpdgat, tadpgat,rhoagat,rpcpgat,trpcgat, &
                         spcpgat,tspcgat,rhsigat,fclogat,dlongat, &
                         ggeogat,gustgat,refgat, bcsngat,depbgat, &
                         ilmos,jlmos, &
                         nml,nl,nt,nm,ilg,ig,ic,icp1,nbs, &
                         tbarrot,thlqrot,thicrot,tpndrot,zpndrot, &
                         tbasrot,albsrot,tsnorot,rhosrot,snorot, &
                         tcanrot,rcanrot,scanrot,grorot, cmairot, &
                         fcanrot,lnz0rot,alvcrot,alicrot,pamxrot, &
                         pamnrot,cmasrot,rootrot,rsmnrot,qa50rot, &
                         vpdarot,vpdbrot,psgarot,psgbrot,paidrot, &
                         hgtdrot,acvdrot,acidrot,tsfsrot,wsnorot, &
                         thprot, thrrot, thmrot, birot,  psisrot, &
                         grksrot,thrarot,hcpsrot,tcsrot, igdrrot, &
                         thfcrot,thlwrot,psiwrot,dlzwrot,zbtwrot, &
                         vmodl,  zsnlrot,zplgrot,zplsrot,tacrot, &
                         qacrot, drnrot, xslprot,grkfrot,wfsfrot, &
                         wfcirot,algwvrot,algwnrot,algdvrot, &
                         algdnrot,asvdrot,asidrot,agvdrot, &
                         agidrot,isndrot,radj   ,zbldrow,z0orrow, &
                         zrfmrow,zrfhrow,zdmrow, zdhrow, fsvrot, &
                         fsirot, fsdbrot,fsfbrot,fssbrot,cszrow, &
                         fsgrot, flgrot, fdlrot, ulrow,  vlrow, &
                         tarow,  qarow,  presrow,prerow, padrrow, &
                         vpdrow, tadprow,rhoarow,rpcprow,trpcrow, &
                         spcprow,tspcrow,rhsirow,fclorow,dlonrow, &
                         ggeorow,gustrol,refrot, bcsnrot,depbrow)
  !
  !     * jan 16, 2015 - m.lazare. new version called by "sfcproc3":
  !     *                          - add thlw.
  !     *                          - {algwv,algwn,algdv,algdn} replace
  !     *                            {algw,algd}.
  !     *                          - fsg,flg,gust added.
  !     *                          - fdlrow changed to fdlrol (cosmetic).
  !     *                          - adds gtgat/gtrot.
  !     *                          - adds nt (ntld in sfcproc2
  !     *                            call) to dimension land-only
  !     *                            rot fields, consistent with
  !     *                            new comrow12.
  !     *                          - unused iwmos,jwmos removed.
  !     * jun 13, 2013 - m.lazare. class gather routine called by
  !     *                          "sfcproc" in new version gcm17.
  !     * note: this contains the following changes compared to the
  !     *       working temporary version used in conjunction with
  !     *       updates to gcm16 (ie not official):
  !     *         1) {DEPB,REF,BCSN} added for Maryam's new code.
  !     *         2) {FSDB,FSFB,FSSB} added for Jason's new code.
  !     * oct 18/11 - m.lazare.  add igdr.
  !     * oct 07/11 - m.lazare.  add vmodl->vmodgat.
  !     * oct 05/11 - m.lazare.  put back in presgrow->presgat
  !     *                        required for added surface rh
  !     *                        calculation.
  !     * oct 03/11 - m.lazare.  remove all initialization to
  !     *                        zero of gat arrays (now done
  !     *                        in class driver).
  !     * sep 16/11 - m.lazare.  - row->rot and grd->row.
  !     *                        - remove initialization of
  !     *                          {alvs,alir} to zero.
  !     *                        - remove presgrow->presgat
  !     *                          (ocean-only now).
  !     *                        - radjrow (64-bit) now radj
  !     *                          (32-bit).
  !     * mar 23/06 - d.verseghy. add wsno,fsno,ggeo.
  !     * mar 18/05 - d.verseghy. additional variables.
  !     * feb 18/05 - d.verseghy. add "TSFS" variables.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * aug 15/02 - d.verseghy. gather operation on class
  !     *                         variables.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: nml !<
  integer, intent(in)  :: nl !<
  integer, intent(in)  :: nm !<
  integer, intent(in)  :: nt !<
  integer, intent(in)  :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)  :: ig !<
  integer, intent(in)  :: ic !<
  integer, intent(in)  :: icp1 !<
  integer  :: k !<
  integer  :: l !<
  integer  :: m !<
  integer, intent(in)  :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  !
  !     * land surface prognostic variables.
  !
  real, intent(in)    :: tbarrot(nl,nt,ig) !<
  real, intent(in)    :: thlqrot(nl,nt,ig) !<
  real, intent(in)    :: thicrot(nl,nt,ig) !<
  real, intent(in)    :: tpndrot(nl,nt) !<
  real, intent(in)    :: zpndrot(nl,nt) !<
  real, intent(in)    :: tbasrot(nl,nt) !<
  real, intent(in)    :: albsrot(nl,nm) !<
  real, intent(in)    :: tsnorot(nl,nm) !<
  real, intent(in)    :: rhosrot(nl,nm) !<
  real, intent(in)    :: snorot (nl,nm) !<
  real, intent(in)    :: tcanrot(nl,nt) !<
  real, intent(in)    :: rcanrot(nl,nt) !<
  real, intent(in)    :: scanrot(nl,nt) !<
  real, intent(in)    :: grorot (nl,nt) !<
  real, intent(in)    :: cmairot(nl,nt) !<
  real, intent(in)    :: tsfsrot(nl,nt,4) !<
  real, intent(in)    :: tacrot (nl,nt) !<
  real, intent(in)    :: qacrot (nl,nt) !<
  real, intent(in)    :: wsnorot(nl,nm) !<
  real, intent(in)    :: refrot (nl,nm) !<
  real, intent(in)    :: bcsnrot(nl,nm) !<
  !
  real, intent(inout)    :: tbargat(ilg,ig) !<
  real, intent(inout)    :: thlqgat(ilg,ig) !<
  real, intent(inout)    :: thicgat(ilg,ig) !<
  real, intent(inout)    :: tpndgat(ilg) !<
  real, intent(inout)    :: zpndgat(ilg) !<
  real, intent(inout)    :: tbasgat(ilg) !<
  real, intent(inout)    :: albsgat(ilg) !<
  real, intent(inout)    :: tsnogat(ilg) !<
  real, intent(inout)    :: rhosgat(ilg) !<
  real, intent(inout)    :: snogat (ilg) !<
  real, intent(inout)    :: tcangat(ilg) !<
  real, intent(inout)    :: rcangat(ilg) !<
  real, intent(inout)    :: scangat(ilg) !<
  real, intent(inout)    :: grogat (ilg) !<
  real, intent(inout)    :: cmaigat(ilg) !<
  real, intent(inout)    :: tsfsgat(ilg,4) !<
  real, intent(inout)    :: tacgat (ilg) !<
  real, intent(inout)    :: qacgat (ilg) !<
  real, intent(inout)    :: wsnogat(ilg) !<
  real, intent(inout)    :: refgat (ilg) !<
  real, intent(inout)    :: bcsngat(ilg) !<
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in)  :: ilmos (ilg) !<
  integer, intent(in)  :: jlmos  (ilg) !<
  !
  !     * canopy and soil information arrays.
  !     * (the length of these arrays is determined by the number
  !     * of soil layers (3) and the number of broad vegetation
  !     * categories (4, or 5 including urban areas).)
  !
  real, intent(in)          :: fcanrot(nl,nt,icp1) !<
  real, intent(in)          :: lnz0rot(nl,nt,icp1) !<
  real, intent(in)          :: alvcrot(nl,nt,icp1) !<
  real, intent(in)          :: alicrot(nl,nt,icp1) !<
  real, intent(in)          :: pamxrot(nl,nt,ic) !<
  real, intent(in)          :: pamnrot(nl,nt,ic) !<
  real, intent(in)          :: cmasrot(nl,nt,ic) !<
  real, intent(in)          :: rootrot(nl,nt,ic) !<
  real, intent(in)          :: rsmnrot(nl,nt,ic) !<
  real, intent(in)          :: qa50rot(nl,nt,ic) !<
  real, intent(in)          :: vpdarot(nl,nt,ic) !<
  real, intent(in)          :: vpdbrot(nl,nt,ic) !<
  real, intent(in)          :: psgarot(nl,nt,ic) !<
  real, intent(in)          :: psgbrot(nl,nt,ic) !<
  real, intent(in)          :: paidrot(nl,nt,ic) !<
  real, intent(in)          :: hgtdrot(nl,nt,ic) !<
  real, intent(in)          :: acvdrot(nl,nt,ic) !<
  real, intent(in)          :: acidrot(nl,nt,ic) !<
  !
  real, intent(inout)          :: fcangat(ilg,icp1) !<
  real, intent(inout)          :: lnz0gat(ilg,icp1) !<
  real, intent(inout)          :: alvcgat(ilg,icp1) !<
  real, intent(inout)          :: alicgat(ilg,icp1) !<
  real, intent(inout)          :: pamxgat(ilg,ic) !<
  real, intent(inout)          :: pamngat(ilg,ic) !<
  real, intent(inout)          :: cmasgat(ilg,ic) !<
  real, intent(inout)          :: rootgat(ilg,ic) !<
  real, intent(inout)          :: rsmngat(ilg,ic) !<
  real, intent(inout)          :: qa50gat(ilg,ic) !<
  real, intent(inout)          :: vpdagat(ilg,ic) !<
  real, intent(inout)          :: vpdbgat(ilg,ic) !<
  real, intent(inout)          :: psgagat(ilg,ic) !<
  real, intent(inout)          :: psgbgat(ilg,ic) !<
  real, intent(inout)          :: paidgat(ilg,ic) !<
  real, intent(inout)          :: hgtdgat(ilg,ic) !<
  real, intent(inout)          :: acvdgat(ilg,ic) !<
  real, intent(inout)          :: acidgat(ilg,ic) !<
  !
  real, intent(in)    :: thprot (nl,nt,ig) !<
  real, intent(in)    :: thrrot (nl,nt,ig) !<
  real, intent(in)    :: thmrot (nl,nt,ig) !<
  real, intent(in)    :: birot  (nl,nt,ig) !<
  real, intent(in)    :: psisrot(nl,nt,ig) !<
  real, intent(in)    :: grksrot(nl,nt,ig) !<
  real, intent(in)    :: thrarot(nl,nt,ig) !<
  real, intent(in)    :: hcpsrot(nl,nt,ig) !<
  real, intent(in)    :: tcsrot (nl,nt,ig) !<
  real, intent(in)    :: thfcrot(nl,nt,ig) !<
  real, intent(in)    :: thlwrot(nl,nt,ig) !<
  real, intent(in)    :: psiwrot(nl,nt,ig) !<
  real, intent(in)    :: dlzwrot(nl,nt,ig) !<
  real, intent(in)    :: zbtwrot(nl,nt,ig) !<
  real, intent(in)    :: drnrot (nl,nt) !<
  real, intent(in)    :: xslprot(nl,nt) !<
  real, intent(in)    :: grkfrot(nl,nt) !<
  real, intent(in)    :: wfsfrot(nl,nt) !<
  real, intent(in)    :: wfcirot(nl,nt) !<
  real, intent(in)    :: algwvrot(nl,nt) !<
  real, intent(in)    :: algwnrot(nl,nt) !<
  real, intent(in)    :: algdvrot(nl,nt) !<
  real, intent(in)    :: algdnrot(nl,nt) !<
  real, intent(in)    :: asvdrot(nl,nm) !<
  real, intent(in)    :: asidrot(nl,nm) !<
  real, intent(in)    :: agvdrot(nl,nt) !<
  real, intent(in)    :: agidrot(nl,nt) !<
  real, intent(in)    :: zsnlrot(nl,nt) !<
  real, intent(in)    :: zplgrot(nl,nt) !<
  real, intent(in)    :: zplsrot(nl,nt) !<
  !

  real, intent(inout)    :: thpgat (ilg,ig) !<
  real, intent(inout)    :: thrgat (ilg,ig) !<
  real, intent(inout)    :: thmgat (ilg,ig) !<
  real, intent(inout)    :: bigat  (ilg,ig) !<
  real, intent(inout)    :: psisgat(ilg,ig) !<
  real, intent(inout)    :: grksgat(ilg,ig) !<
  real, intent(inout)    :: thragat(ilg,ig) !<
  real, intent(inout)    :: hcpsgat(ilg,ig) !<
  real, intent(inout)    :: tcsgat (ilg,ig) !<
  real, intent(inout)    :: thfcgat(ilg,ig) !<
  real, intent(inout)    :: thlwgat(ilg,ig) !<
  real, intent(inout)    :: psiwgat(ilg,ig) !<
  real, intent(inout)    :: dlzwgat(ilg,ig) !<
  real, intent(inout)    :: zbtwgat(ilg,ig) !<
  real, intent(inout)    :: drngat (ilg) !<
  real, intent(inout)    :: xslpgat(ilg) !<
  real, intent(inout)    :: grkfgat(ilg) !<
  real, intent(inout)    :: wfsfgat(ilg) !<
  real, intent(inout)    :: wfcigat(ilg) !<
  real, intent(inout)    :: algwvgat(ilg) !<
  real, intent(inout)    :: algwngat(ilg) !<
  real, intent(inout)    :: algdvgat(ilg) !<
  real, intent(inout)    :: algdngat(ilg) !<
  real, intent(inout)    :: asvdgat(ilg) !<
  real, intent(inout)    :: asidgat(ilg) !<
  real, intent(inout)    :: agvdgat(ilg) !<
  real, intent(inout)    :: agidgat(ilg) !<
  real, intent(inout)    :: zsnlgat(ilg) !<
  real, intent(inout)    :: zplggat(ilg) !<
  real, intent(inout)    :: zplsgat(ilg) !<
  !
  integer, intent(in) :: isndrot(nl,nt,ig) !<
  integer, intent(inout) :: isndgat(ilg,ig) !<
  integer, intent(in) :: igdrrot(nl,nt) !<
  integer, intent(inout) :: igdrgat(ilg) !<

  !     * atmospheric and grid-constant input variables.
  !
  real, intent(in)  :: zrfmrow(nl) !<
  real, intent(in)  :: zrfhrow(nl) !<
  real, intent(in)  :: zdmrow (nl) !<
  real, intent(in)  :: zdhrow (nl) !<
  real, intent(in)  :: cszrow (nl) !<
  real, intent(in)  :: ulrow  (nl) !<
  real, intent(in)  :: vlrow  (nl) !<
  real, intent(in)  :: tarow  (nl) !<
  real, intent(in)  :: qarow  (nl) !<
  real, intent(in)  :: presrow(nl) !<
  real, intent(in)  :: prerow (nl) !<
  real, intent(in)  :: padrrow(nl) !<
  real, intent(in)  :: vpdrow (nl) !<
  real, intent(in)  :: tadprow(nl) !<
  real, intent(in)  :: rhoarow(nl) !<
  real, intent(in)  :: zbldrow(nl) !<
  real, intent(in)  :: z0orrow(nl) !<
  real, intent(in)  :: rpcprow(nl) !<
  real, intent(in)  :: trpcrow(nl) !<
  real, intent(in)  :: spcprow(nl) !<
  real, intent(in)  :: tspcrow(nl) !<
  real, intent(in)  :: rhsirow(nl) !<
  real, intent(in)  :: fclorow(nl) !<
  real, intent(in)  :: dlonrow(nl) !<
  real, intent(in)  :: ggeorow(nl) !<
  real, intent(in)  :: gustrol (nl) !<
  real, intent(in)  :: radj   (nl)  !<
  real, intent(in)  :: vmodl  (nl) !<
  real, intent(in)  :: depbrow(nl) !<

  real, intent(in), dimension(nl,nm,nbs) :: fsdbrot !<
  real, intent(in), dimension(nl,nm,nbs) :: fsfbrot !<
  real, intent(in), dimension(nl,nm,nbs) :: fssbrot !<
  real, intent(in), dimension(nl,nm)     :: fsvrot !<
  real, intent(in), dimension(nl,nm)     :: fsirot !<
  real, intent(in), dimension(nl,nm)     :: fsgrot !<
  real, intent(in), dimension(nl,nm)     :: flgrot !<
  real, intent(in), dimension(nl,nm)     :: fdlrot !<
  !
  real, intent(inout)  :: zrfmgat(ilg) !<
  real, intent(inout)  :: zrfhgat(ilg) !<
  real, intent(inout)  :: zdmgat (ilg) !<
  real, intent(inout)  :: zdhgat (ilg) !<
  real, intent(inout)  :: fsvgat (ilg) !<
  real, intent(inout)  :: fsigat (ilg) !<
  real, intent(inout)  :: cszgat (ilg) !<
  real, intent(inout)  :: fsggat (ilg) !<
  real, intent(inout)  :: flggat (ilg) !<
  real, intent(inout)  :: fdlgat (ilg) !<
  real, intent(inout)  :: ulgat  (ilg) !<
  real, intent(inout)  :: vlgat  (ilg) !<
  real, intent(inout)  :: tagat  (ilg) !<
  real, intent(inout)  :: qagat  (ilg) !<
  real, intent(inout)  :: presgat(ilg) !<
  real, intent(inout)  :: pregat (ilg) !<
  real, intent(inout)  :: padrgat(ilg) !<
  real, intent(inout)  :: vpdgat (ilg) !<
  real, intent(inout)  :: tadpgat(ilg) !<
  real, intent(inout)  :: rhoagat(ilg) !<
  real, intent(inout)  :: zbldgat(ilg) !<
  real, intent(inout)  :: z0orgat(ilg) !<
  real, intent(inout)  :: rpcpgat(ilg) !<
  real, intent(inout)  :: trpcgat(ilg) !<
  real, intent(inout)  :: spcpgat(ilg) !<
  real, intent(inout)  :: tspcgat(ilg) !<
  real, intent(inout)  :: rhsigat(ilg) !<
  real, intent(inout)  :: fclogat(ilg) !<
  real, intent(inout)  :: dlongat(ilg) !<
  real, intent(inout)  :: ggeogat(ilg) !<
  real, intent(inout)  :: gustgat(ilg) !<
  real, intent(inout)  :: radjgat(ilg)   !< Latitude \f$[radians]\f$
  real, intent(inout)  :: vmodgat(ilg) !<
  real, intent(inout)  :: depbgat(ilg) !<

  real, intent(inout), dimension(ilg,nbs) :: fsdbgat !<
  real, intent(inout), dimension(ilg,nbs) :: fsfbgat !<
  real, intent(inout), dimension(ilg,nbs) :: fssbgat !<
  !----------------------------------------------------------------------
  do k=1,nml
    tpndgat(k)=tpndrot(ilmos(k),jlmos(k))
    zpndgat(k)=zpndrot(ilmos(k),jlmos(k))
    tbasgat(k)=tbasrot(ilmos(k),jlmos(k))
    albsgat(k)=albsrot(ilmos(k),jlmos(k))
    tsnogat(k)=tsnorot(ilmos(k),jlmos(k))
    rhosgat(k)=rhosrot(ilmos(k),jlmos(k))
    snogat (k)=snorot (ilmos(k),jlmos(k))
    refgat (k)=refrot (ilmos(k),jlmos(k))
    bcsngat(k)=bcsnrot(ilmos(k),jlmos(k))
    wsnogat(k)=wsnorot(ilmos(k),jlmos(k))
    tcangat(k)=tcanrot(ilmos(k),jlmos(k))
    rcangat(k)=rcanrot(ilmos(k),jlmos(k))
    scangat(k)=scanrot(ilmos(k),jlmos(k))
    grogat (k)=grorot (ilmos(k),jlmos(k))
    cmaigat(k)=cmairot(ilmos(k),jlmos(k))
    drngat (k)=drnrot (ilmos(k),jlmos(k))
    xslpgat(k)=xslprot(ilmos(k),jlmos(k))
    grkfgat(k)=grkfrot(ilmos(k),jlmos(k))
    wfsfgat(k)=wfsfrot(ilmos(k),jlmos(k))
    wfcigat(k)=wfcirot(ilmos(k),jlmos(k))
    algwvgat(k)=algwvrot(ilmos(k),jlmos(k))
    algwngat(k)=algwnrot(ilmos(k),jlmos(k))
    algdvgat(k)=algdvrot(ilmos(k),jlmos(k))
    algdngat(k)=algdnrot(ilmos(k),jlmos(k))
    asvdgat(k)=asvdrot(ilmos(k),jlmos(k))
    asidgat(k)=asidrot(ilmos(k),jlmos(k))
    agvdgat(k)=agvdrot(ilmos(k),jlmos(k))
    agidgat(k)=agidrot(ilmos(k),jlmos(k))
    zsnlgat(k)=zsnlrot(ilmos(k),jlmos(k))
    zplggat(k)=zplgrot(ilmos(k),jlmos(k))
    zplsgat(k)=zplsrot(ilmos(k),jlmos(k))
    tacgat (k)=tacrot (ilmos(k),jlmos(k))
    qacgat (k)=qacrot (ilmos(k),jlmos(k))
    igdrgat(k)=igdrrot(ilmos(k),jlmos(k))
    zbldgat(k)=zbldrow(ilmos(k))
    z0orgat(k)=z0orrow(ilmos(k))
    zrfmgat(k)=zrfmrow(ilmos(k))
    zrfhgat(k)=zrfhrow(ilmos(k))
    zdmgat (k)=zdmrow(ilmos(k))
    zdhgat (k)=zdhrow(ilmos(k))
    fsvgat (k)=fsvrot (ilmos(k),jlmos(k))
    fsigat (k)=fsirot (ilmos(k),jlmos(k))
    cszgat (k)=cszrow (ilmos(k))
    fsggat (k)=fsgrot (ilmos(k),jlmos(k))
    flggat (k)=flgrot (ilmos(k),jlmos(k))
    fdlgat (k)=fdlrot (ilmos(k),jlmos(k))
    ulgat  (k)=ulrow  (ilmos(k))
    vlgat  (k)=vlrow  (ilmos(k))
    tagat  (k)=tarow  (ilmos(k))
    qagat  (k)=qarow  (ilmos(k))
    presgat(k)=presrow(ilmos(k))
    pregat (k)=prerow (ilmos(k))
    padrgat(k)=padrrow(ilmos(k))
    vpdgat (k)=vpdrow (ilmos(k))
    tadpgat(k)=tadprow(ilmos(k))
    rhoagat(k)=rhoarow(ilmos(k))
    rpcpgat(k)=rpcprow(ilmos(k))
    trpcgat(k)=trpcrow(ilmos(k))
    spcpgat(k)=spcprow(ilmos(k))
    tspcgat(k)=tspcrow(ilmos(k))
    rhsigat(k)=rhsirow(ilmos(k))
    fclogat(k)=fclorow(ilmos(k))
    dlongat(k)=dlonrow(ilmos(k))
    ggeogat(k)=ggeorow(ilmos(k))
    gustgat(k)=gustrol(ilmos(k))
    radjgat(k)=radj   (ilmos(k))
    vmodgat(k)=vmodl  (ilmos(k))
    depbgat(k)=depbrow(ilmos(k))
  end do ! loop 100
  !
  do l=1,ig
    do k=1,nml
      tbargat(k,l)=tbarrot(ilmos(k),jlmos(k),l)
      thlqgat(k,l)=thlqrot(ilmos(k),jlmos(k),l)
      thicgat(k,l)=thicrot(ilmos(k),jlmos(k),l)
      thpgat (k,l)=thprot (ilmos(k),jlmos(k),l)
      thrgat (k,l)=thrrot (ilmos(k),jlmos(k),l)
      thmgat (k,l)=thmrot (ilmos(k),jlmos(k),l)
      bigat  (k,l)=birot  (ilmos(k),jlmos(k),l)
      psisgat(k,l)=psisrot(ilmos(k),jlmos(k),l)
      grksgat(k,l)=grksrot(ilmos(k),jlmos(k),l)
      thragat(k,l)=thrarot(ilmos(k),jlmos(k),l)
      hcpsgat(k,l)=hcpsrot(ilmos(k),jlmos(k),l)
      tcsgat (k,l)=tcsrot (ilmos(k),jlmos(k),l)
      thfcgat(k,l)=thfcrot(ilmos(k),jlmos(k),l)
      thlwgat(k,l)=thlwrot(ilmos(k),jlmos(k),l)
      psiwgat(k,l)=psiwrot(ilmos(k),jlmos(k),l)
      dlzwgat(k,l)=dlzwrot(ilmos(k),jlmos(k),l)
      zbtwgat(k,l)=zbtwrot(ilmos(k),jlmos(k),l)
      isndgat(k,l)=isndrot(ilmos(k),jlmos(k),l)
    end do ! loop 200
  end do ! loop 250
  !
  do l=1,icp1
    do k=1,nml
      fcangat(k,l)=fcanrot(ilmos(k),jlmos(k),l)
      lnz0gat(k,l)=lnz0rot(ilmos(k),jlmos(k),l)
      alvcgat(k,l)=alvcrot(ilmos(k),jlmos(k),l)
      alicgat(k,l)=alicrot(ilmos(k),jlmos(k),l)
    end do
  end do ! loop 300
  !
  do l=1,ic
    do k=1,nml
      pamxgat(k,l)=pamxrot(ilmos(k),jlmos(k),l)
      pamngat(k,l)=pamnrot(ilmos(k),jlmos(k),l)
      cmasgat(k,l)=cmasrot(ilmos(k),jlmos(k),l)
      rootgat(k,l)=rootrot(ilmos(k),jlmos(k),l)
      rsmngat(k,l)=rsmnrot(ilmos(k),jlmos(k),l)
      qa50gat(k,l)=qa50rot(ilmos(k),jlmos(k),l)
      vpdagat(k,l)=vpdarot(ilmos(k),jlmos(k),l)
      vpdbgat(k,l)=vpdbrot(ilmos(k),jlmos(k),l)
      psgagat(k,l)=psgarot(ilmos(k),jlmos(k),l)
      psgbgat(k,l)=psgbrot(ilmos(k),jlmos(k),l)
      paidgat(k,l)=paidrot(ilmos(k),jlmos(k),l)
      hgtdgat(k,l)=hgtdrot(ilmos(k),jlmos(k),l)
      acvdgat(k,l)=acvdrot(ilmos(k),jlmos(k),l)
      acidgat(k,l)=acidrot(ilmos(k),jlmos(k),l)
      tsfsgat(k,l)=tsfsrot(ilmos(k),jlmos(k),l)
    end do
  end do ! loop 400
  !
  do l = 1, nbs
    do k = 1, nml
      fsdbgat(k,l) = fsdbrot(ilmos(k),jlmos(k),l)
      fsfbgat(k,l) = fsfbrot(ilmos(k),jlmos(k),l)
      fssbgat(k,l) = fssbrot(ilmos(k),jlmos(k),l)
    end do ! k
  end do ! l

  return
end
