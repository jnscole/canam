subroutine tmelt(zsnow,tsnow,qmelt,r,tr,gzero,ralb, &
                       hmfn,htcs,htc,fi,hcpsno,rhosno,wsnow, &
                       isand,ig,ilg,il1,il2,jl)
  !
  !     * apr 09/17 - d.verseghy. bugfix in calculation of htcs.
  !     * jan 06/09 - d.verseghy/m.lazare. split 100 loop into two.
  !     * mar 24/06 - d.verseghy. allow for presence of water in snow.
  !     * sep 24/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 26/02 - d.verseghy. shortened class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * jan 02/95 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * aug 18/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. melting of snowpack.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ig !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  !
  !     * input/output arrays.
  !
  real, intent(inout), dimension(ilg,ig) :: htc !<

  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: qmelt (ilg) !<
  real, intent(inout) :: r     (ilg) !<
  real, intent(inout) :: tr    (ilg) !<
  real, intent(inout) :: gzero (ilg) !<
  real, intent(inout) :: ralb  (ilg) !<
  real, intent(inout) :: hmfn  (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: wsnow (ilg) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * temporary variables.
  !
  real :: hadd !<
  real :: hconv !<
  real :: zmelt !<
  real :: rmelt !<
  real :: rmelts !<
  real :: trmelt !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class1/ delt,tfrez
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !-----------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0.) then
      if (qmelt(i)>0. .and. zsnow(i)>0.) then
        htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                     zsnow(i)/delt
        hadd =qmelt(i)*delt
        hconv=(0.0-tsnow(i))*hcpsno(i)*zsnow(i) + &
                           clhmlt*rhosno(i)*zsnow(i)
        if (hadd<=hconv) then
          zmelt=hadd/((0.0-tsnow(i))*hcpsno(i)+ &
                       clhmlt*rhosno(i))
          rmelts=zmelt*rhosno(i)/(rhow*delt)
          rmelt=rmelts
          trmelt=0.0
          zsnow(i)=zsnow(i)-zmelt
          hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                     (rhow*zsnow(i))
          htcs (i)=htcs(i)-fi(i)*(qmelt(i)-clhmlt*rmelt* &
                          rhow)
        else
          rmelts=zsnow(i)*rhosno(i)/rhow
          rmelt=rmelts+wsnow(i)/rhow
          hadd=hadd-hconv
          trmelt=hadd/(hcpw*rmelt)
          rmelt=rmelt/delt
          rmelts=rmelts/delt
          zsnow (i)=0.0
          hcpsno(i)=0.0
          tsnow (i)=0.0
          wsnow (i)=0.0
          htcs (i)=htcs(i)-fi(i)*(qmelt(i)-clhmlt*rmelts* &
                          rhow)
        end if
        hmfn (i)=hmfn(i)+fi(i)*clhmlt*rmelts*rhow
        tr   (i)=(r(i)*tr(i)+rmelt*trmelt)/(r(i)+rmelt)
        r    (i)=r(i)+rmelt
        qmelt(i)=0.0
        htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                     zsnow(i)/delt
      end if
      ralb(i)=r(i)
    end if
  end do ! loop 100
  !
  do i=il1,il2
    if (fi(i)>0.) then
      if (qmelt(i)>0 .and. isand(i,1)>-4) then
        gzero(i)=gzero(i)+qmelt(i)
        htcs (i)=htcs(i)-fi(i)*qmelt(i)
        htc(i,1)=htc(i,1)+fi(i)*qmelt(i)
      end if
      ralb(i)=r(i)
    end if
  end do ! loop 200
  !
  return
end
