subroutine classz(istep,  ctvstp, ctsstp, ct1stp, ct2stp, ct3stp, &
                        wtvstp, wtsstp, wtgstp, &
                        fsgv,   flgv,   hfsc,   hevc,   hmfc,   htcc, &
                        fsgs,   flgs,   hfss,   hevs,   hmfn,   htcs, &
                        fsgg,   flgg,   hfsg,   hevg,   hmfg,   htc, &
                        pcfc,   pclc,   qfcf,   qfcl,   rofc,   wtrc, &
                        pcpn,   qfn,    rofn,   wtrs,   pcpg,   qfg, &
                        qfc,    rof,    wtrg,   cmai,   rcan,   scan, &
                        tcan,   sno,    wsnow,  tsnow,  thliq,  thice, &
                        hcps,   thpor,  delzw,  tbar,   zpond,  tpond, &
                        delz,   fcs,    fgs,    fc,     fg, &
                        il1,    il2,    ilg,    ig,     n)
  !
  !     * jan 06/09 - d.verseghy. more variables in print statements
  !     *                         slightly increased accuracy limits.
  !     * nov 10/06 - d.verseghy. check that sums of energy and water
  !     *                         fluxes for canopy, snow and soil match
  !     *                         changes in heat and water storage over
  !     *                         current timestep.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: istep !<
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)  :: ig !<
  integer, intent(in)  :: n !<
  integer  :: i !<
  integer  :: j !<
  !
  !     * diagnostic arrays used for checking energy and water
  !     * balances.
  !
  real, intent(inout) :: ctvstp(ilg) !<
  real, intent(inout) :: ctsstp(ilg) !<
  real, intent(inout) :: ct1stp(ilg) !<
  real, intent(inout) :: ct2stp(ilg) !<
  real, intent(inout) :: ct3stp(ilg) !<
  real, intent(inout) :: wtvstp(ilg) !<
  real, intent(inout) :: wtsstp(ilg) !<
  real, intent(inout) :: wtgstp(ilg) !<
  !
  real :: qsumv !<
  real :: qsums !<
  real :: qsum1 !<
  real :: qsum2 !<
  real :: qsum3 !<
  real :: wsumv !<
  real :: wsums !<
  real :: wsumg !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fsgv  (ilg) !<
  real, intent(in) :: flgv  (ilg) !<
  real, intent(in) :: hfsc  (ilg) !<
  real, intent(in) :: hevc  (ilg) !<
  real, intent(in) :: hmfc  (ilg) !<
  real, intent(in) :: htcc  (ilg) !<
  real, intent(in) :: fsgs  (ilg) !<
  real, intent(in) :: flgs  (ilg) !<
  real, intent(in) :: hfss  (ilg) !<
  real, intent(in) :: hevs  (ilg) !<
  real, intent(in) :: hmfn  (ilg) !<
  real, intent(in) :: htcs  (ilg) !<
  real, intent(in) :: fsgg  (ilg) !<
  real, intent(in) :: flgg  (ilg) !<
  real, intent(in) :: hfsg  (ilg) !<
  real, intent(in) :: hevg  (ilg) !<
  real, intent(in) :: hmfg  (ilg,ig) !<
  real, intent(in) :: htc   (ilg,ig) !<
  real, intent(in) :: pcfc  (ilg) !<
  real, intent(in) :: pclc  (ilg) !<
  real, intent(in) :: qfcf  (ilg) !<
  real, intent(in) :: qfcl  (ilg) !<
  real, intent(in) :: rofc  (ilg) !<
  real, intent(in) :: wtrc  (ilg) !<
  real, intent(in) :: pcpn  (ilg) !<
  real, intent(in) :: qfn   (ilg) !<
  real, intent(in) :: rofn  (ilg) !<
  real, intent(in) :: wtrs  (ilg) !<
  real, intent(in) :: pcpg  (ilg) !<
  real, intent(in) :: qfg   (ilg) !<
  real, intent(in) :: qfc   (ilg,ig) !<
  real, intent(in) :: rof   (ilg) !<
  real, intent(in) :: wtrg  (ilg) !<
  real, intent(in) :: cmai  (ilg) !<
  real, intent(in) :: rcan  (ilg) !<
  real, intent(in) :: scan  (ilg) !<
  real, intent(in) :: tcan  (ilg) !<
  real, intent(in) :: sno   (ilg) !<
  real, intent(in) :: wsnow (ilg) !<
  real, intent(in) :: tsnow (ilg) !<
  real, intent(in) :: thliq (ilg,ig) !<
  real, intent(in) :: thice (ilg,ig) !<
  real, intent(in) :: hcps  (ilg,ig) !<
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: tbar  (ilg,ig) !<
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: tpond (ilg) !<
  real, intent(in) :: delz  (ig) !<
  real, intent(in) :: fcs   (ilg) !<
  real, intent(in) :: fgs   (ilg) !<
  real, intent(in) :: fc    (ilg) !<
  real, intent(in) :: fg    (ilg) !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class1/ delt,tfrez
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !
  ! =================================================================
  !
  if (istep==0) then
    !
    !     * set balance check variables for start of current time step.
    !
    do i=il1,il2
      wtgstp(i)=0.0
      ctvstp(i)=-(cmai(i)*sphveg+rcan(i)*sphw+ &
              scan(i)*sphice)*tcan(i)
      ctsstp(i)=-tsnow(i)*(hcpice*sno(i)/rhoice+ &
              hcpw*wsnow(i)/rhow)
      ct1stp(i)=-((hcpw*thliq(i,1)+hcpice*thice(i,1) &
              +hcps(i,1)*(1.0-thpor(i,1)))*delzw(i,1)+ &
              hcpsnd*(delz(1)-delzw(i,1)))*tbar(i,1)- &
              hcpw*zpond(i)*tpond(i)
      ct2stp(i)=-((hcpw*thliq(i,2)+hcpice*thice(i,2) &
              +hcps(i,2)*(1.0-thpor(i,2)))*delzw(i,2)+ &
              hcpsnd*(delz(2)-delzw(i,2)))*tbar(i,2)
      ct3stp(i)=-((hcpw*thliq(i,3)+hcpice*thice(i,3) &
              +hcps(i,3)*(1.0-thpor(i,3)))*delzw(i,3)+ &
              hcpsnd*(delz(3)-delzw(i,3)))*tbar(i,3)
      wtvstp(i)=-(rcan(i)+scan(i))
      wtsstp(i)=-sno(i)-wsnow(i)
      do j=1,ig
        wtgstp(i)=wtgstp(i)- &
              (thliq(i,j)*rhow+thice(i,j)*rhoice)* &
              delzw(i,j)
      end do ! loop 50
      wtgstp(i)=wtgstp(i)-zpond(i)*rhow
    end do ! loop 100
    !
  end if
  !
  if (istep==1) then
    !
    !     * check energy and water balances over the current time step.
    !
    do i=il1,il2
      ctvstp(i)=ctvstp(i)+(cmai(i)*sphveg+rcan(i)*sphw+ &
              scan(i)*sphice)*tcan(i)
      ctsstp(i)=ctsstp(i)+tsnow(i)*(hcpice*sno(i)/rhoice+ &
              hcpw*wsnow(i)/rhow)
      ct1stp(i)=ct1stp(i)+((hcpw*thliq(i,1)+hcpice*thice(i,1) &
              +hcps(i,1)*(1.0-thpor(i,1)))*delzw(i,1)+ &
              hcpsnd*(delz(1)-delzw(i,1)))*tbar(i,1)+ &
              hcpw*zpond(i)*tpond(i)
      ct2stp(i)=ct2stp(i)+((hcpw*thliq(i,2)+hcpice*thice(i,2) &
              +hcps(i,2)*(1.0-thpor(i,2)))*delzw(i,2)+ &
              hcpsnd*(delz(2)-delzw(i,2)))*tbar(i,2)
      ct3stp(i)=ct3stp(i)+((hcpw*thliq(i,3)+hcpice*thice(i,3) &
              +hcps(i,3)*(1.0-thpor(i,3)))*delzw(i,3)+ &
              hcpsnd*(delz(3)-delzw(i,3)))*tbar(i,3)
      ctvstp(i)=ctvstp(i)/delt
      ctsstp(i)=ctsstp(i)/delt
      ct1stp(i)=ct1stp(i)/delt
      ct2stp(i)=ct2stp(i)/delt
      ct3stp(i)=ct3stp(i)/delt
      wtvstp(i)=wtvstp(i)+rcan(i)+scan(i)
      wtsstp(i)=wtsstp(i)+sno(i)+wsnow(i)
      do j=1,ig
        wtgstp(i)=wtgstp(i)+ &
              (thliq(i,j)*rhow+thice(i,j)*rhoice)* &
              delzw(i,j)
      end do ! loop 150
      wtgstp(i)=wtgstp(i)+zpond(i)*rhow
    end do ! loop 200
    !
    do i=il1,il2
      qsumv=fsgv(i)+flgv(i)-hfsc(i)-hevc(i)- &
           hmfc(i)+htcc(i)
      qsums=fsgs(i)+flgs(i)-hfss(i)-hevs(i)- &
           hmfn(i)+htcs(i)
      qsum1=fsgg(i)+flgg(i)-hfsg(i)-hevg(i)- &
           hmfg(i,1)+htc(i,1)
      qsum2=-hmfg(i,2)+htc(i,2)
      qsum3=-hmfg(i,3)+htc(i,3)
      wsumv=(pcfc(i)+pclc(i)- &
           qfcf(i)-qfcl(i)-rofc(i)+ &
               wtrc(i))*delt
      wsums=(pcpn(i)-qfn(i)- &
               rofn(i)+wtrs(i))*delt
      wsumg=(pcpg(i)-qfg(i)- &
               rof(i)+wtrg(i))*delt
      do j=1,ig
        wsumg=wsumg-qfc(i,j)*delt
      end do ! loop 250
      if (abs(ctvstp(i)-qsumv)>1.0) then
        write(6,6441) n,ctvstp(i),qsumv
6441    format(2x,'CANOPY ENERGY BALANCE  ',i8,2f20.8)
        write(6,6450) fsgv(i),flgv(i),hfsc(i), &
              hevc(i),hmfc(i),htcc(i)
        write(6,6450) rcan(i),scan(i),tcan(i)
        stop
      end if
      if (abs(ctsstp(i)-qsums)>7.0) then
        write(6,6442) n,i,ctsstp(i),qsums
6442    format(2x,'SNOW ENERGY BALANCE  ',2i8,2f20.8)
        write(6,6450) fsgs(i),flgs(i),hfss(i), &
             hevs(i),hmfn(i),htcs(i)
        write(6,6450) tsnow(i),sno(i),wsnow(i)
        write(6,6451) fcs(i),fgs(i),fc(i),fg(i)
        stop
      end if
      if (abs(ct1stp(i)-qsum1)>5.0) then
        write(6,6443) n,i,ct1stp(i),qsum1
        write(6,6450) fsgg(i),flgg(i),hfsg(i), &
             hevg(i),hmfg(i,1),htc(i,1)
        write(6,6450) fsgs(i),flgs(i),hfss(i), &
             hevs(i),hmfn(i),htcs(i)
        write(6,6450) thliq(i,1)*rhow*delzw(i,1), &
             thliq(i,2)*rhow*delzw(i,2), &
             thliq(i,3)*rhow*delzw(i,3), &
             thice(i,1)*rhoice*delzw(i,1), &
             thice(i,2)*rhoice*delzw(i,2), &
             thice(i,3)*rhoice*delzw(i,3), &
             zpond(i)*rhow
        write(6,6451) fcs(i),fgs(i),fc(i),fg(i), &
             delzw(i,1),delzw(i,2),delzw(i,3)
6443    format(2x,'LAYER 1 ENERGY BALANCE  ',2i8,2f20.8)
        stop
      end if
      if (abs(ct2stp(i)-qsum2)>5.0) then
        write(6,6444) n,i,ct2stp(i),qsum2
6444    format(2x,'LAYER 2 ENERGY BALANCE  ',2i8,2f20.8)
        write(6,6450) hmfg(i,2),htc(i,2), &
             thliq(i,2),thice(i,2),thpor(i,2),tbar(i,2)-tfrez
        write(6,6450) hmfg(i,3),htc(i,3), &
             thliq(i,3),thice(i,3),thpor(i,3),tbar(i,3)-tfrez
        write(6,6450) hmfg(i,1),htc(i,1), &
             thliq(i,1),thice(i,1),thpor(i,1),tbar(i,1)-tfrez
        write(6,6451) fcs(i),fgs(i),fc(i),fg(i), &
             delzw(i,2),hcps(i,2),delzw(i,3)
6451    format(2x,7e20.6)
        stop
      end if
      if (abs(ct3stp(i)-qsum3)>10.0) then
        write(6,6445) n,i,ct3stp(i),qsum3
6445    format(2x,'LAYER 3 ENERGY BALANCE  ',2i8,2f20.8)
        write(6,6450) hmfg(i,3),htc(i,3), &
             tbar(i,3)
        write(6,6450) thliq(i,3),thice(i,3),hcps(i,3), &
                       thpor(i,3),delzw(i,3)
        stop
      end if
      if (abs(wtvstp(i)-wsumv)>1.0e-3) then
        write(6,6446) n,wtvstp(i),wsumv
6446    format(2x,'CANOPY WATER BALANCE  ',i8,2f20.8)
        stop
      end if
      if (abs(wtsstp(i)-wsums)>1.0e-2) then
        write(6,6447) n,i,wtsstp(i),wsums
6447    format(2x,'SNOW WATER BALANCE  ',2i8,2f20.8)
        write(6,6450) pcpn(i)*delt,qfn(i)*delt, &
             rofn(i)*delt,wtrs(i)*delt
        write(6,6450) sno(i),wsnow(i),tsnow(i)-tfrez
        write(6,6451) fcs(i),fgs(i),fc(i),fg(i)
        stop
      end if
      if (abs(wtgstp(i)-wsumg)>1.0e-1) then
        write(6,6448) n,i,wtgstp(i),wsumg
6448    format(2x,'GROUND WATER BALANCE  ',2i8,2f20.8)
        write(6,6450) pcpg(i)*delt,qfg(i)*delt, &
             qfc(i,1)*delt,qfc(i,2)*delt, &
             qfc(i,3)*delt,rof(i)*delt, &
             wtrg(i)*delt
        do j=1,ig
          write(6,6450) thliq(i,j)*rhow*delzw(i,j), &
                 thice(i,j)*rhoice*delzw(i,j), &
                 delzw(i,j)
        end do ! loop 390
        write(6,6450) zpond(i)*rhow
6450    format(2x,7f15.6)
        write(6,6451) fcs(i),fgs(i),fc(i),fg(i)
        stop
      end if
    end do ! loop 400
    !
  end if
  !
  return
end
