subroutine wend(thliqx,thicex,tbarwx,zpond,tpond, &
                      basflw,tbasfl,runoff,trunof,fi, &
                      wmove,tmove,lzf,ninf,trmdr,thlinf,delzx, &
                      zmat,zrmdr,fdtbnd,wadd,tadd,fdt,tfdt, &
                      thlmax,thtest,thldum,thidum,tdumw, &
                      tused,rdummy,zero,wexces,xdrain, &
                      thpor,thlret,thlmin,bi,psisat,grksat, &
                      thfc,delzw,isand,igrn,igrd,igdr,izero, &
                      iveg,ig,igp1,igp2,ilg,il1,il2,jl,n)

  !     * oct 18/11 - m.lazare.   pass in "IGDR" as an input field
  !     *                         (originating in classb) rather
  !     *                         than repeating the calculation here
  !     *                         as an internal work field.
  !     * dec 15/10 - d.verseghy. allow for baseflow when bedrock
  !     *                         lies within soil profile.
  !     * jan 06/09 - d.verseghy. add zpond and tpond to subroutine
  !     *                         call; assign residual of wmove to
  !     *                         ponded water; revise loop 550
  !     *                         delete calculation of fdtbnd.
  !     * may 17/06 - d.verseghy. protect against divisions by zero.
  !     * oct 21/05 - d.verseghy. fix minor bugs in cleanup and
  !     *                         runoff temperature calculation.
  !     * mar 23/05 - d.verseghy. add variables to grdran call
  !     *                         add calculation of runoff
  !     *                         temperature.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * apr 24/03 - d.verseghy. add check for overflow in soil
  !     *                         layer containing wetting front.
  !     * oct 15/02 - d.verseghy. bugfix in calculation of fdtbnd
  !     *                         (present only in prototype
  !     *                         versions of class version 3.0).
  !     * jun 21/02 - d.verseghy. update subroutine call; shortened
  !     *                         class4 common block.
  !     * dec 12/01 - d.verseghy. add separate calculation of baseflow
  !     *                         at bottom of soil column.
  !     * oct 20/97 - d.verseghy. apply accuracy limit on flows in and
  !     *                         out of layer containing wetting front,
  !     *                         in order to ensure moisture conservation.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * aug 18/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers.
  !     * apr 24/92 - d.verseghy,m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. recalculate liquid moisture content
  !     *                         of soil layers after infiltration
  !     *                         and evaluate flow ("RUNOFF") from
  !     *                         bottom of soil column.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: iveg !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: igp1 !<
  integer, intent(in) :: igp2 !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer, intent(in) :: n !<
  !
  !     * output fields.
  !
  real, intent(inout) :: thliqx(ilg,igp1) !<
  real, intent(in) :: thicex(ilg,igp1) !<
  real, intent(inout) :: tbarwx(ilg,igp1) !<
  real, intent(inout) :: zpond (ilg) !<
  real, intent(inout) :: tpond (ilg) !<
  real, intent(inout) :: basflw(ilg) !<
  real, intent(inout) :: tbasfl(ilg) !<
  real, intent(inout) :: runoff(ilg) !<
  real, intent(inout) :: trunof(ilg) !<
  !
  !     * input fields.
  !
  real, intent(inout) :: wmove (ilg,igp2) !<
  real, intent(in) :: tmove (ilg,igp2) !<
  real, intent(in) :: thlinf(ilg,igp1) !<
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: trmdr (ilg) !<
  real, intent(in) :: delzx (ilg,igp1) !<
  !
  integer, intent(in)                :: lzf   (ilg) !<
  integer, intent(inout)                :: ninf  (ilg) !<
  integer, intent(in)                :: igrn  (ilg) !<
  !
  !     * internal work arrays.
  !
  real, intent(inout) :: zmat  (ilg,igp2,igp1) !<
  real, intent(inout) :: zrmdr (ilg,igp1) !<
  !
  real, intent(inout) :: fdtbnd(ilg) !<
  real, intent(inout) :: wadd  (ilg) !<
  real, intent(inout) :: tadd  (ilg) !<
  !
  !     * internal arrays used in calling grdran.
  !
  real, intent(inout) :: fdt   (ilg,igp1) !<
  real, intent(in) :: tfdt  (ilg,igp1) !<
  !
  real, intent(in) :: thlmax(ilg,ig) !<
  real, intent(in) :: thtest(ilg,ig) !<
  real, intent(inout) :: thldum(ilg,ig) !<
  real, intent(inout) :: thidum(ilg,ig) !<
  real, intent(inout) :: tdumw (ilg,ig) !<
  !
  real, intent(inout) :: tused (ilg) !<
  real, intent(in) :: rdummy(ilg) !<
  real, intent(in) :: zero  (ilg) !<
  real, intent(in) :: wexces(ilg) !<
  !
  integer, intent(in)              :: igrd  (ilg) !<
  integer, intent(in)              :: izero (ilg) !<
  integer, intent(in)              :: igdr  (ilg) !<
  !
  !     * temporary variables.
  !
  real :: wrem !<
  real :: trem !<
  real :: thdran !<
  real :: thinfl !<
  real :: wdra !<
  real :: tdra !<
  !
  !     * soil information arrays.
  !
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: thlret(ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: bi    (ilg,ig) !<
  real, intent(in) :: psisat(ilg,ig) !<
  real, intent(in) :: grksat(ilg,ig) !<
  real, intent(in) :: thfc  (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: xdrain(ilg) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class1/ delt,tfrez
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !-----------------------------------------------------------------------
  !
  !     * determine amount of time out of current model step during which
  !     * infiltration was occurring.
  !     * set work array "TUSED" to zero for points where wetting front
  !     * is below bottom of lowest soil layer to suppress calculations
  !     * done in "GRDRAN".
  !
  do i=il1,il2
    if (igrn(i)>0 .and. lzf(i)<=ig) then
      tused(i)=delt-trmdr(i)
    else
      tused(i)=0.
    end if
  end do ! loop 100
  !
  !     * initialization.
  !
  do j=1,ig
    do i=il1,il2
      if (igrn(i)>0) then
        thldum(i,j)=thliqx(i,j)
        thidum(i,j)=thicex(i,j)
        tdumw (i,j)=tbarwx(i,j)
      end if
    end do
  end do ! loop 125
  !
  !     * call "GRDRAN" with copies of current liquid and frozen soil
  !     * moisture contents and layer temperatures to determine moisture
  !     * flow between layers below the wetting front.
  !
  call grdran(iveg,thldum,thidum,tdumw,fdt,tfdt,rdummy,rdummy, &
                  rdummy,rdummy,rdummy,rdummy,fi,zero,zero,zero, &
                  tused,wexces,thlmax,thtest,thpor,thlret,thlmin, &
                  bi,psisat,grksat,thfc,delzw,xdrain,isand,lzf, &
                  izero,igrd,igdr,ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  !     * initialization of arrays in preparation for re-allocation of
  !     * moisture stores within soil layers; suppress water flows
  !     * calculated in grdran above wetting front; consistency check
  !     * for water flows into layer containing wetting front.
  !
  do i=il1,il2
    if (igrn(i)>0) then
      ninf(i)=min(ninf(i),igp1)
    end if
  end do ! loop 150
  !
  do j=igp1,1,-1
    do i=il1,il2
      if (igrn(i)>0) then
        zrmdr(i,j)=delzx(i,j)
        if (j<=lzf(i)) then
          fdt(i,j)=0.0
        end if
        if (j<igp1) then
          if (j==lzf(i).and.fdt(i,j+1)<0.) then
            fdt(i,j+1)=0.0
          end if
        end if
      end if
    end do
  end do ! loop 200
  !
  do j=1,igp1
    do k=1,igp1
      do i=il1,il2
        if (igrn(i)>0 .and. k<=ninf(i)) then
          zmat(i,k,j)=0.0
        end if
      end do
    end do
  end do ! loop 300
  !
  !     * assign values in matrix "ZMAT": determine depth out of each
  !     * soil layer j which is filled by water from reservoir k
  !     * in "WMOVE"; find the depth "ZRMDR" left over within each
  !     * soil layer.
  !
  do k=1,igp1
    do j=1,igp1
      do i=il1,il2
        if (igrn(i)>0 .and. k<=ninf(i)) then
          if (zrmdr(i,j)>1.0e-5 .and. wmove(i,k)>0.) then
            zmat(i,k,j)=wmove(i,k)/thlinf(i,j)
            if (zmat(i,k,j)>zrmdr(i,j)) then
              zmat(i,k,j)=zrmdr(i,j)
              wmove(i,k)=wmove(i,k)-zrmdr(i,j)*thlinf(i,j)
              zrmdr(i,j)=0.0
            else
              zrmdr(i,j)=zrmdr(i,j)-zmat(i,k,j)
              wmove(i,k)=0.0
            end if
          end if
        end if
      end do
    end do
  end do ! loop 400
  !
  do j=1,igp1
    do i=il1,il2
      if (igrn(i)>0 .and. wmove(i,j)>0.0) then
        tpond(i)=(tpond(i)*zpond(i)+tmove(i,j)*wmove(i,j))/ &
             (zpond(i)+wmove(i,j))
        zpond(i)=zpond(i)+wmove(i,j)
      end if
    end do
  end do ! loop 450
  !
  !     * add water content and temperature changes due to infiltration
  !     * (wadd, tadd) and drainage (wdra, tdra) to water remaining in
  !     * each soil layer after these processes (wrem, trem).
  !
  do j=ig,1,-1
    do i=il1,il2
      if (igrn(i)>0) then
        wadd(i)=0.
        tadd(i)=0.
      end if
    end do ! loop 500
    !
    do k=1,igp1
      do i=il1,il2
        if (igrn(i)>0 .and. k<=ninf(i)) then
          wadd(i)=wadd(i)+thlinf(i,j)*zmat(i,k,j)
          tadd(i)=tadd(i)+tmove(i,k)*thlinf(i,j)*zmat(i,k,j)
        end if
      end do
    end do ! loop 525
    !
    do i=il1,il2
      if (igrn(i)>0 .and. delzw(i,j)>1.0e-4) then
        if (zrmdr(i,j)>1.0e-5) then
          wrem=thliqx(i,j)*zrmdr(i,j)
          trem=tbarwx(i,j)*thliqx(i,j)*zrmdr(i,j)
        else
          wrem=0.0
          trem=0.0
        end if
        if (j==lzf(i)) then
          thinfl=(wadd(i)+wrem+fdt(i,j)-fdt(i,j+1))/delzw(i,j)
          if (thinfl<thlmin(i,j)) then
            fdt(i,j+1)=wadd(i)+wrem+fdt(i,j)-thlmin(i,j)* &
                             delzw(i,j)
          end if
        end if
        wdra=fdt(i,j)-fdt(i,j+1)
        tdra=fdt(i,j)*tfdt(i,j)-fdt(i,j+1)*tfdt(i,j+1)
        thliqx(i,j)=(wadd(i)+wrem+wdra)/delzw(i,j)
        thliqx(i,j)=max(thliqx(i,j),thlmin(i,j))
        tbarwx(i,j)=(tadd(i)+trem+tdra)/(thliqx(i,j)* &
                         delzw(i,j))
      end if
    end do ! loop 550
  end do ! loop 600
  !
  !     * calculate flow out of bottom of soil column due to infiltration
  !     * and gravity drainage and add to total runoff and baseflow.
  !
  do k=1,igp1
    do i=il1,il2
      if (igrn(i)>0) then
        if (lzf(i)==igp1 .and. k<=ninf(i) .and. &
            thlinf(i,igp1)*zmat(i,k,igp1)>0.0) then
          tbasfl(i)=(tbasfl(i)*basflw(i)+fi(i)*(tmove(i,k)+ &
                 tfrez)*thlinf(i,igp1)*zmat(i,k,igp1))/(basflw(i)+ &
                 fi(i)*thlinf(i,igp1)*zmat(i,k,igp1))
          basflw(i)=basflw(i)+fi(i)*thlinf(i,igp1)* &
                       zmat(i,k,igp1)
          trunof(i)=(trunof(i)*runoff(i)+(tmove(i,k)+tfrez)* &
                 thlinf(i,igp1)*zmat(i,k,igp1))/(runoff(i)+ &
                 thlinf(i,igp1)*zmat(i,k,igp1))
          runoff(i)=runoff(i)+thlinf(i,igp1)*zmat(i,k,igp1)
        else if (k==(igdr(i)+1) .and. fdt(i,k)>1.0e-8) then
          tbasfl(i)=(tbasfl(i)*basflw(i)+fi(i)*(tfdt(i,k)+ &
                 tfrez)*fdt(i,k))/(basflw(i)+fi(i)*fdt(i,k))
          basflw(i)=basflw(i)+fi(i)*fdt(i,k)
          trunof(i)=(trunof(i)*runoff(i)+(tfdt(i,k)+tfrez)* &
                 fdt(i,k))/(runoff(i)+fdt(i,k))
          runoff(i)=runoff(i)+fdt(i,k)
        end if
      end if
    end do
  end do ! loop 700
  !
  return
end
