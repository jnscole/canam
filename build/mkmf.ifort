# Define compiler and linker
FC := ftn
LD := ftn

# Set potential optimization levels
## OPT:   The fastest possible
## DEBUG: Enable debug-compatible flags
## REPRO: Ensures reproducibility while retaining some speed
OPT   ?= 0
DEBUG ?= 0
REPRO ?= 0

# By default, choose the fastest settings to compile
ifeq ($(OPT),0)
ifeq ($(DEBUG),0)
ifeq ($(REPRO),0)
OPT = 1
endif
endif
endif

# Build the FFLAGS variable based on requested configurations
FFLAGS_DEFAULT = -traceback -mkl -Duse_cancpl
FFLAGS = $(FFLAGS_DEFAULT)
FFLAGS_64BIT = -r8 -i8
FFLAGS_OPENMP = -openmp

# Set the options for various levels of optimizations
FFLAGS_OPT  = -fpp -D_IMPI_=4 -D_Cray_XC30 -Qoption,link,--noinhibit-exec -list -O2 -mp1 -static-intel
FFLAGS_OPT += -assume buffered_stdout,protect_parens -convert big_endian -threads
FFLAGS_OPT += -axSKYLAKE-AVX512 -align array64byte -DIntelFTN -mkl
# For now the following is just set to the OPT flags, but this needs to be tested for reproducibility
FFLAGS_REPRO = $(FFLAGS_OPT)

FFLAGS_DEBUG = -O0 -g

# Optimization flags
ifeq ($(OPT),1)
FFLAGS += $(FFLAGS_OPT)
endif
ifeq ($(REPRO),1)
FFLAGS += $(FFLAGS_REPRO)
endif
ifeq ($(DEBUG),1)
FFLAGS += $(FFLAGS_DEBUG)
endif

# 64-bit flags if requested
ifeq ($(COMPILE_32BIT),0)
FFLAGS += $(FFLAGS_64BIT)
endif

# Remaining flags
FFLAGS += $(FFLAGS_OPENMP)
FFLAGS += $(INCLUDE_FLAGS)

# Build the LDFLAGS varibles
LDFLAGS += -L/home/scrd102/package/esmf/linux64-hare-esmf-7.0.1-intel-ftn/lib -lesmf -openmp -mkl
